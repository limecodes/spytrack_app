import { Content } from '@angular/compiler/src/render3/r3_ast';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { IonContent, IonSlides, ToastController } from '@ionic/angular';
import { RestService } from '../rest.service';
@Component({
  selector: 'app-registration',
  templateUrl: './registration.page.html',
  styleUrls: ['./registration.page.scss'],
})
export class RegistrationPage implements OnInit {
  @ViewChild('regsitrationSlides')  regsitrationSlides: IonSlides;
  @ViewChild(IonContent) content: IonContent;

  emailValidation: boolean = true;
  registrationForm: FormGroup;
  regFormStepOne: FormGroup;
  regFormStepTwo: FormGroup;
  regFormStepThree: FormGroup;
  passwordVal = {
    minLen: false,
    number: false,
    symbol: false,
    upperCase: false,
    lowerCase: false
  }
  cPasswordVal = false;
  slideOpts = {
    initialSlide: 0,
    speed: 400,
    allowTouchMove: false
  };
  token: string;
  constructor(
    private _formBuilder: FormBuilder,
    private router: Router,
    private httpRequest: RestService,
    public toastCtrl: ToastController) { }

  ngOnInit() {
    this.formInit();
  }

  ionViewWillEnter(){
    this.slideOpts.initialSlide = 0;
    this.regsitrationSlides?.slideTo(0);
    this.regsitrationSlides.ionSlideTransitionStart.subscribe((end)=>{
      this.content.scrollToTop(100);
    })
  }

  formInit() {
    this.regFormStepOne = this._formBuilder.group({
      firstname: new FormControl('', Validators.required),
      lastname: new FormControl('', Validators.required),
      email: new FormControl('',  [Validators.required, Validators.email]),
      phone: new FormControl('', [Validators.required, Validators.pattern('(([+][(]?[0-9]{1,3}[)]?)|([(]?[0-9]{4}[)]?))\s*[)]?[-\s\.]?[(]?[0-9]{1,3}[)]?([-\s\.]?[0-9]{3})([-\s\.]?[0-9]{3,4})')]),
    });

    this.regFormStepTwo = this._formBuilder.group({
      address: new FormControl('', Validators.required),
      city: new FormControl('', Validators.required),
      state: new FormControl('', Validators.required),
      zipcode: new FormControl('', [Validators.required , Validators.pattern(/^[0-9]\d*$/),Validators.minLength(5)]),
      country: new FormControl('', Validators.required),
    });

    this.regFormStepThree = this._formBuilder.group({
      username: new FormControl('', Validators.required),
      password: new FormControl('', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&]).{8,}')]),
      cpassword: new FormControl('', Validators.required),
      pinCode: new FormControl('', Validators.required),
    });
  }

  passwordInputValidation(event: Event) {
    const e =(event.target as HTMLInputElement).value;
    const number = /\d/;
    const symbol = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
    const upperCase = /[A-Z]/ 
    const lowerCase = /[a-z]/ 
    this.regFormStepThree.controls.password.setValue(e);
    if(this.regFormStepThree.controls.password.value == '') {
      this.passwordVal = {
        minLen: false,
        number: false,
        symbol: false,
        upperCase: false,
        lowerCase: false
      }
    } else {
      if(this.regFormStepThree.controls.password.value.length >= 8) {
        this.passwordVal.minLen = true;
      } else {
        this.passwordVal.minLen = false;
      }

      if(number.test(this.regFormStepThree.controls.password.value)) {
        this.passwordVal.number = true;
      } else {
        this.passwordVal.number = false;
      }
      if(symbol.test(this.regFormStepThree.controls.password.value)) {
        this.passwordVal.symbol = true;
      } else {
        this.passwordVal.symbol = false;
      }
      if(upperCase.test(this.regFormStepThree.controls.password.value)) {
        this.passwordVal.upperCase = true;
      } else {
        this.passwordVal.upperCase = false;
      }
      if(lowerCase.test(this.regFormStepThree.controls.password.value)) {
        this.passwordVal.lowerCase = true;
      } else {
        this.passwordVal.lowerCase = false;
      }
    }
  }

  cpasswordInputValidation(event: Event) {
    const e =(event.target as HTMLInputElement).value;
    this.regFormStepThree.controls.cpassword.setValue(e);
    if(this.regFormStepThree.controls.password.value == this.regFormStepThree.controls.cpassword.value) {
      this.cPasswordVal = true;
    } else {
      this.cPasswordVal = false;
    }
  }

  signUp() {
    let dataToSend = {
      firstname: this.regFormStepOne.controls.firstname.value,
      lastname: this.regFormStepOne.controls.lastname.value,
      email: this.regFormStepOne.controls.email.value,
      phone: this.regFormStepOne.controls.phone.value,
      address: this.regFormStepTwo.controls.address.value,
      city: this.regFormStepTwo.controls.city.value,
      state: this.regFormStepTwo.controls.state.value,
      zipcode: this.regFormStepTwo.controls.zipcode.value,
      country: this.regFormStepTwo.controls.country.value,
      username: this.regFormStepThree.controls.username.value,
      password: this.regFormStepThree.controls.password.value,
      pinCode: this.regFormStepThree.controls.pinCode.value,
    };


    this.httpRequest.post(JSON.stringify(dataToSend), 'register').subscribe((resp: any) => {
      this.token = resp.isSuccess.token;
      //localStorage.setItem('token', resp.isSuccess.token);
      this.swipeNext();
     // this.router.navigate(['/live-tracking']);
    }, error => {
      console.log(error);
      this.openToast();
    });
  }

  login() {
    // this.router.navigate(['/live-tracking']);
  }

  swipeNext() {
    this.regsitrationSlides.slideNext();
  }

  swipePrev() {
    this.regsitrationSlides.slidePrev();
  }

  async openToast() {  
    const toast = await this.toastCtrl.create({  
      message: 'Error occured. Please try again later.',
      position: 'bottom',
      duration: 4000,
      color: 'danger' 
    });  
    toast.present();  
  }

  async resendVarificationEmail(){
    this.httpRequest.newVerification('user/new_verification',this.regFormStepOne.controls.email.value).subscribe(async (resp: any) => {
      if(resp.isSuccess){
        const toast = await this.toastCtrl.create({
          message: 'Email sent successfuly',
          position: 'bottom',
          duration: 1500,
          color: 'success'
        });
        toast.present();
      }else{
        const toast = await this.toastCtrl.create({
          message: resp.error.message || resp.message,
          position: 'bottom',
          duration: 1500,
          color: 'danger'
        });
        toast.present();
      }
    }, error => {
      console.log(error);
      this.openToast();
    });
  }


}
