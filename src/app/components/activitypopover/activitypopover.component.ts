/* eslint-disable max-len */
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-activitypopover',
  templateUrl: './activitypopover.component.html',
  styleUrls: ['./activitypopover.component.scss'],
})
export class ActivitypopoverComponent implements OnInit {
  @Input('data') data: any;

  constructor() { }

  ngOnInit() {
    console.log(this.data);
  }

  redirectToFinish(){
    console.log(this.data);
    console.log('finish taped');
    this.getDirection(this.data.locations[this.data.locations.length - 1].latitude,this.data.locations[this.data.locations.length - 1].longitude);
  }

  redirectToStart(){
    console.log(this.data);
    console.log('start taped');
    this.getDirection(this.data.locations[0].latitude,this.data.locations[0].longitude);
  }

  getDirection(latitude,longitude){
    const url = `https://www.google.com/maps/search/?api=1&query=${latitude},${longitude}`;
    window.open(url, '_system');
  }
}
