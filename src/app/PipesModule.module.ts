import { NgModule } from '@angular/core';
import { DateTrasPipe } from './pipes/date-tras.pipe';

@NgModule({
  declarations: [DateTrasPipe],
  imports: [],
  exports: [DateTrasPipe],
})
export class PipesModule {}
