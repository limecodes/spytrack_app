import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateTras',
})
export class DateTrasPipe implements PipeTransform {
  transform(value: any, ...args: any[]): any {
    if (value) {
      const temp = value.toString().replace(' ', 'T');
      return new Date(temp);
    } else {
      return null;
    }
  }
}
