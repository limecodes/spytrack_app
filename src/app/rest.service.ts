/* eslint-disable @typescript-eslint/naming-convention */
import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HeaderConfigService } from './header-config.service';
import { catchError, retry, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class RestService extends HeaderConfigService {
  url = 'https://api.spytrack.com/api/';
  constructor(private http: HttpClient , private router: Router , private toastCtrl: ToastController) {
    super();
  }

  get(endpoint: string, data: any, token: any): Observable<any> {
    return this.http.get(this.url + endpoint, this._tokenizedHeader()).pipe(
      tap (response => response),catchError((err: any) => {
      if (err.status === 401) {
        this.logout();
      }
     return err;
    }));
  }

  delete(endpoint: string, data: any, token: any): Observable<any> {
    return this.http.delete(this.url + endpoint, this._tokenizedHeader()).pipe(
      tap (response => response),catchError((err: any) => {
      if (err.status === 401) {
        this.logout();
      }
     return err;
    }));
  }

  post(data, endpoint ) {
    return this.http.post(this.api + endpoint, data,  this._tokenizedHeader()).pipe(
      tap (response => response),
    catchError(
      (error: HttpErrorResponse): Observable<any> => {
          // we expect 404, it's not a failure for us.
          if (error.status === 404) {
              return of(null); // or any other stream like of('') etc.
          }else if(error.status === 409){
            return of(error);
          }
          if (error.status === 401) {
            this.logout();
          }
          return throwError(error);
      },
    ),);
  }

  put(data, endpoint ) {
    return this.http.put(this.api + endpoint, data,  this._tokenizedHeader()).pipe(
      tap (response => response),
    catchError(
      (error: HttpErrorResponse): Observable<any> => {
          // we expect 404, it's not a failure for us.
          if (error.status === 404) {
              return of(null); // or any other stream like of('') etc.
          }
          return throwError(error);
      },
    ),);
  }

  login(data, endpoint ) {
    return this.http.post(this.api + endpoint, data,  this._loginHeader()).pipe(
      tap (response => response),
    catchError(
      (error: HttpErrorResponse): Observable<any> => {
          // we expect 404, it's not a failure for us.
          if (error.status === 404) {
              return of(null); // or any other stream like of('') etc.
          }
          if (error.status === 401) {
            this.logout();
          }
          return throwError(error);
      },
    ),);
  }

  getSnap(endpoint: string, data: any, token: any): Observable<any> {
    return this.http.get(this.url + endpoint, this._tokenizedHeader()).pipe(
      tap (response => response),catchError((err: any) => err));
  }

  newVerification(endpoint,email){
    return this.http.post(this.api + endpoint, { email}, {
      headers:{
        'Content-Type': 'application/json',
        Accept: 'application/json'
      }
    }).pipe(
      tap (response => response),
    catchError(
      (error: HttpErrorResponse): Observable<any> => {
          // we expect 404, it's not a failure for us.
          if (error.status === 404) {
              return of(null); // or any other stream like of('') etc.
          }else if(error.status === 409){
            return of(error);
          }
          if (error.status === 401) {
            this.logout();
          }
          return throwError(error);
      },
    ),);
  }

  // getAddress(endpoint:string, data:any, token:any): Observable<any> {
  //   return this.http.get(endpoint, this._tokenizedHeader()).pipe(
  //     tap (response => {
  //       return response;
  //   }),catchError((err: any) => {
  //    return err;
  //   }));
  // }

  async logout(){
    const toast = await this.toastCtrl.create({
      message: 'Please login again',
      position: 'bottom',
      duration: 3000,
      color: 'danger'
    });
    toast.present();
    localStorage.clear();
    this.router.navigate(['/login']);
  }
}
