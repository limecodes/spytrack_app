import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { GlobalService} from './globals.service'

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  activePage = 'live-tracking';
  user: any;
  constructor(
    private router: Router,
    private global: GlobalService
    ) {
    // this.router.navigate(['/live-tracking']);
    this.global.currentMessageSubscriberSummary.subscribe((data: any) => {
      if (data.isRefresh) {
        this.user = this.global.firstname;
      } else {
      }
    })
  }

  selectedPage(data) {
    this.activePage = data;
  }
  
  logout() {
    localStorage.clear();
    this.router.navigate(['/login']);
  }
}
