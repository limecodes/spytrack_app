import { Injectable } from '@angular/core';
import { HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable()
export class HeaderConfigService {

  api = 'https://api.spytrack.com/api/';

  constructor() { }

  _publicHeader = (headers?: HttpHeaders | null): object => {
    headers = headers || new HttpHeaders();
    headers = headers.set('Content-Type', 'application/json');
    headers = headers.set('Accept', 'application/json, text/plain, */*');
    return {
      headers: headers
    }
  }

  _loginHeader = (headers?: HttpHeaders | null): object => {
    headers = headers || new HttpHeaders();
    headers = headers.set('Accept', 'application/json, text/plain, */*');
    // headers = headers.set('Access-Control-Allow-Origin', '*');
    headers = headers.set('Content-Type', 'application/json');
    headers = headers.set('Access-Control-Allow-Headers', 'Content-Type');
    return {
      headers: headers
    }
  }

  _tokenizedHeader = (headers?: HttpHeaders | null): object => {
    headers = headers || new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    headers = headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));
    headers = headers.append('Accept', 'application/json');
    return {
      headers: headers
    };
  }
}
