import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { LoadingController, ToastController } from '@ionic/angular';  
import { RestService } from '../rest.service';
import { GlobalService } from '../globals.service';
import {take} from 'rxjs/operators';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  username: any = '';
  password: any = '';
  userIsInactivate: boolean;
  token: string;
  globalLoading: any;
  constructor(
    private router: Router,
    private httpRequest: RestService,
    public toastCtrl: ToastController,
    public globals: GlobalService,
    private loading: LoadingController
  ) {
    if(localStorage.getItem('token')) {
      this.router.navigate(['/live-tracking']);
    } 
  }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.userIsInactivate = false;
  }

  login() {
    let dataToSend = {
      email: this.username,
      password: this.password
    };

    this.httpRequest.login(JSON.stringify(dataToSend), 'login').pipe(take(1)).subscribe(async (resp: any) => {

      if(resp.status && resp.statusCode === 200){
        this.globals.token = resp.token;
        localStorage.setItem('token', resp.token);
        this.router.navigate(['/live-tracking']);
      }else{
        if(resp.statusCode === 401){
          const toast = await this.toastCtrl.create({
            message: 'Please enter correct credentials!',
            position: 'bottom',
            duration: 1500,
            color: 'danger'
          });
          toast.present();
        }
      }
    }, async (error) => {
      if(error.error.statusCode === 403){
        const toast = await this.toastCtrl.create({
          message: 'Please verify your account!',
          position: 'bottom',
          duration: 1500,
          color: 'danger'
        });
        toast.present();
        this.userIsInactivate = true;
      }else{
        this.openToast();
      }
      console.log(error);
    });
  }

  resendVarificationEmail(){

    this.httpRequest.newVerification('user/new_verification',this.username).subscribe(async (resp: any) => {
      if(resp.isSuccess){
        const toast = await this.toastCtrl.create({
          message: 'Email sent successfuly',
          position: 'bottom',
          duration: 1500,
          color: 'success'
        });
        toast.present();
      }else{
        const toast = await this.toastCtrl.create({
          message: resp.error.message || resp.message,
          position: 'bottom',
          duration: 1500,
          color: 'danger'
        });
        toast.present();
        //this.userIsInactivate = false;
      }
    }, error => {
      console.log(error);
      this.openToast();
    });
  }

  async openToast() {
    const toast = await this.toastCtrl.create({
      message: 'Invalid username or password',
      position: 'bottom',
      duration: 4000,
      color: 'danger'
    });
    toast.present();
  }

}
