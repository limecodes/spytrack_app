/* eslint-disable @typescript-eslint/naming-convention */
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { take } from 'rxjs/operators';
import { GlobalService } from 'src/app/globals.service';
import { RestService } from 'src/app/rest.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
})
export class ForgotPasswordComponent implements OnInit {

  isUserExistConfirm: boolean;
  forgotPasswordFormStepOne: FormGroup;
  forgotPasswordFormStepTwo: FormGroup;
  emailValidation: boolean = true;
  passwordVal = {
    minLen: false,
    number: false,
    symbol: false,
    upperCase: false,
    lowerCase: false
  }
  cPasswordVal = false;
  constructor(
    private formBuilder: FormBuilder,
    private toastCtrl: ToastController,
    private global: GlobalService,
    private rest: RestService,
    private router: Router,
  ) {
  }

  ngOnInit() {
    this.formInit();
  }

  ionViewWillEnter(){
    this.forgotPasswordFormStepOne.reset();
    this.forgotPasswordFormStepTwo.reset();
    this.isUserExistConfirm = false;
  }

  formInit(){
    this.forgotPasswordFormStepOne = this.formBuilder.group({
      email: new FormControl('', [Validators.required, Validators.email], ),
    });

    this.forgotPasswordFormStepTwo = this.formBuilder.group({
      otp: new FormControl('', {
        validators:[
          Validators.required, 
          //Validators.min(9999),Validators.max(99999)
        ]
      }),
      password: new FormControl('', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&]).{8,}')]),
      cpassword: new FormControl('', Validators.required)
    });
  }

  passwordInputValidation(event: Event) {
    const e =(event.target as HTMLInputElement).value;
    const number = /\d/;
    const symbol = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
    const upperCase = /[A-Z]/ 
    const lowerCase = /[a-z]/ 
    this.forgotPasswordFormStepTwo.controls.password.setValue(e);
    if(this.forgotPasswordFormStepTwo.controls.password.value == '') {
      this.passwordVal = {
        minLen: false,
        number: false,
        symbol: false,
        upperCase: false,
        lowerCase: false
      }
    } else {
      if(this.forgotPasswordFormStepTwo.controls.password.value.length >= 8) {
        this.passwordVal.minLen = true;
      } else {
        this.passwordVal.minLen = false;
      }

      if(number.test(this.forgotPasswordFormStepTwo.controls.password.value)) {
        this.passwordVal.number = true;
      } else {
        this.passwordVal.number = false;
      }
      if(symbol.test(this.forgotPasswordFormStepTwo.controls.password.value)) {
        this.passwordVal.symbol = true;
      } else {
        this.passwordVal.symbol = false;
      }
      if(upperCase.test(this.forgotPasswordFormStepTwo.controls.password.value)) {
        this.passwordVal.upperCase = true;
      } else {
        this.passwordVal.upperCase = false;
      }
      if(lowerCase.test(this.forgotPasswordFormStepTwo.controls.password.value)) {
        this.passwordVal.lowerCase = true;
      } else {
        this.passwordVal.lowerCase = false;
      }
    }
  }

  cpasswordInputValidation(event: Event) {
    const e =(event.target as HTMLInputElement).value;
    this.forgotPasswordFormStepTwo.controls.cpassword.setValue(e);
    if(this.forgotPasswordFormStepTwo.controls.password.value == this.forgotPasswordFormStepTwo.controls.cpassword.value) {
      this.cPasswordVal = true;
    } else {
      this.cPasswordVal = false;
    }
  }

  checkUserExist(){
    const sendData = {
      email : this.forgotPasswordFormStepOne.controls.email.value
    };
    this.rest.post(sendData,'forgot_password').pipe(take(1)).subscribe((resp)=>{
      console.log(resp);
     if(!resp || resp == null){
        this.toastCtrl.create({  
          message: "Email doesn't exist.",
          position: 'bottom',
          duration: 3000,
          color: 'danger'
        }).then((toast)=>{
          toast.present();
        });
      } else{
        this.isUserExistConfirm = true;
      }
    });
  }

  resendOTP(){
    const sendData = {
      email : this.forgotPasswordFormStepOne.controls.email.value
    };
    this.rest.post(sendData,'forgot_password').pipe(take(1)).subscribe((resp)=>{
      if(!resp || resp == null){
        this.toastCtrl.create({
          message: 'No such user found in databse!',
          position: 'bottom',
          duration: 3000,
          color: 'danger'
        }).then((toast)=>{
          toast.present();
        });
      } else{
        this.isUserExistConfirm = true;
      }
    });

    this.toastCtrl.create({
      message: 'OTP sent sucessfully to registered phone no.',
      position: 'bottom',
      duration: 3000,
      color: 'success'
    }).then((toast)=>{
      toast.present();
    });
  }

  resetPassword() {
    const requestObj = {
      otp: this.forgotPasswordFormStepTwo.controls.otp.value,
      password: this.forgotPasswordFormStepTwo.controls.password.value,
      password_confirmation: this.forgotPasswordFormStepTwo.controls.cpassword.value
    };
    this.rest.post(requestObj,'change_password').pipe(take(1)).subscribe((resp)=>{
      if(resp.isSuccess){
        this.toastCtrl.create({
          message: 'Rest password done successfully!',
          position: 'bottom',
          duration: 3000,
          color: 'success'
        }).then((toast)=>{
          toast.present();
        });
        this.router.navigate(['/login']);
      }else{
        this.toastCtrl.create({
          message: resp.error.message || resp.errors[0] || resp.message || 'something went wrong!',
          position: 'bottom',
          duration: 3000,
          color: 'danger'
        }).then((toast)=>{
          toast.present();
        });
      }
    });
  }

}
