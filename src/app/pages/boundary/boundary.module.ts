import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BoundaryPageRoutingModule } from './boundary-routing.module';

import { BoundaryPage } from './boundary.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BoundaryPageRoutingModule
  ],
  declarations: [BoundaryPage]
})
export class BoundaryPageModule {}
