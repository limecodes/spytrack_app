import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BoundaryPage } from './boundary.page';

const routes: Routes = [
  {
    path: '',
    component: BoundaryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BoundaryPageRoutingModule {}
