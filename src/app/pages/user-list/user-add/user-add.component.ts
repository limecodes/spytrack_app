import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, NavController, ToastController } from '@ionic/angular';
import { RestService } from 'src/app/rest.service';
import { ActivateComponent } from '../../preferences/modals/trackers/activate/activate.component';
import { take }from 'rxjs/operators'
@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.scss'],
})
export class UserAddComponent implements OnInit {
  accountInformationForm: FormGroup;
  globalLoader: any
  constructor(
    private router: Router,
    private navController: NavController,
    private fb:FormBuilder,
    private loader: LoadingController,
    private httpRequest: RestService,
    private toastCtrl: ToastController,
    private activatedRoute: ActivatedRoute
  ) { 
    this.accountInformationForm = this.fb.group({
      firstname: new FormControl('', Validators.required),
      lastname: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email]),
     
    });
  }

  ngOnInit() {}

  async inviteUser() {
    console.log(this.accountInformationForm.value)
    this.globalLoader = await this.loader.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
      duration: 4000,
    });
    this.globalLoader.present();
    
    this.httpRequest
      .post(
        JSON.stringify(this.accountInformationForm.value),
        'thorton/invite/user'
      )
      .pipe(take(1))
      .subscribe(
        (resp: any) => {
          if (resp.status) {
           
            this.globalLoader.dismiss();
            this.openToastSuccess();
            this.router.navigate(['../'], {relativeTo: this.activatedRoute});
          } else {
            this.openToastError();
          }
        },
        (error) => {
          this.globalLoader.dismiss();
          console.log(error);
          this.openToastError();
        }
      );
  }

  async openToastSuccess() {
    const toast = await this.toastCtrl.create({
      message: 'User invitation has been sent successfully.',
      position: 'bottom',
      duration: 1500,
      color: 'success',
    });
    toast.present();
  }

  async openToastError() {
    const toast = await this.toastCtrl.create({
      message: 'Error occured. Please try again later.',
      position: 'bottom',
      duration: 4000,
      color: 'danger',
    });
    toast.present();
  }
}
