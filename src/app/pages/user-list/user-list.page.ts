import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, LoadingController, ToastController } from '@ionic/angular';
import { take } from 'rxjs/operators';
import { RestService } from 'src/app/rest.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.page.html',
  styleUrls: ['./user-list.page.scss'],
})
export class UserListPage implements OnInit {
  loadingGbl: any;
  users: any;
  constructor(
    private loadingController: LoadingController,
    private restService: RestService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private alertController: AlertController,
    private toastCtrl: ToastController,
    private loader: LoadingController,
    private cdr: ChangeDetectorRef
  ) { }

  ngOnInit() {}

  async ionViewWillEnter(){
    await this.getAllUsers();
  }

  async getAllUsers(){
    this.loadingGbl = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...'
    });

    await this.loadingGbl.present();
    
    this.restService.get('web/users/all','','').pipe(take(1)).subscribe(async (resp: any) => {
      console.log("resp ====>", resp);
      await this.loadingGbl.dismiss();
      if(resp.length !== 0) {
        this.users = resp;
        this.cdr.detectChanges();
      }else{
        this.users = [];
        this.cdr.detectChanges();

      }
    }, async (err) => {
      await this.loadingGbl.dismiss();
    });
  }

  async editUser(user: any){
    console.log(user);
  localStorage.setItem("editUserInfo", JSON.stringify(user));
   this.router.navigate([`edit/${user.id}`],{relativeTo: this.activatedRoute});
  }

  async deleteUser(user: any){
    this.presentAlert(user);
  }

  async addUser(){
    this.router.navigate(['add'],{relativeTo:this.activatedRoute})
  }

  async presentAlert(user:any) {
    const alert = await this.alertController.create({
      header: `Are you sure you want to delete "${user.full_name}" user ?`,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log("canceld")
          },
        },
        {
          text: 'OK',
          role: 'confirm',
          handler: () => {
            // call api 
            //user/delete/:userId
            this.deleteUserApi(user)
            console.log("user delete api " , user);
          },
        },
      ],
    });

    await alert.present();
  }

  async deleteUserApi(user) {
    this.loadingGbl = await this.loader.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
      duration: 4000,
    });

    this.loadingGbl.present();
    
    this.restService.delete(
        `thorton/user/delete/${user.id}`,'',''
      )
      .pipe(take(1))
      .subscribe(
        async (resp: any) => {
          if (resp.status) {
           
            this.loadingGbl.dismiss();
            this.openToastSuccess();

            await this.getAllUsers();
          } else {
            this.openToastError();
          }
        },
        (error) => {
          this.loadingGbl.dismiss();
          console.log(error);
          this.openToastError();
        }
      );
  }

  async openToastSuccess() {
    const toast = await this.toastCtrl.create({
      message: 'User deleted successfully.',
      position: 'bottom',
      duration: 1500,
      color: 'success',
    });
    toast.present();
  }

  async openToastError() {
    const toast = await this.toastCtrl.create({
      message: 'Error occured. Please try again later.',
      position: 'bottom',
      duration: 4000,
      color: 'danger',
    });
    toast.present();
  }

  async changeUserStatus(e,user) {
    console.log(e,user , e.detail.checked)
    this.loadingGbl = await this.loader.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
      duration: 4000,
    });

    this.loadingGbl.present();
    
    this.restService.put(
        { status: e.target.checked},
        `thorton/user/change/status/${user.id}`,
      )
      .pipe(take(1))
      .subscribe(
        async (resp: any) => {
          if (resp.status) {
           
            this.loadingGbl.dismiss();
            const toast = await this.toastCtrl.create({
              message: 'User status updated successfully.',
              position: 'bottom',
              duration: 1500,
              color: 'success',
            });
            toast.present();
          } else {
            this.openToastError();
          }
        },
        (error) => {
          this.loadingGbl.dismiss();
          console.log(error);
          this.openToastError();
        }
      );
  }
}
