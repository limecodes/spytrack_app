import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  LoadingController,
  ModalController,
  ToastController,
} from '@ionic/angular';
import { take } from 'rxjs/operators';
import { GlobalService } from 'src/app/globals.service';
import { RestService } from 'src/app/rest.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss'],
})
export class UserEditComponent implements OnInit {
  userDetails: any;
  accountInformationForm: FormGroup;
  aa = 'asdsad';
  globalLoader: any;
  constructor(
    public modalController: ModalController,
    private router: Router,
    private global: GlobalService,
    private _formBuilder: FormBuilder,
    private httpRequest: RestService,
    public toastCtrl: ToastController,
    private loader: LoadingController,
    private activatedRoute: ActivatedRoute
  ) {
  }

  ngOnInit() {
    
    this.userDetails = JSON.parse(localStorage.getItem("editUserInfo"))
    this.formInit(this.userDetails);
  }

  formInit(userDetails) {
    this.accountInformationForm = this._formBuilder.group({
      firstname: new FormControl(userDetails.firstname, Validators.required),
      lastname: new FormControl(userDetails.lastname, Validators.required),
      email: new FormControl(userDetails.email, [Validators.required,Validators.email]),
      phone: new FormControl(userDetails.phone, [Validators.required,Validators.pattern('(([+][(]?[0-9]{1,3}[)]?)|([(]?[0-9]{4}[)]?))\s*[)]?[-\s\.]?[(]?[0-9]{1,3}[)]?([-\s\.]?[0-9]{3})([-\s\.]?[0-9]{3,4})')]),
      address: new FormControl(userDetails.address, Validators.required),
      city: new FormControl(userDetails.city, Validators.required),
      state: new FormControl(userDetails.state, Validators.required),
      zipcode: new FormControl(userDetails.zipcode, [Validators.required , Validators.pattern(/^[0-9]\d*$/),Validators.minLength(5)]),
      country: new FormControl(userDetails.country, Validators.required),
      username: new FormControl(userDetails.username, Validators.required),
      companyName: new FormControl(userDetails.companyName),
    });
    console.log(this.accountInformationForm.value)
  }

  async closeModal() {
    const onClosedData: string = 'Wrapped Up!';
    await this.modalController.dismiss(onClosedData);
  }

  async updateAccountInformation() {
    console.log(this.accountInformationForm.value)
    this.globalLoader = await this.loader.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
      duration: 4000,
    });

    this.globalLoader.present();
    const oldUserData = Object.assign({}, this.userDetails);
    const newUserData = {
      ...oldUserData,
      ...this.accountInformationForm.value
    };
    this.httpRequest
      .put(
        JSON.stringify(newUserData),
        'thorton/admin/user/update'
      )
      .pipe(take(1))
      .subscribe(
        (resp: any) => {
          if (resp.isUpdated) {
           
            this.globalLoader.dismiss();
            this.openToastSuccess();
            this.router.navigate(['../../'], {relativeTo: this.activatedRoute});
          } else {
            this.openToastError();
          }
        },
        (error) => {
          this.globalLoader.dismiss();
          console.log(error);
          this.openToastError();
        }
      );
  }

  async openToastSuccess() {
    const toast = await this.toastCtrl.create({
      message: 'User has been successfuly updated.',
      position: 'bottom',
      duration: 1500,
      color: 'success',
    });
    toast.present();
  }

  async openToastError() {
    const toast = await this.toastCtrl.create({
      message: 'Error occured. Please try again later.',
      position: 'bottom',
      duration: 4000,
      color: 'danger',
    });
    toast.present();
  }


  goback(){
    console.log("called back")
  }
}
