import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NavController, LoadingController, ToastController } from '@ionic/angular';
import { take } from 'rxjs/operators';
import { RestService } from 'src/app/rest.service';

@Component({
  selector: 'app-recipient-add',
  templateUrl: './recipient-add.component.html',
  styleUrls: ['./recipient-add.component.scss'],
})
export class RecipientAddComponent implements OnInit {
  addRecipientForm: FormGroup;
  globalLoader: any
  constructor(
    private router: Router,
    private navController: NavController,
    private fb: FormBuilder,
    private loader: LoadingController,
    private httpRequest: RestService,
    private toastCtrl: ToastController,
    private activatedRoute: ActivatedRoute
  ) {
    this.addRecipientForm = this.fb.group({
      type: new FormControl('', Validators.required),
      value: new FormControl('', [Validators.required, Validators.email])
    });
  }

  ngOnInit() { }

  handleChange(e): void {
    if (e.detail.value === 'email') {
      this.addRecipientForm.controls['value'].setValidators([Validators.required, Validators.email])
      this.addRecipientForm.controls['value'].updateValueAndValidity()
    } else {
      this.addRecipientForm.controls['value'].setValidators([Validators.required, Validators.pattern('(([+][(]?[0-9]{1,3}[)]?)|([(]?[0-9]{4}[)]?))\s*[)]?[-\s\.]?[(]?[0-9]{1,3}[)]?([-\s\.]?[0-9]{3})([-\s\.]?[0-9]{3,4})')])
      this.addRecipientForm.controls['value'].updateValueAndValidity()
    }
  }

  async addRecipient() {
    console.log(this.addRecipientForm.value)
    this.globalLoader = await this.loader.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
      duration: 4000,
    });

    this.globalLoader.present();

    this.httpRequest
      .post(
        JSON.stringify(this.addRecipientForm.value),
        'thorton/user/alerts/recipient/add'
      )
      .pipe(take(1))
      .subscribe(
        (resp: any) => {
          if (resp.isSuccess) {

            this.globalLoader.dismiss();
            this.openToastSuccess();
            this.router.navigate(['../'], { relativeTo: this.activatedRoute });
          } else {
            this.openToastError();
          }
        },
        (error) => {
          this.globalLoader.dismiss();
          console.log(error);
          this.openToastError();
        }
      );
  }

  async openToastSuccess() {
    const toast = await this.toastCtrl.create({
      message: 'Recipient has been added successfully.',
      position: 'bottom',
      duration: 1500,
      color: 'success',
    });
    toast.present();
  }

  async openToastError() {
    const toast = await this.toastCtrl.create({
      message: 'Error occured. Please try again later.',
      position: 'bottom',
      duration: 4000,
      color: 'danger',
    });
    toast.present();
  }
}
