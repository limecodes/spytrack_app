import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalController, ToastController, LoadingController } from '@ionic/angular';
import { take } from 'rxjs/operators';
import { GlobalService } from 'src/app/globals.service';
import { RestService } from 'src/app/rest.service';

@Component({
  selector: 'app-recipient-edit',
  templateUrl: './recipient-edit.component.html',
  styleUrls: ['./recipient-edit.component.scss'],
})
export class RecipientEditComponent implements OnInit {

  recipientDetails: any;
  recipientEditForm: FormGroup;
  globalLoader: any;
  constructor(
    public modalController: ModalController,
    private router: Router,
    private global: GlobalService,
    private _formBuilder: FormBuilder,
    private httpRequest: RestService,
    public toastCtrl: ToastController,
    private loader: LoadingController,
    private activatedRoute: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.recipientDetails = JSON.parse(localStorage.getItem("editRecipientInfo"))
    this.formInit(this.recipientDetails);
  }

  formInit(recipientDetails) {
    this.recipientEditForm = this._formBuilder.group({
      type: new FormControl(recipientDetails.type, Validators.required),
      value: new FormControl(recipientDetails.value, Validators.required),
    });

    if (recipientDetails.type === 'email') {
      this.recipientEditForm.controls['value'].setValidators([Validators.required, Validators.email])
      this.recipientEditForm.controls['value'].updateValueAndValidity()
    } else {
      this.recipientEditForm.controls['value'].setValidators([Validators.required, Validators.pattern('(([+][(]?[0-9]{1,3}[)]?)|([(]?[0-9]{4}[)]?))\s*[)]?[-\s\.]?[(]?[0-9]{1,3}[)]?([-\s\.]?[0-9]{3})([-\s\.]?[0-9]{3,4})')])
      this.recipientEditForm.controls['value'].updateValueAndValidity()
    }
    
    
    console.log(this.recipientEditForm.value)
  }

  handleChange(e): void {
    if (e.detail.value === 'email') {
      this.recipientEditForm.controls['value'].setValidators([Validators.required, Validators.email])
      this.recipientEditForm.controls['value'].updateValueAndValidity()
    } else {
      this.recipientEditForm.controls['value'].setValidators([Validators.required, Validators.pattern('(([+][(]?[0-9]{1,3}[)]?)|([(]?[0-9]{4}[)]?))\s*[)]?[-\s\.]?[(]?[0-9]{1,3}[)]?([-\s\.]?[0-9]{3})([-\s\.]?[0-9]{3,4})')])
      this.recipientEditForm.controls['value'].updateValueAndValidity()
    }
  }

  async closeModal() {
    const onClosedData: string = 'Wrapped Up!';
    await this.modalController.dismiss(onClosedData);
  }

  async updateRecipient() {
    console.log(this.recipientEditForm.value)
    this.globalLoader = await this.loader.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
      duration: 4000,
    });

    this.globalLoader.present();
    const oldRecipientData = Object.assign({}, this.recipientDetails);
    const newRecipientData = {
      ...oldRecipientData,
      ...this.recipientEditForm.value
    };
    this.httpRequest
      .put(
        JSON.stringify(newRecipientData),
        'thorton/user/alerts/recipient/update'
      )
      .pipe(take(1))
      .subscribe(
        (resp: any) => {
          if (resp.isSuccess) {
           
            this.globalLoader.dismiss();
            this.openToastSuccess();
            this.router.navigate(['../../'], {relativeTo: this.activatedRoute});
          } else {
            this.openToastError();
          }
        },
        (error) => {
          this.globalLoader.dismiss();
          console.log(error);
          this.openToastError();
        }
      );
  }

  async openToastSuccess() {
    const toast = await this.toastCtrl.create({
      message: 'Recipient has been successfuly updated.',
      position: 'bottom',
      duration: 1500,
      color: 'success',
    });
    toast.present();
  }

  async openToastError() {
    const toast = await this.toastCtrl.create({
      message: 'Error occured. Please try again later.',
      position: 'bottom',
      duration: 4000,
      color: 'danger',
    });
    toast.present();
  }


}
