import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoadingController, AlertController, ToastController } from '@ionic/angular';
import { take } from 'rxjs/operators';
import { RestService } from 'src/app/rest.service';

@Component({
  selector: 'app-recipient-list',
  templateUrl: './recipient-list.component.html',
  styleUrls: ['./recipient-list.component.scss'],
})
export class RecipientListComponent implements OnInit {
  loadingGbl: any;
  recipients: any;
  constructor(
    private loadingController: LoadingController,
    private restService: RestService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private alertController: AlertController,
    private toastCtrl: ToastController,
    private loader: LoadingController
  ) { }

  ngOnInit() {}

  async ionViewWillEnter(){
    await this.getAllRecipients();
  }

  async getAllRecipients(){
    this.loadingGbl = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...'
    });

    await this.loadingGbl.present();
    
    this.restService.get('thorton/user/alerts','','').pipe(take(1)).subscribe(async (resp: any) => {
      console.log("resp ====>", resp);
      await this.loadingGbl.dismiss();
      if(resp && resp.recipients.length !== 0) {
        this.recipients = resp.recipients;
      }else{
        this.recipients = [];

      }
    }, async (err) => {
      await this.loadingGbl.dismiss();
    });
  }

  async editRecipient(recipient: any){
    console.log(recipient);
  localStorage.setItem("editRecipientInfo", JSON.stringify(recipient));
   this.router.navigate([`edit/${recipient.id}`],{relativeTo: this.activatedRoute});
  }

  async deleteRecipient(recipient: any){
    this.presentAlert(recipient);
  }

  async addRecipient(){
    this.router.navigate(['add'],{relativeTo:this.activatedRoute})
  }

  async presentAlert(recipient:any) {
    const alert = await this.alertController.create({
      header: `Are you sure you want to delete "${recipient.value}" recipient ?`,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log("canceld")
          },
        },
        {
          text: 'OK',
          role: 'confirm',
          handler: () => {
            // call api 
            //recipient/delete/:recipientId
            this.deleteRecipientApi(recipient)
            console.log("recipient delete api " , recipient);
          },
        },
      ],
    });

    await alert.present();
  }

  async deleteRecipientApi(recipient) {
    this.loadingGbl = await this.loader.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
      duration: 4000,
    });

    this.loadingGbl.present();
    
    this.restService.delete(
        `thorton/user/alerts/recipient/delete/${recipient.id}`,'',''
      )
      .pipe(take(1))
      .subscribe(
        async (resp: any) => {
          if (resp.isSuccess) {
           
            this.loadingGbl.dismiss();
            this.openToastSuccess();

            await this.getAllRecipients();
          } else {
            this.openToastError();
          }
        },
        (error) => {
          this.loadingGbl.dismiss();
          console.log(error);
          this.openToastError();
        }
      );
  }

  async openToastSuccess() {
    const toast = await this.toastCtrl.create({
      message: 'Recipient deleted successfully.',
      position: 'bottom',
      duration: 1500,
      color: 'success',
    });
    toast.present();
  }

  async openToastError() {
    const toast = await this.toastCtrl.create({
      message: 'Error occured. Please try again later.',
      position: 'bottom',
      duration: 4000,
      color: 'danger',
    });
    toast.present();
  }


}
