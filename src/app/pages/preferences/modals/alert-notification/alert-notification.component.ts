import { Component, OnInit } from '@angular/core';
import {
  LoadingController,
  ModalController,
  ToastController,
} from '@ionic/angular';
import { take } from 'rxjs/operators';
import { RestService } from 'src/app/rest.service';

@Component({
  selector: 'app-alert-notification',
  templateUrl: './alert-notification.component.html',
  styleUrls: ['./alert-notification.component.scss'],
})
export class AlertNotificationComponent implements OnInit {
  isChecked: boolean;
  globalLoader: any;
  respDist: boolean;
  recipients: any[];
  response: {
    disturb: boolean,
    speeding: {
      email: boolean,
      mobile: boolean,
      text: boolean,
      web: boolean,
      limit: number,
      availability: boolean
    },
    low_battery: {
      email: boolean,
      mobile: boolean,
      status: boolean,
      text: boolean,
      limit: number,
web: boolean,
    },
    boundary_entry: {
      status: boolean,
      mobile: boolean,
      web: boolean,
      text: boolean,
      email: boolean
    },
    boundary_exit: {
      mobile: boolean,
      web: boolean,
      text: boolean,
      status: boolean,
      email: boolean
    },
    trip_end: {
      status: boolean,
      mobile: boolean,
      web: boolean,
      text: boolean,
      email: boolean
    },
    trip_start: {
      status: boolean,
      mobile: boolean,
      web: boolean,
      text: boolean,
      email: boolean
    }
  }

  constructor(
    public modalController: ModalController,
    private restService: RestService,
    private toastCtrl: ToastController,
    private loader: LoadingController,
  ) {
    this.response = this.getDefaultValue();
  }

  ngOnInit() { }

  ionViewWillEnter() {
    this.getAlertData();
  }

  async closeModal() {
    const onClosedData: string = 'Wrapped Up!';
    await this.modalController.dismiss(onClosedData);
  }

  getAlertData() {
    this.showLoader();
    this.restService
      .get('thorton/user/alerts', '', '')
      .pipe(take(1))
      .subscribe((resp: any) => {
        this.hideLoader();
        if (resp) {
          delete resp.recipients
          this.isChecked = resp['disturb'];
          this.respDist = resp['disturb'];
          this.response = resp;
          this.recipients = resp.recipients;
        } else {
        }
      });
  }

  async showLoader() {
    this.globalLoader = await this.loader.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
      duration: 4000,
    });

    this.globalLoader.present();
  }

  hideLoader() {
    this.globalLoader.dismiss();
  }

  saveChange() {
    this.showLoader();
    console.log(this.response)
    // const objdata = {
    //   disturb: this.isChecked ? 1 : 0,
    // };
    this.restService
      .put(JSON.stringify(this.response), 'thorton/user/alerts/update')
      .pipe(take(1))
      .subscribe(
        async (resp: any) => {
          this.hideLoader();
          if (resp.isSuccess) {
            const toast = await this.toastCtrl.create({
              message: 'notification data set.',
              position: 'bottom',
              duration: 1500,
              color: 'success',
            });
            await toast.present();
            this.closeModal();
          } else {
            const toast = await this.toastCtrl.create({
              message: resp.error.message || resp.message,
              position: 'bottom',
              duration: 1500,
              color: 'danger',
            });
            await toast.present();
          }
        },
        async (error) => {
          this.hideLoader();
          const toast = await this.toastCtrl.create({
            message: 'Something went wrong while update notification settings.',
            position: 'bottom',
            duration: 1500,
            color: 'error',
          });
          toast.present();
        }
      );
  }


  getDefaultValue() {
    return {
      disturb: false,
      speeding: {
        email: false,
        mobile: false,
        text: false,
        web: false,
        limit: 40,
        availability: false
      },
      low_battery: {
        email: false,
        mobile: false,
        status: false,
        text: false,
        web: false,
        limit: 15,
      },
      boundary_entry: {
        status: false,
        mobile: false,
        web: false,
        text: false,
        email: false
      },
      boundary_exit: {
        status: false,
        mobile: false,
        web: false,
        text: false,
        email: false
      },
      trip_end: {
        status: false,
        mobile: false,
        web: false,
        text: false,
        email: false
      },
      trip_start: {
        status: false,
        mobile: false,
        web: false,
        text: false,
        email: false
      }
    }
  }

  addRecipient() {
    this.recipients.push({
      email: '',
      type: '',
      value: ''
    })
  }

  async saveRecipient(index: number) {
    this.showLoader();

    if (this.recipients[index].id) {
      this.restService
        .put(JSON.stringify(this.recipients[index]), 'thorton/user/alerts/recipient/update')
        .pipe(take(1))
        .subscribe(
          async (resp: any) => {
            this.hideLoader();
            if (resp.isSuccess) {
              const toast = await this.toastCtrl.create({
                message: 'Recipient save successfully.',
                position: 'bottom',
                duration: 1500,
                color: 'success',
              });
              this.updateRecipients();
              await toast.present();
              this.closeModal();
            } else {
              const toast = await this.toastCtrl.create({
                message: resp.error.message || resp.message,
                position: 'bottom',
                duration: 1500,
                color: 'danger',
              });
              await toast.present();
            }
          },
          async (error) => {
            this.hideLoader();
            const toast = await this.toastCtrl.create({
              message: 'Something went wrong while save recipient.',
              position: 'bottom',
              duration: 1500,
              color: 'error',
            });
            toast.present();
          }
        );
    } else {
      this.restService
        .post(JSON.stringify(this.recipients[index]), 'thorton/user/alerts/recipient/add')
        .pipe(take(1))
        .subscribe(
          async (resp: any) => {
            this.hideLoader();
            if (resp.isSuccess) {
              const toast = await this.toastCtrl.create({
                message: 'Recipient save successfully.',
                position: 'bottom',
                duration: 1500,
                color: 'success',
              });
              this.updateRecipients();
              await toast.present();
            } else {
              const toast = await this.toastCtrl.create({
                message: resp.error.message || resp.message,
                position: 'bottom',
                duration: 1500,
                color: 'danger',
              });
              await toast.present();
            }
          },
          async (error) => {
            this.hideLoader();
            const toast = await this.toastCtrl.create({
              message: 'Something went wrong while save recipient.',
              position: 'bottom',
              duration: 1500,
              color: 'error',
            });
            toast.present();
          }
        );
    }
  }

  async deleteRecipientApi(recipient,index) {
    this.showLoader();
    if(recipient.id){
    this.restService.delete(
      `thorton/user/alerts/recipient/delete/${recipient.id}`, '', ''
    )
      .pipe(take(1))
      .subscribe(
        async (resp: any) => {
          if (resp.isSuccess) {

            this.globalLoader.dismiss();
            this.updateRecipients();
            const toast = await this.toastCtrl.create({
              message: 'Recipient save successfully.',
              position: 'bottom',
              duration: 1500,
              color: 'success',
            });
            await toast.present();
            this.hideLoader();
          } else {
            const toast = await this.toastCtrl.create({
              message: resp.error.message || resp.message,
              position: 'bottom',
              duration: 1500,
              color: 'danger',
            });
            await toast.present();
            this.hideLoader();
          }
        },
        async (error) => {
          console.log(error);
          this.hideLoader();
          const toast = await this.toastCtrl.create({
            message: 'Something went wrong while save recipient.',
            position: 'bottom',
            duration: 1500,
            color: 'error',
          });
          toast.present();
        }
      );
    }else{
      this.recipients.splice(index,1);
      this.hideLoader();
    }
  }

  updateRecipients() {
    this.restService
      .get('thorton/user/alerts', '', '')
      .pipe(take(1))
      .subscribe((resp: any) => {
        this.hideLoader();
        if (resp) {
          this.recipients = resp.recipients;
        } else {
        }
      });
  }
}
