import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import {
  LoadingController,
  ModalController,
  ToastController,
} from '@ionic/angular';
import { take } from 'rxjs/operators';
import { GlobalService } from 'src/app/globals.service';
import { RestService } from 'src/app/rest.service';

@Component({
  selector: 'app-edit-account',
  templateUrl: './edit-account.component.html',
  styleUrls: ['./edit-account.component.scss'],
})
export class EditAccountComponent implements OnInit {
  userDetails: any;
  accountInformationForm: FormGroup;
  aa = 'asdsad';
  globalLoader: any;
  constructor(
    public modalController: ModalController,
    private router: Router,
    private global: GlobalService,
    private _formBuilder: FormBuilder,
    private httpRequest: RestService,
    public toastCtrl: ToastController,
    private loader: LoadingController
  ) {}

  ngOnInit() {
    this.global.currentMessageSubscriberSummary.subscribe((data: any) => {
      if (data.isRefresh) {
        this.userDetails = this.global.userDetails;
      } else {
      }
    });
    this.formInit(this.userDetails);
  }

  formInit(userDetails) {
    this.accountInformationForm = this._formBuilder.group({
      firstname: new FormControl(userDetails.firstname, Validators.required),
      lastname: new FormControl(userDetails.lastname, Validators.required),
      email: new FormControl(userDetails.email,  [Validators.required, Validators.email]),
      phone: new FormControl(userDetails.phone, [Validators.required, Validators.pattern('(([+][(]?[0-9]{1,3}[)]?)|([(]?[0-9]{4}[)]?))\s*[)]?[-\s\.]?[(]?[0-9]{1,3}[)]?([-\s\.]?[0-9]{3})([-\s\.]?[0-9]{3,4})')]),
      address: new FormControl(userDetails.address, Validators.required),
      city: new FormControl(userDetails.city, Validators.required),
      state: new FormControl(userDetails.state, Validators.required),
      zipcode: new FormControl(userDetails.zipcode, [Validators.required , Validators.pattern(/^[0-9]\d*$/),Validators.minLength(5)]),
      country: new FormControl(userDetails.country, Validators.required),
      username: new FormControl(userDetails.username, Validators.required),
      companyName: new FormControl(userDetails.companyName),
    });
  }

  async closeModal() {
    const onClosedData: string = 'Wrapped Up!';
    await this.modalController.dismiss(onClosedData);
  }

  async updateAccountInformation() {
    this.globalLoader = await this.loader.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
      duration: 4000,
    });

    this.globalLoader.present();

    this.httpRequest
      .put(
        JSON.stringify(this.accountInformationForm.value),
        'thorton/user/update'
      )
      .pipe(take(1))
      .subscribe(
        (resp: any) => {
          if (resp.isUpdated) {
            const oldUserData = Object.assign({}, this.global.userDetails);
            const newUserData = {
              ...oldUserData,
              ...resp.Details,
              ...resp.User,
            };
            this.global.userDetails = newUserData;
            this.global.notifySummary({ isRefresh: true });
            this.closeModal();
            this.globalLoader.dismiss();
            this.openToastSuccess();
          } else {
            this.openToastError();
          }
        },
        (error) => {
          this.globalLoader.dismiss();
          console.log(error);
          this.openToastError();
        }
      );
  }

  async openToastSuccess() {
    const toast = await this.toastCtrl.create({
      message: 'Account information has been successfuly updated.',
      position: 'bottom',
      duration: 1500,
      color: 'success',
    });
    toast.present();
  }

  async openToastError() {
    const toast = await this.toastCtrl.create({
      message: 'Error occured. Please try again later.',
      position: 'bottom',
      duration: 4000,
      color: 'danger',
    });
    toast.present();
  }

  resetForm() {
    this.formInit(this.userDetails);
  }
}
