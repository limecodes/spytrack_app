import { Component, OnInit } from '@angular/core';
import {
  LoadingController,
  ModalController,
  ToastController,
} from '@ionic/angular';
import { PickerController } from '@ionic/angular';
import { PickerOptions } from '@ionic/core';
import { take } from 'rxjs/operators';
import { RestService } from 'src/app/rest.service';
import { timeZones } from './time-zones';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent implements OnInit {
  animals: string[] = ['Tiger', 'Lion', 'Elephant', 'Fox', 'Wolf'];
  selectedTimeZone = 'US/Eastern';
  allTimeZones: any = timeZones;
  userSettings: any;
  defaultMapStyle: any;
  defaultDistanceUnit: any;
  defaultSpeedUnit: any;
  globalLoader: any;
  constructor(
    public modalController: ModalController,
    private rest: RestService,
    public toastCtrl: ToastController,
    private pickerController: PickerController,
    private loader: LoadingController
  ) {}

  ngOnInit() {}

  ionViewDidEnter() {
    this.getUserSettings();
    this.defaultMapStyle = this.userSettings.map_style;
    this.selectedTimeZone = this.userSettings.timezone;
    this.defaultDistanceUnit = this.userSettings.distance_unit;
    this.defaultSpeedUnit = this.userSettings.speed_unit;
  }

  async closeModal() {
    const onClosedData: string = 'Wrapped Up!';
    await this.modalController.dismiss(onClosedData);
  }

  async showPicker() {
    let options: PickerOptions = {
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Ok',
          handler: (value: any) => {
            console.log(value);
            this.selectedTimeZone = value.Timezones.text;
          },
        },
      ],
      columns: [
        {
          name: 'Timezones',
          options: this.getColumnOptions(),
        },
      ],
    };

    let picker = await this.pickerController.create(options);
    picker.present();
  }

  getColumnOptions() {
    let options = [];
    this.allTimeZones.forEach((x) => {
      options.push({
        text: x.offset + ' ' + x.name,
        value: x.offset,
      });
    });
    return options;
  }

  getUserSettings() {
    this.userSettings = JSON.parse(localStorage.getItem('userSettings'));
  }

  selectedMapType(data) {
    console.log(data);
    this.defaultMapStyle = data;
  }

  selectedDistanceUnit(data) {
    console.log(data);
    this.defaultDistanceUnit = data;
  }

  selectedSpeedUnit(data) {
    console.log(data);
    this.defaultSpeedUnit = data;
  }

  async applyChanges() {
    this.globalLoader = await this.loader.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
    });

    this.globalLoader.present();
    let dataToSend = {
      timezone: this.selectedTimeZone,
      map_style: this.defaultMapStyle,
      speed_unit: this.defaultSpeedUnit,
      distance_unit: this.defaultDistanceUnit,
    };
    console.log(dataToSend);
    this.rest
      .put(dataToSend, 'thorton/user/settings')
      .pipe(take(1))
      .subscribe(
        (resp: any) => {
          this.globalLoader.dismiss();
          console.log(resp);
          localStorage.setItem('userSettings', JSON.stringify(resp));
          this.closeModal();
        },
        (error) => {
          this.globalLoader.dismiss();
          this.errorToast();
          this.closeModal();
        }
      );
  }

  resetForm() {
    this.defaultMapStyle = this.userSettings.map_style;
    this.selectedTimeZone = this.userSettings.timezone;
    this.defaultDistanceUnit = this.userSettings.distance_unit;
    this.defaultSpeedUnit = this.userSettings.speed_unit;
    //this.closeModal();
  }

  async errorToast() {
    const toast = await this.toastCtrl.create({
      message: 'Error in saving the data, please try again later.',
      position: 'bottom',
      duration: 4000,
      color: 'danger',
    });
    toast.present();
  }
}
