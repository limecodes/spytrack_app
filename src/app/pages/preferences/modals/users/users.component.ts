import { Component, OnInit } from '@angular/core';
import { LoadingController, ModalController } from '@ionic/angular';
import { RestService } from 'src/app/rest.service';
import { take } from 'rxjs/operators'; 
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  loadingGbl: any;
  users: any;
  constructor(
    public modalController: ModalController,
    private loadingController: LoadingController,
    private restService: RestService
  ) { }

  ngOnInit() {}

  async ionViewWillEnter(){
    await this.getAllUsers();
  }

  async getAllUsers(){
    this.loadingGbl = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...'
    });

    await this.loadingGbl.present();
    
    this.restService.get('web/users/all','','').pipe(take(1)).subscribe(async (resp: any) => {
      console.log("resp ====>", resp);
      await this.loadingGbl.dismiss();
      if(resp.length !== 0) {
        this.users  = resp;
      }else{
        this.users = [];
      }
    }, async (err) => {
      await this.loadingGbl.dismiss();
    });
  }

  async editUser(user: any){
    console.log(user)
  }

  async deleteUser(user: any){
    console.log(user)
  }

  async AddUser(){
    
  }

  async closeModal() {
    const onClosedData: string = "Wrapped Up!";
    await this.modalController.dismiss(onClosedData);
  }
}
