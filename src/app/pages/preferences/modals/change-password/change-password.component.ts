import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { LoadingController, ModalController, ToastController } from '@ionic/angular';
import { RestService } from 'src/app/rest.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent implements OnInit {

  changePasswordForm: FormGroup;
  passwordVal = {
    minLen: false,
    number: false,
    symbol: false,
    upperCase: false,
    lowerCase: false
  };
  cPasswordVal = false;
  globalLoader: any;

  constructor(
    private modalController: ModalController,
    private formBuilder: FormBuilder,
    private restService: RestService,
    private toastCtrl: ToastController,
    private loader: LoadingController
  ) {
    this.formInit();
   }

  ngOnInit() {}

  ionViewWillEnter(){
  }

  formInit() {
    this.changePasswordForm = this.formBuilder.group({
      opassword: new FormControl('', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&]).{8,}')]),
      password: new FormControl('', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&]).{8,}')]),
      cpassword: new FormControl('', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&]).{8,}')]),
    });
  }

  passwordInputValidation(event: Event) {
    const e =(event.target as HTMLInputElement).value;
    const number = /\d/;
    const symbol = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
    const upperCase = /[A-Z]/;
    const lowerCase = /[a-z]/;
    this.changePasswordForm.controls.password.setValue(e);
    if(this.changePasswordForm.controls.password.value == '') {
      this.passwordVal = {
        minLen: false,
        number: false,
        symbol: false,
        upperCase: false,
        lowerCase: false
      };
    } else {
      if(this.changePasswordForm.controls.password.value.length >= 8) {
        this.passwordVal.minLen = true;
      } else {
        this.passwordVal.minLen = false;
      }

      if(number.test(this.changePasswordForm.controls.password.value)) {
        this.passwordVal.number = true;
      } else {
        this.passwordVal.number = false;
      }
      if(symbol.test(this.changePasswordForm.controls.password.value)) {
        this.passwordVal.symbol = true;
      } else {
        this.passwordVal.symbol = false;
      }
      if(upperCase.test(this.changePasswordForm.controls.password.value)) {
        this.passwordVal.upperCase = true;
      } else {
        this.passwordVal.upperCase = false;
      }
      if(lowerCase.test(this.changePasswordForm.controls.password.value)) {
        this.passwordVal.lowerCase = true;
      } else {
        this.passwordVal.lowerCase = false;
      }
    }
  }

  cpasswordInputValidation(event: Event) {
    const e =(event.target as HTMLInputElement).value;
    this.changePasswordForm.controls.cpassword.setValue(e);
    if(this.changePasswordForm.controls.password.value === this.changePasswordForm.controls.cpassword.value) {
      this.cPasswordVal = true;
    } else {
      this.cPasswordVal = false;
    }
  }

  async changePasswordInitiate(){
    console.log(this.changePasswordForm);
    const sendData = {
      old_password: this.changePasswordForm.controls.opassword.value,
      new_password: this.changePasswordForm.controls.password.value
    };

    this.globalLoader = await this.loader.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
      duration: 4000
    });

    this.globalLoader.present();

    this.restService.post(JSON.stringify(sendData), 'thorton/user/change_password').subscribe(async (resp: any) => {
      console.log(resp);
      if(resp.isSuccess){
        this.globalLoader.dismiss();
        const toast = await this.toastCtrl.create({
          message: 'Password has been changed successfuly updated.',
          position: 'bottom',
          duration: 1500,
          color: 'success'
        });
        await toast.present();
        this.closeModal();

      }else{
        this.globalLoader.dismiss();
        const toast = await this.toastCtrl.create({
          message: resp.error.message || resp.message,
          position: 'bottom',
          duration: 1500,
          color: 'danger'
        });
        await toast.present();
      }
    },async (error) =>{
      const toast = await this.toastCtrl.create({
        message: 'Something went wrong while updating the password.',
        position: 'bottom',
        duration: 1500,
        color: 'error'
      });
      toast.present();
    });
  }

  async closeModal() {
    const onClosedData = 'Wrapped Up!';
    await this.modalController.dismiss(onClosedData);
  }

  resetForm(){
    this.formInit();
  }
}
