import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { LoadingController, ModalController, ToastController } from '@ionic/angular';
import { RestService } from 'src/app/rest.service';
// import { InAppBrowser } from '@ionic-native/in-app-browser';
@Component({
  selector: 'app-trackers',
  templateUrl: './trackers.component.html',
  styleUrls: ['./trackers.component.scss'],
})
export class TrackersComponent implements OnInit {
  activateTracker = false;
  activationForm: FormGroup;
  trackerForm: FormGroup;
  userTrackers:any = [];
  selectedTracker:string = '';
  selectedTrackerColor: any;
  selectedTrackerIcon: any;
  dataLoad = false;
  trackerResp : any;
  iconClass: any = [
    { icon: "location-arrow" },
    { icon: "map-marker" },
    { icon: "map-marker-alt" },
    { icon: "car" },
    { icon: "motorcycle" },
    { icon: "shuttle-van" },
    { icon: "truck" },
    { icon: "truck-pickup" },
    { icon: "caravan" },
    { icon: "user" },
    { icon: "male" },
    { icon: "female" },
    { icon: "child" },
    { icon: "baby" },
    { icon: "baby-carriage" },
  ]
  colorClass = [
    { color: "#229954" },
    { color: "#CD6155" },
    { color: "#A569BD" },
    { color: "#85C1E9" },
    { color: "#45B39D " },
    { color: "#F4D03F " },
    { color: "#DC7633 " },
    { color: "#CCCCFF " },
    { color: "#6495ED " },
    { color: "#40E0D0 " },
    { color: "#9FE2BF " },
    { color: "#DE3163 " },
    { color: "#DFFF00 " },
    { color: "#FFBF00 " },
    { color: "#FF7F50 " },
  ];
  constructor(
    public modalController: ModalController,
    private _formBuilder: FormBuilder,
    public rest: RestService,
    public toastCtrl: ToastController,
    public loadingController: LoadingController
    // private inAppBrowser: InAppBrowser
  ) { }

  ngOnInit() {
  }
  
  ionViewDidEnter() {
    this.getUserTracker();
    this.presentLoading();
  }

  getUserTracker() {
    this.rest.get('thorton/trackers/user', '', '').subscribe((resp: any) => {
      console.log('all user tracker', resp);
      if(resp.length != 0) {
        this.trackerResp = resp.slice(0);
        for(let x = 0; x < resp.length; x++) {
          if(resp[x].tracker_icon) {
            resp[x].tracker_icon = resp[x].tracker_icon.replace('fa-', '')
            this.userTrackers.push(resp[x]);
          } else {
            this.userTrackers.push(resp[x]);
          }
        }
        console.log(this.userTrackers);
        this.dataLoad = true;
      } else {
        this.dataLoad = true;
      }
    });
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
      duration: 5000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }

  formInitTracker(tracker) {
    console.log(tracker)
    this.trackerForm = this._formBuilder.group({
      trackerName: new FormControl(tracker.tracker_name, Validators.required),
      trackerDescription: new FormControl(tracker.description, Validators.required),
      imei: new FormControl(tracker.imei, Validators.required)
    })
  }

  async closeModal() {
    const onClosedData: string = "Wrapped Up!";
    await this.modalController.dismiss(onClosedData);
  }

  selectTracker(event, tracker) {
    this.formInitTracker(tracker);
    this.selectedTracker = tracker;
    this.selectedTrackerColor = tracker.tracker_icon_color;
    this.selectedTrackerIcon = tracker.tracker_icon.replace('fa-', '');
  }

  colorChange(event, color) {
    this.selectedTrackerColor = color;
  }

  iconChange(event, icon) {
    console.log(icon);
    this.selectedTrackerIcon = icon;
  }

  updateTrackerSetting() {
    let dataToSend = {
      tracker_name: this.trackerForm.get('trackerName').value,
      description: this.trackerForm.get('trackerDescription').value,
      imei: this.trackerForm.get('imei').value,
      tracker_icon: 'fa-' + this.selectedTrackerIcon,
      tracker_icon_color: this.selectedTrackerColor
    }
    console.log(dataToSend);
    this.rest.put(dataToSend, 'thorton/tracker/preferences/update').subscribe((resp: any) => {
      this.closeModal();
    }, error => {
      this.errorToast();
    });
  }

  resetForm() {
   this.formInitTracker(this.selectedTracker);
  }

  async errorToast() {  
    const toast = await this.toastCtrl.create({  
      message: 'Error on updating the tracker details.',
      position: 'bottom',
      duration: 4000,
      color: 'danger' 
    });  
    toast.present();  
  }  
}
