import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RecipientAddComponent } from './components/recipient-add/recipient-add.component';
import { RecipientEditComponent } from './components/recipient-edit/recipient-edit.component';
import { RecipientListComponent } from './components/recipient-list/recipient-list.component';

import { PreferencesPage } from './preferences.page';

const routes: Routes = [
  {
    path: '',
    component: PreferencesPage,
  },
  {
    path: 'recipient',
    component: RecipientListComponent,
  },
  {
    path: 'recipient/add',
    component: RecipientAddComponent,
  },
  {
    path: 'recipient/edit/:id',
    component: RecipientEditComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PreferencesPageRoutingModule {}
