import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PreferencesPageRoutingModule } from './preferences-routing.module';

import { PreferencesPage } from './preferences.page';
import { EditAccountComponent } from './modals/edit-account/edit-account.component';
import { TrackersComponent } from './modals/trackers/trackers.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AlertNotificationComponent } from './modals/alert-notification/alert-notification.component';
import { SettingsComponent } from './modals/settings/settings.component';
import { UsersComponent } from './modals/users/users.component';
import { ChangePasswordComponent } from './modals/change-password/change-password.component';
import { RecipientListComponent } from './components/recipient-list/recipient-list.component';
import { RecipientAddComponent } from './components/recipient-add/recipient-add.component';
import { RecipientEditComponent } from './components/recipient-edit/recipient-edit.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    PreferencesPageRoutingModule,
    FontAwesomeModule,
  ],
  declarations: [
    PreferencesPage,
    EditAccountComponent,
    TrackersComponent,
    AlertNotificationComponent,
    SettingsComponent,
    UsersComponent,
    ChangePasswordComponent,
    RecipientListComponent,
    RecipientAddComponent,
    RecipientEditComponent
  ]
})
export class PreferencesPageModule {}
