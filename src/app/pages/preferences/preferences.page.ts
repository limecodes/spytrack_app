import { Component, OnDestroy, OnInit } from '@angular/core';
import { LoadingController, ModalController } from '@ionic/angular';

import { EditAccountComponent } from './modals/edit-account/edit-account.component';
import { AlertNotificationComponent } from './modals/alert-notification/alert-notification.component';
import { SettingsComponent } from './modals/settings/settings.component';
import { UsersComponent } from './modals/users/users.component';
import { TrackersComponent } from './modals/trackers/trackers.component';
import { GlobalService } from 'src/app/globals.service';
import { RestService } from 'src/app/rest.service';
import { take } from 'rxjs/operators';
import { ChangePasswordComponent } from './modals/change-password/change-password.component';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-preferences',
  templateUrl: './preferences.page.html',
  styleUrls: ['./preferences.page.scss'],
})
export class PreferencesPage implements OnInit, OnDestroy {
  user: any;
  loadingGbl: any;
  currentMessageSubscription: Subscription;
  constructor(
    public modalController: ModalController,
    private global: GlobalService,
    private rest: RestService,
    private loadingController: LoadingController,
    private router: Router
  ) { }

  ngOnInit() {
    this.currentMessageSubscription = this.global.currentMessageSubscriberSummary.subscribe((data: any) => {
      if (data.isRefresh) {
        this.user = this.global.userDetails;
      } else {
        console.log(this.global.userDetails)
        if(this.global.userDetails) {
          this.getUser();
        } else {
          this.user = {
            address: "",
            city: "",
            companyName: "",
            country: "",
            created_at: "",
            email: "",
            email_verified_at: "",
            firstname: "",
            id: "",
            lastname: "",
            middlename: "",
            phone: "",
            state: "",
            updated_at: "",
            username: "",
            zipcode: "",
          }
          this.getUser();
        }
      }
    })
  }


  async openEditAccountModal() {
    const modal = await this.modalController.create({
      component: EditAccountComponent,
      cssClass: 'filter-modal'
    });
    // modal.onDidDismiss().then((data: any) => {
    //   this.user = this.global.userDetails;
    // });
    return await modal.present();
  }

  async openEditAlertNotificationModal() {
    const modal = await this.modalController.create({
      component: AlertNotificationComponent,
      cssClass: 'filter-modal'
    });
    return await modal.present();
  }

  async openEditSettingModal() {
    const modal = await this.modalController.create({
      component: SettingsComponent,
      cssClass: 'filter-modal'
    });
    return await modal.present();
  }

  async openEditUsersModal() {
    this.router.navigate(['/userlist'])
  }

  async openEditTrackersModal() {
    const modal = await this.modalController.create({
      component: TrackersComponent,
      cssClass: 'filter-modal'
    });
    modal.onDidDismiss().then((data: any) => {
    });
    return await modal.present();
  }

  async openEditChangePasswordModal() {
    const modal = await this.modalController.create({
      component: ChangePasswordComponent,
      cssClass: 'filter-modal'
    });
    modal.onDidDismiss().then((data: any) => {
    });
    return await modal.present();
  }

  async getUser() {
    this.loadingGbl = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...'
    });

    await this.loadingGbl.present();
    this.rest.get('user', '', '').pipe(take(1)).subscribe(async (resp: any) => {
      await this.loadingGbl.dismiss();
      this.global.firstname = resp.firstname;
      this.global.userDetails = resp;
      console.log(this.global.userDetails);
      this.user = resp;
      this.global.notifySummary({ isRefresh: true });
    });
  }


  ngOnDestroy(){
    this.currentMessageSubscription?.unsubscribe();
  }
}
