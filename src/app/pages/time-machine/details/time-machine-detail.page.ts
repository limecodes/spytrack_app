/* eslint-disable arrow-body-style */
/* eslint-disable @typescript-eslint/no-inferrable-types */
/* eslint-disable max-len */
/* eslint-disable no-var */
/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable @typescript-eslint/type-annotation-spacing */
/* eslint-disable no-trailing-spaces */
/* eslint-disable @typescript-eslint/dot-notation */
/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable @typescript-eslint/semi */
/* eslint-disable quote-props */
/* eslint-disable @typescript-eslint/quotes */
/* eslint-disable @typescript-eslint/prefer-for-of */
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { GlobalService } from 'src/app/globals.service';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';
import {
  LoadingController,
  NavController,
  ToastController,
} from '@ionic/angular';
import { Screenshot } from '@ionic-native/screenshot/ngx';

import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';
@Component({
  selector: 'app-time-machine-detail',
  templateUrl: './time-machine-detail.page.html',
  styleUrls: ['./time-machine-detail.page.scss'],
})
export class TimeMachineDetailPage {
  selectedPoint: any;
  pointSubscription: Subscription;
  @ViewChild('mapDiv', { read: ElementRef, static: false }) mapRef: ElementRef;
  loadingGbl: any;
  map2: any;
  imagePrint: boolean = false;
  isMobilePlatform: boolean;
  constructor(
    private globalService: GlobalService,
    private socialSharing: SocialSharing,
    public loadingController: LoadingController,
    private screenShot: Screenshot,
    private platform: Platform,
    public toastCtrl: ToastController,
    private navController: NavController,
    private route: Router
  ) {
    this.isMobilePlatform = !(
      this.platform.is('desktop') || this.platform.is('mobileweb')
    );
    this.pointSubscription = this.globalService.selectedPoint.subscribe(
      (point: any) => {
        console.log('point', point);
        if (point == '') {
          this.toastCtrl
            .create({
              message: 'Error occured. something went wrong!!',
              position: 'bottom',
              duration: 4000,
              color: 'danger',
            })
            .then((toast) => {
              toast.present();
              this.route.navigate(['/time-machine']);
            });
          return '';
        }
        this.selectedPoint = point;
        let options: any = {
          weekday: 'short',
          year: 'numeric',
          month: 'short',
          day: 'numeric',
          hour: 'numeric',
          minute: 'numeric',
          hour12: true,
        };
        this.selectedPoint.dateTime = new Date(
          this.selectedPoint.timestamp.replace(' ', 'T')
        ).toLocaleDateString('en-US', options);
        console.log(this.selectedPoint);
      }
    );
  }

  ionViewWillEnter() {
    this.pointSubscription = this.globalService.selectedPoint.subscribe(
      (point: any) => {
        console.log('point', point);
        if (point == '') {
          this.toastCtrl
            .create({
              message: 'Error occured. something went wrong!!',
              position: 'bottom',
              duration: 4000,
              color: 'danger',
            })
            .then((toast) => {
              toast.present();
              this.route.navigate(['/time-machine']);
            });
          return '';
        } else {
          this.selectedPoint = point;
          let options: any = {
            weekday: 'short',
            year: 'numeric',
            month: 'short',
            day: 'numeric',
            hour: 'numeric',
            minute: 'numeric',
            hour12: true,
          };
          this.selectedPoint.dateTime = new Date(
            this.selectedPoint.timestamp.replace(' ', 'T')
          ).toLocaleDateString('en-US', options);
          console.log(this.selectedPoint);
          this.renderMap();
        }
      }
    );
    console.log(this.selectedPoint);
    this.isMobilePlatform = !(
      this.platform.is('desktop') || this.platform.is('mobileweb')
    );
  }

  renderMap() {
    this.map2 = new google.maps.Map(
      document.getElementById('mapDiv') as HTMLElement,
      {
        mapTypeId: google.maps.MapTypeId.HYBRID,
        mapTypeControl: false,
        streetViewControl: false,
        keyboardShortcuts: false,
        center: {
          lat: parseFloat(this.selectedPoint.latitude),
          lng: parseFloat(this.selectedPoint.longitude),
        },
        zoom: 18,
        zoomControl: false,
        fullscreenControl: false,
      }
    );
    // var bounds = new google.maps.LatLngBounds();
    var pos = new google.maps.LatLng(
      this.selectedPoint.latitude,
      this.selectedPoint.longitude
    );
    // bounds.extend(pos);
    new google.maps.Marker({
      position: pos,
      map: this.map2,
      title: this.selectedPoint.formatted_address,
      clickable: true,
      animation: google.maps.Animation.DROP,
      icon: {
        url: this.selectedPoint.tracker_icon_url, //SVG path of awesomefont marker
        fillOpacity: 1,
        strokeWeight: 0,
        size: new google.maps.Size(30, 30),
        scaledSize: new google.maps.Size(30, 30),
        anchor: new google.maps.Point(0, 32),
      },
    });
  }

  getDirection() {
    let url = `https://www.google.com/maps/search/?api=1&query=${this.selectedPoint.latitude},${this.selectedPoint.longitude}`;
    window.open(url, '_system');
  }

  async shareLocation() {
    console.log('is mobile platform  => ', this.isMobilePlatform);
    if (this.isMobilePlatform) {
      this.imagePrint = true;

      this.loadingGbl = await this.loadingController.create({
        cssClass: 'my-custom-class',
        message: 'Please wait...',
      });

      await this.loadingGbl.present();

      await this.loadingGbl.dismiss();
      this.screenShot.URI(100).then(
        async (imageBase64) => {
          this.imagePrint = false;
          let prepareText = `${this.selectedPoint.trackerName} was near ${this.selectedPoint.formatted_address} on ${this.selectedPoint.dateTime}`;
          await this.socialSharing.share(
            prepareText,
            null,
            imageBase64.URI,
            null
          );
        },
        async (error) => {
          this.imagePrint = false;
          console.log(error);
        }
      );
    } else {
      console.log('this feature only supported in mobile application');
    }
  }

  blobToBase64(blob) {
    return new Promise((resolve, _) => {
      const reader = new FileReader();
      reader.onloadend = () => resolve(reader.result);
      reader.readAsDataURL(blob);
    });
  }

  ionViewDidLeave() {
    if (this.pointSubscription) {
      this.pointSubscription.unsubscribe();
    }
  }
}
