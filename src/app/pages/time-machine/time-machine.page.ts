/* eslint-disable max-len */
/* eslint-disable no-var */
/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable @typescript-eslint/type-annotation-spacing */
/* eslint-disable no-trailing-spaces */
/* eslint-disable @typescript-eslint/dot-notation */
/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable @typescript-eslint/semi */
/* eslint-disable quote-props */
/* eslint-disable @typescript-eslint/quotes */
/* eslint-disable @typescript-eslint/prefer-for-of */
import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { RestService } from 'src/app/rest.service';
import { GlobalService } from 'src/app/globals.service';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { OwlCarousel } from 'ngx-owl-carousel/src/owl-carousel.component';
import { take } from 'rxjs/operators';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-time-machine',
  templateUrl: './time-machine.page.html',
  styleUrls: ['./time-machine.page.scss'],
})
export class TimeMachinePage{
  selectedDate: any;
  selectedStartTime: any;
  selectedEndTime: any;
  selectedTracker: any;
  globalData : string;
  userTrackers: any;
  selectedTrackerData: any;
  dataLoad = false;
  displayMap: boolean;
  filteredTrackerResult: any = [];
  items: any = [];
  map: google.maps.Map;
  @ViewChild('tmmap', {read: ElementRef, static: false}) mapRef: ElementRef;
  defaultMapLocation = {
    last_location: { latitude: 32.3143016666667, longitude: -84.5017333333333},
  }; // LAT LONG POINTS TO DAVAO, PHILIPPINES (Subject to change)
  mapStyle = '0'; // map style by default is set to 0 (ROADMAP style)
  mapOptions: any;
  flightPath: Array<google.maps.Polyline>;
  userSettings: any;
  slideOptions = { items: 2, dots: false};  
  carouselOptions = { items: 3, dots: true, nav: true };
  loadingGbl: any;
  globalDataMessageSubscription: Subscription;
  currentMarker: google.maps.Marker;
  @ViewChild('owlElement') owlElement: OwlCarousel;

  constructor(
    private rest: RestService,
    public global: GlobalService,
    private router: Router,
    public loadingController: LoadingController
  ) {
    this.getUserSettings();
    this.displayMap = false;
    this.globalDataMessageSubscription = this.global.currentMessage.pipe(take(1)).subscribe(message => this.globalData = message);
    this.items = [
      { expanded: true },      
     ];
    // this.flightPath = [];
    // this.selectedTracker = {
    //   "imei": 4900104424,
    //   "date": "2022/2/24",
    //   "starttime": "8:41:15",
    //   "endtime": "20:41:15"
    // };
    // this.selectedDate = "2022-02-24T03:11:15.395-00:00";
    // this.selectedStartTime = "2022-04-09T08:41:15.397+05:30";
    // this.selectedEndTime = "2022-04-09T12:41:15.397+05:30";
    // this.selectedTracker = {
    //   "imei": 4900104424,
    //   "date": "2022/2/24",
    //   "starttime": "8:41:15",
    //   "endtime": "20:41:15"
    // };

    this.items[0].expanded = true;
  }

  ionViewDidEnter(){
    //this.getUserSettings();
    //this.displayMap = false;
    //this.globalDataMessageSubscription = this.global.currentMessage.pipe(take(1)).subscribe(message => this.globalData = message);
    // this.items = [
    //   { expanded: true },      
    // ];
    
    // this.displayMap = false;
    // this.items[0].expanded = true;
    //this.global.currentMessage.subscribe(message => this.globalData = message)
    this.slideOptions = { items: 2, dots: false}; 
    if(this.owlElement){
      this.owlElement?.reInit();
    }
    this.getUserTracker();
    this.presentLoading();
  }

  getUserTracker() {
    this.rest.get('thorton/trackers/user', '', '').subscribe((resp: any) => {
      if(resp.length != 0) {
        this.userTrackers = resp;
        this.dataLoad = true;
      } else {
        this.dataLoad = true;
      }
    });
  }

  selectTracker(tracker) {
    if(tracker != 'All') {
      this.selectedTracker = tracker;
    } else {
      this.selectedTracker = tracker;
    }
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
      duration: 4000
    });
    await loading.present();
    const { role, data } = await loading.onDidDismiss();
  }

  async applyFilter() {
    this.loadingGbl = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...'
    });

    await this.loadingGbl.present();
    let selectedTracker = JSON.parse(localStorage.getItem('selectedUserTrackers'));
    let date = new Date(this.selectedDate);
    let startTime = new Date(this.selectedStartTime)
    let endTime = new Date(this.selectedEndTime)
    let dataToSend: any;
    if(this.selectedTracker == 'All') {
      dataToSend = {
        "date": date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate(),
        "starttime": startTime.getHours() + ':' + startTime.getMinutes() + ':' + startTime.getSeconds(),
        "endtime": endTime.getHours() + ':' + endTime.getMinutes() + ':' + endTime.getSeconds(),
      }
    } else {
      dataToSend = {
        "imei": this.selectedTracker.imei,
        "date": date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate(),
        "starttime": startTime.getHours() + ':' + startTime.getMinutes() + ':' + startTime.getSeconds(),
        "endtime": endTime.getHours() + ':' + endTime.getMinutes() + ':' + endTime.getSeconds(),
      }
    }
    this.rest.post(dataToSend, 'thorton/time/machine').subscribe(async (resp: any) => {
      if(resp?.length == 0){
        await this.loadingGbl.dismiss();
        this.displayMap = true;
      }else{
        for(let x = 0; x < resp.length; x++) {
          resp[x].tracker_icon_url = this.global.getMarkerLocation(resp[x].tracker_icon, resp[x].tracker_icon_color);
        }
      
        // let uniqueLocations = this.removeDuplicatesLatLong(resp);
        // let response = resp;
        // response[0].locations = uniqueLocations;
        this.filteredTrackerResult = resp;
        if(this.owlElement){
          this.owlElement.reInit();
        }else{
          setTimeout(()=>{
            this.owlElement?.reInit();
          },0)
        }
        if(this.filteredTrackerResult.length > 0){
          this.selectedTrackerData = resp[0];
          this.displayMap = true;
          //this.owlElement.reInit();
          this.drawMap();
        }else{
          this.displayMap = true;
          await this.loadingGbl.dismiss();
        }
      }
      // this.newMessage(JSON.stringify(resp));
      // this.router.navigate(['/live-tracking']);
    });
  }

  viewInLiveTracking(trackerDetails, location) {
    // console.log(trackerDetails);
    // console.log(location);
    // trackerDetails.locations = [];
    // let a = trackerDetails['tracker_icon'];
    // let output = a;
    // trackerDetails['tracker_icon'] = output;
    // trackerDetails.locations.push(location);
    // console.log(trackerDetails);
    // this.newMessage(JSON.stringify(trackerDetails));
    // console.log(trackerDetails);
    // this.router.navigate(['/live-tracking']);
    let requiredData = {
      trackerName: trackerDetails.tracker_name,
      tracker_icon_url: trackerDetails.tracker_icon_url,
      ...location
    }
    this.global.userSelectedNewPoint(requiredData);
    this.router.navigate(['/time-machine/detail'])
  }

  newMessage(locations) {
    this.global.changeMessage(locations)
  }

  async drawMap(selectedLocation?: any){
    if(this.selectedTrackerData.locations.length > 0){
    let currentTrackerLocation = this.selectedTrackerData.locations;
    let latu =  selectedLocation ? selectedLocation.latitude :  currentTrackerLocation[0].latitude;
    let long =  selectedLocation ? selectedLocation.longitude :  currentTrackerLocation[0].longitude;
    this.map = null;
    this.flightPath = [];
    this.map = new google.maps.Map(document.getElementById("tmmap") as HTMLElement, {
      mapTypeId: google.maps.MapTypeId[(this.userSettings.map_style).toUpperCase()],
      mapTypeControl: false,
      streetViewControl: false,
      keyboardShortcuts: false,
      zoom: 0
    });
    let bounds = new google.maps.LatLngBounds();
    var poly = [];


    let index = 0;
    for(let location of currentTrackerLocation){
      const latLong = {lat: parseFloat(location.latitude), lng : parseFloat(location.longitude)}
      if(selectedLocation){
        if(location.latitude == selectedLocation.latitude && location.longitude == selectedLocation.longitude){
          this.currentMarker = new google.maps.Marker({
          position:  {lat: parseFloat(selectedLocation.latitude), lng : parseFloat(selectedLocation.longitude)},
          map: this.map,
          title: location.formatted_address,
          clickable: true,
          icon:{
            url: this.selectedTrackerData.tracker_icon_url, //SVG path of awesomefont marker
            fillOpacity: 1,
            strokeWeight: 0,
            size: new google.maps.Size(30, 30),
            scaledSize: new google.maps.Size(30, 30),
            anchor: new google.maps.Point(0, 32)
          },
        });
        }
      }else{
        if(index == 0){
          this.currentMarker = new google.maps.Marker({
            position: latLong,
            map: this.map,
            title: location.formatted_address,
            clickable: true,
            animation:google.maps.Animation.DROP,
            icon:{
              url: this.selectedTrackerData.tracker_icon_url, //SVG path of awesomefont marker
              fillOpacity: 1,
              strokeWeight: 0,
              size: new google.maps.Size(30, 30),
              scaledSize: new google.maps.Size(30, 30),
              anchor: new google.maps.Point(0, 32)
            }
            
          });
        }
      }
      index++;
    }

    let color = '#lightgray';
    if(selectedLocation){
      color = this.selectedTrackerData.tracker_icon_color;
    }
    for(var i = 0 ; i < currentTrackerLocation.length ; i=i+1 ){
      let j = i+1;
      if(selectedLocation && currentTrackerLocation[i].latitude == selectedLocation.latitude && currentTrackerLocation[i].longitude == selectedLocation.longitude){
        color = "#lightgray";
      }
      if(currentTrackerLocation[i] && currentTrackerLocation[j]){
        var pos = new google.maps.LatLng(currentTrackerLocation[i].latitude,currentTrackerLocation[i].longitude);
        var pos2 = new google.maps.LatLng(currentTrackerLocation[j].latitude,currentTrackerLocation[j].longitude);
        //bounds.extend(pos);
        bounds.extend(pos2);
        poly.push(pos);
        this.flightPath.push(new google.maps.Polyline({
          path: [pos,pos2],
          geodesic: true,
          strokeColor: "lightgray",
          strokeOpacity: 1.0,
          strokeWeight: 8,
          map: this.map,
        }));
        this.flightPath.push(new google.maps.Polyline({
          path: [pos,pos2],
          geodesic: true,
          strokeColor: color,
          strokeOpacity: 1.0,
          strokeWeight: 5,
          map: this.map,
        }));
        
      }
    }
    setTimeout(async()=>{
      this.displayMap = true;
      this.map.fitBounds(bounds);
      this.map.panToBounds(bounds);
      this.map.panTo({ 
          lat: parseFloat(latu) , 
          lng: parseFloat(long) 
        });
        await this.loadingGbl.dismiss();
    },100)
  }else{
    //this.displayMap = false;
    await this.loadingGbl.dismiss();
  }
}
 

  async drawPathInToMap(selectedLocation?: any){

    let latu =  selectedLocation.latitude;
    let long =  selectedLocation.longitude;

    let currentTrackerLocation = this.selectedTrackerData.locations;
    let bounds = new google.maps.LatLngBounds();
    let poly = [];
    
    this.currentMarker.setMap(null);
    for(let existingPoly of this.flightPath){
      existingPoly.setMap(null);
    }
    this.flightPath = [];
    let index = 0;
    for(let location of currentTrackerLocation){
      const latLong = {lat: parseFloat(location.latitude), lng : parseFloat(location.longitude)}
      if(selectedLocation){
        if(location.latitude == selectedLocation.latitude && location.longitude == selectedLocation.longitude){
        this.currentMarker = new google.maps.Marker({
          position:  {lat: parseFloat(selectedLocation.latitude), lng : parseFloat(selectedLocation.longitude)},
          map: this.map,
          title: location.formatted_address,
          clickable: true,
          icon:{
            url: this.selectedTrackerData.tracker_icon_url, //SVG path of awesomefont marker
            fillOpacity: 1,
            strokeWeight: 0,
            size: new google.maps.Size(30, 30),
            scaledSize: new google.maps.Size(30, 30),
            anchor: new google.maps.Point(0, 32)
          },
        });
        }
      }else{
        if(index == 0){
          this.currentMarker = new google.maps.Marker({
            position: latLong,
            map: this.map,
            title: location.formatted_address,
            clickable: true,
            animation:google.maps.Animation.DROP,
            icon:{
              url: this.selectedTrackerData.tracker_icon_url, //SVG path of awesomefont marker
              fillOpacity: 1,
              strokeWeight: 0,
              size: new google.maps.Size(30, 30),
              scaledSize: new google.maps.Size(30, 30),
              anchor: new google.maps.Point(0, 32)
            }
            
          });
        }
      }
      index++;
    }


    let color = 'lightgray';
    if(selectedLocation){
      color = this.selectedTrackerData.tracker_icon_color;
    }
    for(var i = 0 ; i < currentTrackerLocation.length ; i=i+1 ){
      let j = i+1;
      if(selectedLocation && currentTrackerLocation[i].latitude == selectedLocation.latitude && currentTrackerLocation[i].longitude == selectedLocation.longitude){
        color = "lightgray";
      }
      if(currentTrackerLocation[i] && currentTrackerLocation[j]){
        var pos = new google.maps.LatLng(currentTrackerLocation[i].latitude,currentTrackerLocation[i].longitude);
        var pos2 = new google.maps.LatLng(currentTrackerLocation[j].latitude,currentTrackerLocation[j].longitude);
        //bounds.extend(pos);
        bounds.extend(pos2);
        poly.push(pos);
        this.flightPath.push(new google.maps.Polyline({
          path: [pos,pos2],
          geodesic: true,
          strokeColor: "lightgray",
          strokeOpacity: 1.0,
          strokeWeight: 8,
          map: this.map,
        }));
        this.flightPath.push(new google.maps.Polyline({
          path: [pos,pos2],
          geodesic: true,
          strokeColor: color,
          strokeOpacity: 1.0,
          strokeWeight: 5,
          map: this.map,
        }));
        
      }
    }
    this.displayMap = true;
    this.map.panTo({lat: parseFloat(latu) , lng: parseFloat(long)});
    //this.map.fitBounds(bounds);
    //this.map.panToBounds(bounds);
    // this.map.panTo({ 
    //     lat: parseFloat(latu) , 
    //     lng: parseFloat(long) 
    //   });
  }
  
  async viewInCurrentMap(trackerDetails, location,index){
    // console.log(trackerDetails, location,index)
    // this.loadingGbl = await this.loadingController.create({
    //   cssClass: 'my-custom-class',
    //   message: 'Please wait...'
    // });
    // await this.loadingGbl.present();
    //this.map.setCenter({lat:parseFloat(location.latitude),lng:parseFloat(location.longitude)});
    this.drawPathInToMap(location);
  }

  expandItem(item): void {
    if (item.expanded) {
      item.expanded = false;
    } else {
      this.items.map(listItem => {
        if (item == listItem) {
          listItem.expanded = !listItem.expanded;
        } else {
          listItem.expanded = false;
        }
        return listItem;
      });
    }
  }

  async changeSelectedTracker(index) {
    this.loadingGbl = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...'
    });
    await this.loadingGbl.present();
    this.selectedTrackerData = this.filteredTrackerResult[index];
    this.displayMap = true;
    this.drawMap();
  }

  getUserSettings() {
    this.userSettings = JSON.parse(localStorage.getItem('userSettings'))
  }

  ionViewDidLeave(){
    // this.displayMap = false;
    // this.items[0].expanded = true;
    // this.selectedDate = undefined;
    // this.selectedStartTime = undefined;
    // this.selectedEndTime = undefined;
    // this.selectedTracker = null;
    if(this.globalDataMessageSubscription){
      this.globalDataMessageSubscription.unsubscribe();
    }
  }
}
