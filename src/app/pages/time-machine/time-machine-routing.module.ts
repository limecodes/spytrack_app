import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TimeMachineDetailPage } from './details/time-machine-detail.page';

import { TimeMachinePage } from './time-machine.page';

const routes: Routes = [
  {
    path: '',
    component: TimeMachinePage
  },
  {
    path: 'detail',
    component: TimeMachineDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TimeMachinePageRoutingModule {}
