import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TimeMachinePageRoutingModule } from './time-machine-routing.module';

import { TimeMachinePage } from './time-machine.page';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ExpandableComponent } from '../../components/expandable/expandable.component';

import { OwlModule } from 'ngx-owl-carousel';
import { TimeMachineDetailPage } from './details/time-machine-detail.page';
import { SocialSharing } from '@awesome-cordova-plugins/social-sharing/ngx';
import { Screenshot } from '@ionic-native/screenshot/ngx';
import { PipesModule } from 'src/app/PipesModule.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TimeMachinePageRoutingModule,
    FontAwesomeModule,
    OwlModule,
    PipesModule,
  ],
  declarations: [TimeMachinePage, ExpandableComponent, TimeMachineDetailPage],
  providers: [SocialSharing, Screenshot],
})
export class TimeMachinePageModule {}
