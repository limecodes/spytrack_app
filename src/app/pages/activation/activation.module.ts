import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ActivationPageRoutingModule } from './activation-routing.module';

import { ActivationPage } from './activation.page';
import { LiveTrackingPage } from '../live-tracking/live-tracking.page';
import { GetLocationUrlService } from '../live-tracking/get-location-url.service';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    ActivationPageRoutingModule
  ],
  declarations: [ActivationPage],
  providers: [GetLocationUrlService]
})
export class ActivationPageModule {}
