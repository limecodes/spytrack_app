import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { IonSlides, ToastController } from '@ionic/angular';
import { GlobalService } from 'src/app/globals.service';
import { RestService } from 'src/app/rest.service';
//import { LiveTrackingPage } from '../live-tracking/live-tracking.page';

@Component({
  selector: 'app-activation',
  templateUrl: './activation.page.html',
  styleUrls: ['./activation.page.scss'],
})
export class ActivationPage implements OnInit {
  @ViewChild('activationSlides')  activationSlides: IonSlides;
  slideOpts = {
    initialSlide: 0,
    speed: 400,
    allowTouchMove: false
  };
  user = '';
  userDetails: any;
  isCheckedAdmin = false;
  isCheckedAPN = false;
  isCheckedIP= false;
  isCheckedDomain = false;
  isCheckedPulse = false;
  isCheckedUpload = false;
  isCheckedSleep = false;
  adminCheck;

  showSpinner = false;
  status = 'wait';
  tPurpose = 'personal'
  tImei = '';
  tName = '';
  tDescription = '';

  constructor(
    private _formBuilder: FormBuilder,
    private router: Router,
    public toastCtrl: ToastController,
    private global: GlobalService,
    private rest: RestService,
    //private live: LiveTrackingPage
    ) { }

  ngOnInit() {
    this.global.currentMessageSubscriberSummary.subscribe((data: any) => {
      if (data.isRefresh) {
        this.user = this.global.firstname;
        this.userDetails = this.global.userDetails;
      } else {
      }
    })
    console.log(this.userDetails)
    // setTimeout(()=>{
    //   this.swipeNext();
    // }, 5000);
    // for(let x = 0; x <= 7; x++) {
    //   setTimeout(()=>{
    //     if(x == 1) {
    //       this.isCheckedAdmin = true;
    //     } else if(x == 2) {
    //       this.isCheckedAPN = true;
    //     } else if(x == 3) {
    //       this.isCheckedIP = true;
    //     } else if(x == 4) {
    //       this.isCheckedDomain = true;
    //     } else if(x == 5) {
    //       this.isCheckedPulse = true;
    //     } else if(x == 6) {
    //       this.isCheckedUpload = true;
    //     } else if(x == 7) {
    //       this.isCheckedSleep = true;
    //       this.swipeNext();
    //     }
    // }, (x+1)*5000);
    // }
  }
  

  login() {
    // this.router.navigate(['/live-tracking']);
  }

  swipeNext() {
    this.activationSlides.slideNext();
  }

  swipePrev() {
    this.activationSlides.slidePrev();
  }

  swipeInit(index) {
    this.activationSlides.slideTo(index)
  }

  async openToast(message) {
    if(message == 'already activated'){
      const toast = await this.toastCtrl.create({  
        message: 'Error occured. Tracker has already been activated',
        position: 'bottom',
        duration: 4000,
        color: 'danger' 
      });  
      toast.present();  
    } else {
      const toast = await this.toastCtrl.create({  
        message: 'Error occured. Please try again later.',
        position: 'bottom',
        duration: 4000,
        color: 'danger' 
      });  
      toast.present();  
    }
    
  }

  activateTracker() {
    //this.live.displayMap = false;
    this.showSpinner = true;
    let dataToSend = {
      "imei": this.tImei,
      "tracker_name": this.tName,
      "tracking_purpose": this.tPurpose,
      "description": this.tDescription
    };
    console.log(dataToSend);
    this.swipeNext();
    this.rest.post(dataToSend, 'thorton/activate').subscribe((resp: any) => {
      console.log(resp)
      this.showSpinner = false;
      this.swipeNext();
    }, error => {
      this.showSpinner = false;
      this.status = 'failed'
      if(error.error.message == 'Tracker already activated!') {
        this.openToast('already activated');
      } else {
        this.openToast('error');
      }
    });
  }

  retryActivate() {
    this.showSpinner = true;
    let dataToSend = {
      "imei": this.tImei,
      "tracker_name": this.tName,
      "tracking_purpose": "personal",
    };
    console.log(dataToSend);
    this.rest.post(dataToSend, 'thorton/activate').subscribe((resp: any) => {
      console.log(resp)
      this.showSpinner = false;
      this.swipeNext();
    }, error => {
      console.log(error)
      this.showSpinner = false;
      this.status = 'failed'
      if(error.error.message == 'Tracker already activated!') {
        this.openToast('already activated');
      } else {
        this.openToast('error');
      }
    });
  }

}
