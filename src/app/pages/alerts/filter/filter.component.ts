/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/naming-convention */
import { Component, OnInit } from '@angular/core';
import { LoadingController, ModalController, ToastController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { GlobalService } from 'src/app/globals.service';
import { RestService } from 'src/app/rest.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
})
export class AlertFilterComponent implements OnInit {

  defaultAlertFilterSort = 'none';
  defaultAlertFilterType = 'none';
  defaultFilterTracker = 'any';
  defaultSort = 'none';

  userTrackers: any;
  dataLoad = false;
  alertFilterSubscription: Subscription;
  fromDate: string;
  toDate: string;
  gblLoading: any;
  constructor(
    public rest: RestService,
    public modalController: ModalController,
    private globalService: GlobalService,
    public toastCtrl: ToastController,
    public loadingController: LoadingController,
  ) { }

  ngOnInit() {}

  async closeModal() {
    const onClosedData: string = 'Wrapped Up!';
    await this.modalController.dismiss(onClosedData);
  }
  ionViewWillEnter(){

    this.getUserTracker();
    this.alertFilterSubscription = this.globalService.currectSelectedAlertsFilter.subscribe((currentFilter)=>{
      this.defaultAlertFilterSort = (currentFilter.sort_by && this.defaultSort) ? currentFilter.sort_by +  currentFilter.sort.charAt(0) : 'none';
      this.defaultAlertFilterType = currentFilter.type || 'none';
      this.defaultFilterTracker =  currentFilter.seletedTracker || 'any';
      this.defaultSort =  currentFilter.sort || 'none';
      this.fromDate = currentFilter.start_date || (new Date()).toISOString();
      this.toDate = currentFilter.end_date || (new Date()).toISOString();
    });
  }

  async getUserTracker() {
    this.gblLoading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
    });
    await this.gblLoading.present();
    this.rest.get('thorton/trackers/user', '', '').subscribe(async (resp: any) => {
      await this.gblLoading.dismiss();
      if(resp.length != 0) {
        this.userTrackers = resp
        console.log(this.userTrackers);
        this.dataLoad = true;
        this.gblLoading.dismiss();
      } else {
        this.dataLoad = true;
      }
    });
  }

  applyActivityFilters(){
    console.log(this.defaultAlertFilterSort , this.defaultAlertFilterType , this.defaultFilterTracker , this.fromDate , this.toDate);
    if(this.defaultAlertFilterSort !== "none"){
      this.defaultSort = this.defaultAlertFilterSort.charAt(this.defaultAlertFilterSort.length - 1) === 'a' ? 'ase' : 'desc';
      this.defaultAlertFilterSort =  this.defaultAlertFilterSort.slice(0,-1);
    }
    this.globalService.userChangedAlertsFilter({
      seletedTracker: this.defaultFilterTracker,
      type: this.defaultAlertFilterType,
      sort_by: this.defaultAlertFilterSort,
      start_date: this.fromDate,
      end_date: this.toDate,
      sort:  this.defaultSort,
    });
    this.closeModal();
  }

  resetFilter(){
    this.globalService.setAlertFilterAsDefault();
    this.closeModal();
  }

  ionViewDidLeave(){
    this.alertFilterSubscription?.unsubscribe();
  }
}
