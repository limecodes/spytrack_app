/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/prefer-for-of */
import { Component, OnInit, ViewChild } from '@angular/core';
import { LoadingController, ModalController } from '@ionic/angular';
import { OwlCarousel } from 'ngx-owl-carousel';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { GlobalService } from 'src/app/globals.service';
import { RestService } from 'src/app/rest.service';
import { AlertFilterComponent } from './filter/filter.component';

@Component({
  selector: 'app-alerts',
  templateUrl: './alerts.page.html',
  styleUrls: ['./alerts.page.scss'],
})
export class AlertsPage implements OnInit {
  @ViewChild('owlElement') owlElement: OwlCarousel;

  alertsData: any;
  loadingGbl: any;
  dateF: string;
  timeF: string;
  totalRecord: number;
  displayAlertData: any;
  recordFetchInReq: number;
  filteredData: any;
  isAllRecordLoaded: boolean;
  alertFilterSubscription: Subscription;
  alertFilter: any;
  selectedTrackerData: any;

  slideOptions = { items: 2, dots: false};
  constructor(
    public modalController: ModalController,
    private loadingController: LoadingController,
    private globalService: GlobalService,
    private rest: RestService,
  ) {
    this.dateF = 'EEE, MMM dd YYYY';
    this.timeF = "h:mm a"
    this.recordFetchInReq = 5;
    this.isAllRecordLoaded = false;
  }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.alertFilterSubscription = this.globalService.currectSelectedAlertsFilter.subscribe((newFilter)=>{
      this.alertFilter = newFilter;
      console.log('activity filter ===>' , this.alertFilter);
      this.getActivityData();
    });
  }

  async getActivityData(){
    this.loadingGbl = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...'
    });
    await this.loadingGbl.present();

    const sendData = Object.assign({},this.alertFilter);

    console.log(sendData);
    delete sendData.seletedTracker;
    if(this.alertFilter.seletedTracker !== 'any'){
      sendData.imei = this.alertFilter.seletedTracker.imei;
    }else{
      sendData.imei = 'any';
    }

    const startTime = new Date(sendData.start_date);
    const endTime = new Date(sendData.end_date);

    const startDate = startTime.getFullYear()  + '-' + (startTime.getMonth() + 1) + '-' + startTime.getDate();
    const endDate = endTime.getFullYear() + '-' + (endTime.getMonth() + 1) + '-' + endTime.getDate();

    sendData.start_date = startDate;
    sendData.end_date = endDate;
    this.rest.get('thorton/trackers/logs?' + new URLSearchParams(sendData).toString(), '' , '').pipe(take(1)).subscribe(async (resp: any) => {
     // const resp: any = this.getRecords();
      console.log('resp ====>', resp);
      if(resp.length !== 0) {
        for(let x = 0; x < resp.length; x++) {
          resp[x].tracker_icon_url = this.globalService.getMarkerLocation(resp[x].tracker_icon, resp[x].tracker_icon_color);
        }
       this.alertsData = resp;
       this.selectedTrackerData =  this.alertsData[0];
       this.renderData();
      }else{
        await this.loadingGbl.dismiss();
      }
    });
  }

  async renderData(){

    this.filteredData = this.selectedTrackerData.locations.slice(0);
    this.totalRecord = this.filteredData.length;
    this.displayAlertData = this.filteredData.slice(0,this.recordFetchInReq);
    if(this.filteredData.length < this.recordFetchInReq){
    this.isAllRecordLoaded = true;
    }else{
      this.isAllRecordLoaded = false;
    }
    console.log('selectedTrackerData ===>' , this.filteredData);
    console.log('total rec ===>' , this.totalRecord);
    if(this.owlElement){
      this.owlElement.reInit();
    }
    await this.loadingGbl.dismiss();

  }

  loadMoreData(){
    console.log('clicked',this.displayAlertData.length,this.totalRecord);
    if(this.displayAlertData.length < this.totalRecord){
      const newRecords = this.filteredData.slice(0).splice(this.displayAlertData.length-1, this.recordFetchInReq);
      this.displayAlertData.push(...newRecords);
      if(this.displayAlertData.length >= this.totalRecord){
        console.log('all record loaded!!');
        this.isAllRecordLoaded = true;
      }
    }else{
      console.log('all record loaded!!');
      this.isAllRecordLoaded = true;
    }
  }


  async fetchData(){
    this.loadingGbl = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...'
    });

    await this.loadingGbl.present();

    this.filteredData = this.selectedTrackerData.data.slice(0);
    this.totalRecord = this.filteredData.length;
    this.displayAlertData = this.filteredData.slice(0,this.recordFetchInReq);
    if(this.filteredData.length < this.recordFetchInReq){
    this.isAllRecordLoaded = true;
    }else{
      this.isAllRecordLoaded = false;
    }
    await this.loadingGbl.dismiss();
  }

  async changeSelectedTracker(index){
    this.loadingGbl = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...'
    });

    await this.loadingGbl.present();
    console.log("tracker changed index is ", index);
    this.selectedTrackerData =  this.alertsData[index];
    this.renderData();
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: AlertFilterComponent,
      cssClass: 'filter-modal'
    });
    return await modal.present();
  }

  ionViewDidLeave(){
    this.alertFilterSubscription?.unsubscribe();
  }
}
