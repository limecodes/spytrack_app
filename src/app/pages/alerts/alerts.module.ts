import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AlertsPageRoutingModule } from './alerts-routing.module';

import { OwlModule } from 'ngx-owl-carousel';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AlertsPage } from './alerts.page';
import { AlertFilterComponent } from './filter/filter.component';
import { PipesModule } from 'src/app/PipesModule.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FontAwesomeModule,
    AlertsPageRoutingModule,
    OwlModule,
    PipesModule
  ],
  declarations: [AlertsPage,AlertFilterComponent]
})
export class AlertsPageModule {}
