/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/prefer-for-of */
/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable quote-props */
/* eslint-disable no-trailing-spaces */
/* eslint-disable @typescript-eslint/quotes */
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, ModalController, PopoverController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { GlobalService } from 'src/app/globals.service';
import { RestService } from 'src/app/rest.service';
import { FilterComponent } from './filter/filter.component';

import { OwlCarousel } from 'ngx-owl-carousel/src/owl-carousel.component';

import { ActivitypopoverComponent } from '../../components/activitypopover/activitypopover.component'; 
@Component({
  selector: 'app-activities',
  templateUrl: './activities.page.html',
  styleUrls: ['./activities.page.scss'],
})
export class ActivitiesPage implements OnInit {

  @ViewChild('owlElement') owlElement: OwlCarousel;
  activitiesData: any;
  startAndStopActivityData: any;
  selectedTrackerData: any = {};

  loadingGbl: any;
  dateF: string;
  totalRecord: number;
  displayActivityData: any;
  recordFetchInReq: number;
  filteredData: any;
  isAllRecordLoaded: boolean;
  activityFilterSubscription: Subscription;
  activityFilter: any;
  slideOptions = { items: 2, dots: false}; 
  constructor(
    private router: Router,
    public modalController: ModalController,
    public rest: RestService,
    public loadingController: LoadingController,
    public globalService: GlobalService,
    public popoverCtrl: PopoverController
  ) { 
    this.dateF = "EEE, MMM dd";
    this.recordFetchInReq = 5;
    this.isAllRecordLoaded = false;
    
  
  }

  ngOnInit() {
  }

  ionViewWillEnter(){
   
    this.activityFilterSubscription = this.globalService.currectSelectedAvtivityFilter.subscribe((newFilter)=>{
      this.activityFilter = newFilter;
      console.log("activity filter ===>" , this.activityFilter);
      this.getActivityData();
    });
  }

  async getActivityData(){
    this.loadingGbl = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...'
    });

    await this.loadingGbl.present();
   
    
    const sendData = Object.assign({},this.activityFilter);
    delete sendData.seletedTracker;
    if(this.activityFilter.seletedTracker !== 'all'){
      sendData.imei = this.activityFilter.seletedTracker.imei;
    }else{
      delete sendData.imei;
    }

    const startTime = new Date(sendData.start_date);
    const endTime = new Date(sendData.end_date);

    const startDate = startTime.getFullYear() + '-' + (startTime.getMonth() + 1) + '-' + startTime.getDate();
    const endDate = endTime.getFullYear() + '-' + (endTime.getMonth() + 1) + '-' + endTime.getDate();

    sendData.start_date = startDate;
    sendData.end_date = endDate;
    this.rest.get('thorton/trackers/activities?' + new URLSearchParams(sendData).toString(), '' , '').pipe(take(1)).subscribe(async (resp: any) => {
      console.log("resp ====>", resp);
      if(resp.length !== 0) {
        for(let x = 0; x < resp.length; x++) {
          resp[x].tracker_icon_url = this.globalService.getMarkerLocation(resp[x].tracker_icon, resp[x].tracker_icon_color);
        }
       this.activitiesData = resp;
       this.selectedTrackerData =  this.activitiesData[0];
       this.renderData();
      }else{
        await this.loadingGbl.dismiss();
      }
    });
  }

  async renderData(){
   
    this.filteredData = this.selectedTrackerData.data.slice(0);
    this.totalRecord = this.filteredData.length;
    this.displayActivityData = this.filteredData.slice(0,this.recordFetchInReq);
    if(this.filteredData.length < this.recordFetchInReq){
    this.isAllRecordLoaded = true;
    }else{
      this.isAllRecordLoaded = false;
    }
    console.log("selectedTrackerData ===>" , this.filteredData);
    console.log("total rec ===>" , this.totalRecord);
    if(this.owlElement){
      this.owlElement.reInit();
    }
    await this.loadingGbl.dismiss();

  }

  async loadMoreData(){
    // this.loadingGbl = await this.loadingController.create({
    //   cssClass: 'my-custom-class',
    //   message: 'Please wait...'
    // });

    // await this.loadingGbl.present();

    if(this.displayActivityData.length < this.totalRecord){
      const newRecords = this.filteredData.slice(0).splice(this.displayActivityData.length-1, this.recordFetchInReq);
      this.displayActivityData.push(...newRecords);
      if(this.displayActivityData.length >= this.totalRecord){
        console.log("all record loaded!!");
        this.isAllRecordLoaded = true;
      }
    }else{
      console.log("all record loaded!!");
      this.isAllRecordLoaded = true;
    }
    //setTimeout(async ()=>{
    //  await this.loadingGbl.dismiss();
    //},200);
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: FilterComponent,
      cssClass: 'filter-modal'
    });
    return await modal.present();
  }

  activityClicked(activityData){
    console.log(activityData);
    const requiredData = {
      'tracker_icon': this.selectedTrackerData.tracker_icon,
      'tracker_icon_color': this.selectedTrackerData.tracker_icon_color,
      'tracker_name': this.selectedTrackerData.tracker_name,
      ...activityData
    };
    this.globalService.userSelectedNewActivity(requiredData);
    this.router.navigate(['/activities/detail']);
  }

  async changeSelectedTracker(index){
    this.loadingGbl = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...'
    });

    await this.loadingGbl.present();
    console.log("tracker changed index is ", index);
    this.selectedTrackerData =  this.activitiesData[index];
    this.renderData();
  }

  async notifications(ev: any,activityData: any) {  
    const popover = await this.popoverCtrl.create({  
        component: ActivitypopoverComponent,  
        event: ev,  
        animated: true,  
        showBackdrop: true,
        cssClass: 'pop-over-style',
        componentProps: { data : activityData }   
    });  
    return await popover.present();  
  }

  ionViewDidLeave(){
    this.activityFilterSubscription?.unsubscribe();
  }
}
