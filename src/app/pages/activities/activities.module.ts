import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ActivitiesPageRoutingModule } from './activities-routing.module';

import { ActivitiesPage } from './activities.page';
import { FilterComponent } from './filter/filter.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ActivitiesDetailPage } from './details/activities-detail.page';
import { OwlModule } from 'ngx-owl-carousel';
import { ActivitypopoverComponent } from 'src/app/components/activitypopover/activitypopover.component';
import { PipesModule } from 'src/app/PipesModule.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ActivitiesPageRoutingModule,
    FontAwesomeModule,
    OwlModule,
    PipesModule,
  ],
  declarations: [ActivitiesPage, FilterComponent, ActivitiesDetailPage,ActivitypopoverComponent]
})
export class ActivitiesPageModule {}
