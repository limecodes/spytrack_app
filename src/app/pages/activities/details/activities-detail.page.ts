/* eslint-disable max-len */
/* eslint-disable eqeqeq */
/* eslint-disable no-var */
/* eslint-disable prefer-const */
/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable quote-props */
/* eslint-disable no-trailing-spaces */
/* eslint-disable @typescript-eslint/quotes */
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { LoadingController, Platform, ToastController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { GlobalService } from 'src/app/globals.service';
import { PopoverController } from '@ionic/angular';  
import { ActivitypopoverComponent } from '../../../components/activitypopover/activitypopover.component';  
import { Router } from '@angular/router';

@Component({
  selector: 'app-activities-detail',
  templateUrl: './activities-detail.page.html',
  styleUrls: ['./activities-detail.page.scss'],
})
export class ActivitiesDetailPage implements OnInit{

  activitySubscription: Subscription;
  activityData: any;
  dateF: string;
  activityDetailMapRef: any;
  mapHeight: string;
  @ViewChild('mapDiv', {read: ElementRef, static: false}) mapRef: ElementRef;
  isDisplay: boolean;
  constructor(
    public loadingController: LoadingController,
    private globalService: GlobalService,
    public popoverCtrl: PopoverController,
    private platform: Platform,
    public toastCtrl: ToastController,
    private route: Router
  ) { 
    this.dateF = "EEE, MMM dd";
    this.mapHeight = "400px";
  }

  ngOnInit() {
  }

  async notifications(ev: any) {  
    const popover = await this.popoverCtrl.create({  
        component: ActivitypopoverComponent,  
        event: ev,  
        animated: true,  
        showBackdrop: true,
        cssClass: 'pop-over-style',
        componentProps: { data : this.activityData }   
    });  
    return await popover.present();  
  }  

  ionViewWillEnter(){
   console.log("called");
   this.isDisplay = false;
   this.activitySubscription = this.globalService.selectedActivity.pipe(take(1)).subscribe((activity: any)=>{
     console.log("activity data", activity);
     this.activityData = activity;
    
    setTimeout(()=>{
      this.drawMap();
    },0);
  
   });

  }

  async drawMap(selectedLocation?: any){
    if(this.activityData?.locations?.length > 0){
    const totalHeight = this.platform.height();
    const headerHeight = document?.getElementById('header-a-d')?.offsetHeight || 0;
    const detailsHeight = document?.getElementById('detail-card-a-d')?.offsetHeight || 0;

    this.mapHeight = `${totalHeight - headerHeight - detailsHeight - 20}px`;
    console.log(totalHeight ,headerHeight ,detailsHeight, this.mapHeight);
    const currentTrackerLocation = this.activityData.locations;
    let latu =  currentTrackerLocation[0].latitude;
    let long =  currentTrackerLocation[0].longitude;
    this.activityData.tracker_icon_url = this.globalService.getMarkerLocation(this.activityData.tracker_icon, this.activityData.tracker_icon_color);
    this.activityDetailMapRef = null;
    //this.flightPath = [];
    console.log(this.activityData);
    this.activityDetailMapRef = new google.maps.Map(document.getElementById("activityDetailMapDiv") as HTMLElement, {
      mapTypeId: google.maps.MapTypeId.ROADMAP,//[(this.userSettings.map_style).toUpperCase()],
      mapTypeControl: false,
      streetViewControl: false,
      keyboardShortcuts: false,      
    });
    let bounds = new google.maps.LatLngBounds();
    var poly = [];

    let index = 0;
    for(let location of currentTrackerLocation){
      const latLong = {lat: parseFloat(location.latitude), lng : parseFloat(location.longitude)};   
      if(index == 0){
        new google.maps.Marker({
          position: latLong,
          map: this.activityDetailMapRef,
          title: location.formatted_address,
          clickable: true,
          animation:google.maps.Animation.DROP,
          icon:{
            // url: "../assets/markers/location-arrow/location-arrow-229954.png", //SVG path of awesomefont marker
            // fillOpacity: 1,
            // strokeWeight: 0,
            //  size: new google.maps.Size(30, 30),
            //  scaledSize: new google.maps.Size(30, 30),
            //  anchor: new google.maps.Point(0, 32)

            path: google.maps.SymbolPath.CIRCLE,
            scale: 4,
            strokeColor: "#04dd04",
            strokeWeight:3,
            fillColor: "white",
            fillOpacity:1
          }
          
        });
      }else if(currentTrackerLocation.length-1 == index){
        new google.maps.Marker({
          position: latLong,
          map: this.activityDetailMapRef,
          title: location.formatted_address,
          clickable: true,
          animation:google.maps.Animation.DROP,
          icon:{
            // url: "../assets/markers/location-arrow/location-arrow-229954.png", //SVG path of awesomefont marker
            // fillOpacity: 1,
            // strokeWeight: 0,
            //  size: new google.maps.Size(30, 30),
            //  scaledSize: new google.maps.Size(30, 30),
            //  anchor: new google.maps.Point(0, 32)
            path: google.maps.SymbolPath.CIRCLE,
            scale: 4,
            strokeColor: "#b72a2a",
            strokeWeight:3,
            fillColor: "white",
            fillOpacity:1
          }
          
        });
      }else{
        new google.maps.Marker({
          position: latLong,
          map: this.activityDetailMapRef,
          title: location.formatted_address,
          clickable: true,
          animation:google.maps.Animation.DROP,
          icon:{
            // url: "../assets/markers/location-arrow/location-arrow.png", //SVG path of awesomefont marker
            // fillOpacity: 1,
            // strokeWeight: 0,
            //  size: new google.maps.Size(30, 30),
            //  scaledSize: new google.maps.Size(30, 30),
            //  anchor: new google.maps.Point(0, 32)
            path: google.maps.SymbolPath.CIRCLE,
            scale: 4,
            strokeColor: "#454745",
            strokeWeight:3,
            fillColor: "white",
            fillOpacity:0.7
          }
          
        });
      }
      index++;
    }

    let color = this.activityData.tracker_icon_color;
    let isboundExtended = false;
    for(var i = 0 ; i < currentTrackerLocation.length ; i=i+1 ){
      let j = i+1;
      
      if(currentTrackerLocation[i] && currentTrackerLocation[j]){
        var pos = new google.maps.LatLng(currentTrackerLocation[i].latitude,currentTrackerLocation[i].longitude);
        var pos2 = new google.maps.LatLng(currentTrackerLocation[j].latitude,currentTrackerLocation[j].longitude);
        //bounds.extend(pos);
        bounds.extend(pos2);
        isboundExtended = true;
        poly.push(pos);
       new google.maps.Polyline({
          path: [pos,pos2],
          geodesic: true,
          strokeColor: color,
          strokeOpacity: 1.0,
          strokeWeight: 8,
          map: this.activityDetailMapRef,
        });
        // new google.maps.Polyline({
        //   path: [pos,pos2],
        //   geodesic: true,
        //   strokeColor: color,
        //   strokeOpacity: 1.0,
        //   strokeWeight: 5,
        //   map: this.activityDetailMapRef,
        // });
        
      }
    }
    console.log("bounds => ", bounds);
    if(!isboundExtended && currentTrackerLocation.length > 0){
      bounds.extend(new google.maps.LatLng(currentTrackerLocation[0].latitude,currentTrackerLocation[0].longitude));
    }
    try{
    setTimeout(async ()=>{
      //this.displayMap = true;
      this.activityDetailMapRef.fitBounds(bounds);
      this.activityDetailMapRef.panToBounds(bounds);
      this.activityDetailMapRef.panTo({ 
          lat: parseFloat(latu) , 
          lng: parseFloat(long) 
        });
        //await this.loadingGbl.dismiss();
        this.isDisplay = true;
    },100);
    }catch(ex){
      this.isDisplay = true;
    }
  }else{
    this.toastCtrl.create({  
      message: 'Error occured. something went wrong!!',
      position: 'bottom',
      duration: 4000,
      color: 'danger' 
    }).then((toast)=>{  
      toast.present();   
      this.route.navigate(['/activities']);
     });  
   return '';
  }
}


  ionViewDidLeave(){
    if(this.activitySubscription){
      this.activitySubscription.unsubscribe();
    }
  }

}
