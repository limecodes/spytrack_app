import { Component, OnInit } from '@angular/core';
import {
  LoadingController,
  ModalController,
  ToastController,
} from '@ionic/angular';
import { GlobalService } from 'src/app/globals.service';
import { RestService } from 'src/app/rest.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
})
export class FilterComponent implements OnInit {
  defaultFilterType: string;
  defaultFilterTracker: string;
  userTrackers: any;
  dataLoad = false;
  gblLoading: any;
  fromDate: string;
  toDate: string;
  constructor(
    private globalService: GlobalService,
    public rest: RestService,
    public modalController: ModalController,
    public toastCtrl: ToastController,
    public loadingController: LoadingController
  ) {
    this.defaultFilterType = 'all';
    this.defaultFilterTracker = 'all';
    this.fromDate = new Date().toISOString();
    this.toDate = new Date().toISOString();
  }

  ngOnInit() {}

  ionViewDidEnter() {
    this.getUserTracker();
    this.presentLoading();
  }

  async presentLoading() {
    this.gblLoading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
    });
    await this.gblLoading.present();
    //  const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }

  async closeModal() {
    const onClosedData: string = 'Wrapped Up!';
    await this.modalController.dismiss(onClosedData);
  }

  getUserTracker() {
    this.rest.get('thorton/trackers/user', '', '').subscribe((resp: any) => {
      console.log('all user tracker', resp);
      if (resp.length !== 0) {
        this.userTrackers = resp;
        console.log(this.userTrackers);
        this.dataLoad = true;
        this.gblLoading.dismiss();
      } else {
        this.dataLoad = true;
        this.gblLoading.dismiss();
      }
    });
  }

  resetFilter() {
    this.globalService.setActivityFilterDefault();
    this.closeModal();
  }

  applyActivityFilters() {
    this.globalService.userChangedActivityFilter({
      seletedTracker: this.defaultFilterTracker,
      activity: this.defaultFilterType,
      start_date: this.fromDate,
      end_date: this.toDate,
    });
    this.closeModal();
  }
}
