import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LiveTrackingPageRoutingModule } from './live-tracking-routing.module';

import { LiveTrackingPage } from './live-tracking.page';
import { FilterComponent } from './filter/filter.component';
import { FontAwesomeModule, FaIconLibrary  } from '@fortawesome/angular-fontawesome';

import { faCoffee, fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { GetLocationUrlService } from './get-location-url.service';
import { OwlModule } from 'ngx-owl-carousel';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LiveTrackingPageRoutingModule,
    FontAwesomeModule,
    OwlModule
  ],
  declarations: [
    LiveTrackingPage,
    FilterComponent
  ],
  providers:[GetLocationUrlService]
})
export class LiveTrackingPageModule {
  constructor(library: FaIconLibrary) { 
		library.addIconPacks(fas, fab, far);
	}
}
