/* eslint-disable @typescript-eslint/no-inferrable-types */
/* eslint-disable object-shorthand */
/* eslint-disable @typescript-eslint/no-shadow */
/* eslint-disable @typescript-eslint/no-unused-expressions */
/* eslint-disable curly */
/* eslint-disable @typescript-eslint/dot-notation */
/* eslint-disable quote-props */
/* eslint-disable max-len */
/* eslint-disable prefer-const */
/* eslint-disable @typescript-eslint/prefer-for-of */
/* eslint-disable @typescript-eslint/semi */
/* eslint-disable eqeqeq */
/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable no-trailing-spaces */
/* eslint-disable @typescript-eslint/quotes */
/* eslint-disable @typescript-eslint/member-ordering */
import { GetLocationUrlService } from './get-location-url.service';
import { RestService } from './../../rest.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ViewChild, ElementRef } from '@angular/core';
import { ModalController, Platform, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

import { GlobalService } from 'src/app/globals.service';

import { FilterComponent } from './filter/filter.component';
import {} from 'googlemaps';
import { HttpClient } from '@angular/common/http';
import { Subscription, timer } from 'rxjs';
import { take } from 'rxjs/operators';
import { OwlCarousel } from 'ngx-owl-carousel';

declare let google: any;
declare const Pusher: any;
@Component({
  selector: 'app-live-tracking',
  templateUrl: './live-tracking.page.html',
  styleUrls: ['./live-tracking.page.scss'],
})
export class LiveTrackingPage implements OnInit, OnDestroy {
  @ViewChild('owlElement') owlElement: OwlCarousel;
  trackerHistorySetting = 'noHistory';
  hasTracker = false;
  dataLoad = false;
  hasTrackerActivity = false;
  trip: any = [];
  slideOptions = { items: 2, dots: false };
  months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];
  // MAP variables
  displayMap = false;
  map: google.maps.Map;
  @ViewChild('map', { read: ElementRef, static: false }) mapRef: ElementRef;
  defaultMapLocation = {
    last_location: { latitude: 7.196276, longitude: 125.461807 },
  }; // LAT LONG POINTS TO DAVAO, PHILIPPINES (Subject to change)
  mapStyle = '0'; // map style by default is set to 0 (ROADMAP style)
  mapOptions: any;
  markers = (google.maps.Markers = []);
  flightPath: google.maps.Polyline;

  globalData: string;
  items: any;
  startTracking: boolean = false;
  dataFilterHours: any = 'Now';
  userSettings: any;

  resolvedFlag = true;
  selectedTrackers: any = [];
  trackerDetails: any;
  trackerImeiSelected: any;

  //timerSubscription: Subscription;

  liveInfoInterval: any;
  selectedLiveTrack: any;
  globalCurrentMessageSubscription: Subscription;

  channelNewTrip: any;
  channelStop: any;
  channelSpeed: any;
  channelNotification: any;

  pusher: any;
  isPageFirstTimeReloaded: boolean;
  currentLocationLatandLng: any;
  liveLocations: any;
  platformHeight: number;
  constructor(
    private http: HttpClient,
    public modalController: ModalController,
    private rest: RestService,
    private router: Router,
    public global: GlobalService,
    public toastCtrl: ToastController,
    public getMarkerLocation: GetLocationUrlService
  ) {
    if (localStorage.getItem('trackerHistorySetting')) {
      this.trackerHistorySetting = localStorage.getItem(
        'trackerHistorySetting'
      );
    }
    this.globalCurrentMessageSubscription =
      this.global.currentMessage.subscribe(
        (message) => (this.globalData = message)
      );
    this.pusher = new Pusher('b478d55d6148b087811a', {
      cluster: 'ap1',
    });
    this.liveLocations = [];
  }
  ngOnInit() {
    this.displayMap = false;
  }

  ngOnDestroy() {
    //this.timerSubscription.unsubscribe();
    clearInterval(this.liveInfoInterval);
    this.globalCurrentMessageSubscription.unsubscribe();
  }

  ionViewDidEnter() {
    this.initMap();
    this.getUserDetails();
    this.getUserSettings();
    this.getUserTracker();
    this.socketSubscriptionForActivities('asd');
    //this.loadAssetIcons();
    this.isPageFirstTimeReloaded = true;
  }

  loadAssetIcons() {
    if (localStorage) {
      if (!localStorage.getItem('firstLoad')) {
        localStorage['firstLoad'] = true;
        window.location.reload();
      } else localStorage.removeItem('firstLoad');
    }
  }

  ionViewWillEnter() {
    this.displayMap = false;
    this.dataLoad = false;
  }

  initMap() {
    this.map = new google.maps.Map(
      document.getElementById('map') as HTMLElement,
      {
        center: { lat: -34.397, lng: 150.644 },
        zoom: 8,
      }
    );
  }

  reInitMapPolyLine(trackers: any) {
    if (trackers.last_location.length == 0) {
      this.noActivityFoundToast();
    } else {
      let location = new google.maps.LatLng(
        Number(trackers.last_location[0].latitude),
        Number(trackers.last_location[0].longitude)
      );
      this.map = new google.maps.Map(
        document.getElementById('map') as HTMLElement,
        {
          center: location,
          zoom: 16,
        }
      );
    }
  }

  getUserSettings() {
    this.rest
      .get('thorton/user/settings', '', '')
      .pipe(take(1))
      .subscribe((resp: any) => {
        console.log(resp);
        this.userSettings = resp;
        localStorage.setItem('mapStyle', resp.map_style);
        localStorage.setItem('userSettings', JSON.stringify(resp));
      });
  }

  getUserDetails() {
    this.rest
      .get('user', '', '')
      .pipe(take(1))
      .subscribe((resp: any) => {
        this.global.firstname = resp.firstname;
        this.global.userDetails = resp;
        this.global.notifySummary({ isRefresh: true });
      });
  }

  getUserTracker() {
    this.rest
      .get('thorton/trackers/user', '', '')
      .pipe(take(1))
      .subscribe((resp: any) => {
        if (resp.length == 0) {
          this.hasTracker = false;
          setTimeout(() => {
            this.initMap();
            this.dataLoad = true;
            this.displayMap = true;
          }, 3000);
        } else {
          this.setDefaultTracker(resp).then((ret) => {
            if (resp.length != 0) {
              this.hasTracker = true;
              this.selectedTrackers = JSON.parse(
                localStorage.getItem('selectedUserTrackers')
              );
              for (const st of this.selectedTrackers) {
                st.tracker_icon_url = this.global.getMarkerLocation(
                  st.tracker_icon,
                  st.tracker_icon_color
                );
              }
              if (ret) {
                let trackerLocations = [];
                if (this.trackerHistorySetting == 'noHistory') {
                  for (let x = 0; x < this.selectedTrackers.length; x++) {
                    this.liveTrack(this.selectedTrackers[x].imei);
                    // if(this.selectedTrackers[x].imei) {
                    //   this.setMarkerForMap(this.selectedTrackers[x], 'single');
                    // } else if(this.selectedTrackers[x].trackerID) {
                    //   this.setMarkerForMap(this.selectedTrackers[x], 'single');
                    //   // this.getTrackerLastLocation(selectedTrackers[x].trackerID);
                    // }
                  }
                  // this.setMarkerForMap(trackerLocations);
                } else {
                  // for(let x = 0; x < selectedTrackers.length; x++) {
                  //   this.getTrackerLocations(selectedTrackers[x], localStorage.getItem('trackerActivityFilter'))
                  // }
                }
              } else {
                // this.setDefaultTracker(resp);
              }
            } else {
              this.hasTracker = false;
              setTimeout(() => {
                this.initMap();
                this.dataLoad = true;
              }, 3000);
            }
          });
        }
      });
  }

  liveTrack(imei) {
    // this.timerSubscription = timer(0, 10000).pipe(
    //   map(() => {
    //     this.getTrackerLastLocation(imei); // load data contains the http request
    //   })
    // )
    if (this.liveInfoInterval) {
      clearInterval(this.liveInfoInterval);
    }
    console.log(this.liveInfoInterval);
    this.getTrackerLastLocation(imei);
    this.liveInfoInterval = setInterval(() => {
      this.getTrackerLastLocation(imei);
    }, 10000);
    this.socketSubscriptionForActivities(imei);
  }

  async setDefaultTracker(trackerList) {
    // index 0 in trackerlist is always the selected tracker (default or not)
    if (trackerList.length == 0) {
      return false;
    } else {
      let trackerToSave: any = [];
      for (let x = 0; x < trackerList.length; x++) {
        let saveData = {
          created_at: trackerList[x].created_at,
          imei: trackerList[x].imei,
          name: trackerList[x].name,
          description: trackerList[x].description,
          receive_notif: trackerList[x].receive_notif,
          track_id: trackerList[x].track_id,
          trackerID: trackerList[x].trackerID,
          tracker_icon: trackerList[x].tracker_icon,
          tracker_name: trackerList[x].tracker_name,
          tracking_purpose: trackerList[x].tracking_purpose,
          updated_at: trackerList[x].updated_at,
          user_id_creator: trackerList[x].user_id_creator,
          tracker_icon_color: trackerList[x].tracker_icon_color,
          last_location: trackerList[x].last_location,
          city: trackerList[x].city,
          complete_address: trackerList[x].complete_address,
          country: trackerList[x].country,
          state: trackerList[x].state,
          battery_level: trackerList[x].battery_level,
        };
        if (x == 0) {
          saveData['isChecked'] = true;
          trackerToSave.push(saveData);
        } else {
          saveData['isChecked'] = false;
          trackerToSave.push(saveData);
        }
      }
      localStorage.setItem('userTrackers', JSON.stringify(trackerToSave));
      let tempArr = [];
      tempArr.push(trackerToSave[0]);
      localStorage.setItem('selectedUserTrackers', JSON.stringify(tempArr));
      this.trackerImeiSelected = tempArr[0].imei;
      this.showTrackerDetails(tempArr[0]);
      // this.timerSubscription = timer(0, 10000).pipe(
      //   map(() => {
      //     this.getTrackerLastLocation(this.trackerImeiSelected); // load data contains the http request
      //   })
      // ).subscribe();
      return true;
    }
  }

  getTrackerLastLocation(imei) {
    this.rest
      .get('thorton/tracker/last/location/' + imei, '', '')
      .pipe(take(1))
      .subscribe((resp: any) => {
        this.setMapOnAll(null);
        this.markers = [];
        this.setMarkerForLiveMap(resp[0]);
        // if(resp.length == 0) {
        //   setTimeout(() =>{
        //     this.hasTrackerActivity = false;
        //     this.showMap(this.defaultMapLocation);
        //     this.dataLoad = true;
        //   }, 3000);
        // } else {
        //   setTimeout(() =>{
        //     this.hasTrackerActivity = true;
        //     this.showMap(resp[0]);
        //     this.dataLoad = true;
        //   }, 3000);
        // }
      });
  }

  setMarkerForLiveMap(trackers: any) {
    if (trackers.last_location.length == 0) {
      this.displayMap = true;
      this.dataLoad = true;
      this.hasTrackerActivity = false;
      this.initMap();
    } else {
      let iconR = {
        url: this.getMarkerLocation.getMarkerLocation(
          trackers.tracker_icon,
          trackers.tracker_icon_color
        ), // url
        scaledSize: new google.maps.Size(40, 40), // scaled size
        origin: new google.maps.Point(0, 0), // origin
        anchor: new google.maps.Point(0, 0), // anchor
      };
      let location = new google.maps.LatLng(
        Number(trackers.last_location[0].latitude),
        Number(trackers.last_location[0].longitude)
      );
      this.currentLocationLatandLng = location;
      this.map.setOptions(
        this.getMapOptions(location, this.isPageFirstTimeReloaded)
      );
      this.isPageFirstTimeReloaded = false;

      let marker = new google.maps.Marker({
        position: {
          lat: Number(trackers.last_location[0].latitude),
          lng: Number(trackers.last_location[0].longitude),
        },
        map: this.map,
        title: trackers.tracker_name,
        icon: iconR,
      });
      this.markers.push(marker);
      const infowindow = new google.maps.InfoWindow({
        content: this.infoWindowContent(trackers),
      });
      google.maps.event.addListener(marker, 'click', function () {
        infowindow.open({
          anchor: marker,
          map: this.map,
          shouldFocus: false,
        });
      });
      this.showTrackerDetails(trackers);

      if (!this.liveLocations[trackers.imei]) {
        this.liveLocations[trackers.imei] = [];
        this.liveLocations[trackers.imei].push({
          lat: Number(trackers.last_location[0].latitude),
          lng: Number(trackers.last_location[0].longitude),
        });
        console.log('new location founded!!', this.liveLocations);
      } else if (
        this.liveLocations[trackers.imei].length > 0 &&
        (this.liveLocations[trackers.imei][
          this.liveLocations[trackers.imei].length - 1
        ].lat != marker.position.lat() ||
          this.liveLocations[trackers.imei][
            this.liveLocations[trackers.imei].length - 1
          ].lng != marker.position.lng())
      ) {
        this.liveLocations[trackers.imei].push({
          lat: Number(trackers.last_location[0].latitude),
          lng: Number(trackers.last_location[0].longitude),
        });
        console.log(
          'new location founded!!',
          this.liveLocations,
          this.liveLocations[trackers.imei]
        );
        if (this.liveLocations[trackers.imei].length > 1) {
          this.drawPath(trackers.imei);
        }
      }

      this.displayMap = true;
      this.dataLoad = true;
      this.hasTrackerActivity = true;
    }
  }

  setMarkerForMap(trackers: any, type) {
    if (type == 'single') {
      if (this.liveInfoInterval) {
        clearInterval(this.liveInfoInterval);
      }
      if (trackers.last_location.length == 0) {
        this.displayMap = true;
        this.dataLoad = true;
        this.hasTrackerActivity = false;
        this.initMap();
      } else {
        let iconR = {
          url: this.getMarkerLocation.getMarkerLocation(
            trackers.tracker_icon,
            trackers.tracker_icon_color
          ), // url
          scaledSize: new google.maps.Size(40, 40), // scaled size
          origin: new google.maps.Point(0, 0), // origin
          anchor: new google.maps.Point(0, 0), // anchor
        };
        let location = new google.maps.LatLng(
          Number(trackers.last_location[0].latitude),
          Number(trackers.last_location[0].longitude)
        );
        this.map.setOptions(this.getMapOptions(location));
        let marker = new google.maps.Marker({
          position: {
            lat: Number(trackers.last_location[0].latitude),
            lng: Number(trackers.last_location[0].longitude),
          },
          map: this.map,
          title: trackers.tracker_name,
          icon: iconR,
        });
        this.markers.push(marker);
        const infowindow = new google.maps.InfoWindow({
          content: this.infoWindowContent(trackers),
        });
        google.maps.event.addListener(marker, 'click', function () {
          infowindow.open({
            anchor: marker,
            map: this.map,
            shouldFocus: false,
          });
        });
        this.showTrackerDetails(trackers);
        if (this.liveInfoInterval) {
          clearInterval(this.liveInfoInterval);
          this.liveTrack(trackers.imei);
          this.selectedLiveTrack = trackers;
        } else {
          this.liveTrack(trackers.imei);
          this.selectedLiveTrack = trackers;
        }
        // this.displayMap = true;
        // this.dataLoad = true;
        // this.hasTrackerActivity = true;
      }
    } else {
      for (let x = 0; x < trackers.length; x++) {
        let iconR = {
          url: this.getMarkerLocation.getMarkerLocation(
            trackers[x].tracker_icon,
            trackers[x].tracker_icon_color
          ), // url
          scaledSize: new google.maps.Size(40, 40), // scaled size
          origin: new google.maps.Point(0, 0), // origin
          anchor: new google.maps.Point(0, 0), // anchor
        };
        let location = new google.maps.LatLng(
          Number(trackers[x].last_location[0].latitude),
          Number(trackers[x].last_location[0].longitude)
        );

        this.map.setOptions(this.getMapOptions(location));
        let marker = new google.maps.Marker({
          position: {
            lat: Number(trackers[x].last_location[0].latitude),
            lng: Number(trackers[x].last_location[0].longitude),
          },
          map: this.map,
          title: trackers[x].tracker_name,
          icon: iconR,
        });
        this.markers.push(marker);
        const infowindow = new google.maps.InfoWindow({
          content: this.infoWindowContent(trackers[x]),
        });
        google.maps.event.addListener(marker, 'click', function () {
          infowindow.open({
            anchor: marker,
            map: this.map,
            shouldFocus: false,
          });
        });
        this.showTrackerDetails(trackers[x]);
        if (x == trackers.length - 1) {
          if (this.liveInfoInterval) {
            clearInterval(this.liveInfoInterval);
            this.liveTrack(trackers[x].imei);
            this.selectedLiveTrack = trackers;
          } else {
            this.liveTrack(trackers[x].imei);
            this.selectedLiveTrack = trackers;
          }
        }
      }
      this.displayMap = true;
      this.dataLoad = true;
      this.hasTrackerActivity = true;
    }
  }

  getTrackerLocations(tracker, filterBy) {
    let dataToSend = {
      imei: String(tracker.imei),
    };

    if (filterBy == 'last hour') {
      dataToSend['recent'] = 'last hour';
    } else if (filterBy == '8 hours') {
      dataToSend['recent'] = '8 hours';
    } else if (filterBy == '12 hours') {
      dataToSend['recent'] = '12 hours';
    } else if (filterBy == '24 hours') {
      dataToSend['recent'] = '24 hours';
    } else if (filterBy == 'today') {
      dataToSend['recent'] = 'today';
    } else if (filterBy == 'all') {
      dataToSend['recent'] = 'all';
    }
    this.rest
      .post(dataToSend, 'thorton/trackerData/recent')
      .subscribe((resp: any) => {
        this.hasTrackerActivity = true;
        this.trip = resp;
        this.trip['locations'] = this.removeIncorrectLocations(resp.locations);
        this.showMapPolyLines(this.trip);
        this.dataLoad = true;
      });
  }

  // eslint-disable-next-line @typescript-eslint/type-annotation-spacing
  showMapPolyLines(trip: any) {
    let flightPlanCoordinates = [];
    let path = [];
    let checkpoint = null;
    let icon = {
      url: '../assets/circle-solid.png', // url
      scaledSize: new google.maps.Size(10, 10), // scaled size
      origin: new google.maps.Point(0, 0), // origin
      anchor: new google.maps.Point(0, 0), // anchor
    };
    for (let x = 0; x < trip.locations.length - 1; x++) {
      flightPlanCoordinates.push({
        lat: Number(trip.locations[x].latitude),
        lng: Number(trip.locations[x].longitude),
      });
      if (x + 1 >= trip.locations.length) {
      } else {
        let source = {
          lat: Number(trip.locations[x].latitude),
          lng: Number(trip.locations[x].longitude),
        };
        if (x <= 50) checkpoint = source;
        let destination = {
          lat: Number(trip.locations[x + 1].latitude),
          lng: Number(trip.locations[x + 1].longitude),
        };

        let distance =
          Number(
            this.calcCrow(
              checkpoint.lat,
              checkpoint.lng,
              source.lat,
              source.lng
            ).toFixed(5)
          ) * 1000;
        if (distance < 1) {
          // continue;
        } else {
          checkpoint = source;
        }
        let marker = new google.maps.Marker({
          position: {
            lat: Number(source.lat),
            lng: Number(source.lng),
          },
          map: this.map,
          title: trip.tracker_name,
          icon: icon,
        });
        this.markers.push(marker);
        const infowindow = new google.maps.InfoWindow({
          content: this.infoWindowContentMultipleMarker(
            trip,
            trip.locations[x]
          ),
        });
        google.maps.event.addListener(marker, 'click', function () {
          infowindow.open({
            anchor: marker,
            map: this.map,
            shouldFocus: false,
          });
        });
        path.push(source.lat + ',' + source.lng);
      }
      if (this.selectedTrackers.length == 1) {
        if (this.flightPath) {
          this.flightPath.setMap(null);
          this.flightPath.setPath(flightPlanCoordinates);
          this.flightPath = new google.maps.Polyline({
            path: flightPlanCoordinates,
            geodesic: true,
            strokeColor: trip.tracker_icon_color,
            strokeOpacity: 3.0,
            strokeWeight: 5,
          });
          this.flightPath.setMap(this.map);
        } else {
          this.flightPath = new google.maps.Polyline({
            path: flightPlanCoordinates,
            geodesic: true,
            strokeColor: trip.tracker_icon_color,
            strokeOpacity: 3.0,
            strokeWeight: 5,
          });
          this.flightPath.setMap(this.map);
        }
      } else if (this.selectedTrackers.length > 1) {
        if (this.flightPath) {
          this.flightPath.setPath(flightPlanCoordinates);
          this.flightPath = new google.maps.Polyline({
            path: flightPlanCoordinates,
            geodesic: true,
            strokeColor: trip.tracker_icon_color,
            strokeOpacity: 3.0,
            strokeWeight: 5,
          });
          this.flightPath.setMap(this.map);
        } else {
          this.flightPath = new google.maps.Polyline({
            path: flightPlanCoordinates,
            geodesic: true,
            strokeColor: trip.tracker_icon_color,
            strokeOpacity: 3.0,
            strokeWeight: 5,
          });
          this.flightPath.setMap(this.map);
        }
      }
    }
  }

  removeIncorrectLocations(locations: any) {
    let filteredLocations = [];
    for (let x = 0; x < locations.length; x++) {
      if (
        !locations[x].latitude.startsWith('0') ||
        !locations[x].longitude.startsWith('0')
      ) {
        filteredLocations.push(locations[x]);
      }
    }
    return filteredLocations;
  }

  getMapOptions(location, isZoomAndCenterRequired = true) {
    if (localStorage.getItem('mapStyle')) {
      this.mapStyle = localStorage.getItem('mapStyle');
    } else {
    }
    if (this.userSettings.map_style == 'roadmap') {
      this.mapOptions = {
        disableDefaultUI: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
      };
    } else if (this.userSettings.map_style == 'satellite') {
      this.mapOptions = {
        disableDefaultUI: true,
        mapTypeId: google.maps.MapTypeId.SATELLITE,
      };
    } else if (this.userSettings.map_style == 'hybrid') {
      this.mapOptions = {
        disableDefaultUI: true,
        mapTypeId: google.maps.MapTypeId.HYBRID,
      };
    } else {
      this.mapOptions = {
        disableDefaultUI: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
      };
    }

    if (isZoomAndCenterRequired) {
      this.mapOptions['zoom'] = 16;
      this.mapOptions['center'] = location;
    }
    return this.mapOptions;
  }

  infoWindowContent(data) {
    let dateTime = new Date(data.last_location[0].timestamp.replace(' ', 'T'));
    let contentString =
      '<div id="content">' +
      '<div id="siteNotice">' +
      '</div>' +
      '<h1 id="firstHeading" class="firstHeading">' +
      data.tracker_name +
      '</h1>' +
      '<div id="bodyContent">' +
      '<p><b>Date visited: </b>' +
      this.months[dateTime.getMonth()] +
      ' ' +
      dateTime.getDate() +
      ' ' +
      dateTime.getFullYear() +
      '<br><p><b>Time visited: </b>' +
      ('0' + dateTime.getHours()).slice(-2) +
      ':' +
      ('0' + dateTime.getMinutes()).slice(-2) +
      ':' +
      ('0' + dateTime.getSeconds()).slice(-2) +
      '<p><b>Position: </b>' +
      data.last_location[0].latitude +
      ',' +
      data.last_location[0].longitude;
    '</div>' + '</div>';
    return contentString;
  }

  infoWindowContentMultipleMarker(data, locations) {
    let dateTime = new Date(locations.timestamp);
    let contentString =
      '<div id="content">' +
      '<div id="siteNotice">' +
      '</div>' +
      '<h1 id="firstHeading" class="firstHeading">' +
      data.tracker_name +
      '</h1>' +
      '<div id="bodyContent">' +
      '<p><b>Date visited: </b>' +
      this.months[dateTime.getMonth()] +
      ' ' +
      dateTime.getDate() +
      ' ' +
      dateTime.getFullYear() +
      '<br><p><b>Time visited: </b>' +
      ('0' + dateTime.getHours()).slice(-2) +
      ':' +
      ('0' + dateTime.getMinutes()).slice(-2) +
      ':' +
      ('0' + dateTime.getSeconds()).slice(-2) +
      '<p><b>Position: </b>' +
      locations.latitude +
      ',' +
      locations.longitude;
    '</div>' + '</div>';
    return contentString;
  }

  calcCrow(latOne, lon1, latTwo, lon2) {
    let R = 6371; // km
    let dLat = this.toRad(latOne - latTwo);
    let dLon = this.toRad(lon2 - lon1);
    let lat1 = this.toRad(latOne);
    let lat2 = this.toRad(latTwo);

    let a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    let d = R * c;
    return d;
  }

  // Converts numeric degrees to radians
  toRad(Value) {
    return (Value * Math.PI) / 180;
  }

  setMapOnAll(map: google.maps.Map | null) {
    for (let i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(map);
    }
  }

  showTrackerDetails(tracker) {
    this.trackerImeiSelected = tracker.imei;
    this.trackerDetails = tracker;
    this.trackerDetails.tracker_icon_url = this.global.getMarkerLocation(
      this.trackerDetails.tracker_icon,
      this.trackerDetails.tracker_icon_color
    );

    // this.liveTrack(tracker.imei)

    this.selectedLiveTrack = tracker;
    //this.liveTrack(tracker.imei)
    if (this.trackerDetails.last_location.length == 0) {
      this.initMap();
    } else {
      let location = new google.maps.LatLng(
        Number(tracker.last_location[0].latitude),
        Number(tracker.last_location[0].longitude)
      );
      this.map.setCenter(location);
    }
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: FilterComponent,
      cssClass: 'filter-modal',
    });
    modal.onDidDismiss().then((data: any) => {
      this.setMapOnAll(null);
      this.markers = [];
      this.userSettings = JSON.parse(localStorage.getItem('userSettings'));
      if (data.data) {
        for (const st of data.data.selectedTracker) {
          st.tracker_icon_url = this.global.getMarkerLocation(
            st.tracker_icon,
            st.tracker_icon_color
          );
        }
        this.selectedTrackers = data.data.selectedTracker;
        this.owlElement?.reInit();
        if (this.selectedTrackers?.length != 0) {
          if (data.data.filterHours == '') {
            this.trackerHistorySetting = 'noHistory';
            this.reInitMapPolyLine(
              this.selectedTrackers[this.selectedTrackers.length - 1]
            );
            if (this.selectedTrackers?.length == 1) {
              for (let x = 0; x < this.selectedTrackers.length; x++) {
                this.setMarkerForMap(this.selectedTrackers[x], 'single');
              }
            } else {
              this.setMarkerForMap(this.selectedTrackers, 'multiple');
            }
          } else {
            this.trackerHistorySetting = 'withHistory';
            if (!this.selectedTrackers) {
              console.log('no tracker found!!');
            } else if (this.selectedTrackers?.length == 1) {
              // this.getTrackerLocations(this.selectedTrackers[0], data.data.filterHours)
              this.reInitMapPolyLine(this.selectedTrackers[0]);
              this.setMarkerForMap(this.selectedTrackers[0], 'single');
            } else {
              this.reInitMapPolyLine(
                this.selectedTrackers[this.selectedTrackers?.length - 1]
              );
              for (let x = 0; x < this.selectedTrackers.length; x++) {
                this.setMarkerForMap(this.selectedTrackers[x], 'single');
                // this.getTrackerLocations(this.selectedTrackers[x], data.data.filterHours)
              }
            }
          }
        }
      }
    });
    return await modal.present();
  }

  async cantStartTrackingToast() {
    const toast = await this.toastCtrl.create({
      message: 'Cannot start tracking, please try again later.',
      position: 'bottom',
      duration: 10000,
      color: 'danger',
    });
    toast.present();
  }

  async noActivityFoundToast() {
    const toast = await this.toastCtrl.create({
      message: 'No activity found for this tracker',
      position: 'bottom',
      duration: 10000,
      color: 'danger',
    });
    toast.present();
  }

  async socketSubscriptionForActivities(imei) {
    this.channelNewTrip = this.pusher.subscribe(`new-trip-${imei}`);

    this.channelNewTrip.bind('new-trip', (data) => {
      console.log('new-trip', data);
      localStorage.setItem('new-trip=================', JSON.stringify(data));
      this.toastCtrl
        .create({
          message: data.message,
          position: 'bottom',
          duration: 10000,
          color: 'dark',
        })
        .then((tost) => tost.present());
    });

    this.channelStop = this.pusher.subscribe(`tracker-stopped-${imei}`);

    this.channelStop.bind('tracker-stopped', (data) => {
      console.log('tracker stopped ======================', data);
      localStorage.setItem('tracker-stopped', JSON.stringify(data));
      this.toastCtrl
        .create({
          message: data.message,
          position: 'bottom',
          duration: 10000,
          color: 'dark',
        })
        .then((tost) => tost.present());
    });

    this.channelSpeed = this.pusher.subscribe(`speeding-event-${imei}`);

    this.channelSpeed.bind('speeding-event', (data) => {
      console.log('speeding====================', data);
      localStorage.setItem('speeding-event', JSON.stringify(data));
      this.toastCtrl
        .create({
          message: data.message,
          position: 'bottom',
          duration: 10000,
          color: 'dark',
        })
        .then((tost) => tost.present());
    });

    this.channelNotification = this.pusher.subscribe(
      `low-battery-channel-${imei}`
    );

    this.channelSpeed.bind('low-battery-event', (data) => {
      console.log('Low battery ===================', data);
      localStorage.setItem('low-battery-event', JSON.stringify(data));
      this.toastCtrl
        .create({
          message: 'Test message',
          position: 'bottom',
          duration: 10000,
          color: 'danger',
        })
        .then((tost) => tost.present());
    });
  }

  ionViewDidLeave() {
    clearInterval(this.liveInfoInterval);
    this.channelNewTrip?.unsubscribe();
    this.channelStop?.unsubscribe();
    this.channelSpeed?.unsubscribe();
    this.channelNotification?.unsubscribe();
  }

  recenterToMarker() {
    this.map.setOptions(
      this.getMapOptions(this.currentLocationLatandLng, true)
    );
  }

  redirectActivate() {
    window.open('https://spytrack.com/', '_system');
  }

  drawPath(imei) {
    let bounds = new google.maps.LatLngBounds();
    const color = 'green';
    for (let i = 0; i < this.liveLocations[imei].length; i = i + 1) {
      let j = i + 1;

      if (this.liveLocations[imei][i] && this.liveLocations[imei][j]) {
        let pos = new google.maps.LatLng(
          this.liveLocations[imei][i].lat,
          this.liveLocations[imei][i].lng
        );
        let pos2 = new google.maps.LatLng(
          this.liveLocations[imei][j].lat,
          this.liveLocations[imei][j].lng
        );
        //bounds.extend(pos);
        bounds.extend(pos2);
        // poly.push(pos);
        new google.maps.Polyline({
          path: [pos, pos2],
          geodesic: true,
          strokeColor: 'lightgray',
          strokeOpacity: 1.0,
          strokeWeight: 8,
          map: this.map,
        });
        new google.maps.Polyline({
          path: [pos, pos2],
          geodesic: true,
          strokeColor: color,
          strokeOpacity: 1.0,
          strokeWeight: 5,
          map: this.map,
        });
      }
    }
    setTimeout(async () => {
      this.displayMap = true;
      this.map.fitBounds(bounds);
      this.map.panToBounds(bounds);
    }, 100);
  }
}
