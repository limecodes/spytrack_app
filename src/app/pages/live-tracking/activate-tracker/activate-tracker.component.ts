import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides, ModalController } from '@ionic/angular';
import { GlobalService } from 'src/app/globals.service';

@Component({
  selector: 'app-activate-tracker',
  templateUrl: './activate-tracker.component.html',
  styleUrls: ['./activate-tracker.component.scss'],
})
export class ActivateTrackerComponent implements OnInit {
  @ViewChild('activationSlides')  activationSlides: IonSlides;
  slideOpts = {
    initialSlide: 0,
    speed: 400,
    allowTouchMove: false
  };
  user = '';
  userDetails: any;
  constructor(
    private global: GlobalService,
    public modalController: ModalController,
  ) { }

  ngOnInit() {
    this.global.currentMessageSubscriberSummary.subscribe((data: any) => {
      if (data.isRefresh) {
        this.user = this.global.firstname;
        this.userDetails = this.global.userDetails;
      } else {
      }
    })
    console.log(this.userDetails)
  }

  async closeModal() {
    const onClosedData: string = "Wrapped Up!";
    await this.modalController.dismiss(onClosedData);
  }

}
