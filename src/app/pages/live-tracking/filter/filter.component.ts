/* eslint-disable @typescript-eslint/prefer-for-of */
import { Component, OnInit } from '@angular/core';
import { LoadingController, ModalController } from '@ionic/angular';
import { GlobalService } from 'src/app/globals.service';
import { RestService } from 'src/app/rest.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
})
export class FilterComponent implements OnInit {
  defaultTrackerActivityFilter: any = '';
  defaultMapStyle: any = '0';
  defaultTrackerHistory: any = '0';
  userTrackers: any = [];
  selectedTrackers: any = [];
  hasTracker = false;
  userSettings: any;
  initialSelectedTrackers = [];
  globalLoader: any;
  constructor(
    public modalController: ModalController,
    private rest: RestService,
    private globalSerice: GlobalService,
    private loader: LoadingController
  ) {}

  ngOnInit() {
    this.initGetSettingsValue();
    this.initGetMapStyleSettingsValue();
    this.getTrackerUser();
    this.getUserSettings();
    // this.getUserTracker();
  }

  getUserSettings() {
    this.userSettings = JSON.parse(localStorage.getItem('userSettings'));
  }

  initGetSettingsValue() {
    if (localStorage.getItem('trackerHistorySetting')) {
      if (localStorage.getItem('trackerHistorySetting') === 'noHistory') {
        this.defaultTrackerHistory = '0';
      } else {
        this.defaultTrackerHistory = '1';
      }
    }

    if (localStorage.getItem('trackerActivityFilter')) {
      if (localStorage.getItem('trackerActivityFilter') === 'all') {
        this.defaultTrackerActivityFilter = '0';
      } else if (
        localStorage.getItem('trackerActivityFilter') === 'last hour'
      ) {
        this.defaultTrackerActivityFilter = '1';
      } else if (localStorage.getItem('trackerActivityFilter') === '8 hours') {
        this.defaultTrackerActivityFilter = '8';
      } else if (localStorage.getItem('trackerActivityFilter') === '12 hours') {
        this.defaultTrackerActivityFilter = '12';
      } else if (localStorage.getItem('trackerActivityFilter') === '24 hours') {
        this.defaultTrackerActivityFilter = '24';
      } else if (localStorage.getItem('trackerActivityFilter') === 'today') {
        this.defaultTrackerActivityFilter = 'today';
      }
    }
  }

  initSetSettingsValue() {
    if (this.defaultTrackerHistory === '0') {
      localStorage.setItem('trackerHistorySetting', 'noHistory');
    } else {
      localStorage.setItem('trackerHistorySetting', 'withHistory');
    }
    if (this.defaultTrackerActivityFilter === '0') {
      localStorage.setItem('trackerActivityFilter', 'all');
    } else if (this.defaultTrackerActivityFilter === '1') {
      localStorage.setItem('trackerActivityFilter', 'last hour');
    } else if (this.defaultTrackerActivityFilter === '8') {
      localStorage.setItem('trackerActivityFilter', '8 hours');
    } else if (this.defaultTrackerActivityFilter === '12') {
      localStorage.setItem('trackerActivityFilter', '12 hours');
    } else if (this.defaultTrackerActivityFilter === '24') {
      localStorage.setItem('trackerActivityFilter', '24 hours');
    } else if (this.defaultTrackerActivityFilter === 'today') {
      localStorage.setItem('trackerActivityFilter', 'today');
    }
  }

  initGetMapStyleSettingsValue() {
    if (localStorage.getItem('mapStyle')) {
      if (localStorage.getItem('mapStyle') === 'roadmap') {
        this.defaultMapStyle = 'roadmap';
      } else if (localStorage.getItem('mapStyle') === 'satellite') {
        this.defaultMapStyle = 'satellite';
      } else if (localStorage.getItem('mapStyle') === 'hybrid') {
        this.defaultMapStyle = 'hybrid';
      }
    } else {
      this.defaultMapStyle = 'roadmap';
    }
  }

  initSetMapStyleSettingsValue() {
    if (this.defaultMapStyle === 'roadmap') {
      localStorage.setItem('mapStyle', 'roadmap');
    } else if (this.defaultMapStyle === 'satellite') {
      localStorage.setItem('mapStyle', 'satellite');
    } else if (this.defaultMapStyle === 'hybrid') {
      localStorage.setItem('mapStyle', 'hybrid');
    }
  }

  async closeModal(data = {}) {
    console.log(data);
    await this.modalController.dismiss(data);
  }

  selectedHistory(value: any) {
    this.defaultTrackerHistory = value;
    this.defaultTrackerActivityFilter = '';
  }

  resetFilter() {
    this.initGetSettingsValue();
    this.initGetMapStyleSettingsValue();
    this.getTrackerUser();
    //  this.selectedTrackers = this.initialSelectedTrackers;
    //  console.log(this.initialSelectedTrackers);
    //  localStorage.setItem('selectedUserTrackers', JSON.stringify(this.selectedTrackers));
    this.getUserSettings();
  }

  async applyChanges() {
    this.globalLoader = await this.loader.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
    });

    this.globalLoader.present();

    this.initSetSettingsValue();
    this.initSetMapStyleSettingsValue();
    let dataToClose = {
      filterHours: '',
    };
    if (this.defaultTrackerHistory === '1') {
      if (this.defaultTrackerActivityFilter === '0') {
        dataToClose.filterHours = 'all';
      } else if (this.defaultTrackerActivityFilter === '1') {
        dataToClose.filterHours = 'last hour';
      } else if (this.defaultTrackerActivityFilter === '8') {
        dataToClose.filterHours = '8 hours';
      } else if (this.defaultTrackerActivityFilter === '12') {
        dataToClose.filterHours = '12 hours';
      } else if (this.defaultTrackerActivityFilter === '24') {
        dataToClose.filterHours = '24 hours';
      } else if (this.defaultTrackerActivityFilter === 'today') {
        dataToClose.filterHours = 'today';
      } else {
        dataToClose.filterHours = '';
      }
    } else {
      dataToClose.filterHours = '';
    }

    // dataToClose['userSettings'] = this.
    console.log(dataToClose);
    for (let x = 0; x < this.selectedTrackers.length; x++) {
      this.selectedTrackers[x].tracker_icon =
        'fa-' + this.selectedTrackers[x].tracker_icon;
    }
    dataToClose['selectedTracker'] = this.selectedTrackers;
    this.saveMapStyle()
      .then((response) => {
        console.log('response', response);
      })
      .catch((error) => {
        console.error(error);
      })
      .finally(() => {
        this.globalLoader.dismiss();
        this.closeModal(dataToClose);
      });
  }

  async saveMapStyle() {
    let dataToSend = {
      timezone: this.userSettings.timezone,
      map_style: this.defaultMapStyle,
      speed_unit: this.userSettings.speed_unit,
      distance_unit: this.userSettings.distance_unit,
    };
    localStorage.setItem('userSettings', JSON.stringify(dataToSend));

    // Return a new promise object
    return new Promise((resolve, reject) => {
      // Initialise an api call
      this.rest.put(dataToSend, 'thorton/user/settings').subscribe(
        (resp: any) => {
          console.log(resp);
          resolve(resp);
        },
        (error) => {
          reject(error);
        }
      );
    });
  }

  selectedActivityFilter(value: any) {
    this.defaultTrackerActivityFilter = value;
  }

  selectedMapType(value: any) {
    this.defaultMapStyle = value;
  }

  selectTrackerEvent(event, tracker, index) {
    if (event.detail.checked) {
      tracker['isChecked'] = true;
      this.userTrackers[index].isChecked = true;
      this.selectedTrackers.push(tracker);
      localStorage.setItem('userTrackers', JSON.stringify(this.userTrackers));
    } else {
      tracker['isChecked'] = false;
      this.userTrackers[index].isChecked = false;
      this.selectedTrackers = this.selectedTrackers.filter(
        (item) => item.imei !== tracker.imei
      );
      localStorage.setItem('userTrackers', JSON.stringify(this.userTrackers));
    }
    localStorage.setItem(
      'selectedUserTrackers',
      JSON.stringify(this.selectedTrackers)
    );
  }

  getTrackerUser() {
    console.log(JSON.parse(localStorage.getItem('userTrackers')));
    if (localStorage.getItem('userTrackers')) {
      this.userTrackers = [];
      this.selectedTrackers = [];
      let temp = JSON.parse(localStorage.getItem('userTrackers'));
      for (let x = 0; x < temp.length; x++) {
        if (temp[x].tracker_icon) {
          temp[x].tracker_icon_url = this.globalSerice.getMarkerLocation(
            temp[x].tracker_icon,
            temp[x].tracker_icon_color
          );
          temp[x].tracker_icon = temp[x].tracker_icon.replace('fa-', '');
        }
        this.userTrackers.push(temp[x]);
      }
      for (let x = 0; x < this.userTrackers.length; x++) {
        if (this.userTrackers[x].isChecked) {
          this.selectedTrackers.push(this.userTrackers[x]);
        }
      }
      localStorage.setItem(
        'selectedUserTrackers',
        JSON.stringify(this.selectedTrackers)
      );
      this.initialSelectedTrackers = this.selectedTrackers.slice(0);
      this.hasTracker = true;
    } else {
      this.hasTracker = false;
    }
  }

  getUserTracker() {
    let dataToSend = {
      trackerID: '4900112612',
      recent: 'today',
    };
    this.rest
      .get('thorton/trackerData/recent', dataToSend, '')
      .subscribe((resp: any) => {
        console.log(resp);
      });
  }
}
