(self["webpackChunkspytrack"] = self["webpackChunkspytrack"] || []).push([["src_app_login_login_module_ts"],{

/***/ 3827:
/*!********************************************************************!*\
  !*** ./src/app/login/forgot-password/forgot-password.component.ts ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ForgotPasswordComponent": () => (/* binding */ ForgotPasswordComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_forgot_password_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./forgot-password.component.html */ 645);
/* harmony import */ var _forgot_password_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./forgot-password.component.scss */ 3334);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ 5257);
/* harmony import */ var src_app_globals_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/globals.service */ 3221);
/* harmony import */ var src_app_rest_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/rest.service */ 1881);



/* eslint-disable @typescript-eslint/naming-convention */







let ForgotPasswordComponent = class ForgotPasswordComponent {
    constructor(formBuilder, toastCtrl, global, rest, router) {
        this.formBuilder = formBuilder;
        this.toastCtrl = toastCtrl;
        this.global = global;
        this.rest = rest;
        this.router = router;
        this.emailValidation = true;
        this.passwordVal = {
            minLen: false,
            number: false,
            symbol: false,
            upperCase: false,
            lowerCase: false
        };
        this.cPasswordVal = false;
    }
    ngOnInit() {
        this.formInit();
    }
    ionViewWillEnter() {
        this.forgotPasswordFormStepOne.reset();
        this.forgotPasswordFormStepTwo.reset();
        this.isUserExistConfirm = false;
    }
    formInit() {
        this.forgotPasswordFormStepOne = this.formBuilder.group({
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormControl('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.email]),
        });
        this.forgotPasswordFormStepTwo = this.formBuilder.group({
            otp: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormControl('', {
                validators: [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required,
                    //Validators.min(9999),Validators.max(99999)
                ]
            }),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormControl('', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&]).{8,}')]),
            cpassword: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required)
        });
    }
    passwordInputValidation(event) {
        const e = event.target.value;
        const number = /\d/;
        const symbol = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
        const upperCase = /[A-Z]/;
        const lowerCase = /[a-z]/;
        this.forgotPasswordFormStepTwo.controls.password.setValue(e);
        if (this.forgotPasswordFormStepTwo.controls.password.value == '') {
            this.passwordVal = {
                minLen: false,
                number: false,
                symbol: false,
                upperCase: false,
                lowerCase: false
            };
        }
        else {
            if (this.forgotPasswordFormStepTwo.controls.password.value.length >= 8) {
                this.passwordVal.minLen = true;
            }
            else {
                this.passwordVal.minLen = false;
            }
            if (number.test(this.forgotPasswordFormStepTwo.controls.password.value)) {
                this.passwordVal.number = true;
            }
            else {
                this.passwordVal.number = false;
            }
            if (symbol.test(this.forgotPasswordFormStepTwo.controls.password.value)) {
                this.passwordVal.symbol = true;
            }
            else {
                this.passwordVal.symbol = false;
            }
            if (upperCase.test(this.forgotPasswordFormStepTwo.controls.password.value)) {
                this.passwordVal.upperCase = true;
            }
            else {
                this.passwordVal.upperCase = false;
            }
            if (lowerCase.test(this.forgotPasswordFormStepTwo.controls.password.value)) {
                this.passwordVal.lowerCase = true;
            }
            else {
                this.passwordVal.lowerCase = false;
            }
        }
    }
    cpasswordInputValidation(event) {
        const e = event.target.value;
        this.forgotPasswordFormStepTwo.controls.cpassword.setValue(e);
        if (this.forgotPasswordFormStepTwo.controls.password.value == this.forgotPasswordFormStepTwo.controls.cpassword.value) {
            this.cPasswordVal = true;
        }
        else {
            this.cPasswordVal = false;
        }
    }
    checkUserExist() {
        const sendData = {
            email: this.forgotPasswordFormStepOne.controls.email.value
        };
        this.rest.post(sendData, 'forgot_password').pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.take)(1)).subscribe((resp) => {
            console.log(resp);
            if (!resp || resp == null) {
                this.toastCtrl.create({
                    message: "Email doesn't exist.",
                    position: 'bottom',
                    duration: 3000,
                    color: 'danger'
                }).then((toast) => {
                    toast.present();
                });
            }
            else {
                this.isUserExistConfirm = true;
            }
        });
    }
    resendOTP() {
        const sendData = {
            email: this.forgotPasswordFormStepOne.controls.email.value
        };
        this.rest.post(sendData, 'forgot_password').pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.take)(1)).subscribe((resp) => {
            if (!resp || resp == null) {
                this.toastCtrl.create({
                    message: 'No such user found in databse!',
                    position: 'bottom',
                    duration: 3000,
                    color: 'danger'
                }).then((toast) => {
                    toast.present();
                });
            }
            else {
                this.isUserExistConfirm = true;
            }
        });
        this.toastCtrl.create({
            message: 'OTP sent sucessfully to registered phone no.',
            position: 'bottom',
            duration: 3000,
            color: 'success'
        }).then((toast) => {
            toast.present();
        });
    }
    resetPassword() {
        const requestObj = {
            otp: this.forgotPasswordFormStepTwo.controls.otp.value,
            password: this.forgotPasswordFormStepTwo.controls.password.value,
            password_confirmation: this.forgotPasswordFormStepTwo.controls.cpassword.value
        };
        this.rest.post(requestObj, 'change_password').pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.take)(1)).subscribe((resp) => {
            if (resp.isSuccess) {
                this.toastCtrl.create({
                    message: 'Rest password done successfully!',
                    position: 'bottom',
                    duration: 3000,
                    color: 'success'
                }).then((toast) => {
                    toast.present();
                });
                this.router.navigate(['/login']);
            }
            else {
                this.toastCtrl.create({
                    message: resp.error.message || resp.errors[0] || resp.message || 'something went wrong!',
                    position: 'bottom',
                    duration: 3000,
                    color: 'danger'
                }).then((toast) => {
                    toast.present();
                });
            }
        });
    }
};
ForgotPasswordComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormBuilder },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.ToastController },
    { type: src_app_globals_service__WEBPACK_IMPORTED_MODULE_2__.GlobalService },
    { type: src_app_rest_service__WEBPACK_IMPORTED_MODULE_3__.RestService },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__.Router }
];
ForgotPasswordComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_9__.Component)({
        selector: 'app-forgot-password',
        template: _raw_loader_forgot_password_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_forgot_password_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], ForgotPasswordComponent);



/***/ }),

/***/ 5393:
/*!***********************************************!*\
  !*** ./src/app/login/login-routing.module.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginPageRoutingModule": () => (/* binding */ LoginPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login.page */ 6825);
/* harmony import */ var _forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./forgot-password/forgot-password.component */ 3827);





const routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_0__.LoginPage
    },
    {
        path: 'forgot-password',
        component: _forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_1__.ForgotPasswordComponent
    }
];
let LoginPageRoutingModule = class LoginPageRoutingModule {
};
LoginPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__.RouterModule],
    })
], LoginPageRoutingModule);



/***/ }),

/***/ 107:
/*!***************************************!*\
  !*** ./src/app/login/login.module.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginPageModule": () => (/* binding */ LoginPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _login_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login-routing.module */ 5393);
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./login.page */ 6825);
/* harmony import */ var _forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./forgot-password/forgot-password.component */ 3827);








let LoginPageModule = class LoginPageModule {
};
LoginPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.ReactiveFormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonicModule,
            _login_routing_module__WEBPACK_IMPORTED_MODULE_0__.LoginPageRoutingModule
        ],
        declarations: [
            _login_page__WEBPACK_IMPORTED_MODULE_1__.LoginPage,
            _forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_2__.ForgotPasswordComponent
        ]
    })
], LoginPageModule);



/***/ }),

/***/ 6825:
/*!*************************************!*\
  !*** ./src/app/login/login.page.ts ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginPage": () => (/* binding */ LoginPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_login_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./login.page.html */ 6770);
/* harmony import */ var _login_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./login.page.scss */ 1339);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../rest.service */ 1881);
/* harmony import */ var _globals_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../globals.service */ 3221);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ 5257);









let LoginPage = class LoginPage {
    constructor(router, httpRequest, toastCtrl, globals, loading) {
        this.router = router;
        this.httpRequest = httpRequest;
        this.toastCtrl = toastCtrl;
        this.globals = globals;
        this.loading = loading;
        this.username = '';
        this.password = '';
        if (localStorage.getItem('token')) {
            this.router.navigate(['/live-tracking']);
        }
    }
    ngOnInit() {
    }
    ionViewWillEnter() {
        this.userIsInactivate = false;
    }
    login() {
        let dataToSend = {
            email: this.username,
            password: this.password
        };
        this.httpRequest.login(JSON.stringify(dataToSend), 'login').pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_4__.take)(1)).subscribe((resp) => (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            if (resp.status && resp.statusCode === 200) {
                this.globals.token = resp.token;
                localStorage.setItem('token', resp.token);
                this.router.navigate(['/live-tracking']);
            }
            else {
                if (resp.statusCode === 401) {
                    const toast = yield this.toastCtrl.create({
                        message: 'Please enter correct credentials!',
                        position: 'bottom',
                        duration: 1500,
                        color: 'danger'
                    });
                    toast.present();
                }
            }
        }), (error) => (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            if (error.error.statusCode === 403) {
                const toast = yield this.toastCtrl.create({
                    message: 'Please verify your account!',
                    position: 'bottom',
                    duration: 1500,
                    color: 'danger'
                });
                toast.present();
                this.userIsInactivate = true;
            }
            else {
                this.openToast();
            }
            console.log(error);
        }));
    }
    resendVarificationEmail() {
        this.httpRequest.newVerification('user/new_verification', this.username).subscribe((resp) => (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            if (resp.isSuccess) {
                const toast = yield this.toastCtrl.create({
                    message: 'Email sent successfuly',
                    position: 'bottom',
                    duration: 1500,
                    color: 'success'
                });
                toast.present();
            }
            else {
                const toast = yield this.toastCtrl.create({
                    message: resp.error.message || resp.message,
                    position: 'bottom',
                    duration: 1500,
                    color: 'danger'
                });
                toast.present();
                //this.userIsInactivate = false;
            }
        }), error => {
            console.log(error);
            this.openToast();
        });
    }
    openToast() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: 'Invalid username or password',
                position: 'bottom',
                duration: 4000,
                color: 'danger'
            });
            toast.present();
        });
    }
};
LoginPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__.Router },
    { type: _rest_service__WEBPACK_IMPORTED_MODULE_2__.RestService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.ToastController },
    { type: _globals_service__WEBPACK_IMPORTED_MODULE_3__.GlobalService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.LoadingController }
];
LoginPage = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Component)({
        selector: 'app-login',
        template: _raw_loader_login_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_login_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], LoginPage);



/***/ }),

/***/ 3334:
/*!**********************************************************************!*\
  !*** ./src/app/login/forgot-password/forgot-password.component.scss ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("ion-content.background {\n  --ion-background-color: #181C32;\n}\n\nion-item {\n  --ion-background-color: #fff;\n}\n\n.title {\n  color: #fff;\n  text-align: center;\n  margin-top: 250px;\n  margin-bottom: 30px;\n  font-weight: bolder;\n}\n\n#rounded {\n  border-radius: 10px;\n  margin-bottom: 20px;\n  text-align: center;\n}\n\n.input {\n  height: 65px;\n}\n\n#container {\n  width: 350px;\n  margin-left: auto;\n  margin-right: auto;\n}\n\n.container {\n  width: 560px;\n  margin-left: auto;\n  margin-right: auto;\n}\n\n.menudiv {\n  margin-left: 30px;\n}\n\n.btn-login {\n  text-transform: none;\n  font-weight: bold;\n  height: 45px;\n  width: 350px;\n}\n\n.ion-btn {\n  width: 500px;\n  display: flex;\n}\n\n.my-custom-class {\n  --background: #e5e5e5;\n}\n\n.btn-cancel {\n  --color: #fff;\n  --ion-color-contrast: #fff !important;\n}\n\nion-list {\n  background: white;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImZvcmdvdC1wYXNzd29yZC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLCtCQUFBO0FBQ0o7O0FBRUE7RUFDSSw0QkFBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0FBQ0o7O0FBRUE7RUFDSSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFDQTtFQUNJLFlBQUE7QUFFSjs7QUFBQTtFQUNJLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBR0o7O0FBQUE7RUFDSSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQUdKOztBQUFBO0VBQ0ksaUJBQUE7QUFHSjs7QUFBQTtFQUNJLG9CQUFBO0VBQ0EsaUJBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtBQUdKOztBQURBO0VBQ0ksWUFBQTtFQUNBLGFBQUE7QUFJSjs7QUFGQTtFQUNFLHFCQUFBO0FBS0Y7O0FBSEE7RUFDSSxhQUFBO0VBQ0EscUNBQUE7QUFNSjs7QUFIQTtFQUNJLGlCQUFBO0FBTUoiLCJmaWxlIjoiZm9yZ290LXBhc3N3b3JkLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnQuYmFja2dyb3VuZCB7XG4gICAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjogIzE4MUMzMjtcbn1cblxuaW9uLWl0ZW0ge1xuICAgIC0taW9uLWJhY2tncm91bmQtY29sb3I6ICNmZmY7XG59XG5cbi50aXRsZSB7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIG1hcmdpbi10b3A6IDI1MHB4O1xuICAgIG1hcmdpbi1ib3R0b206IDMwcHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbn1cblxuI3JvdW5kZWQge1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uaW5wdXR7XG4gICAgaGVpZ2h0OiA2NXB4O1xufVxuI2NvbnRhaW5lciB7XG4gICAgd2lkdGg6IDM1MHB4O1xuICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICAgIG1hcmdpbi1yaWdodDogYXV0bztcbn1cblxuLmNvbnRhaW5lciB7XG4gICAgd2lkdGg6IDU2MHB4O1xuICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICAgIG1hcmdpbi1yaWdodDogYXV0bztcbn1cblxuLm1lbnVkaXYge1xuICAgIG1hcmdpbi1sZWZ0OiAzMHB4O1xufVxuXG4uYnRuLWxvZ2luIHtcbiAgICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBoZWlnaHQ6IDQ1cHg7XG4gICAgd2lkdGg6IDM1MHB4O1xufVxuLmlvbi1idG57XG4gICAgd2lkdGg6IDUwMHB4O1xuICAgIGRpc3BsYXk6IGZsZXg7XG59XG4ubXktY3VzdG9tLWNsYXNzIHtcbiAgLS1iYWNrZ3JvdW5kOiAjZTVlNWU1O1xufVxuLmJ0bi1jYW5jZWx7XG4gICAgLS1jb2xvcjogI2ZmZjtcbiAgICAtLWlvbi1jb2xvci1jb250cmFzdDogI2ZmZiAhaW1wb3J0YW50O1xufVxuXG5pb24tbGlzdHtcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbn1cbiJdfQ== */");

/***/ }),

/***/ 1339:
/*!***************************************!*\
  !*** ./src/app/login/login.page.scss ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("ion-content.background {\n  --ion-background-color: #181C32;\n}\n\nion-item {\n  --ion-background-color: #fff;\n}\n\n.title {\n  color: #fff;\n  text-align: center;\n  margin-top: 170px;\n  margin-bottom: 30px;\n  font-weight: bolder;\n}\n\n.title-spy {\n  font-weight: bold;\n}\n\n#rounded {\n  border-radius: 10px;\n  margin-bottom: 20px;\n}\n\n.input {\n  color: black;\n}\n\n#container {\n  width: 350px;\n  margin-left: auto;\n  margin-right: auto;\n}\n\n.container {\n  width: 560px;\n  margin-left: auto;\n  margin-right: auto;\n}\n\nsection {\n  margin-top: 1em;\n  margin-bottom: 3em;\n}\n\n.list {\n  color: #fff;\n  font-weight: bold;\n  font-size: 15px;\n  text-align: center;\n  margin-top: -20px;\n}\n\n.menudiv {\n  margin-left: 30px;\n}\n\n.btn-login {\n  text-transform: none;\n  font-weight: bold;\n  height: 45px;\n  width: 350px;\n}\n\n.ion-btn {\n  width: 500px;\n  display: flex;\n}\n\n.my-custom-class {\n  --background: #e5e5e5;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxvZ2luLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLCtCQUFBO0FBQ0o7O0FBRUE7RUFDSSw0QkFBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0FBQ0o7O0FBRUE7RUFDSSxpQkFBQTtBQUNKOztBQUVBO0VBQ0ksbUJBQUE7RUFDQSxtQkFBQTtBQUNKOztBQUNBO0VBQ0ksWUFBQTtBQUVKOztBQUFBO0VBQ0ksWUFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUFHSjs7QUFBQTtFQUNJLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBR0o7O0FBQUE7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7QUFHSjs7QUFBQTtFQUNJLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0FBR0o7O0FBQUE7RUFDSSxpQkFBQTtBQUdKOztBQUFBO0VBQ0ksb0JBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0FBR0o7O0FBREE7RUFDSSxZQUFBO0VBQ0EsYUFBQTtBQUlKOztBQUZBO0VBQ0UscUJBQUE7QUFLRiIsImZpbGUiOiJsb2dpbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudC5iYWNrZ3JvdW5kIHtcbiAgICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiAjMTgxQzMyO1xufVxuXG5pb24taXRlbSB7XG4gICAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbn1cblxuLnRpdGxlIHtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgbWFyZ2luLXRvcDogMTcwcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMzBweDtcbiAgICBmb250LXdlaWdodDogYm9sZGVyO1xufVxuXG4udGl0bGUtc3B5IHtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuI3JvdW5kZWQge1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcbn1cbi5pbnB1dHtcbiAgICBjb2xvcjogYmxhY2s7XG59XG4jY29udGFpbmVyIHtcbiAgICB3aWR0aDogMzUwcHg7XG4gICAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xufVxuXG4uY29udGFpbmVyIHtcbiAgICB3aWR0aDogNTYwcHg7XG4gICAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xufVxuXG5zZWN0aW9uIHtcbiAgICBtYXJnaW4tdG9wOiAxZW07XG4gICAgbWFyZ2luLWJvdHRvbTogM2VtO1xufVxuXG4ubGlzdCB7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgZm9udC1zaXplOiAxNXB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBtYXJnaW4tdG9wOiAtMjBweDtcbn1cblxuLm1lbnVkaXYge1xuICAgIG1hcmdpbi1sZWZ0OiAzMHB4O1xufVxuXG4uYnRuLWxvZ2luIHtcbiAgICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBoZWlnaHQ6IDQ1cHg7XG4gICAgd2lkdGg6IDM1MHB4O1xufVxuLmlvbi1idG57XG4gICAgd2lkdGg6IDUwMHB4O1xuICAgIGRpc3BsYXk6IGZsZXg7XG59XG4ubXktY3VzdG9tLWNsYXNzIHtcbiAgLS1iYWNrZ3JvdW5kOiAjZTVlNWU1O1xufSJdfQ== */");

/***/ }),

/***/ 645:
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/login/forgot-password/forgot-password.component.html ***!
  \************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-content class=\"background\" *ngIf=\"!isUserExistConfirm; else confirmUserExist;\">\n\t<div class=\"title\">\n      \t<span>Enter your email to reset your password</span>\n    </div>\n    <div id=\"container\">\n      <form class=\"form\" [formGroup]=\"forgotPasswordFormStepOne\">\n        <ion-item id=\"rounded\">\n          <ion-input class=\"input\" placeholder=\"Email\" autocomplete=\"off\" formControlName=\"email\" type=\"email\" [email]=\"emailValidation\" ></ion-input>\n        </ion-item>\n        <div style=\"text-align: center;\">\n          <ion-button class=\"btn-cancel\" color=\"medium\" size=\"large\" [routerLink]=\"['/login']\">Cancel</ion-button>\n\t\t\t    <ion-button color=\"secondary\" size=\"large\" [disabled]=\"!forgotPasswordFormStepOne.valid\"  (click)=\"checkUserExist()\">Submit</ion-button>\n        </div>\n      </form>\n    </div>\n</ion-content>\n\n<ng-template #confirmUserExist>\n  <ion-content class=\"background\" >\n      <div style=\"max-width: 350px; margin-left: auto; margin-right: auto; margin-top: 20px;\" class=\"title\">\n          <span>Enter your one time password which you received into your registered mobile number</span> \n      </div>\n      <div style=\"max-width: 350px; margin-left: auto; margin-right: auto;\"> <a href=\"javascript:;\" (click)=\"resendOTP()\" style=\"float: right; margin-right: 10px;\">Resend OTP</a> </div>\n      <br/>\n      <div id=\"container\" style=\"max-width: 350px; margin-left: auto; margin-right: auto;\">\n        <form class=\"form\" [formGroup]=\"forgotPasswordFormStepTwo\">\n          <ion-item id=\"rounded\">\n            <ion-input class=\"input\" placeholder=\"Verification Code\" autocomplete=\"off\" formControlName=\"otp\" type=\"number\" OnlyNumber=\"true\" ></ion-input>\n          </ion-item>\n          <div *ngIf=\"forgotPasswordFormStepTwo.get('otp').invalid && \n          forgotPasswordFormStepTwo.get('otp').errors && \n\t\t\t\t\t\t\t(forgotPasswordFormStepTwo.get('otp').dirty || forgotPasswordFormStepTwo.get('otp').touched)\">\n\t\t\t\t\t\t<small class=\"text-danger\"\n\t\t\t\t\t\t\t*ngIf=\"forgotPasswordFormStepTwo.get('otp').hasError('required')\">\n\t\t\t\t\t\t\tOTP is required.\n\t\t\t\t\t\t</small>\n            <small class=\"text-danger\"\n\t\t\t\t\t\t\t*ngIf=\"forgotPasswordFormStepTwo.get('otp').hasError('min') || forgotPasswordFormStepTwo.get('otp').hasError('max') \">\n\t\t\t\t\t\t\tOTP should be of 5 digit.\n\t\t\t\t\t\t</small>\n\t\t\t\t\t</div>\n          <ion-item id=\"rounded\">\n            <ion-input class=\"input\" placeholder=\"New Password\" (input)='passwordInputValidation($event)' autocomplete=\"off\"  formControlName=\"password\" type=\"password\" ></ion-input>\n          </ion-item>\n          <div *ngIf=\"(forgotPasswordFormStepTwo.get('password').invalid && forgotPasswordFormStepTwo.get('password').touched) || forgotPasswordFormStepTwo.get('password').dirty\">\n\t\t\t\t\t\t<small class=\"text-danger\" *ngIf=\"forgotPasswordFormStepTwo.get('password').errors?.required\">Password is required</small><br>\n\t\t\t\t\t</div>\n          <ion-item id=\"rounded\">\n            <ion-input class=\"input\" placeholder=\"Confirm Password\" (input)='cpasswordInputValidation($event)'  autocomplete=\"off\" formControlName=\"cpassword\" type=\"password\" ></ion-input>\n          </ion-item>\n\n          <ion-list class=\"list\" style=\"max-width: 350px; margin-left: auto; margin-right: auto;\">\n\t\t\t\t\t\t<ion-icon class=\"icon\" name=\"checkmark-circle\" style=\"color: green;\" *ngIf=\"passwordVal.minLen\"></ion-icon>\n\t\t\t\t\t\t<ion-icon class=\"icon\" name=\"checkmark-circle\" *ngIf=\"!passwordVal.minLen\"></ion-icon>\n\t\t\t\t\t\t<ion-label class=\"label\">Be at least 8 characters long</ion-label><br>\n\n\t\t\t\t\t\t<ion-icon class=\"icon\" name=\"checkmark-circle\" style=\"color: green;\" *ngIf=\"passwordVal.number\"></ion-icon>\n\t\t\t\t\t\t<ion-icon class=\"icon\" name=\"checkmark-circle\" *ngIf=\"!passwordVal.number\"></ion-icon>\n\t\t\t\t\t\t<ion-label class=\"label\">Have at least one number</ion-label><br>\n\n\t\t\t\t\t\t<ion-icon class=\"icon\" name=\"checkmark-circle\" style=\"color: green;\" *ngIf=\"passwordVal.symbol\"></ion-icon>\n\t\t\t\t\t\t<ion-icon class=\"icon\" name=\"checkmark-circle\" *ngIf=\"!passwordVal.symbol\"></ion-icon>\n\t\t\t\t\t\t<ion-label class=\"label\">Have at least one symbol</ion-label><br>\n\n\t\t\t\t\t\t<ion-icon class=\"icon\" name=\"checkmark-circle\" style=\"color: green;\" *ngIf=\"passwordVal.upperCase\"></ion-icon>\n\t\t\t\t\t\t<ion-icon class=\"icon\" name=\"checkmark-circle\" *ngIf=\"!passwordVal.upperCase\"></ion-icon>\n\t\t\t\t\t\t<ion-label class=\"label\">Have at least one capital letter</ion-label><br>\n\t\t\t\t\t\t\n\t\t\t\t\t\t<ion-icon class=\"icon\" name=\"checkmark-circle\" style=\"color: green;\" *ngIf=\"passwordVal.lowerCase\"></ion-icon>\n\t\t\t\t\t\t<ion-icon class=\"icon\" name=\"checkmark-circle\" *ngIf=\"!passwordVal.lowerCase\"></ion-icon>\n\t\t\t\t\t\t<ion-label class=\"label\">Have at least one small letter</ion-label><br>\n\t\t\t\t\t</ion-list>\n          <div style=\"text-align: center; margin-top: 10px;\">\n            <ion-button class=\"btn-cancel\" color=\"medium\" size=\"large\" [routerLink]=\"['/login']\">Cancel</ion-button>\n            <ion-button color=\"secondary\" size=\"large\" (click)=\"resetPassword()\" [disabled]=\"!forgotPasswordFormStepTwo.valid || !cPasswordVal\" >Reset Password</ion-button>\n          </div>\n        </form>\n      </div>\n  </ion-content>\n</ng-template>\n\n\n");

/***/ }),

/***/ 6770:
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/login/login.page.html ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-content class=\"background\">\n\t<div class=\"title\">\n         <h2 class=\"title-spy\">\n            <img src=\"assets/icon/spytrack_logo.PNG\" style=\"width: 250px;\">\n        </h2>\n    </div>\n\n    <div id=\"container\" *ngIf=\"!userIsInactivate\">\n      <ion-item id=\"rounded\">\n        <ion-label position=\"floating\">Username or Email</ion-label>\n        <ion-input [(ngModel)]=\"username\"></ion-input>\n      </ion-item>\n      <ion-item id=\"rounded\">\n        <ion-label position=\"floating\">Password</ion-label>\n        <ion-input type=\"password\" [(ngModel)]=\"password\"></ion-input>\n      </ion-item>\n      <section class=\"ion-btn\">\n        <ion-button type=\"submit\" class=\"btn-login\" expand=\"block\" color=\"secondary\"  (click)=\"login()\">Login</ion-button>\n      </section>\n        <div class=\"list\">\n            <p [routerLink]=\"['/registration']\">New to Thorton?</p>\n            <p [routerLink]=\"['/login/forgot-password']\">Forgot Password?</p>\n        </div>\n    </div>\n\n    <div id=\"container\" *ngIf=\"userIsInactivate\">\n      <div style=\"color: white\">\n        <p class=\"form-title\">Please check you inbox for verify your email id.</p>\n        <p class=\"form-desc\">Didn't get an email please click <a href=\"javascript:;\" (click)=\"resendVarificationEmail()\">here</a> to resend the email.</p>\n        <p class=\"form-desc\">Already have an account? or verification done ? </p>\n      </div>\n      <div style=\"text-align: center;\">\n        <ion-button (click)=\"userIsInactivate = false\" color=\"light\"> Back </ion-button>\n      </div>\n    </div>\n</ion-content>\n");

/***/ })

}]);
//# sourceMappingURL=src_app_login_login_module_ts.js.map