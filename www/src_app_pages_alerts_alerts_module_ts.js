(self["webpackChunkspytrack"] = self["webpackChunkspytrack"] || []).push([["src_app_pages_alerts_alerts_module_ts"],{

/***/ 2572:
/*!*******************************************************!*\
  !*** ./src/app/pages/alerts/alerts-routing.module.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AlertsPageRoutingModule": () => (/* binding */ AlertsPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _alerts_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./alerts.page */ 99);




const routes = [
    {
        path: '',
        component: _alerts_page__WEBPACK_IMPORTED_MODULE_0__.AlertsPage
    }
];
let AlertsPageRoutingModule = class AlertsPageRoutingModule {
};
AlertsPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], AlertsPageRoutingModule);



/***/ }),

/***/ 8659:
/*!***********************************************!*\
  !*** ./src/app/pages/alerts/alerts.module.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AlertsPageModule": () => (/* binding */ AlertsPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _alerts_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./alerts-routing.module */ 2572);
/* harmony import */ var ngx_owl_carousel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ngx-owl-carousel */ 3189);
/* harmony import */ var ngx_owl_carousel__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(ngx_owl_carousel__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @fortawesome/angular-fontawesome */ 5002);
/* harmony import */ var _alerts_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./alerts.page */ 99);
/* harmony import */ var _filter_filter_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./filter/filter.component */ 9912);










let AlertsPageModule = class AlertsPageModule {
};
AlertsPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_6__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_8__.IonicModule,
            _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_9__.FontAwesomeModule,
            _alerts_routing_module__WEBPACK_IMPORTED_MODULE_0__.AlertsPageRoutingModule,
            ngx_owl_carousel__WEBPACK_IMPORTED_MODULE_1__.OwlModule
        ],
        declarations: [_alerts_page__WEBPACK_IMPORTED_MODULE_2__.AlertsPage, _filter_filter_component__WEBPACK_IMPORTED_MODULE_3__.AlertFilterComponent]
    })
], AlertsPageModule);



/***/ }),

/***/ 99:
/*!*********************************************!*\
  !*** ./src/app/pages/alerts/alerts.page.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AlertsPage": () => (/* binding */ AlertsPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_alerts_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./alerts.page.html */ 1853);
/* harmony import */ var _alerts_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./alerts.page.scss */ 1112);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ 5257);
/* harmony import */ var src_app_globals_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/globals.service */ 3221);
/* harmony import */ var src_app_rest_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/rest.service */ 1881);
/* harmony import */ var _filter_filter_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./filter/filter.component */ 9912);



/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/prefer-for-of */






let AlertsPage = class AlertsPage {
    constructor(modalController, loadingController, globalService, rest) {
        this.modalController = modalController;
        this.loadingController = loadingController;
        this.globalService = globalService;
        this.rest = rest;
        this.slideOptions = { items: 2, dots: false };
        this.dateF = 'EEE, MMM dd YYYY';
        this.timeF = "h:mm a";
        this.recordFetchInReq = 5;
        this.isAllRecordLoaded = false;
    }
    ngOnInit() {
    }
    ionViewWillEnter() {
        this.alertFilterSubscription = this.globalService.currectSelectedAlertsFilter.subscribe((newFilter) => {
            this.alertFilter = newFilter;
            console.log('activity filter ===>', this.alertFilter);
            this.getActivityData();
        });
    }
    getActivityData() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            this.loadingGbl = yield this.loadingController.create({
                cssClass: 'my-custom-class',
                message: 'Please wait...'
            });
            yield this.loadingGbl.present();
            const sendData = Object.assign({}, this.alertFilter);
            console.log(sendData);
            delete sendData.seletedTracker;
            if (this.alertFilter.seletedTracker !== 'any') {
                sendData.imei = this.alertFilter.seletedTracker.imei;
            }
            else {
                sendData.imei = 'any';
            }
            const startTime = new Date(sendData.start_date);
            const endTime = new Date(sendData.end_date);
            const startDate = startTime.getFullYear() + '-' + (startTime.getMonth() + 1) + '-' + startTime.getDate();
            const endDate = endTime.getFullYear() + '-' + (endTime.getMonth() + 1) + '-' + endTime.getDate();
            sendData.start_date = startDate;
            sendData.end_date = endDate;
            this.rest.get('thorton/trackers/logs?' + new URLSearchParams(sendData).toString(), '', '').pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_6__.take)(1)).subscribe((resp) => (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
                // const resp: any = this.getRecords();
                console.log('resp ====>', resp);
                if (resp.length !== 0) {
                    for (let x = 0; x < resp.length; x++) {
                        resp[x].tracker_icon_url = this.globalService.getMarkerLocation(resp[x].tracker_icon, resp[x].tracker_icon_color);
                    }
                    this.alertsData = resp;
                    this.selectedTrackerData = this.alertsData[0];
                    this.renderData();
                }
                else {
                    yield this.loadingGbl.dismiss();
                }
            }));
        });
    }
    renderData() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            this.filteredData = this.selectedTrackerData.locations.slice(0);
            this.totalRecord = this.filteredData.length;
            this.displayAlertData = this.filteredData.slice(0, this.recordFetchInReq);
            if (this.filteredData.length < this.recordFetchInReq) {
                this.isAllRecordLoaded = true;
            }
            else {
                this.isAllRecordLoaded = false;
            }
            console.log('selectedTrackerData ===>', this.filteredData);
            console.log('total rec ===>', this.totalRecord);
            if (this.owlElement) {
                this.owlElement.reInit();
            }
            yield this.loadingGbl.dismiss();
        });
    }
    loadMoreData() {
        console.log('clicked', this.displayAlertData.length, this.totalRecord);
        if (this.displayAlertData.length < this.totalRecord) {
            const newRecords = this.filteredData.slice(0).splice(this.displayAlertData.length - 1, this.recordFetchInReq);
            this.displayAlertData.push(...newRecords);
            if (this.displayAlertData.length >= this.totalRecord) {
                console.log('all record loaded!!');
                this.isAllRecordLoaded = true;
            }
        }
        else {
            console.log('all record loaded!!');
            this.isAllRecordLoaded = true;
        }
    }
    fetchData() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            this.loadingGbl = yield this.loadingController.create({
                cssClass: 'my-custom-class',
                message: 'Please wait...'
            });
            yield this.loadingGbl.present();
            this.filteredData = this.selectedTrackerData.data.slice(0);
            this.totalRecord = this.filteredData.length;
            this.displayAlertData = this.filteredData.slice(0, this.recordFetchInReq);
            if (this.filteredData.length < this.recordFetchInReq) {
                this.isAllRecordLoaded = true;
            }
            else {
                this.isAllRecordLoaded = false;
            }
            yield this.loadingGbl.dismiss();
        });
    }
    changeSelectedTracker(index) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            this.loadingGbl = yield this.loadingController.create({
                cssClass: 'my-custom-class',
                message: 'Please wait...'
            });
            yield this.loadingGbl.present();
            console.log("tracker changed index is ", index);
            this.selectedTrackerData = this.alertsData[index];
            this.renderData();
        });
    }
    presentModal() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _filter_filter_component__WEBPACK_IMPORTED_MODULE_4__.AlertFilterComponent,
                cssClass: 'filter-modal'
            });
            return yield modal.present();
        });
    }
    ionViewDidLeave() {
        var _a;
        (_a = this.alertFilterSubscription) === null || _a === void 0 ? void 0 : _a.unsubscribe();
    }
};
AlertsPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.ModalController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.LoadingController },
    { type: src_app_globals_service__WEBPACK_IMPORTED_MODULE_2__.GlobalService },
    { type: src_app_rest_service__WEBPACK_IMPORTED_MODULE_3__.RestService }
];
AlertsPage.propDecorators = {
    owlElement: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.ViewChild, args: ['owlElement',] }]
};
AlertsPage = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Component)({
        selector: 'app-alerts',
        template: _raw_loader_alerts_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_alerts_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], AlertsPage);



/***/ }),

/***/ 9912:
/*!*********************************************************!*\
  !*** ./src/app/pages/alerts/filter/filter.component.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AlertFilterComponent": () => (/* binding */ AlertFilterComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_filter_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./filter.component.html */ 3047);
/* harmony import */ var _filter_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./filter.component.scss */ 500);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var src_app_globals_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/globals.service */ 3221);
/* harmony import */ var src_app_rest_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/rest.service */ 1881);



/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/naming-convention */




let AlertFilterComponent = class AlertFilterComponent {
    constructor(rest, modalController, globalService, toastCtrl, loadingController) {
        this.rest = rest;
        this.modalController = modalController;
        this.globalService = globalService;
        this.toastCtrl = toastCtrl;
        this.loadingController = loadingController;
        this.defaultAlertFilterSort = 'none';
        this.defaultAlertFilterType = 'none';
        this.defaultFilterTracker = 'any';
        this.defaultSort = 'none';
        this.dataLoad = false;
    }
    ngOnInit() { }
    closeModal() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            const onClosedData = 'Wrapped Up!';
            yield this.modalController.dismiss(onClosedData);
        });
    }
    ionViewWillEnter() {
        this.getUserTracker();
        this.alertFilterSubscription = this.globalService.currectSelectedAlertsFilter.subscribe((currentFilter) => {
            this.defaultAlertFilterSort = (currentFilter.sort_by && this.defaultSort) ? currentFilter.sort_by + currentFilter.sort.charAt(0) : 'none';
            this.defaultAlertFilterType = currentFilter.type || 'none';
            this.defaultFilterTracker = currentFilter.seletedTracker || 'any';
            this.defaultSort = currentFilter.sort || 'none';
            this.fromDate = currentFilter.start_date || (new Date()).toISOString();
            this.toDate = currentFilter.end_date || (new Date()).toISOString();
        });
    }
    getUserTracker() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.gblLoading = yield this.loadingController.create({
                cssClass: 'my-custom-class',
                message: 'Please wait...',
            });
            yield this.gblLoading.present();
            this.rest.get('thorton/trackers/user', '', '').subscribe((resp) => (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
                yield this.gblLoading.dismiss();
                if (resp.length != 0) {
                    this.userTrackers = resp;
                    console.log(this.userTrackers);
                    this.dataLoad = true;
                    this.gblLoading.dismiss();
                }
                else {
                    this.dataLoad = true;
                }
            }));
        });
    }
    applyActivityFilters() {
        console.log(this.defaultAlertFilterSort, this.defaultAlertFilterType, this.defaultFilterTracker, this.fromDate, this.toDate);
        if (this.defaultAlertFilterSort !== "none") {
            this.defaultSort = this.defaultAlertFilterSort.charAt(this.defaultAlertFilterSort.length - 1) === 'a' ? 'ase' : 'desc';
            this.defaultAlertFilterSort = this.defaultAlertFilterSort.slice(0, -1);
        }
        this.globalService.userChangedAlertsFilter({
            seletedTracker: this.defaultFilterTracker,
            type: this.defaultAlertFilterType,
            sort_by: this.defaultAlertFilterSort,
            start_date: this.fromDate,
            end_date: this.toDate,
            sort: this.defaultSort,
        });
        this.closeModal();
    }
    resetFilter() {
        this.globalService.setAlertFilterAsDefault();
        this.closeModal();
    }
    ionViewDidLeave() {
        var _a;
        (_a = this.alertFilterSubscription) === null || _a === void 0 ? void 0 : _a.unsubscribe();
    }
};
AlertFilterComponent.ctorParameters = () => [
    { type: src_app_rest_service__WEBPACK_IMPORTED_MODULE_3__.RestService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.ModalController },
    { type: src_app_globals_service__WEBPACK_IMPORTED_MODULE_2__.GlobalService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.ToastController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.LoadingController }
];
AlertFilterComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-filter',
        template: _raw_loader_filter_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_filter_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], AlertFilterComponent);



/***/ }),

/***/ 1112:
/*!***********************************************!*\
  !*** ./src/app/pages/alerts/alerts.page.scss ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("#filter {\n  font-size: 21px;\n  padding-right: 0.5em;\n}\n\nh4 {\n  text-align: center;\n  width: 100%;\n}\n\n.alert-icon {\n  width: 40px;\n}\n\n.alert-main-text {\n  font-size: 12px;\n}\n\n.header-row .alert-main-text {\n  width: 77%;\n  font-weight: 600;\n  color: black;\n}\n\n.alert-main-box {\n  padding-right: 2px;\n}\n\n.alert-ellipses {\n  float: right;\n}\n\n.ellipsis-icon {\n  font-size: 20px;\n}\n\n.row-one-line {\n  height: 40px;\n  border-bottom: 1px solid lightgray;\n}\n\n.row-two-line {\n  height: 60px;\n}\n\n.main-info {\n  font-size: 11px;\n  font-weight: 550;\n}\n\n.child-info {\n  font-size: 9px;\n}\n\n.padding-top-5 {\n  padding-top: 5px;\n}\n\n.custom-owl-item {\n  text-align: center !important;\n  box-shadow: 2px 0 5px -2px;\n}\n\n.btn-row {\n  width: 100%;\n  margin-bottom: 5px;\n}\n\n.load-more-btn {\n  margin: 0 auto 10px auto;\n  text-align: center;\n}\n\n.text-cap {\n  text-transform: capitalize;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFsZXJ0cy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxlQUFBO0VBQ0Esb0JBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0VBQ0EsV0FBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtBQUNKOztBQUVBO0VBQ0ksZUFBQTtBQUNKOztBQUVBO0VBQ0ksVUFBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtBQUNKOztBQUlBO0VBQ0ksa0JBQUE7QUFESjs7QUFJQTtFQUVJLFlBQUE7QUFGSjs7QUFLQTtFQUNJLGVBQUE7QUFGSjs7QUFLQTtFQUNJLFlBQUE7RUFDQSxrQ0FBQTtBQUZKOztBQUtBO0VBQ0ksWUFBQTtBQUZKOztBQVNBO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0FBTko7O0FBU0E7RUFDSSxjQUFBO0FBTko7O0FBU0E7RUFDSSxnQkFBQTtBQU5KOztBQVNBO0VBQ0ksNkJBQUE7RUFDQSwwQkFBQTtBQU5KOztBQVNBO0VBQ0ksV0FBQTtFQUNBLGtCQUFBO0FBTko7O0FBU0E7RUFDSSx3QkFBQTtFQUNBLGtCQUFBO0FBTko7O0FBU0E7RUFDSSwwQkFBQTtBQU5KIiwiZmlsZSI6ImFsZXJ0cy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjZmlsdGVyIHtcbiAgICBmb250LXNpemU6IDIxcHg7XG4gICAgcGFkZGluZy1yaWdodDogMC41ZW07XG59XG5cbmg0IHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbi5hbGVydC1pY29uIHtcbiAgICB3aWR0aDogNDBweDtcbn1cblxuLmFsZXJ0LW1haW4tdGV4dCB7XG4gICAgZm9udC1zaXplOiAxMnB4O1xufVxuXG4uaGVhZGVyLXJvdyAuYWxlcnQtbWFpbi10ZXh0IHtcbiAgICB3aWR0aDogNzclO1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgY29sb3I6IGJsYWNrO1xufVxuXG5cblxuLmFsZXJ0LW1haW4tYm94IHtcbiAgICBwYWRkaW5nLXJpZ2h0OiAycHg7XG59XG5cbi5hbGVydC1lbGxpcHNlcyB7XG4gIFxuICAgIGZsb2F0OiByaWdodDtcbn1cblxuLmVsbGlwc2lzLWljb24ge1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbn1cblxuLnJvdy1vbmUtbGluZSB7XG4gICAgaGVpZ2h0OiA0MHB4O1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG59XG5cbi5yb3ctdHdvLWxpbmUge1xuICAgIGhlaWdodDogNjBweDtcbn1cblxuLy8gLmFsZXJ0LW1haW4tYm94IGlvbi1yb3cge1xuLy8gICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyYXk7XG4vLyB9XG5cbi5tYWluLWluZm8ge1xuICAgIGZvbnQtc2l6ZTogMTFweDtcbiAgICBmb250LXdlaWdodDogNTUwO1xufVxuXG4uY2hpbGQtaW5mb3tcbiAgICBmb250LXNpemU6IDlweDtcbn1cblxuLnBhZGRpbmctdG9wLTV7XG4gICAgcGFkZGluZy10b3A6IDVweDtcbn1cblxuLmN1c3RvbS1vd2wtaXRlbSB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyICFpbXBvcnRhbnQ7XG4gICAgYm94LXNoYWRvdzogMnB4IDAgNXB4IC0ycHggXG59XG5cbi5idG4tcm93e1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIG1hcmdpbi1ib3R0b206IDVweDtcbn1cblxuLmxvYWQtbW9yZS1idG4ge1xuICAgIG1hcmdpbjogMCBhdXRvIDEwcHggYXV0bztcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi50ZXh0LWNhcCB7XG4gICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG59Il19 */");

/***/ }),

/***/ 500:
/*!***********************************************************!*\
  !*** ./src/app/pages/alerts/filter/filter.component.scss ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("ion-card {\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImZpbHRlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7QUFDSiIsImZpbGUiOiJmaWx0ZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY2FyZCB7XG4gICAgd2lkdGg6IDEwMCU7XG59Il19 */");

/***/ }),

/***/ 1853:
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/alerts/alerts.page.html ***!
  \*************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>Alerts</ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button menu=\"main-menu\"></ion-menu-button>\n    </ion-buttons>\n    <ion-buttons id=\"filter\" slot=\"end\" (click)=\"presentModal()\">\n      <ion-icon name=\"settings\"></ion-icon>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-card style=\"margin-top: 5px\">\n    <owl-carousel\n      #owlElement\n      [options]=\"slideOptions\"\n      [carouselClasses]=\"['owl-theme', 'sliding']\"\n      *ngIf=\"alertsData?.length > 1; else noTrackerFound\"\n    >\n      <div\n        class=\"item custom-owl-item\"\n        *ngFor=\"let activityTracker of alertsData; let index = index\"\n        (click)=\"changeSelectedTracker(index)\"\n      >\n        <div style=\"align-content: center\">\n          <div>\n            <img\n              class=\"tracker-icon-view\"\n              [src]=\"activityTracker.tracker_icon_url\"\n              style=\"width: 20%; margin: 0 auto\"\n            />\n          </div>\n          {{ activityTracker.tracker_name }}\n        </div>\n      </div>\n    </owl-carousel>\n  </ion-card>\n  <ion-grid *ngIf=\"displayAlertData?.length > 0\">\n    <ion-card *ngFor=\"let alert of displayAlertData\">\n      <ion-card-content class=\"alert-main-box\">\n        <ion-row class=\"row-one-line header-row\">\n          <div class=\"alert-icon\">\n            <ion-avatar>\n              <fa-icon\n                *ngIf=\"alert.type === 'trip_start'\"\n                class=\"tracker-sample-view\"\n                icon=\"play-circle\"\n                style=\"margin-right: 10px\"\n              ></fa-icon>\n              <fa-icon\n                *ngIf=\"alert.type === 'trip_end'\"\n                class=\"tracker-sample-view\"\n                icon=\"pause-circle\"\n                style=\"margin-right: 10px\"\n              ></fa-icon>\n              <fa-icon\n                *ngIf=\"alert.type === 'low_battery'\"\n                class=\"tracker-sample-view\"\n                icon=\"battery-quarter\"\n                style=\"margin-right: 10px\"\n              ></fa-icon>\n            </ion-avatar>\n          </div>\n          <div class=\"alert-main-text text-cap\">\n            {{alert?.type?.split(\"_\")?.join(\" \")}}\n          </div>\n          <!-- <div class=\"alert-ellipses\">\n            <fa-icon\n              class=\"ellipsis-icon\"\n              icon=\"ellipsis\"\n              style=\"margin-right: 10px\"\n            ></fa-icon>\n          </div> -->\n        </ion-row>\n        <ion-row class=\"row-one-line padding-top-5\">\n          <div class=\"alert-icon\">\n            <ion-avatar>\n              <fa-icon\n                class=\"tracker-sample-view\"\n                icon=\"circle-info\"\n                style=\"margin-right: 10px\"\n              ></fa-icon>\n            </ion-avatar>\n          </div>\n          <div class=\"alert-main-text\">\n            <div class=\"main-info\">{{alert.details}}</div>\n            <div class=\"child-info\">Details</div>\n          </div>\n          <!-- <div class=\"alert-ellipses\">\n           \n          </div> -->\n        </ion-row>\n        <ion-row class=\"row-one-line padding-top-5\">\n          <div class=\"alert-icon\">\n            <ion-avatar>\n              <fa-icon\n                class=\"tracker-sample-view\"\n                icon=\"battery-quarter\"\n                style=\"margin-right: 10px\"\n              ></fa-icon>\n            </ion-avatar>\n          </div>\n          <div class=\"alert-main-text\">\n            <div class=\"main-info\">{{selectedTrackerData.tracker_name}}</div>\n            <div class=\"child-info\">Tracker</div>\n          </div>\n        </ion-row>\n        <ion-row class=\"row-one-line padding-top-5\">\n          <div class=\"alert-icon\">\n            <ion-avatar>\n              <fa-icon\n                class=\"tracker-sample-view\"\n                icon=\"calendar-days\"\n                style=\"margin-right: 10px\"\n              ></fa-icon>\n            </ion-avatar>\n          </div>\n          <div class=\"alert-main-text\">\n            <div class=\"main-info\">{{alert.timestamp | date: dateF}}</div>\n            <div class=\"child-info\">Date</div>\n          </div>\n        </ion-row>\n        <ion-row class=\"row-one-line padding-top-5\">\n          <div class=\"alert-icon\">\n            <ion-avatar>\n              <fa-icon\n                class=\"tracker-sample-view\"\n                icon=\"clock\"\n                style=\"margin-right: 10px\"\n              ></fa-icon>\n            </ion-avatar>\n          </div>\n          <div class=\"alert-main-text\">\n            <div class=\"main-info\">{{alert.timestamp | date: timeF}}</div>\n            <div class=\"child-info\">Time</div>\n          </div>\n        </ion-row>\n        <ion-row class=\"row-two-line padding-top-5\">\n          <div class=\"alert-icon\">\n            <ion-avatar>\n              <fa-icon\n                class=\"tracker-sample-view\"\n                icon=\"location-dot\"\n                style=\"margin-right: 10px\"\n              ></fa-icon>\n            </ion-avatar>\n          </div>\n          <div class=\"alert-main-text\">\n            <div class=\"main-info\">{{alert?.address?.split(',')[0]}}</div>\n            <div class=\"main-info\">\n              {{alert?.address?.split(',')[1]}} , {{\n              alert?.address?.split(',')[2]}}\n            </div>\n            <div class=\"child-info\">location</div>\n          </div>\n        </ion-row>\n      </ion-card-content>\n    </ion-card>\n    <ion-row>\n      <ion-button\n        class=\"load-more-btn\"\n        color=\"dark\"\n        *ngIf=\"!isAllRecordLoaded\"\n        (click)=\"loadMoreData()\"\n      >\n        Load more\n      </ion-button>\n    </ion-row>\n  </ion-grid>\n\n  <ion-grid *ngIf=\"alertsData?.length > 0 && displayAlertData?.length === 0\">\n    <ion-row>\n      <div style=\"margin: 0 auto\">\n        No alerts found for this tracker on this filter.\n      </div>\n    </ion-row>\n  </ion-grid>\n\n  <ion-grid *ngIf=\"!alertsData || alertsData.length === 0\">\n    <ion-row>\n      <div style=\"margin: 0 auto\">No Tracker found for this filter.</div>\n    </ion-row>\n  </ion-grid>\n  <!-- <div> No alerts found for this tracker </div> -->\n</ion-content>\n");

/***/ }),

/***/ 3047:
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/alerts/filter/filter.component.html ***!
  \*************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\n  <ion-toolbar color=\"white\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"alerts\" (click)=\"closeModal()\"></ion-back-button>\n    </ion-buttons>\n    <div style=\"text-align: center;\">      \n      <span id=\"title\" style=\"margin-right: 50px ;\" ><strong>Filters</strong></span>\n    </div>\n  </ion-toolbar>\n\n</ion-header>\n\n<ion-content class=\"ion-padding\">\n  <ion-grid>\n    <ion-row>\n      <ion-card>\n        <ion-card-header>\n          <ion-card-subtitle>Sort By</ion-card-subtitle>\n        </ion-card-header>\n        <ion-card-content>\n          <ion-list>\n            <ion-radio-group [(ngModel)]=\"defaultAlertFilterSort\" mode=\"md\">\n              <ion-item>\n                <ion-label><p>None</p></ion-label>\n                <ion-radio slot=\"start\" value=\"None\"></ion-radio>\n              </ion-item>\n              <ion-item>\n                <ion-label><p>Type - Ascending</p></ion-label>\n                <ion-radio slot=\"start\" value=\"typea\"></ion-radio>\n              </ion-item>\n              <ion-item>\n                <ion-label><p>Type - Descending</p></ion-label>\n                <ion-radio slot=\"start\" value=\"typed\"></ion-radio>\n              </ion-item>\n              <ion-item>\n                <ion-label><p>Tracker - Ascending</p></ion-label>\n                <ion-radio slot=\"start\" value=\"trackera\"></ion-radio>\n              </ion-item>\n              <ion-item>\n                <ion-label><p>Tracker - Descending</p></ion-label>\n                <ion-radio slot=\"start\" value=\"trackerd\"></ion-radio>\n              </ion-item>\n              <ion-item>\n                <ion-label><p>Date - Ascending</p></ion-label>\n                <ion-radio slot=\"start\" value=\"datea\"></ion-radio>\n              </ion-item>\n              <ion-item>\n                <ion-label><p>Date - Descending</p></ion-label>\n                <ion-radio slot=\"start\" value=\"dated\"></ion-radio>\n              </ion-item>\n              <ion-item>\n                <ion-label><p>Location - Ascending</p></ion-label>\n                <ion-radio slot=\"start\" value=\"locationa\"></ion-radio>\n              </ion-item>\n              <ion-item>\n                <ion-label><p>Location - Descending</p></ion-label>\n                <ion-radio slot=\"start\" value=\"locationd\"></ion-radio>\n              </ion-item>\n            </ion-radio-group>\n          </ion-list>\n        </ion-card-content>\n      </ion-card>\n      <ion-card>\n        <ion-card-header>\n          <ion-card-subtitle>Type</ion-card-subtitle>\n        </ion-card-header>\n        <ion-card-content>\n          <ion-list>\n            <ion-radio-group [(ngModel)]=\"defaultAlertFilterType\" mode=\"md\">\n            <ion-item>\n              <ion-label class=\"alerts-label\">None</ion-label>\n              <ion-radio slot=\"start\" value=\"none\"></ion-radio>\n            </ion-item>\n            <ion-item>\n              <ion-label class=\"alerts-label\">Low Battery</ion-label>\n                <ion-radio slot=\"start\" value=\"low_battery\"></ion-radio>\n            </ion-item>\n            <ion-item>\n              <ion-label class=\"alerts-label\">Trip Finished</ion-label>\n                <ion-radio slot=\"start\" value=\"trid_finished\"></ion-radio>\n            </ion-item>\n            <ion-item>\n              <ion-label class=\"alerts-label\">Trip Started</ion-label>\n                <ion-radio slot=\"start\" value=\"trip_started\"></ion-radio>\n            </ion-item>\n          </ion-radio-group>\n        </ion-list>\n        </ion-card-content>\n      </ion-card>\n      <ion-card>\n        <ion-card-header>\n          <ion-card-subtitle>Tracker</ion-card-subtitle>\n        </ion-card-header>\n        <ion-card-content>\n          <ion-list>\n            <ion-radio-group [(ngModel)]=\"defaultFilterTracker\" mode=\"md\">\n              <ion-item>\n                <ion-label><p>All</p></ion-label>\n                <ion-radio slot=\"start\" value=\"any\"></ion-radio>\n              </ion-item>\n              <ion-item *ngFor=\"let tracker of userTrackers\">\n                <ion-label><p>{{ tracker.tracker_name }}</p></ion-label>\n                  <ion-radio slot=\"start\" [value]=\"tracker\"></ion-radio>\n              </ion-item>\n            </ion-radio-group>\n          </ion-list>\n        </ion-card-content>\n      </ion-card>\n    </ion-row>\n    <ion-row>\n      <ion-card>\n        <ion-card-header>\n          <ion-card-subtitle>Date</ion-card-subtitle>\n        </ion-card-header>\n        <ion-card-content>\n          <div>\n          <ion-label> From :</ion-label>\n          <ion-datetime presentation=\"date\" displayFormat=\"DD/MM/YYYY\" placeholder=\"From Date\" id=\"date-selection\" [(ngModel)]=\"fromDate\"></ion-datetime>\n        </div>\n        <div>\n          <ion-label> To :</ion-label>\n          <ion-datetime presentation=\"date\" displayFormat=\"DD/MM/YYYY\" placeholder=\"To Date\"  id=\"date-selection\" [(ngModel)]=\"toDate\"></ion-datetime>\n        </div>\n        </ion-card-content>\n      </ion-card>      \n    </ion-row>\n  </ion-grid>\n</ion-content>\n\n<ion-footer>\n  <ion-toolbar>\n    <ion-button id=\"pref\" slot=\"start\" color=\"dark\" (click)=\"resetFilter()\">\n      Reset\n    </ion-button>\n    <ion-button slot=\"end\" color=\"dark\" (click)=\"applyActivityFilters()\">\n      Apply filters\n    </ion-button>\n  </ion-toolbar>\n</ion-footer>");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_alerts_alerts_module_ts.js.map