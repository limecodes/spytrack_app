(self["webpackChunkspytrack"] = self["webpackChunkspytrack"] || []).push([["src_app_pages_activation_activation_module_ts"],{

/***/ 168:
/*!***************************************************************!*\
  !*** ./src/app/pages/activation/activation-routing.module.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ActivationPageRoutingModule": () => (/* binding */ ActivationPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _activation_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./activation.page */ 8226);




const routes = [
    {
        path: '',
        component: _activation_page__WEBPACK_IMPORTED_MODULE_0__.ActivationPage
    }
];
let ActivationPageRoutingModule = class ActivationPageRoutingModule {
};
ActivationPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], ActivationPageRoutingModule);



/***/ }),

/***/ 2998:
/*!*******************************************************!*\
  !*** ./src/app/pages/activation/activation.module.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ActivationPageModule": () => (/* binding */ ActivationPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _activation_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./activation-routing.module */ 168);
/* harmony import */ var _activation_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./activation.page */ 8226);
/* harmony import */ var _live_tracking_get_location_url_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../live-tracking/get-location-url.service */ 2311);








let ActivationPageModule = class ActivationPageModule {
};
ActivationPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.ReactiveFormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonicModule,
            _activation_routing_module__WEBPACK_IMPORTED_MODULE_0__.ActivationPageRoutingModule
        ],
        declarations: [_activation_page__WEBPACK_IMPORTED_MODULE_1__.ActivationPage],
        providers: [_live_tracking_get_location_url_service__WEBPACK_IMPORTED_MODULE_2__.GetLocationUrlService]
    })
], ActivationPageModule);



/***/ }),

/***/ 8226:
/*!*****************************************************!*\
  !*** ./src/app/pages/activation/activation.page.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ActivationPage": () => (/* binding */ ActivationPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_activation_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./activation.page.html */ 7173);
/* harmony import */ var _activation_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./activation.page.scss */ 8792);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var src_app_globals_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/globals.service */ 3221);
/* harmony import */ var src_app_rest_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/rest.service */ 1881);









//import { LiveTrackingPage } from '../live-tracking/live-tracking.page';
let ActivationPage = class ActivationPage {
    constructor(_formBuilder, router, toastCtrl, global, rest) {
        this._formBuilder = _formBuilder;
        this.router = router;
        this.toastCtrl = toastCtrl;
        this.global = global;
        this.rest = rest;
        this.slideOpts = {
            initialSlide: 0,
            speed: 400,
            allowTouchMove: false
        };
        this.user = '';
        this.isCheckedAdmin = false;
        this.isCheckedAPN = false;
        this.isCheckedIP = false;
        this.isCheckedDomain = false;
        this.isCheckedPulse = false;
        this.isCheckedUpload = false;
        this.isCheckedSleep = false;
        this.showSpinner = false;
        this.status = 'wait';
        this.tPurpose = 'personal';
        this.tImei = '';
        this.tName = '';
    }
    ngOnInit() {
        this.global.currentMessageSubscriberSummary.subscribe((data) => {
            if (data.isRefresh) {
                this.user = this.global.firstname;
                this.userDetails = this.global.userDetails;
            }
            else {
            }
        });
        console.log(this.userDetails);
        // setTimeout(()=>{
        //   this.swipeNext();
        // }, 5000);
        // for(let x = 0; x <= 7; x++) {
        //   setTimeout(()=>{
        //     if(x == 1) {
        //       this.isCheckedAdmin = true;
        //     } else if(x == 2) {
        //       this.isCheckedAPN = true;
        //     } else if(x == 3) {
        //       this.isCheckedIP = true;
        //     } else if(x == 4) {
        //       this.isCheckedDomain = true;
        //     } else if(x == 5) {
        //       this.isCheckedPulse = true;
        //     } else if(x == 6) {
        //       this.isCheckedUpload = true;
        //     } else if(x == 7) {
        //       this.isCheckedSleep = true;
        //       this.swipeNext();
        //     }
        // }, (x+1)*5000);
        // }
    }
    login() {
        // this.router.navigate(['/live-tracking']);
    }
    swipeNext() {
        this.activationSlides.slideNext();
    }
    swipePrev() {
        this.activationSlides.slidePrev();
    }
    swipeInit(index) {
        this.activationSlides.slideTo(index);
    }
    openToast(message) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            if (message == 'already activated') {
                const toast = yield this.toastCtrl.create({
                    message: 'Error occured. Tracker has already been activated',
                    position: 'bottom',
                    duration: 4000,
                    color: 'danger'
                });
                toast.present();
            }
            else {
                const toast = yield this.toastCtrl.create({
                    message: 'Error occured. Please try again later.',
                    position: 'bottom',
                    duration: 4000,
                    color: 'danger'
                });
                toast.present();
            }
        });
    }
    activateTracker() {
        //this.live.displayMap = false;
        this.showSpinner = true;
        let dataToSend = {
            "imei": this.tImei,
            "tracker_name": this.tName,
            "tracking_purpose": "personal",
        };
        console.log(dataToSend);
        this.swipeNext();
        this.rest.post(dataToSend, 'thorton/activate').subscribe((resp) => {
            console.log(resp);
            this.showSpinner = false;
            this.swipeNext();
        }, error => {
            this.showSpinner = false;
            this.status = 'failed';
            if (error.error.message == 'Tracker already activated!') {
                this.openToast('already activated');
            }
            else {
                this.openToast('error');
            }
        });
    }
    retryActivate() {
        this.showSpinner = true;
        let dataToSend = {
            "imei": this.tImei,
            "tracker_name": this.tName,
            "tracking_purpose": "personal",
        };
        console.log(dataToSend);
        this.rest.post(dataToSend, 'thorton/activate').subscribe((resp) => {
            console.log(resp);
            this.showSpinner = false;
            this.swipeNext();
        }, error => {
            console.log(error);
            this.showSpinner = false;
            this.status = 'failed';
            if (error.error.message == 'Tracker already activated!') {
                this.openToast('already activated');
            }
            else {
                this.openToast('error');
            }
        });
    }
};
ActivationPage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormBuilder },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__.Router },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.ToastController },
    { type: src_app_globals_service__WEBPACK_IMPORTED_MODULE_2__.GlobalService },
    { type: src_app_rest_service__WEBPACK_IMPORTED_MODULE_3__.RestService }
];
ActivationPage.propDecorators = {
    activationSlides: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.ViewChild, args: ['activationSlides',] }]
};
ActivationPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Component)({
        selector: 'app-activation',
        template: _raw_loader_activation_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_activation_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], ActivationPage);



/***/ }),

/***/ 8792:
/*!*******************************************************!*\
  !*** ./src/app/pages/activation/activation.page.scss ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("ion-item {\n  --ion-background-color: #fff;\n}\n\n.title {\n  color: black;\n  text-align: center;\n  margin-top: 20px;\n  margin-bottom: 30px;\n  font-weight: bolder;\n}\n\n.title-spy {\n  font-weight: bold;\n}\n\n.bg-form {\n  background-color: #ffffff;\n  padding: 25px;\n  color: #212529;\n  width: 350px;\n  margin-left: auto;\n  margin-right: auto;\n}\n\n.form-title {\n  font-size: 24px;\n  line-height: 1.5;\n  text-align: center;\n}\n\n.form-desc {\n  text-align: center;\n}\n\na {\n  color: #3699ff;\n  text-decoration: none;\n}\n\n.input {\n  border: 1px solid #bdbdbd;\n  margin-top: 15px;\n}\n\n.form {\n  margin-top: 20px;\n}\n\n.list {\n  margin-top: 20px;\n}\n\n.icon {\n  color: #b5b5c3;\n  font-size: 18px;\n  margin-right: 15px;\n}\n\n.label {\n  color: #212529;\n}\n\n.button {\n  text-transform: capitalize;\n}\n\n.terms {\n  text-align: center;\n}\n\nion-list {\n  background: white;\n}\n\nion-item {\n  background: white;\n}\n\n.rounded {\n  margin-top: 1em;\n  border: 1px solid grey;\n}\n\nion-item {\n  width: 100%;\n}\n\nion-checkbox.checkbox-disabled {\n  opacity: 1;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFjdGl2YXRpb24ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUlBO0VBQ0ksNEJBQUE7QUFISjs7QUFLQTtFQUNJLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQUZKOztBQUlBO0VBQ0ksaUJBQUE7QUFESjs7QUFHQTtFQUNFLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7RUFDQSxZQUFBO0VBQ0UsaUJBQUE7RUFDQSxrQkFBQTtBQUFKOztBQUVBO0VBQ0MsZUFBQTtFQUNDLGdCQUFBO0VBQ0Esa0JBQUE7QUFDRjs7QUFDQTtFQUNDLGtCQUFBO0FBRUQ7O0FBQUE7RUFDQyxjQUFBO0VBQ0EscUJBQUE7QUFHRDs7QUFEQTtFQUNDLHlCQUFBO0VBQ0EsZ0JBQUE7QUFJRDs7QUFEQTtFQUNDLGdCQUFBO0FBSUQ7O0FBRkE7RUFDQyxnQkFBQTtBQUtEOztBQUhBO0VBQ0MsY0FBQTtFQUNBLGVBQUE7RUFDQyxrQkFBQTtBQU1GOztBQUpBO0VBQ0MsY0FBQTtBQU9EOztBQUxBO0VBQ0MsMEJBQUE7QUFRRDs7QUFOQTtFQUNDLGtCQUFBO0FBU0Q7O0FBTkE7RUFDSSxpQkFBQTtBQVNKOztBQU5BO0VBQ0ksaUJBQUE7QUFTSjs7QUFOQTtFQUNJLGVBQUE7RUFDQSxzQkFBQTtBQVNKOztBQU5BO0VBQ0ksV0FBQTtBQVNKOztBQU5BO0VBQ0ksVUFBQTtBQVNKIiwiZmlsZSI6ImFjdGl2YXRpb24ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gaW9uLWNvbnRlbnQuYmFja2dyb3VuZCB7XG4vLyAgICAgLS1pb24tYmFja2dyb3VuZC1jb2xvcjogIzE4MUMzMjtcbi8vIH1cblxuaW9uLWl0ZW0ge1xuICAgIC0taW9uLWJhY2tncm91bmQtY29sb3I6ICNmZmY7XG59XG4udGl0bGUge1xuICAgIGNvbG9yOiBibGFjaztcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICBtYXJnaW4tYm90dG9tOiAzMHB4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG59XG4udGl0bGUtc3B5IHtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5iZy1mb3Jte1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmOyBcbiAgcGFkZGluZzogMjVweDtcbiAgY29sb3I6ICMyMTI1Mjk7XG4gIHdpZHRoOiAzNTBweDtcbiAgICBtYXJnaW4tbGVmdDogYXV0bztcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XG59XG4uZm9ybS10aXRsZXtcblx0Zm9udC1zaXplOiAyNHB4OyBcbiAgbGluZS1oZWlnaHQ6IDEuNTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmZvcm0tZGVzY3tcblx0dGV4dC1hbGlnbjogY2VudGVyO1xufVxuYXtcblx0Y29sb3I6ICMzNjk5ZmY7XG5cdHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbn1cbi5pbnB1dHtcblx0Ym9yZGVyOiAxcHggc29saWQgI2JkYmRiZDtcblx0bWFyZ2luLXRvcDogMTVweDtcblx0XG59XG4uZm9ybXtcblx0bWFyZ2luLXRvcDogMjBweDtcbn1cbi5saXN0e1xuXHRtYXJnaW4tdG9wOiAyMHB4O1xufVxuLmljb257XG5cdGNvbG9yOiAjYjViNWMzOyBcblx0Zm9udC1zaXplOiAxOHB4O1xuXHQgbWFyZ2luLXJpZ2h0OiAxNXB4O1xufVxuLmxhYmVse1xuXHRjb2xvcjogIzIxMjUyOTtcbn1cbi5idXR0b257XG5cdHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xufVxuLnRlcm1ze1xuXHR0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbmlvbi1saXN0e1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xufVxuXG5pb24taXRlbSB7XG4gICAgYmFja2dyb3VuZDogd2hpdGU7XG59XG5cbi5yb3VuZGVkIHtcbiAgICBtYXJnaW4tdG9wOiAxZW07XG4gICAgYm9yZGVyOiAxcHggc29saWQgZ3JleTtcbn1cblxuaW9uLWl0ZW0ge1xuICAgIHdpZHRoOiAxMDAlO1xufVxuXG5pb24tY2hlY2tib3guY2hlY2tib3gtZGlzYWJsZWQge1xuICAgIG9wYWNpdHkgOiAxO1xufSJdfQ== */");

/***/ }),

/***/ 7173:
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/activation/activation.page.html ***!
  \*********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\n  <ion-toolbar>\n    <ion-title text-wrap><strong>Tracker Activation</strong></ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button menu=\"main-menu\"></ion-menu-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n<ion-content class=\"background\"> \n\t<!-- <div class=\"title\">\n        <h2 class=\"title-spy\">\n            Activate a tracker\n        </h2>\n    </div> -->\n\t<ion-slides pager=\"true\" [options]=\"slideOpts\" #activationSlides style=\"height: 100%;\">\n    <ion-slide>\n      <ion-content class=\"ion-padding\">\n        <ion-grid>\n          <ion-row>\n            <h3 style=\"text-align: center; width: 100%; font-size: 18px;\">Hi {{ user }}! Let’s set up your tracking device</h3>\n          </ion-row>\n            <ion-card>\n              <ion-card-header>\n                <ion-card-title>Device Details</ion-card-title>\n              </ion-card-header>\n              <ion-card-content>\n                <ion-row>\n                  <ion-item class=\"input-details\">\n                    <ion-label position=\"floating\" style=\"color: grey;\" text-wrap>Tracking Device IMEI#</ion-label>\n                    <ion-input [(ngModel)]=\"tImei\" required></ion-input>\n                  </ion-item>\n                </ion-row>\n                <ion-row>\n                  <ion-item lines=\"none\">\n                    <ion-list style=\"width: 100%;\">\n                      <ion-radio-group [(ngModel)]=\"tPurpose\" mode=\"md\">\n                        <ion-list-header>\n                          <ion-label text-wrap>\n                            Tracking Purpose\n                          </ion-label>\n                        </ion-list-header>\n                        <ion-item lines=\"none\">\n                          <ion-label text-wrap>Personal Use</ion-label>\n                          <ion-radio slot=\"start\" value=\"personal\"></ion-radio>\n                        </ion-item>\n                        <ion-item lines=\"none\">\n                          <ion-label text-wrap>Business Use</ion-label>\n                          <ion-radio slot=\"start\" value=\"business\"></ion-radio>\n                        </ion-item>\n                      </ion-radio-group>\n                    </ion-list>\n                  </ion-item>\n                </ion-row>\n                <ion-row>\n                  <ion-item class=\"input-details\">\n                    <ion-label position=\"floating\" style=\"color: grey;\">Tracker Name</ion-label>\n                    <ion-input [(ngModel)]=\"tName\" required></ion-input>\n                  </ion-item>\n                </ion-row>\n              </ion-card-content>\n              <div style=\"margin-top: 15px; margin-bottom: 15px;\">\n                <ion-button class=\"button\" color=\"dark\" (click)=\"activateTracker()\" [disabled]=\"tImei == '' || tName == ''\">Activate</ion-button>\n              </div>\n            </ion-card>\n        </ion-grid>\n      </ion-content>\n    </ion-slide>\n    <ion-slide>\n      <ion-content class=\"ion-padding\">\n        <ion-grid>\n          <!-- <ion-row>\n            <h3 style=\"text-align: center; width: 100%; font-size: 18px;\">Setting up your device ......</h3>\n          </ion-row> -->\n            <ion-card>\n              <ion-card-header>\n                <ion-card-subtitle *ngIf=\"status == 'wait'\">Please wait while we set up your device  ... </ion-card-subtitle>\n                <ion-card-subtitle *ngIf=\"status == 'failed'\">Error occured while activating your tracker</ion-card-subtitle>\n              </ion-card-header>\n              <ion-card-content>\n                <ion-spinner name=\"lines\" *ngIf=\"showSpinner\"></ion-spinner>\n                <ion-grid>\n                  <ion-row>\n                    <ion-col>\n                      <div style=\"margin-top: 15px; margin-bottom: 15px;\" *ngIf=\"status == 'failed'\">\n                        <ion-button expand=\"full\" class=\"button\" color=\"dark\" (click)=\"swipePrev(); status = 'wait'\">Back</ion-button>\n                      </div>\n                    </ion-col>\n                    <ion-col>\n                      <div style=\"margin-top: 15px; margin-bottom: 15px;\" *ngIf=\"status == 'failed'\">\n                        <ion-button expand=\"full\" class=\"button\" color=\"dark\" (click)=\"status = 'wait'; retryActivate()\" [disabled]=\"tImei == '' || tName == ''\">Retry</ion-button>\n                      </div>\n                    </ion-col>\n                  </ion-row>\n                </ion-grid>\n                <!-- <ion-item lines=\"none\">\n                  <ion-label text-wrap style=\"opacity: 1;\">Setting up admin</ion-label>\n                  <ion-checkbox disabled=\"true\" [(ngModel)]=\"isCheckedAdmin\"></ion-checkbox>\n                </ion-item>\n                <ion-item lines=\"none\">\n                  <ion-label text-wrap style=\"opacity: 1;\">Setting up APN</ion-label>\n                  <ion-checkbox disabled=\"true\" [(ngModel)]=\"isCheckedAPN\"></ion-checkbox>\n                </ion-item>\n                <ion-item lines=\"none\">\n                  <ion-label text-wrap style=\"opacity: 1;\">Setting up IP</ion-label>\n                  <ion-checkbox disabled=\"true\" [(ngModel)]=\"isCheckedIP\"></ion-checkbox>\n                </ion-item>\n                <ion-item lines=\"none\">\n                  <ion-label text-wrap style=\"opacity: 1;\">Setting up Domain</ion-label>\n                  <ion-checkbox disabled=\"true\" [(ngModel)]=\"isCheckedDomain\"></ion-checkbox>\n                </ion-item>\n                <ion-item lines=\"none\">\n                  <ion-label text-wrap style=\"opacity: 1;\">Setting up Pulse</ion-label>\n                  <ion-checkbox disabled=\"true\" [(ngModel)]=\"isCheckedPulse\"></ion-checkbox>\n                </ion-item>\n                <ion-item lines=\"none\">\n                  <ion-label text-wrap style=\"opacity: 1;\">Setting up Upload Frequency</ion-label>\n                  <ion-checkbox disabled=\"true\" [(ngModel)]=\"isCheckedUpload\"></ion-checkbox>\n                </ion-item>\n                <ion-item lines=\"none\">\n                  <ion-label text-wrap style=\"opacity: 1;\">Turning off sleep mode</ion-label>\n                  <ion-checkbox disabled=\"true\" [(ngModel)]=\"isCheckedSleep\"></ion-checkbox>\n                </ion-item> -->\n              </ion-card-content>\n            </ion-card>\n          </ion-grid>\n        </ion-content>\n    </ion-slide>\n    <ion-slide>\n      <ion-content class=\"ion-padding\">\n        <ion-grid>\n          <!-- <ion-row>\n            <h3 style=\"text-align: center; width: 100%; font-size: 18px;\">Setting up your device ......</h3>\n          </ion-row> -->\n          <h1>Tracker has been activated succesfully.</h1>\n          <br>\n          <h2>Proceed to <a [routerLink]=\"['/live-tracking']\"> live tracking now. </a></h2>\n          <h2>Activate <a (click)=\"swipeInit(0)\" > another tracker. </a></h2>\n            <!-- <ion-card>\n              <ion-card-header>\n                <ion-card-subtitle>Please wait while we set up your device  ...</ion-card-subtitle>\n              </ion-card-header>\n              <ion-card-content>\n\n              </ion-card-content>\n            </ion-card> -->\n          </ion-grid>\n        </ion-content>\n    </ion-slide>\n  </ion-slides>\n</ion-content>");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_activation_activation_module_ts.js.map