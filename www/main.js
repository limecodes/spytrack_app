(self["webpackChunkspytrack"] = self["webpackChunkspytrack"] || []).push([["main"],{

/***/ 8255:
/*!*******************************************************!*\
  !*** ./$_lazy_route_resources/ lazy namespace object ***!
  \*******************************************************/
/***/ ((module) => {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(() => {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = () => ([]);
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 8255;
module.exports = webpackEmptyAsyncContext;

/***/ }),

/***/ 9024:
/*!***************************************!*\
  !*** ./src/app/PipesModule.module.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PipesModule": () => (/* binding */ PipesModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _pipes_date_tras_pipe__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./pipes/date-tras.pipe */ 1291);



let PipesModule = class PipesModule {
};
PipesModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        declarations: [_pipes_date_tras_pipe__WEBPACK_IMPORTED_MODULE_0__.DateTrasPipe],
        imports: [],
        exports: [_pipes_date_tras_pipe__WEBPACK_IMPORTED_MODULE_0__.DateTrasPipe],
    })
], PipesModule);



/***/ }),

/***/ 158:
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppRoutingModule": () => (/* binding */ AppRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ 9895);



const routes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
    },
    {
        path: 'home',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_home_home_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./home/home.module */ 3467)).then(m => m.HomePageModule)
    },
    {
        path: 'live-tracking',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ngx-owl-carousel___ivy_ngcc___index_js"), __webpack_require__.e("default-src_app_pages_live-tracking_get-location-url_service_ts"), __webpack_require__.e("src_app_pages_live-tracking_live-tracking_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./pages/live-tracking/live-tracking.module */ 6888)).then(m => m.LiveTrackingPageModule)
    },
    {
        path: 'time-machine',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ngx-owl-carousel___ivy_ngcc___index_js"), __webpack_require__.e("src_app_pages_time-machine_time-machine_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./pages/time-machine/time-machine.module */ 6803)).then(m => m.TimeMachinePageModule)
    },
    {
        path: 'activities',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ngx-owl-carousel___ivy_ngcc___index_js"), __webpack_require__.e("src_app_pages_activities_activities_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./pages/activities/activities.module */ 3299)).then(m => m.ActivitiesPageModule)
    },
    {
        path: 'alerts',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ngx-owl-carousel___ivy_ngcc___index_js"), __webpack_require__.e("src_app_pages_alerts_alerts_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./pages/alerts/alerts.module */ 8659)).then(m => m.AlertsPageModule)
    },
    {
        path: 'preferences',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_pages_preferences_preferences_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./pages/preferences/preferences.module */ 855)).then(m => m.PreferencesPageModule)
    },
    {
        path: 'help',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_pages_help_help_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./pages/help/help.module */ 998)).then(m => m.HelpPageModule)
    },
    {
        path: 'login',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_login_login_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./login/login.module */ 107)).then(m => m.LoginPageModule)
    },
    {
        path: 'registration',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_registration_registration_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./registration/registration.module */ 5375)).then(m => m.RegistrationPageModule)
    },
    {
        path: 'activation',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_pages_live-tracking_get-location-url_service_ts"), __webpack_require__.e("src_app_pages_activation_activation_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./pages/activation/activation.module */ 2998)).then(m => m.ActivationPageModule)
    }
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_1__.NgModule)({
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule.forRoot(routes, { preloadingStrategy: _angular_router__WEBPACK_IMPORTED_MODULE_2__.PreloadAllModules })
        ],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule]
    })
], AppRoutingModule);



/***/ }),

/***/ 5041:
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppComponent": () => (/* binding */ AppComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./app.component.html */ 1106);
/* harmony import */ var _app_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app.component.scss */ 3069);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _globals_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./globals.service */ 3221);






let AppComponent = class AppComponent {
    constructor(router, global) {
        this.router = router;
        this.global = global;
        this.activePage = 'live-tracking';
        // this.router.navigate(['/live-tracking']);
        this.global.currentMessageSubscriberSummary.subscribe((data) => {
            if (data.isRefresh) {
                this.user = this.global.firstname;
            }
            else {
            }
        });
    }
    selectedPage(data) {
        this.activePage = data;
    }
    logout() {
        localStorage.clear();
        this.router.navigate(['/login']);
    }
};
AppComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__.Router },
    { type: _globals_service__WEBPACK_IMPORTED_MODULE_2__.GlobalService }
];
AppComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-root',
        template: _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_app_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], AppComponent);



/***/ }),

/***/ 6747:
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppModule": () => (/* binding */ AppModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/platform-browser */ 9075);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app.component */ 5041);
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app-routing.module */ 158);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/common/http */ 1841);
/* harmony import */ var _rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./rest.service */ 1881);
/* harmony import */ var _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @fortawesome/angular-fontawesome */ 5002);
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ 9976);
/* harmony import */ var _fortawesome_free_regular_svg_icons__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @fortawesome/free-regular-svg-icons */ 1903);
/* harmony import */ var _fortawesome_free_brands_svg_icons__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @fortawesome/free-brands-svg-icons */ 5115);
/* harmony import */ var _PipesModule_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./PipesModule.module */ 9024);




// import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';











let AppModule = class AppModule {
    constructor(library) {
        library.addIconPacks(_fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_4__.fas, _fortawesome_free_brands_svg_icons__WEBPACK_IMPORTED_MODULE_5__.fab, _fortawesome_free_regular_svg_icons__WEBPACK_IMPORTED_MODULE_6__.far);
        library.addIcons(_fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_4__.faCoffee);
    }
};
AppModule.ctorParameters = () => [
    { type: _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_7__.FaIconLibrary }
];
AppModule = (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_9__.NgModule)({
        declarations: [_app_component__WEBPACK_IMPORTED_MODULE_0__.AppComponent],
        entryComponents: [],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_10__.BrowserModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_11__.IonicModule.forRoot(),
            _app_routing_module__WEBPACK_IMPORTED_MODULE_1__.AppRoutingModule,
            _angular_common_http__WEBPACK_IMPORTED_MODULE_12__.HttpClientModule,
            _angular_common__WEBPACK_IMPORTED_MODULE_13__.CommonModule,
            _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_7__.FontAwesomeModule,
            _PipesModule_module__WEBPACK_IMPORTED_MODULE_3__.PipesModule,
        ],
        providers: [
            // InAppBrowser,
            { provide: _angular_router__WEBPACK_IMPORTED_MODULE_14__.RouteReuseStrategy, useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_11__.IonicRouteStrategy },
            _rest_service__WEBPACK_IMPORTED_MODULE_2__.RestService,
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_0__.AppComponent],
    })
], AppModule);



/***/ }),

/***/ 3221:
/*!************************************!*\
  !*** ./src/app/globals.service.ts ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "GlobalService": () => (/* binding */ GlobalService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ 6215);




// export const token='';
let GlobalService = class GlobalService {
    constructor() {
        this.firstname = '';
        this.messageSourceSummary = new rxjs__WEBPACK_IMPORTED_MODULE_0__.BehaviorSubject('');
        this.currentMessageSubscriberSummary = this.messageSourceSummary.asObservable();
        this.messageSource = new rxjs__WEBPACK_IMPORTED_MODULE_0__.BehaviorSubject('');
        this.currentMessage = this.messageSource.asObservable();
        this.selectedPoint = new rxjs__WEBPACK_IMPORTED_MODULE_0__.BehaviorSubject('');
        this.currectSelectedPoint = this.selectedPoint.asObservable();
        this.selectedActivity = new rxjs__WEBPACK_IMPORTED_MODULE_0__.BehaviorSubject('');
        this.currectSelectedAvtivity = this.selectedActivity.asObservable();
        this.selectedActivityFilter = new rxjs__WEBPACK_IMPORTED_MODULE_0__.BehaviorSubject({
            seletedTracker: "all",
            activity: "all",
            start_date: new Date().toISOString(),
            end_date: new Date().toISOString()
        });
        this.currectSelectedAvtivityFilter = this.selectedActivityFilter.asObservable();
        this.selectedAlertsFilter = new rxjs__WEBPACK_IMPORTED_MODULE_0__.BehaviorSubject({
            seletedTracker: "any",
            type: "all",
            start_date: new Date().toISOString(),
            end_date: new Date().toISOString(),
            sort_by: 'none',
            sort: 'none',
        });
        this.currectSelectedAlertsFilter = this.selectedAlertsFilter.asObservable();
    }
    notifySummary(message) {
        this.messageSourceSummary.next(message);
    }
    changeMessage(message) {
        this.messageSource.next(message);
    }
    userSelectedNewPoint(point) {
        this.selectedPoint.next(point);
    }
    userSelectedNewActivity(activity) {
        this.selectedActivity.next(activity);
    }
    userChangedActivityFilter(activityFilter) {
        this.selectedActivityFilter.next(activityFilter);
    }
    setActivityFilterDefault() {
        this.selectedActivityFilter.next({
            seletedTracker: "all",
            activity: "all",
            start_date: new Date().toISOString(),
            end_date: new Date().toISOString()
        });
    }
    userChangedAlertsFilter(alertFilter) {
        this.selectedAlertsFilter.next(alertFilter);
    }
    setAlertFilterAsDefault() {
        this.selectedAlertsFilter.next({
            seletedTracker: "any",
            type: "all",
            start_date: new Date().toISOString(),
            end_date: new Date().toISOString(),
            sort_by: 'none',
            sort: 'none',
        });
    }
    getMarkerLocation(icon, color) {
        if (icon === 'fa-location-arrow') {
            if (color === '#229954') {
                return '../assets/markers/location-arrow/location-arrow-229954.png';
            }
            else if (color === '#CD6155') {
                return '../assets/markers/location-arrow/location-arrow-CD6155.png';
            }
            else if (color === '#A569BD') {
                return '../assets/markers/location-arrow/location-arrow-A569BD.png';
            }
            else if (color === '#85C1E9') {
                return '../assets/markers/location-arrow/location-arrow-85C1E9.png';
            }
            else if (color === '#45B39D') {
                return '../assets/markers/location-arrow/location-arrow-45B39D.png';
            }
            else if (color === '#F4D03F') {
                return '../assets/markers/location-arrow/location-arrow-F4D03F.png';
            }
            else if (color === '#DC7633') {
                return '../assets/markers/location-arrow/location-arrow-DC7633.png';
            }
            else if (color === '#CCCCFF') {
                return '../assets/markers/location-arrow/location-arrow-CCCCFF.png';
            }
            else if (color === '#6495ED') {
                return '../assets/markers/location-arrow/location-arrow-6495ED.png';
            }
            else if (color === '#40E0D0') {
                return '../assets/markers/location-arrow/location-arrow-40E0D0.png';
            }
            else if (color === '#9FE2BF') {
                return '../assets/markers/location-arrow/location-arrow-9FE2BF.png';
            }
            else if (color === '#DE3163') {
                return '../assets/markers/location-arrow/location-arrow-DE3163.png';
            }
            else if (color === '#DFFF00') {
                return '../assets/markers/location-arrow/location-arrow-DFFF00.png';
            }
            else if (color === '#FFBF00') {
                return '../assets/markers/location-arrow/location-arrow-FFBF00.png';
            }
            else if (color === '#FF7F50') {
                return '../assets/markers/location-arrow/location-arrow-FF7F50.png';
            }
        }
        else if (icon === 'fa-map-marker') {
            if (color === '#229954') {
                return '../assets/markers/map-marker/map-marker-229954.png';
            }
            else if (color === '#CD6155') {
                return '../assets/markers/map-marker/map-marker-CD6155.png';
            }
            else if (color === '#A569BD') {
                return '../assets/markers/map-marker/map-marker-A569BD.png';
            }
            else if (color === '#85C1E9') {
                return '../assets/markers/map-marker/map-marker-85C1E9.png';
            }
            else if (color === '#45B39D') {
                return '../assets/markers/map-marker/map-marker-45B39D.png';
            }
            else if (color === '#F4D03F') {
                return '../assets/markers/map-marker/map-marker-F4D03F.png';
            }
            else if (color === '#DC7633') {
                return '../assets/markers/map-marker/map-marker-DC7633.png';
            }
            else if (color === '#CCCCFF') {
                return '../assets/markers/map-marker/map-marker-CCCCFF.png';
            }
            else if (color === '#6495ED') {
                return '../assets/markers/map-marker/map-marker-6495ED.png';
            }
            else if (color === '#40E0D0') {
                return '../assets/markers/map-marker/map-marker-40E0D0.png';
            }
            else if (color === '#9FE2BF') {
                return '../assets/markers/map-marker/map-marker-9FE2BF.png';
            }
            else if (color === '#DE3163') {
                return '../assets/markers/map-marker/map-marker-DE3163.png';
            }
            else if (color === '#DFFF00') {
                return '../assets/markers/map-marker/map-marker-DFFF00.png';
            }
            else if (color === '#FFBF00') {
                return '../assets/markers/map-marker/map-marker-FFBF00.png';
            }
            else if (color === '#FF7F50') {
                return '../assets/markers/map-marker/map-marker-FF7F50.png';
            }
        }
        else if (icon === 'fa-map-marker-alt') {
            if (color === '#229954') {
                return '../assets/markers/map-marker-alt/map-marker-229954.png';
            }
            else if (color === '#CD6155') {
                return '../assets/markers/map-marker-alt/map-marker-CD6155.png';
            }
            else if (color === '#A569BD') {
                return '../assets/markers/map-marker-alt/map-marker-A569BD.png';
            }
            else if (color === '#85C1E9') {
                return '../assets/markers/map-marker-alt/map-marker-85C1E9.png';
            }
            else if (color === '#45B39D') {
                return '../assets/markers/map-marker-alt/map-marker-45B39D.png';
            }
            else if (color === '#F4D03F') {
                return '../assets/markers/map-marker-alt/map-marker-F4D03F.png';
            }
            else if (color === '#DC7633') {
                return '../assets/markers/map-marker-alt/map-marker-DC7633.png';
            }
            else if (color === '#CCCCFF') {
                return '../assets/markers/map-marker-alt/map-marker-CCCCFF.png';
            }
            else if (color === '#6495ED') {
                return '../assets/markers/map-marker-alt/map-marker-6495ED.png';
            }
            else if (color === '#40E0D0') {
                return '../assets/markers/map-marker-alt/map-marker-40E0D0.png';
            }
            else if (color === '#9FE2BF') {
                return '../assets/markers/map-marker-alt/map-marker-9FE2BF.png';
            }
            else if (color === '#DE3163') {
                return '../assets/markers/map-marker-alt/map-marker-DE3163.png';
            }
            else if (color === '#DFFF00') {
                return '../assets/markers/map-marker-alt/map-marker-DFFF00.png';
            }
            else if (color === '#FFBF00') {
                return '../assets/markers/map-marker-alt/map-marker-FFBF00.png';
            }
            else if (color === '#FF7F50') {
                return '../assets/markers/map-marker-alt/map-marker-FF7F50.png';
            }
        }
        else if (icon === 'fa-car') {
            if (color === '#229954') {
                return '../assets/markers/car/car-229954.png';
            }
            else if (color === '#CD6155') {
                return '../assets/markers/car/car-CD6155.png';
            }
            else if (color === '#A569BD') {
                return '../assets/markers/car/car-A569BD.png';
            }
            else if (color === '#85C1E9') {
                return '../assets/markers/car/car-85C1E9.png';
            }
            else if (color === '#45B39D') {
                return '../assets/markers/car/car-45B39D.png';
            }
            else if (color === '#F4D03F') {
                return '../assets/markers/car/car-F4D03F.png';
            }
            else if (color === '#DC7633') {
                return '../assets/markers/car/car-DC7633.png';
            }
            else if (color === '#CCCCFF') {
                return '../assets/markers/car/car-CCCCFF.png';
            }
            else if (color === '#6495ED') {
                return '../assets/markers/car/car-6495ED.png';
            }
            else if (color === '#40E0D0') {
                return '../assets/markers/car/car-40E0D0.png';
            }
            else if (color === '#9FE2BF') {
                return '../assets/markers/car/car-9FE2BF.png';
            }
            else if (color === '#DE3163') {
                return '../assets/markers/car/car-DE3163.png';
            }
            else if (color === '#DFFF00') {
                return '../assets/markers/car/car-DFFF00.png';
            }
            else if (color === '#FFBF00') {
                return '../assets/markers/car/car-FFBF00.png';
            }
            else if (color === '#FF7F50') {
                return '../assets/markers/car/car-FF7F50.png';
            }
        }
        else if (icon === 'fa-motorcycle') {
            if (color === '#229954') {
                return '../assets/markers/motorcycle/motorcycle-229954.png';
            }
            else if (color === '#CD6155') {
                return '../assets/markers/motorcycle/motorcycle-CD6155.png';
            }
            else if (color === '#A569BD') {
                return '../assets/markers/motorcycle/motorcycle-A569BD.png';
            }
            else if (color === '#85C1E9') {
                return '../assets/markers/motorcycle/motorcycle-85C1E9.png';
            }
            else if (color === '#45B39D') {
                return '../assets/markers/motorcycle/motorcycle-45B39D.png';
            }
            else if (color === '#F4D03F') {
                return '../assets/markers/motorcycle/motorcycle-F4D03F.png';
            }
            else if (color === '#DC7633') {
                return '../assets/markers/motorcycle/motorcycle-DC7633.png';
            }
            else if (color === '#CCCCFF') {
                return '../assets/markers/motorcycle/motorcycle-CCCCFF.png';
            }
            else if (color === '#6495ED') {
                return '../assets/markers/motorcycle/motorcycle-6495ED.png';
            }
            else if (color === '#40E0D0') {
                return '../assets/markers/motorcycle/motorcycle-40E0D0.png';
            }
            else if (color === '#9FE2BF') {
                return '../assets/markers/motorcycle/motorcycle-9FE2BF.png';
            }
            else if (color === '#DE3163') {
                return '../assets/markers/motorcycle/motorcycle-DE3163.png';
            }
            else if (color === '#DFFF00') {
                return '../assets/markers/motorcycle/motorcycle-DFFF00.png';
            }
            else if (color === '#FFBF00') {
                return '../assets/markers/motorcycle/motorcycle-FFBF00.png';
            }
            else if (color === '#FF7F50') {
                return '../assets/markers/motorcycle/motorcycle-FF7F50.png';
            }
        }
        else if (icon === 'fa-shuttle-van') {
            if (color === '#229954') {
                return '../assets/markers/shuttle-van/shuttle-van-229954.png';
            }
            else if (color === '#CD6155') {
                return '../assets/markers/shuttle-van/shuttle-van-CD6155.png';
            }
            else if (color === '#A569BD') {
                return '../assets/markers/shuttle-van/shuttle-van-A569BD.png';
            }
            else if (color === '#85C1E9') {
                return '../assets/markers/shuttle-van/shuttle-van-85C1E9.png';
            }
            else if (color === '#45B39D') {
                return '../assets/markers/shuttle-van/shuttle-van-45B39D.png';
            }
            else if (color === '#F4D03F') {
                return '../assets/markers/shuttle-van/shuttle-van-F4D03F.png';
            }
            else if (color === '#DC7633') {
                return '../assets/markers/shuttle-van/shuttle-van-DC7633.png';
            }
            else if (color === '#CCCCFF') {
                return '../assets/markers/shuttle-van/shuttle-van-CCCCFF.png';
            }
            else if (color === '#6495ED') {
                return '../assets/markers/shuttle-van/shuttle-van-6495ED.png';
            }
            else if (color === '#40E0D0') {
                return '../assets/markers/shuttle-van/shuttle-van-40E0D0.png';
            }
            else if (color === '#9FE2BF') {
                return '../assets/markers/shuttle-van/shuttle-van-9FE2BF.png';
            }
            else if (color === '#DE3163') {
                return '../assets/markers/shuttle-van/shuttle-van-DE3163.png';
            }
            else if (color === '#DFFF00') {
                return '../assets/markers/shuttle-van/shuttle-van-DFFF00.png';
            }
            else if (color === '#FFBF00') {
                return '../assets/markers/shuttle-van/shuttle-van-FFBF00.png';
            }
            else if (color === '#FF7F50') {
                return '../assets/markers/shuttle-van/shuttle-van-FF7F50.png';
            }
        }
        else if (icon === 'fa-truck') {
            if (color === '#229954') {
                return '../assets/markers/truck/truck-229954.png';
            }
            else if (color === '#CD6155') {
                return '../assets/markers/truck/truck-CD6155.png';
            }
            else if (color === '#A569BD') {
                return '../assets/markers/truck/truck-A569BD.png';
            }
            else if (color === '#85C1E9') {
                return '../assets/markers/truck/truck-85C1E9.png';
            }
            else if (color === '#45B39D') {
                return '../assets/markers/truck/truck-45B39D.png';
            }
            else if (color === '#F4D03F') {
                return '../assets/markers/truck/truck-F4D03F.png';
            }
            else if (color === '#DC7633') {
                return '../assets/markers/truck/truck-DC7633.png';
            }
            else if (color === '#CCCCFF') {
                return '../assets/markers/truck/truck-CCCCFF.png';
            }
            else if (color === '#6495ED') {
                return '../assets/markers/truck/truck-6495ED.png';
            }
            else if (color === '#40E0D0') {
                return '../assets/markers/truck/truck-40E0D0.png';
            }
            else if (color === '#9FE2BF') {
                return '../assets/markers/truck/truck-9FE2BF.png';
            }
            else if (color === '#DE3163') {
                return '../assets/markers/truck/truck-DE3163.png';
            }
            else if (color === '#DFFF00') {
                return '../assets/markers/truck/truck-DFFF00.png';
            }
            else if (color === '#FFBF00') {
                return '../assets/markers/truck/truck-FFBF00.png';
            }
            else if (color === '#FF7F50') {
                return '../assets/markers/truck/truck-FF7F50.png';
            }
        }
        else if (icon === 'fa-truck-pickup') {
            if (color === '#229954') {
                return '../assets/markers/truck-pickup/truck-pickup-229954.png';
            }
            else if (color === '#CD6155') {
                return '../assets/markers/truck-pickup/truck-pickup-CD6155.png';
            }
            else if (color === '#A569BD') {
                return '../assets/markers/truck-pickup/truck-pickup-A569BD.png';
            }
            else if (color === '#85C1E9') {
                return '../assets/markers/truck-pickup/truck-pickup-85C1E9.png';
            }
            else if (color === '#45B39D') {
                return '../assets/markers/truck-pickup/truck-pickup-45B39D.png';
            }
            else if (color === '#F4D03F') {
                return '../assets/markers/truck-pickup/truck-pickup-F4D03F.png';
            }
            else if (color === '#DC7633') {
                return '../assets/markers/truck-pickup/truck-pickup-DC7633.png';
            }
            else if (color === '#CCCCFF') {
                return '../assets/markers/truck-pickup/truck-pickup-CCCCFF.png';
            }
            else if (color === '#6495ED') {
                return '../assets/markers/truck-pickup/truck-pickup-6495ED.png';
            }
            else if (color === '#40E0D0') {
                return '../assets/markers/truck-pickup/truck-pickup-40E0D0.png';
            }
            else if (color === '#9FE2BF') {
                return '../assets/markers/truck-pickup/truck-pickup-9FE2BF.png';
            }
            else if (color === '#DE3163') {
                return '../assets/markers/truck-pickup/truck-pickup-DE3163.png';
            }
            else if (color === '#DFFF00') {
                return '../assets/markers/truck-pickup/truck-pickup-DFFF00.png';
            }
            else if (color === '#FFBF00') {
                return '../assets/markers/truck-pickup/truck-pickup-FFBF00.png';
            }
            else if (color === '#FF7F50') {
                return '../assets/markers/truck-pickup/truck-pickup-FF7F50.png';
            }
        }
        else if (icon === 'fa-caravan') {
            if (color === '#229954') {
                return '../assets/markers/caravan/caravan-229954.png';
            }
            else if (color === '#CD6155') {
                return '../assets/markers/caravan/caravan-CD6155.png';
            }
            else if (color === '#A569BD') {
                return '../assets/markers/caravan/caravan-A569BD.png';
            }
            else if (color === '#85C1E9') {
                return '../assets/markers/caravan/caravan-85C1E9.png';
            }
            else if (color === '#45B39D') {
                return '../assets/markers/caravan/caravan-45B39D.png';
            }
            else if (color === '#F4D03F') {
                return '../assets/markers/caravan/caravan-F4D03F.png';
            }
            else if (color === '#DC7633') {
                return '../assets/markers/caravan/caravan-DC7633.png';
            }
            else if (color === '#CCCCFF') {
                return '../assets/markers/caravan/caravan-CCCCFF.png';
            }
            else if (color === '#6495ED') {
                return '../assets/markers/caravan/caravan-6495ED.png';
            }
            else if (color === '#40E0D0') {
                return '../assets/markers/caravan/caravan-40E0D0.png';
            }
            else if (color === '#9FE2BF') {
                return '../assets/markers/caravan/caravan-9FE2BF.png';
            }
            else if (color === '#DE3163') {
                return '../assets/markers/caravan/caravan-DE3163.png';
            }
            else if (color === '#DFFF00') {
                return '../assets/markers/caravan/caravan-DFFF00.png';
            }
            else if (color === '#FFBF00') {
                return '../assets/markers/caravan/caravan-FFBF00.png';
            }
            else if (color === '#FF7F50') {
                return '../assets/markers/caravan/caravan-FF7F50.png';
            }
        }
        else if (icon === 'fa-user') {
            if (color === '#229954') {
                return '../assets/markers/shuttle-van/shuttle-van-229954.png';
            }
            else if (color === '#CD6155') {
                return '../assets/markers/shuttle-van/shuttle-van-CD6155.png';
            }
            else if (color === '#A569BD') {
                return '../assets/markers/shuttle-van/shuttle-van-A569BD.png';
            }
            else if (color === '#85C1E9') {
                return '../assets/markers/shuttle-van/shuttle-van-85C1E9.png';
            }
            else if (color === '#45B39D') {
                return '../assets/markers/shuttle-van/shuttle-van-45B39D.png';
            }
            else if (color === '#F4D03F') {
                return '../assets/markers/shuttle-van/shuttle-van-F4D03F.png';
            }
            else if (color === '#DC7633') {
                return '../assets/markers/shuttle-van/shuttle-van-DC7633.png';
            }
            else if (color === '#CCCCFF') {
                return '../assets/markers/shuttle-van/shuttle-van-CCCCFF.png';
            }
            else if (color === '#6495ED') {
                return '../assets/markers/shuttle-van/shuttle-van-6495ED.png';
            }
            else if (color === '#40E0D0') {
                return '../assets/markers/shuttle-van/shuttle-van-40E0D0.png';
            }
            else if (color === '#9FE2BF') {
                return '../assets/markers/shuttle-van/shuttle-van-9FE2BF.png';
            }
            else if (color === '#DE3163') {
                return '../assets/markers/shuttle-van/shuttle-van-DE3163.png';
            }
            else if (color === '#DFFF00') {
                return '../assets/markers/shuttle-van/shuttle-van-DFFF00.png';
            }
            else if (color === '#FFBF00') {
                return '../assets/markers/shuttle-van/shuttle-van-FFBF00.png';
            }
            else if (color === '#FF7F50') {
                return '../assets/markers/shuttle-van/shuttle-van-FF7F50.png';
            }
        }
        else if (icon === 'fa-male') {
            if (color === '#229954') {
                return '../assets/markers/male/male-229954.png';
            }
            else if (color === '#CD6155') {
                return '../assets/markers/male/male-CD6155.png';
            }
            else if (color === '#A569BD') {
                return '../assets/markers/male/male-A569BD.png';
            }
            else if (color === '#85C1E9') {
                return '../assets/markers/male/male-85C1E9.png';
            }
            else if (color === '#45B39D') {
                return '../assets/markers/male/male-45B39D.png';
            }
            else if (color === '#F4D03F') {
                return '../assets/markers/male/male-F4D03F.png';
            }
            else if (color === '#DC7633') {
                return '../assets/markers/male/male-DC7633.png';
            }
            else if (color === '#CCCCFF') {
                return '../assets/markers/male/male-CCCCFF.png';
            }
            else if (color === '#6495ED') {
                return '../assets/markers/male/male-6495ED.png';
            }
            else if (color === '#40E0D0') {
                return '../assets/markers/male/male-40E0D0.png';
            }
            else if (color === '#9FE2BF') {
                return '../assets/markers/male/male-9FE2BF.png';
            }
            else if (color === '#DE3163') {
                return '../assets/markers/male/male-DE3163.png';
            }
            else if (color === '#DFFF00') {
                return '../assets/markers/male/male-DFFF00.png';
            }
            else if (color === '#FFBF00') {
                return '../assets/markers/male/male-FFBF00.png';
            }
            else if (color === '#FF7F50') {
                return '../assets/markers/male/male-FF7F50.png';
            }
        }
        else if (icon === 'fa-female') {
            if (color === '#229954') {
                return '../assets/markers/female/female-229954.png';
            }
            else if (color === '#CD6155') {
                return '../assets/markers/female/female-CD6155.png';
            }
            else if (color === '#A569BD') {
                return '../assets/markers/female/female-A569BD.png';
            }
            else if (color === '#85C1E9') {
                return '../assets/markers/female/female-85C1E9.png';
            }
            else if (color === '#45B39D') {
                return '../assets/markers/female/female-45B39D.png';
            }
            else if (color === '#F4D03F') {
                return '../assets/markers/female/female-F4D03F.png';
            }
            else if (color === '#DC7633') {
                return '../assets/markers/female/female-DC7633.png';
            }
            else if (color === '#CCCCFF') {
                return '../assets/markers/female/female-CCCCFF.png';
            }
            else if (color === '#6495ED') {
                return '../assets/markers/female/female-6495ED.png';
            }
            else if (color === '#40E0D0') {
                return '../assets/markers/female/female-40E0D0.png';
            }
            else if (color === '#9FE2BF') {
                return '../assets/markers/female/female-9FE2BF.png';
            }
            else if (color === '#DE3163') {
                return '../assets/markers/female/female-DE3163.png';
            }
            else if (color === '#DFFF00') {
                return '../assets/markers/female/female-DFFF00.png';
            }
            else if (color === '#FFBF00') {
                return '../assets/markers/female/female-FFBF00.png';
            }
            else if (color === '#FF7F50') {
                return '../assets/markers/female/female-FF7F50.png';
            }
        }
        else if (icon === 'fa-child') {
            if (color === '#229954') {
                return '../assets/markers/child/child-229954.png';
            }
            else if (color === '#CD6155') {
                return '../assets/markers/child/child-CD6155.png';
            }
            else if (color === '#A569BD') {
                return '../assets/markers/child/child-A569BD.png';
            }
            else if (color === '#85C1E9') {
                return '../assets/markers/child/child-85C1E9.png';
            }
            else if (color === '#45B39D') {
                return '../assets/markers/child/child-45B39D.png';
            }
            else if (color === '#F4D03F') {
                return '../assets/markers/child/child-F4D03F.png';
            }
            else if (color === '#DC7633') {
                return '../assets/markers/child/child-DC7633.png';
            }
            else if (color === '#CCCCFF') {
                return '../assets/markers/child/child-CCCCFF.png';
            }
            else if (color === '#6495ED') {
                return '../assets/markers/child/child-6495ED.png';
            }
            else if (color === '#40E0D0') {
                return '../assets/markers/child/child-40E0D0.png';
            }
            else if (color === '#9FE2BF') {
                return '../assets/markers/child/child-9FE2BF.png';
            }
            else if (color === '#DE3163') {
                return '../assets/markers/child/child-DE3163.png';
            }
            else if (color === '#DFFF00') {
                return '../assets/markers/child/child-DFFF00.png';
            }
            else if (color === '#FFBF00') {
                return '../assets/markers/child/child-FFBF00.png';
            }
            else if (color === '#FF7F50') {
                return '../assets/markers/child/child-FF7F50.png';
            }
        }
        else if (icon === 'fa-baby') {
            if (color === '#229954') {
                return '../assets/markers/baby/baby-229954.png';
            }
            else if (color === '#CD6155') {
                return '../assets/markers/baby/baby-CD6155.png';
            }
            else if (color === '#A569BD') {
                return '../assets/markers/baby/baby-A569BD.png';
            }
            else if (color === '#85C1E9') {
                return '../assets/markers/baby/baby-85C1E9.png';
            }
            else if (color === '#45B39D') {
                return '../assets/markers/baby/baby-45B39D.png';
            }
            else if (color === '#F4D03F') {
                return '../assets/markers/baby/baby-F4D03F.png';
            }
            else if (color === '#DC7633') {
                return '../assets/markers/baby/baby-DC7633.png';
            }
            else if (color === '#CCCCFF') {
                return '../assets/markers/baby/baby-CCCCFF.png';
            }
            else if (color === '#6495ED') {
                return '../assets/markers/baby/baby-6495ED.png';
            }
            else if (color === '#40E0D0') {
                return '../assets/markers/baby/baby-40E0D0.png';
            }
            else if (color === '#9FE2BF') {
                return '../assets/markers/baby/baby-9FE2BF.png';
            }
            else if (color === '#DE3163') {
                return '../assets/markers/baby/baby-DE3163.png';
            }
            else if (color === '#DFFF00') {
                return '../assets/markers/baby/baby-DFFF00.png';
            }
            else if (color === '#FFBF00') {
                return '../assets/markers/baby/baby-FFBF00.png';
            }
            else if (color === '#FF7F50') {
                return '../assets/markers/baby/baby-FF7F50.png';
            }
        }
        else if (icon === 'fa-baby-carriage') {
            if (color === '#229954') {
                return '../assets/markers/baby-carriage/baby-carriage-229954.png';
            }
            else if (color === '#CD6155') {
                return '../assets/markers/baby-carriage/baby-carriage-CD6155.png';
            }
            else if (color === '#A569BD') {
                return '../assets/markers/baby-carriage/baby-carriage-A569BD.png';
            }
            else if (color === '#85C1E9') {
                return '../assets/markers/baby-carriage/baby-carriage-85C1E9.png';
            }
            else if (color === '#45B39D') {
                return '../assets/markers/baby-carriage/baby-carriage-45B39D.png';
            }
            else if (color === '#F4D03F') {
                return '../assets/markers/baby-carriage/baby-carriage-F4D03F.png';
            }
            else if (color === '#DC7633') {
                return '../assets/markers/baby-carriage/baby-carriage-DC7633.png';
            }
            else if (color === '#CCCCFF') {
                return '../assets/markers/baby-carriage/baby-carriage-CCCCFF.png';
            }
            else if (color === '#6495ED') {
                return '../assets/markers/baby-carriage/baby-carriage-6495ED.png';
            }
            else if (color === '#40E0D0') {
                return '../assets/markers/baby-carriage/baby-carriage-40E0D0.png';
            }
            else if (color === '#9FE2BF') {
                return '../assets/markers/baby-carriage/baby-carriage-9FE2BF.png';
            }
            else if (color === '#DE3163') {
                return '../assets/markers/baby-carriage/baby-carriage-DE3163.png';
            }
            else if (color === '#DFFF00') {
                return '../assets/markers/baby-carriage/baby-carriage-DFFF00.png';
            }
            else if (color === '#FFBF00') {
                return '../assets/markers/baby-carriage/baby-carriage-FFBF00.png';
            }
            else if (color === '#FF7F50') {
                return '../assets/markers/baby-carriage/baby-carriage-FF7F50.png';
            }
        }
    }
};
GlobalService.ctorParameters = () => [];
GlobalService = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable)({
        providedIn: 'root'
    })
], GlobalService);



/***/ }),

/***/ 4166:
/*!******************************************!*\
  !*** ./src/app/header-config.service.ts ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HeaderConfigService": () => (/* binding */ HeaderConfigService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ 1841);



let HeaderConfigService = class HeaderConfigService {
    constructor() {
        this.api = 'https://api.spytrack.com/api/';
        this._publicHeader = (headers) => {
            headers = headers || new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__.HttpHeaders();
            headers = headers.set('Content-Type', 'application/json');
            headers = headers.set('Accept', 'application/json, text/plain, */*');
            return {
                headers: headers
            };
        };
        this._loginHeader = (headers) => {
            headers = headers || new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__.HttpHeaders();
            headers = headers.set('Accept', 'application/json, text/plain, */*');
            // headers = headers.set('Access-Control-Allow-Origin', '*');
            headers = headers.set('Content-Type', 'application/json');
            headers = headers.set('Access-Control-Allow-Headers', 'Content-Type');
            return {
                headers: headers
            };
        };
        this._tokenizedHeader = (headers) => {
            headers = headers || new _angular_common_http__WEBPACK_IMPORTED_MODULE_0__.HttpHeaders();
            headers = headers.append('Content-Type', 'application/json');
            headers = headers.append('Authorization', 'Bearer ' + localStorage.getItem('token'));
            headers = headers.append('Accept', 'application/json');
            return {
                headers: headers
            };
        };
    }
};
HeaderConfigService.ctorParameters = () => [];
HeaderConfigService = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable)()
], HeaderConfigService);



/***/ }),

/***/ 1291:
/*!*****************************************!*\
  !*** ./src/app/pipes/date-tras.pipe.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DateTrasPipe": () => (/* binding */ DateTrasPipe)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 7716);


let DateTrasPipe = class DateTrasPipe {
    transform(value, ...args) {
        if (value) {
            const temp = value.toString().replace(' ', 'T');
            return new Date(temp);
        }
        else {
            return null;
        }
    }
};
DateTrasPipe = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_1__.Pipe)({
        name: 'dateTras',
    })
], DateTrasPipe);



/***/ }),

/***/ 1881:
/*!*********************************!*\
  !*** ./src/app/rest.service.ts ***!
  \*********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RestService": () => (/* binding */ RestService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ 1841);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ 5917);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ 205);
/* harmony import */ var _header_config_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./header-config.service */ 4166);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ 8307);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ 5304);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/angular */ 476);

/* eslint-disable @typescript-eslint/naming-convention */







let RestService = class RestService extends _header_config_service__WEBPACK_IMPORTED_MODULE_0__.HeaderConfigService {
    constructor(http, router, toastCtrl) {
        super();
        this.http = http;
        this.router = router;
        this.toastCtrl = toastCtrl;
        this.url = 'https://api.spytrack.com/api/';
    }
    get(endpoint, data, token) {
        return this.http.get(this.url + endpoint, this._tokenizedHeader()).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.tap)(response => response), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)((err) => {
            if (err.status === 401) {
                this.logout();
            }
            return err;
        }));
    }
    post(data, endpoint) {
        return this.http.post(this.api + endpoint, data, this._tokenizedHeader()).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.tap)(response => response), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)((error) => {
            // we expect 404, it's not a failure for us.
            if (error.status === 404) {
                return (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.of)(null); // or any other stream like of('') etc.
            }
            else if (error.status === 409) {
                return (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.of)(error);
            }
            if (error.status === 401) {
                this.logout();
            }
            return (0,rxjs__WEBPACK_IMPORTED_MODULE_4__.throwError)(error);
        }));
    }
    put(data, endpoint) {
        return this.http.put(this.api + endpoint, data, this._tokenizedHeader()).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.tap)(response => response), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)((error) => {
            // we expect 404, it's not a failure for us.
            if (error.status === 404) {
                return (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.of)(null); // or any other stream like of('') etc.
            }
            return (0,rxjs__WEBPACK_IMPORTED_MODULE_4__.throwError)(error);
        }));
    }
    login(data, endpoint) {
        return this.http.post(this.api + endpoint, data, this._loginHeader()).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.tap)(response => response), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)((error) => {
            // we expect 404, it's not a failure for us.
            if (error.status === 404) {
                return (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.of)(null); // or any other stream like of('') etc.
            }
            if (error.status === 401) {
                this.logout();
            }
            return (0,rxjs__WEBPACK_IMPORTED_MODULE_4__.throwError)(error);
        }));
    }
    getSnap(endpoint, data, token) {
        return this.http.get(this.url + endpoint, this._tokenizedHeader()).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.tap)(response => response), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)((err) => err));
    }
    newVerification(endpoint, email) {
        return this.http.post(this.api + endpoint, { email }, {
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json'
            }
        }).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.tap)(response => response), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.catchError)((error) => {
            // we expect 404, it's not a failure for us.
            if (error.status === 404) {
                return (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.of)(null); // or any other stream like of('') etc.
            }
            else if (error.status === 409) {
                return (0,rxjs__WEBPACK_IMPORTED_MODULE_3__.of)(error);
            }
            if (error.status === 401) {
                this.logout();
            }
            return (0,rxjs__WEBPACK_IMPORTED_MODULE_4__.throwError)(error);
        }));
    }
    // getAddress(endpoint:string, data:any, token:any): Observable<any> {
    //   return this.http.get(endpoint, this._tokenizedHeader()).pipe(
    //     tap (response => {
    //       return response;
    //   }),catchError((err: any) => {
    //    return err;
    //   }));
    // }
    logout() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: 'Please login again',
                position: 'bottom',
                duration: 3000,
                color: 'danger'
            });
            toast.present();
            localStorage.clear();
            this.router.navigate(['/login']);
        });
    }
};
RestService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_6__.HttpClient },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__.Router },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__.ToastController }
];
RestService = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_9__.Injectable)({
        providedIn: 'root'
    })
], RestService);



/***/ }),

/***/ 2340:
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "environment": () => (/* binding */ environment)
/* harmony export */ });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ 4431:
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ 4608);
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app/app.module */ 6747);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./environments/environment */ 2340);




if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__.environment.production) {
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.enableProdMode)();
}
(0,_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_3__.platformBrowserDynamic)().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_0__.AppModule)
    .catch(err => console.log(err));


/***/ }),

/***/ 863:
/*!******************************************************************************************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/ lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
  \******************************************************************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var map = {
	"./ion-action-sheet.entry.js": [
		7321,
		"common",
		"node_modules_ionic_core_dist_esm_ion-action-sheet_entry_js"
	],
	"./ion-alert.entry.js": [
		6108,
		"common",
		"node_modules_ionic_core_dist_esm_ion-alert_entry_js"
	],
	"./ion-app_8.entry.js": [
		1489,
		"common",
		"node_modules_ionic_core_dist_esm_ion-app_8_entry_js"
	],
	"./ion-avatar_3.entry.js": [
		305,
		"common",
		"node_modules_ionic_core_dist_esm_ion-avatar_3_entry_js"
	],
	"./ion-back-button.entry.js": [
		5830,
		"common",
		"node_modules_ionic_core_dist_esm_ion-back-button_entry_js"
	],
	"./ion-backdrop.entry.js": [
		7757,
		"node_modules_ionic_core_dist_esm_ion-backdrop_entry_js"
	],
	"./ion-button_2.entry.js": [
		392,
		"common",
		"node_modules_ionic_core_dist_esm_ion-button_2_entry_js"
	],
	"./ion-card_5.entry.js": [
		6911,
		"common",
		"node_modules_ionic_core_dist_esm_ion-card_5_entry_js"
	],
	"./ion-checkbox.entry.js": [
		937,
		"common",
		"node_modules_ionic_core_dist_esm_ion-checkbox_entry_js"
	],
	"./ion-chip.entry.js": [
		8695,
		"common",
		"node_modules_ionic_core_dist_esm_ion-chip_entry_js"
	],
	"./ion-col_3.entry.js": [
		6034,
		"node_modules_ionic_core_dist_esm_ion-col_3_entry_js"
	],
	"./ion-datetime_3.entry.js": [
		8837,
		"common",
		"node_modules_ionic_core_dist_esm_ion-datetime_3_entry_js"
	],
	"./ion-fab_3.entry.js": [
		4195,
		"common",
		"node_modules_ionic_core_dist_esm_ion-fab_3_entry_js"
	],
	"./ion-img.entry.js": [
		1709,
		"node_modules_ionic_core_dist_esm_ion-img_entry_js"
	],
	"./ion-infinite-scroll_2.entry.js": [
		5931,
		"node_modules_ionic_core_dist_esm_ion-infinite-scroll_2_entry_js"
	],
	"./ion-input.entry.js": [
		4513,
		"common",
		"node_modules_ionic_core_dist_esm_ion-input_entry_js"
	],
	"./ion-item-option_3.entry.js": [
		8056,
		"common",
		"node_modules_ionic_core_dist_esm_ion-item-option_3_entry_js"
	],
	"./ion-item_8.entry.js": [
		862,
		"common",
		"node_modules_ionic_core_dist_esm_ion-item_8_entry_js"
	],
	"./ion-loading.entry.js": [
		7509,
		"common",
		"node_modules_ionic_core_dist_esm_ion-loading_entry_js"
	],
	"./ion-menu_3.entry.js": [
		6272,
		"common",
		"node_modules_ionic_core_dist_esm_ion-menu_3_entry_js"
	],
	"./ion-modal.entry.js": [
		1855,
		"common",
		"node_modules_ionic_core_dist_esm_ion-modal_entry_js"
	],
	"./ion-nav_2.entry.js": [
		8708,
		"common",
		"node_modules_ionic_core_dist_esm_ion-nav_2_entry_js"
	],
	"./ion-popover.entry.js": [
		3527,
		"common",
		"node_modules_ionic_core_dist_esm_ion-popover_entry_js"
	],
	"./ion-progress-bar.entry.js": [
		4694,
		"common",
		"node_modules_ionic_core_dist_esm_ion-progress-bar_entry_js"
	],
	"./ion-radio_2.entry.js": [
		9222,
		"common",
		"node_modules_ionic_core_dist_esm_ion-radio_2_entry_js"
	],
	"./ion-range.entry.js": [
		5277,
		"common",
		"node_modules_ionic_core_dist_esm_ion-range_entry_js"
	],
	"./ion-refresher_2.entry.js": [
		9921,
		"common",
		"node_modules_ionic_core_dist_esm_ion-refresher_2_entry_js"
	],
	"./ion-reorder_2.entry.js": [
		3122,
		"common",
		"node_modules_ionic_core_dist_esm_ion-reorder_2_entry_js"
	],
	"./ion-ripple-effect.entry.js": [
		1602,
		"node_modules_ionic_core_dist_esm_ion-ripple-effect_entry_js"
	],
	"./ion-route_4.entry.js": [
		5174,
		"common",
		"node_modules_ionic_core_dist_esm_ion-route_4_entry_js"
	],
	"./ion-searchbar.entry.js": [
		7895,
		"common",
		"node_modules_ionic_core_dist_esm_ion-searchbar_entry_js"
	],
	"./ion-segment_2.entry.js": [
		6164,
		"common",
		"node_modules_ionic_core_dist_esm_ion-segment_2_entry_js"
	],
	"./ion-select_3.entry.js": [
		592,
		"common",
		"node_modules_ionic_core_dist_esm_ion-select_3_entry_js"
	],
	"./ion-slide_2.entry.js": [
		7162,
		"node_modules_ionic_core_dist_esm_ion-slide_2_entry_js"
	],
	"./ion-spinner.entry.js": [
		1374,
		"common",
		"node_modules_ionic_core_dist_esm_ion-spinner_entry_js"
	],
	"./ion-split-pane.entry.js": [
		7896,
		"node_modules_ionic_core_dist_esm_ion-split-pane_entry_js"
	],
	"./ion-tab-bar_2.entry.js": [
		5043,
		"common",
		"node_modules_ionic_core_dist_esm_ion-tab-bar_2_entry_js"
	],
	"./ion-tab_2.entry.js": [
		7802,
		"common",
		"node_modules_ionic_core_dist_esm_ion-tab_2_entry_js"
	],
	"./ion-text.entry.js": [
		9072,
		"common",
		"node_modules_ionic_core_dist_esm_ion-text_entry_js"
	],
	"./ion-textarea.entry.js": [
		2191,
		"common",
		"node_modules_ionic_core_dist_esm_ion-textarea_entry_js"
	],
	"./ion-toast.entry.js": [
		801,
		"common",
		"node_modules_ionic_core_dist_esm_ion-toast_entry_js"
	],
	"./ion-toggle.entry.js": [
		7110,
		"common",
		"node_modules_ionic_core_dist_esm_ion-toggle_entry_js"
	],
	"./ion-virtual-scroll.entry.js": [
		431,
		"node_modules_ionic_core_dist_esm_ion-virtual-scroll_entry_js"
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(() => {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(() => {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = () => (Object.keys(map));
webpackAsyncContext.id = 863;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 3069:
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("#welcome-message {\n  text-align: center;\n}\n\nion-item {\n  padding-left: 20px;\n  margin-bottom: 10px;\n}\n\n.active {\n  border-left: 5px solid;\n  color: #2EB3C2;\n  padding-left: 15px;\n}\n\n.active ion-icon {\n  color: #2EB3C2;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLG1CQUFBO0FBQ0o7O0FBRUE7RUFDSSxzQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtBQUNKOztBQUNJO0VBQ0ksY0FBQTtBQUNSIiwiZmlsZSI6ImFwcC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiN3ZWxjb21lLW1lc3NhZ2Uge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuaW9uLWl0ZW0ge1xuICAgIHBhZGRpbmctbGVmdDogMjBweDtcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xufVxuXG4uYWN0aXZlIHtcbiAgICBib3JkZXItbGVmdDogNXB4IHNvbGlkO1xuICAgIGNvbG9yOiAjMkVCM0MyO1xuICAgIHBhZGRpbmctbGVmdDogMTVweDtcblxuICAgIGlvbi1pY29uIHtcbiAgICAgICAgY29sb3I6ICMyRUIzQzI7XG4gICAgfVxufSJdfQ== */");

/***/ }),

/***/ 1106:
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-app>\n  <ion-router-outlet id=\"main\"></ion-router-outlet>\n  <ion-menu menuId=\"main-menu\" content-id=\"main\">\n    <ion-header>\n      <ion-toolbar>\n          <ion-card-header>\n            <ion-card-title><img src=\"assets/icon/spytrack_logo.PNG\"></ion-card-title>\n          </ion-card-header>\n          <ion-card-content>\n           <p id=\"welcome-message\"><strong>Welcome, {{ user }}</strong></p>\n          </ion-card-content>\n        <!-- <ion-title>\n          Menu\n        </ion-title> -->\n      </ion-toolbar>\n    </ion-header>\n    <ion-content>\n      <ion-list>\n        <ion-menu-toggle>\n          <ion-item [routerLink]=\"['/live-tracking']\" [ngClass]=\"{'active' : activePage == 'live-tracking'}\"  (click)=\"selectedPage('live-tracking')\">\n            <ion-icon name=\"navigate\" slot=\"start\"></ion-icon>\n              <ion-label>Live Tracking</ion-label>\n          </ion-item>\n        </ion-menu-toggle>\n        <ion-menu-toggle>\n          <ion-item [routerLink]=\"['/time-machine']\" [ngClass]=\"{'active' : activePage == 'time-machine'}\" (click)=\"selectedPage('time-machine')\">\n            <ion-icon name=\"time\" slot=\"start\"></ion-icon>\n              <ion-label>Time Machine</ion-label>\n          </ion-item>\n        </ion-menu-toggle>\n        <ion-menu-toggle>\n          <ion-item ion-item [routerLink]=\"['activities']\" [ngClass]=\"{'active' : activePage == 'activities'}\" (click)=\"selectedPage('activities')\">\n            <ion-icon name=\"heart\" slot=\"start\"></ion-icon>\n              <ion-label>Activities</ion-label>\n          </ion-item>\n        </ion-menu-toggle>\n        <ion-menu-toggle>\n          <ion-item [routerLink]=\"['alerts']\" [ngClass]=\"{'active' : activePage == 'alerts'}\" (click)=\"selectedPage('alerts')\">\n            <ion-icon name=\"notifications\" slot=\"start\"></ion-icon>\n              <ion-label>Alerts</ion-label>\n          </ion-item>\n        </ion-menu-toggle>\n        <ion-menu-toggle>\n          <ion-item [routerLink]=\"['preferences']\" [ngClass]=\"{'active' : activePage == 'preferences'}\" (click)=\"selectedPage('preferences')\">\n            <ion-icon name=\"settings\" slot=\"start\"></ion-icon>\n              <ion-label>Preferences</ion-label>\n          </ion-item>\n        </ion-menu-toggle>\n        <ion-menu-toggle>\n          <ion-item [routerLink]=\"['activation']\" [ngClass]=\"{'active' : activePage == 'activation'}\" (click)=\"selectedPage('activation')\">\n            <ion-icon name=\"analytics-outline\" slot=\"start\"></ion-icon>\n              <ion-label>Activate Tracker</ion-label>\n          </ion-item>\n        </ion-menu-toggle>\n        <ion-menu-toggle>\n          <ion-item [routerLink]=\"['help']\" [ngClass]=\"{'active' : activePage == 'help'}\" (click)=\"selectedPage('help')\">\n            <ion-icon name=\"warning\" slot=\"start\"></ion-icon>\n              <ion-label>Help</ion-label>\n          </ion-item>\n        </ion-menu-toggle>\n        <ion-menu-toggle>\n          <ion-item (click)=\"logout()\">\n            <ion-icon name=\"power\" slot=\"start\"></ion-icon>\n              <ion-label style=\"color: red;\">Sign Out</ion-label>\n          </ion-item>\n        </ion-menu-toggle>\n      </ion-list>\n    </ion-content>\n  </ion-menu>\n</ion-app>\n");

/***/ })

},
/******/ __webpack_require__ => { // webpackRuntimeModules
/******/ "use strict";
/******/ 
/******/ var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
/******/ __webpack_require__.O(0, ["vendor"], () => (__webpack_exec__(4431)));
/******/ var __webpack_exports__ = __webpack_require__.O();
/******/ }
]);
//# sourceMappingURL=main.js.map