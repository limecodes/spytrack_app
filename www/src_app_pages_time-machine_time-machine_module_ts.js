(self["webpackChunkspytrack"] = self["webpackChunkspytrack"] || []).push([["src_app_pages_time-machine_time-machine_module_ts"],{

/***/ 6351:
/*!*******************************************************************************************!*\
  !*** ./node_modules/@awesome-cordova-plugins/core/__ivy_ngcc__/awesome-cordova-plugin.js ***!
  \*******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AwesomeCordovaNativePlugin": () => (/* binding */ AwesomeCordovaNativePlugin)
/* harmony export */ });
/* harmony import */ var _decorators_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./decorators/common */ 9343);
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./util */ 1674);


var AwesomeCordovaNativePlugin = /** @class */ (function () {
    function AwesomeCordovaNativePlugin() {
    }
    /**
     * Returns a boolean that indicates whether the plugin is installed
     *
     * @returns {boolean}
     */
    AwesomeCordovaNativePlugin.installed = function () {
        var isAvailable = (0,_decorators_common__WEBPACK_IMPORTED_MODULE_0__.checkAvailability)(this.pluginRef) === true;
        return isAvailable;
    };
    /**
     * Returns the original plugin object
     */
    AwesomeCordovaNativePlugin.getPlugin = function () {
        if (typeof window !== 'undefined') {
            return (0,_util__WEBPACK_IMPORTED_MODULE_1__.get)(window, this.pluginRef);
        }
        return null;
    };
    /**
     * Returns the plugin's name
     */
    AwesomeCordovaNativePlugin.getPluginName = function () {
        var pluginName = this.pluginName;
        return pluginName;
    };
    /**
     * Returns the plugin's reference
     */
    AwesomeCordovaNativePlugin.getPluginRef = function () {
        var pluginRef = this.pluginRef;
        return pluginRef;
    };
    /**
     * Returns the plugin's install name
     */
    AwesomeCordovaNativePlugin.getPluginInstallName = function () {
        var plugin = this.plugin;
        return plugin;
    };
    /**
     * Returns the plugin's supported platforms
     */
    AwesomeCordovaNativePlugin.getSupportedPlatforms = function () {
        var platform = this.platforms;
        return platform;
    };
    AwesomeCordovaNativePlugin.pluginName = '';
    AwesomeCordovaNativePlugin.pluginRef = '';
    AwesomeCordovaNativePlugin.plugin = '';
    AwesomeCordovaNativePlugin.repo = '';
    AwesomeCordovaNativePlugin.platforms = [];
    AwesomeCordovaNativePlugin.install = '';
    return AwesomeCordovaNativePlugin;
}());

//# sourceMappingURL=awesome-cordova-plugin.js.map

/***/ }),

/***/ 6367:
/*!******************************************************************************!*\
  !*** ./node_modules/@awesome-cordova-plugins/core/__ivy_ngcc__/bootstrap.js ***!
  \******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "checkReady": () => (/* binding */ checkReady)
/* harmony export */ });
/**
 *
 */
function checkReady() {
    if (typeof process === 'undefined') {
        var win_1 = typeof window !== 'undefined' ? window : {};
        var DEVICE_READY_TIMEOUT_1 = 5000;
        // To help developers using cordova, we listen for the device ready event and
        // log an error if it didn't fire in a reasonable amount of time. Generally,
        // when this happens, developers should remove and reinstall plugins, since
        // an inconsistent plugin is often the culprit.
        var before_1 = Date.now();
        var didFireReady_1 = false;
        win_1.document.addEventListener('deviceready', function () {
            console.log("Ionic Native: deviceready event fired after " + (Date.now() - before_1) + " ms");
            didFireReady_1 = true;
        });
        setTimeout(function () {
            if (!didFireReady_1 && win_1.cordova) {
                console.warn("Ionic Native: deviceready did not fire within " + DEVICE_READY_TIMEOUT_1 + "ms. This can happen when plugins are in an inconsistent state. Try removing plugins from plugins/ and reinstalling them.");
            }
        }, DEVICE_READY_TIMEOUT_1);
    }
}
//# sourceMappingURL=bootstrap.js.map

/***/ }),

/***/ 9343:
/*!**************************************************************************************!*\
  !*** ./node_modules/@awesome-cordova-plugins/core/__ivy_ngcc__/decorators/common.js ***!
  \**************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ERR_CORDOVA_NOT_AVAILABLE": () => (/* binding */ ERR_CORDOVA_NOT_AVAILABLE),
/* harmony export */   "ERR_PLUGIN_NOT_INSTALLED": () => (/* binding */ ERR_PLUGIN_NOT_INSTALLED),
/* harmony export */   "getPromise": () => (/* binding */ getPromise),
/* harmony export */   "wrapPromise": () => (/* binding */ wrapPromise),
/* harmony export */   "checkAvailability": () => (/* binding */ checkAvailability),
/* harmony export */   "instanceAvailability": () => (/* binding */ instanceAvailability),
/* harmony export */   "setIndex": () => (/* binding */ setIndex),
/* harmony export */   "callCordovaPlugin": () => (/* binding */ callCordovaPlugin),
/* harmony export */   "callInstance": () => (/* binding */ callInstance),
/* harmony export */   "getPlugin": () => (/* binding */ getPlugin),
/* harmony export */   "get": () => (/* binding */ get),
/* harmony export */   "pluginWarn": () => (/* binding */ pluginWarn),
/* harmony export */   "cordovaWarn": () => (/* binding */ cordovaWarn),
/* harmony export */   "wrap": () => (/* binding */ wrap),
/* harmony export */   "wrapInstance": () => (/* binding */ wrapInstance)
/* harmony export */ });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ 9165);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ 2759);

var ERR_CORDOVA_NOT_AVAILABLE = { error: 'cordova_not_available' };
var ERR_PLUGIN_NOT_INSTALLED = { error: 'plugin_not_installed' };
/**
 * @param callback
 */
function getPromise(callback) {
    var tryNativePromise = function () {
        if (Promise) {
            return new Promise(function (resolve, reject) {
                callback(resolve, reject);
            });
        }
        else {
            console.error('No Promise support or polyfill found. To enable Ionic Native support, please add the es6-promise polyfill before this script, or run with a library like Angular or on a recent browser.');
        }
    };
    if (typeof window !== 'undefined' && window.angular) {
        var doc = window.document;
        var injector = window.angular.element(doc.querySelector('[ng-app]') || doc.body).injector();
        if (injector) {
            var $q = injector.get('$q');
            return $q(function (resolve, reject) {
                callback(resolve, reject);
            });
        }
        console.warn("Angular 1 was detected but $q couldn't be retrieved. This is usually when the app is not bootstrapped on the html or body tag. Falling back to native promises which won't trigger an automatic digest when promises resolve.");
    }
    return tryNativePromise();
}
/**
 * @param pluginObj
 * @param methodName
 * @param args
 * @param opts
 */
function wrapPromise(pluginObj, methodName, args, opts) {
    if (opts === void 0) { opts = {}; }
    var pluginResult, rej;
    var p = getPromise(function (resolve, reject) {
        if (opts.destruct) {
            pluginResult = callCordovaPlugin(pluginObj, methodName, args, opts, function () {
                var args = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    args[_i] = arguments[_i];
                }
                return resolve(args);
            }, function () {
                var args = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    args[_i] = arguments[_i];
                }
                return reject(args);
            });
        }
        else {
            pluginResult = callCordovaPlugin(pluginObj, methodName, args, opts, resolve, reject);
        }
        rej = reject;
    });
    // Angular throws an error on unhandled rejection, but in this case we have already printed
    // a warning that Cordova is undefined or the plugin is uninstalled, so there is no reason
    // to error
    if (pluginResult && pluginResult.error) {
        p.catch(function () { });
        typeof rej === 'function' && rej(pluginResult.error);
    }
    return p;
}
/**
 * @param pluginObj
 * @param methodName
 * @param args
 * @param opts
 */
function wrapOtherPromise(pluginObj, methodName, args, opts) {
    if (opts === void 0) { opts = {}; }
    return getPromise(function (resolve, reject) {
        var pluginResult = callCordovaPlugin(pluginObj, methodName, args, opts);
        if (pluginResult) {
            if (pluginResult.error) {
                reject(pluginResult.error);
            }
            else if (pluginResult.then) {
                pluginResult.then(resolve).catch(reject);
            }
        }
        else {
            reject({ error: 'unexpected_error' });
        }
    });
}
/**
 * @param pluginObj
 * @param methodName
 * @param args
 * @param opts
 */
function wrapObservable(pluginObj, methodName, args, opts) {
    if (opts === void 0) { opts = {}; }
    return new rxjs__WEBPACK_IMPORTED_MODULE_0__.Observable(function (observer) {
        var pluginResult;
        if (opts.destruct) {
            pluginResult = callCordovaPlugin(pluginObj, methodName, args, opts, function () {
                var args = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    args[_i] = arguments[_i];
                }
                return observer.next(args);
            }, function () {
                var args = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    args[_i] = arguments[_i];
                }
                return observer.error(args);
            });
        }
        else {
            pluginResult = callCordovaPlugin(pluginObj, methodName, args, opts, observer.next.bind(observer), observer.error.bind(observer));
        }
        if (pluginResult && pluginResult.error) {
            observer.error(pluginResult.error);
            observer.complete();
        }
        return function () {
            try {
                if (opts.clearFunction) {
                    if (opts.clearWithArgs) {
                        return callCordovaPlugin(pluginObj, opts.clearFunction, args, opts, observer.next.bind(observer), observer.error.bind(observer));
                    }
                    return callCordovaPlugin(pluginObj, opts.clearFunction, []);
                }
            }
            catch (e) {
                console.warn('Unable to clear the previous observable watch for', pluginObj.constructor.getPluginName(), methodName);
                console.warn(e);
            }
        };
    });
}
/**
 * Wrap the event with an observable
 *
 * @private
 * @param event event name
 * @param element The element to attach the event listener to
 * @returns {Observable}
 */
function wrapEventObservable(event, element) {
    element =
        typeof window !== 'undefined' && element
            ? get(window, element)
            : element || (typeof window !== 'undefined' ? window : {});
    return (0,rxjs__WEBPACK_IMPORTED_MODULE_1__.fromEvent)(element, event);
}
/**
 * @param plugin
 * @param methodName
 * @param pluginName
 */
function checkAvailability(plugin, methodName, pluginName) {
    var pluginRef, pluginPackage;
    if (typeof plugin === 'string') {
        pluginRef = plugin;
    }
    else {
        pluginRef = plugin.constructor.getPluginRef();
        pluginName = plugin.constructor.getPluginName();
        pluginPackage = plugin.constructor.getPluginInstallName();
    }
    var pluginInstance = getPlugin(pluginRef);
    if (!pluginInstance || (!!methodName && typeof pluginInstance[methodName] === 'undefined')) {
        if (typeof window === 'undefined' || !window.cordova) {
            cordovaWarn(pluginName, methodName);
            return ERR_CORDOVA_NOT_AVAILABLE;
        }
        pluginWarn(pluginName, pluginPackage, methodName);
        return ERR_PLUGIN_NOT_INSTALLED;
    }
    return true;
}
/**
 * Checks if _objectInstance exists and has the method/property
 *
 * @param pluginObj
 * @param methodName
 * @private
 */
function instanceAvailability(pluginObj, methodName) {
    return pluginObj._objectInstance && (!methodName || typeof pluginObj._objectInstance[methodName] !== 'undefined');
}
/**
 * @param args
 * @param opts
 * @param resolve
 * @param reject
 */
function setIndex(args, opts, resolve, reject) {
    if (opts === void 0) { opts = {}; }
    // ignore resolve and reject in case sync
    if (opts.sync) {
        return args;
    }
    // If the plugin method expects myMethod(success, err, options)
    if (opts.callbackOrder === 'reverse') {
        // Get those arguments in the order [resolve, reject, ...restOfArgs]
        args.unshift(reject);
        args.unshift(resolve);
    }
    else if (opts.callbackStyle === 'node') {
        args.push(function (err, result) {
            if (err) {
                reject(err);
            }
            else {
                resolve(result);
            }
        });
    }
    else if (opts.callbackStyle === 'object' && opts.successName && opts.errorName) {
        var obj = {};
        obj[opts.successName] = resolve;
        obj[opts.errorName] = reject;
        args.push(obj);
    }
    else if (typeof opts.successIndex !== 'undefined' || typeof opts.errorIndex !== 'undefined') {
        var setSuccessIndex = function () {
            // If we've specified a success/error index
            if (opts.successIndex > args.length) {
                args[opts.successIndex] = resolve;
            }
            else {
                args.splice(opts.successIndex, 0, resolve);
            }
        };
        var setErrorIndex = function () {
            // We don't want that the reject cb gets spliced into the position of an optional argument that has not been
            // defined and thus causing non expected behavior.
            if (opts.errorIndex > args.length) {
                args[opts.errorIndex] = reject; // insert the reject fn at the correct specific index
            }
            else {
                args.splice(opts.errorIndex, 0, reject); // otherwise just splice it into the array
            }
        };
        if (opts.successIndex > opts.errorIndex) {
            setErrorIndex();
            setSuccessIndex();
        }
        else {
            setSuccessIndex();
            setErrorIndex();
        }
    }
    else {
        // Otherwise, let's tack them on to the end of the argument list
        // which is 90% of cases
        args.push(resolve);
        args.push(reject);
    }
    return args;
}
/**
 * @param pluginObj
 * @param methodName
 * @param args
 * @param opts
 * @param resolve
 * @param reject
 */
function callCordovaPlugin(pluginObj, methodName, args, opts, resolve, reject) {
    if (opts === void 0) { opts = {}; }
    // Try to figure out where the success/error callbacks need to be bound
    // to our promise resolve/reject handlers.
    args = setIndex(args, opts, resolve, reject);
    var availabilityCheck = checkAvailability(pluginObj, methodName);
    if (availabilityCheck === true) {
        var pluginInstance = getPlugin(pluginObj.constructor.getPluginRef());
        // eslint-disable-next-line prefer-spread
        return pluginInstance[methodName].apply(pluginInstance, args);
    }
    else {
        return availabilityCheck;
    }
}
/**
 * @param pluginObj
 * @param methodName
 * @param args
 * @param opts
 * @param resolve
 * @param reject
 */
function callInstance(pluginObj, methodName, args, opts, resolve, reject) {
    if (opts === void 0) { opts = {}; }
    args = setIndex(args, opts, resolve, reject);
    if (instanceAvailability(pluginObj, methodName)) {
        // eslint-disable-next-line prefer-spread
        return pluginObj._objectInstance[methodName].apply(pluginObj._objectInstance, args);
    }
}
/**
 * @param pluginRef
 */
function getPlugin(pluginRef) {
    if (typeof window !== 'undefined') {
        return get(window, pluginRef);
    }
    return null;
}
/**
 * @param element
 * @param path
 */
function get(element, path) {
    var paths = path.split('.');
    var obj = element;
    for (var i = 0; i < paths.length; i++) {
        if (!obj) {
            return null;
        }
        obj = obj[paths[i]];
    }
    return obj;
}
/**
 * @param pluginName
 * @param plugin
 * @param method
 */
function pluginWarn(pluginName, plugin, method) {
    if (method) {
        console.warn('Native: tried calling ' + pluginName + '.' + method + ', but the ' + pluginName + ' plugin is not installed.');
    }
    else {
        console.warn("Native: tried accessing the " + pluginName + " plugin but it's not installed.");
    }
    if (plugin) {
        console.warn("Install the " + pluginName + " plugin: 'ionic cordova plugin add " + plugin + "'");
    }
}
/**
 * @private
 * @param pluginName
 * @param method
 */
function cordovaWarn(pluginName, method) {
    if (typeof process === 'undefined') {
        if (method) {
            console.warn('Native: tried calling ' +
                pluginName +
                '.' +
                method +
                ', but Cordova is not available. Make sure to include cordova.js or run in a device/simulator');
        }
        else {
            console.warn('Native: tried accessing the ' +
                pluginName +
                ' plugin but Cordova is not available. Make sure to include cordova.js or run in a device/simulator');
        }
    }
}
/**
 * @param pluginObj
 * @param methodName
 * @param opts
 * @private
 */
var wrap = function (pluginObj, methodName, opts) {
    if (opts === void 0) { opts = {}; }
    return function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        if (opts.sync) {
            // Sync doesn't wrap the plugin with a promise or observable, it returns the result as-is
            return callCordovaPlugin(pluginObj, methodName, args, opts);
        }
        else if (opts.observable) {
            return wrapObservable(pluginObj, methodName, args, opts);
        }
        else if (opts.eventObservable && opts.event) {
            return wrapEventObservable(opts.event, opts.element);
        }
        else if (opts.otherPromise) {
            return wrapOtherPromise(pluginObj, methodName, args, opts);
        }
        else {
            return wrapPromise(pluginObj, methodName, args, opts);
        }
    };
};
/**
 * @param pluginObj
 * @param methodName
 * @param opts
 * @private
 */
function wrapInstance(pluginObj, methodName, opts) {
    if (opts === void 0) { opts = {}; }
    return function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        if (opts.sync) {
            return callInstance(pluginObj, methodName, args, opts);
        }
        else if (opts.observable) {
            return new rxjs__WEBPACK_IMPORTED_MODULE_0__.Observable(function (observer) {
                var pluginResult;
                if (opts.destruct) {
                    pluginResult = callInstance(pluginObj, methodName, args, opts, function () {
                        var args = [];
                        for (var _i = 0; _i < arguments.length; _i++) {
                            args[_i] = arguments[_i];
                        }
                        return observer.next(args);
                    }, function () {
                        var args = [];
                        for (var _i = 0; _i < arguments.length; _i++) {
                            args[_i] = arguments[_i];
                        }
                        return observer.error(args);
                    });
                }
                else {
                    pluginResult = callInstance(pluginObj, methodName, args, opts, observer.next.bind(observer), observer.error.bind(observer));
                }
                if (pluginResult && pluginResult.error) {
                    observer.error(pluginResult.error);
                }
                return function () {
                    try {
                        if (opts.clearWithArgs) {
                            return callInstance(pluginObj, opts.clearFunction, args, opts, observer.next.bind(observer), observer.error.bind(observer));
                        }
                        return callInstance(pluginObj, opts.clearFunction, []);
                    }
                    catch (e) {
                        console.warn('Unable to clear the previous observable watch for', pluginObj.constructor.getPluginName(), methodName);
                        console.warn(e);
                    }
                };
            });
        }
        else if (opts.otherPromise) {
            return getPromise(function (resolve, reject) {
                var result;
                if (opts.destruct) {
                    result = callInstance(pluginObj, methodName, args, opts, function () {
                        var args = [];
                        for (var _i = 0; _i < arguments.length; _i++) {
                            args[_i] = arguments[_i];
                        }
                        return resolve(args);
                    }, function () {
                        var args = [];
                        for (var _i = 0; _i < arguments.length; _i++) {
                            args[_i] = arguments[_i];
                        }
                        return reject(args);
                    });
                }
                else {
                    result = callInstance(pluginObj, methodName, args, opts, resolve, reject);
                }
                if (result && result.then) {
                    result.then(resolve, reject);
                }
                else {
                    reject();
                }
            });
        }
        else {
            var pluginResult_1, rej_1;
            var p = getPromise(function (resolve, reject) {
                if (opts.destruct) {
                    pluginResult_1 = callInstance(pluginObj, methodName, args, opts, function () {
                        var args = [];
                        for (var _i = 0; _i < arguments.length; _i++) {
                            args[_i] = arguments[_i];
                        }
                        return resolve(args);
                    }, function () {
                        var args = [];
                        for (var _i = 0; _i < arguments.length; _i++) {
                            args[_i] = arguments[_i];
                        }
                        return reject(args);
                    });
                }
                else {
                    pluginResult_1 = callInstance(pluginObj, methodName, args, opts, resolve, reject);
                }
                rej_1 = reject;
            });
            // Angular throws an error on unhandled rejection, but in this case we have already printed
            // a warning that Cordova is undefined or the plugin is uninstalled, so there is no reason
            // to error
            if (pluginResult_1 && pluginResult_1.error) {
                p.catch(function () { });
                typeof rej_1 === 'function' && rej_1(pluginResult_1.error);
            }
            return p;
        }
    };
}
//# sourceMappingURL=common.js.map

/***/ }),

/***/ 531:
/*!*********************************************************************************************************!*\
  !*** ./node_modules/@awesome-cordova-plugins/core/__ivy_ngcc__/decorators/cordova-function-override.js ***!
  \*********************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "cordovaFunctionOverride": () => (/* binding */ cordovaFunctionOverride)
/* harmony export */ });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ 9165);
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./common */ 9343);


/**
 * @param pluginObj
 * @param methodName
 */
function overrideFunction(pluginObj, methodName) {
    return new rxjs__WEBPACK_IMPORTED_MODULE_1__.Observable(function (observer) {
        var availabilityCheck = (0,_common__WEBPACK_IMPORTED_MODULE_0__.checkAvailability)(pluginObj, methodName);
        if (availabilityCheck === true) {
            var pluginInstance_1 = (0,_common__WEBPACK_IMPORTED_MODULE_0__.getPlugin)(pluginObj.constructor.getPluginRef());
            pluginInstance_1[methodName] = observer.next.bind(observer);
            return function () { return (pluginInstance_1[methodName] = function () { }); };
        }
        else {
            observer.error(availabilityCheck);
            observer.complete();
        }
    });
}
/**
 * @param pluginObj
 * @param methodName
 * @param args
 */
function cordovaFunctionOverride(pluginObj, methodName, args) {
    if (args === void 0) { args = []; }
    return overrideFunction(pluginObj, methodName);
}
//# sourceMappingURL=cordova-function-override.js.map

/***/ }),

/***/ 808:
/*!************************************************************************************************!*\
  !*** ./node_modules/@awesome-cordova-plugins/core/__ivy_ngcc__/decorators/cordova-instance.js ***!
  \************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "cordovaInstance": () => (/* binding */ cordovaInstance)
/* harmony export */ });
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./common */ 9343);

/**
 * @param pluginObj
 * @param methodName
 * @param config
 * @param args
 */
function cordovaInstance(pluginObj, methodName, config, args) {
    args = Array.from(args);
    return (0,_common__WEBPACK_IMPORTED_MODULE_0__.wrapInstance)(pluginObj, methodName, config).apply(this, args);
}
//# sourceMappingURL=cordova-instance.js.map

/***/ }),

/***/ 4070:
/*!************************************************************************************************!*\
  !*** ./node_modules/@awesome-cordova-plugins/core/__ivy_ngcc__/decorators/cordova-property.js ***!
  \************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "cordovaPropertyGet": () => (/* binding */ cordovaPropertyGet),
/* harmony export */   "cordovaPropertySet": () => (/* binding */ cordovaPropertySet)
/* harmony export */ });
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./common */ 9343);

/**
 * @param pluginObj
 * @param key
 */
function cordovaPropertyGet(pluginObj, key) {
    if ((0,_common__WEBPACK_IMPORTED_MODULE_0__.checkAvailability)(pluginObj, key) === true) {
        return (0,_common__WEBPACK_IMPORTED_MODULE_0__.getPlugin)(pluginObj.constructor.getPluginRef())[key];
    }
    return null;
}
/**
 * @param pluginObj
 * @param key
 * @param value
 */
function cordovaPropertySet(pluginObj, key, value) {
    if ((0,_common__WEBPACK_IMPORTED_MODULE_0__.checkAvailability)(pluginObj, key) === true) {
        (0,_common__WEBPACK_IMPORTED_MODULE_0__.getPlugin)(pluginObj.constructor.getPluginRef())[key] = value;
    }
}
//# sourceMappingURL=cordova-property.js.map

/***/ }),

/***/ 3364:
/*!***************************************************************************************!*\
  !*** ./node_modules/@awesome-cordova-plugins/core/__ivy_ngcc__/decorators/cordova.js ***!
  \***************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "cordova": () => (/* binding */ cordova)
/* harmony export */ });
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./common */ 9343);

/**
 * @param pluginObj
 * @param methodName
 * @param config
 * @param args
 */
function cordova(pluginObj, methodName, config, args) {
    return (0,_common__WEBPACK_IMPORTED_MODULE_0__.wrap)(pluginObj, methodName, config).apply(this, args);
}
//# sourceMappingURL=cordova.js.map

/***/ }),

/***/ 422:
/*!*************************************************************************************************!*\
  !*** ./node_modules/@awesome-cordova-plugins/core/__ivy_ngcc__/decorators/instance-property.js ***!
  \*************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "instancePropertyGet": () => (/* binding */ instancePropertyGet),
/* harmony export */   "instancePropertySet": () => (/* binding */ instancePropertySet)
/* harmony export */ });
/**
 * @param pluginObj
 * @param key
 */
function instancePropertyGet(pluginObj, key) {
    if (pluginObj._objectInstance && pluginObj._objectInstance[key]) {
        return pluginObj._objectInstance[key];
    }
    return null;
}
/**
 * @param pluginObj
 * @param key
 * @param value
 */
function instancePropertySet(pluginObj, key, value) {
    if (pluginObj._objectInstance) {
        pluginObj._objectInstance[key] = value;
    }
}
//# sourceMappingURL=instance-property.js.map

/***/ }),

/***/ 303:
/*!******************************************************************************************!*\
  !*** ./node_modules/@awesome-cordova-plugins/core/__ivy_ngcc__/decorators/interfaces.js ***!
  \******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);

//# sourceMappingURL=interfaces.js.map

/***/ }),

/***/ 6887:
/*!**************************************************************************!*\
  !*** ./node_modules/@awesome-cordova-plugins/core/__ivy_ngcc__/index.js ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AwesomeCordovaNativePlugin": () => (/* reexport safe */ _awesome_cordova_plugin__WEBPACK_IMPORTED_MODULE_1__.AwesomeCordovaNativePlugin),
/* harmony export */   "checkAvailability": () => (/* reexport safe */ _decorators_common__WEBPACK_IMPORTED_MODULE_2__.checkAvailability),
/* harmony export */   "instanceAvailability": () => (/* reexport safe */ _decorators_common__WEBPACK_IMPORTED_MODULE_2__.instanceAvailability),
/* harmony export */   "wrap": () => (/* reexport safe */ _decorators_common__WEBPACK_IMPORTED_MODULE_2__.wrap),
/* harmony export */   "getPromise": () => (/* reexport safe */ _decorators_common__WEBPACK_IMPORTED_MODULE_2__.getPromise),
/* harmony export */   "cordova": () => (/* reexport safe */ _decorators_cordova__WEBPACK_IMPORTED_MODULE_3__.cordova),
/* harmony export */   "cordovaFunctionOverride": () => (/* reexport safe */ _decorators_cordova_function_override__WEBPACK_IMPORTED_MODULE_4__.cordovaFunctionOverride),
/* harmony export */   "cordovaInstance": () => (/* reexport safe */ _decorators_cordova_instance__WEBPACK_IMPORTED_MODULE_5__.cordovaInstance),
/* harmony export */   "cordovaPropertyGet": () => (/* reexport safe */ _decorators_cordova_property__WEBPACK_IMPORTED_MODULE_6__.cordovaPropertyGet),
/* harmony export */   "cordovaPropertySet": () => (/* reexport safe */ _decorators_cordova_property__WEBPACK_IMPORTED_MODULE_6__.cordovaPropertySet),
/* harmony export */   "instancePropertyGet": () => (/* reexport safe */ _decorators_instance_property__WEBPACK_IMPORTED_MODULE_7__.instancePropertyGet),
/* harmony export */   "instancePropertySet": () => (/* reexport safe */ _decorators_instance_property__WEBPACK_IMPORTED_MODULE_7__.instancePropertySet)
/* harmony export */ });
/* harmony import */ var _bootstrap__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./bootstrap */ 6367);
/* harmony import */ var _awesome_cordova_plugin__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./awesome-cordova-plugin */ 6351);
/* harmony import */ var _decorators_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./decorators/common */ 9343);
/* harmony import */ var _decorators_cordova__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./decorators/cordova */ 3364);
/* harmony import */ var _decorators_cordova_function_override__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./decorators/cordova-function-override */ 531);
/* harmony import */ var _decorators_cordova_instance__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./decorators/cordova-instance */ 808);
/* harmony import */ var _decorators_cordova_property__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./decorators/cordova-property */ 4070);
/* harmony import */ var _decorators_instance_property__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./decorators/instance-property */ 422);
/* harmony import */ var _decorators_interfaces__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./decorators/interfaces */ 303);


// Decorators







(0,_bootstrap__WEBPACK_IMPORTED_MODULE_0__.checkReady)();

//# sourceMappingURL=index.js.map

/***/ }),

/***/ 1674:
/*!*************************************************************************!*\
  !*** ./node_modules/@awesome-cordova-plugins/core/__ivy_ngcc__/util.js ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "get": () => (/* binding */ get),
/* harmony export */   "getPromise": () => (/* binding */ getPromise)
/* harmony export */ });
/**
 * @param element
 * @param path
 * @private
 */
function get(element, path) {
    var paths = path.split('.');
    var obj = element;
    for (var i = 0; i < paths.length; i++) {
        if (!obj) {
            return null;
        }
        obj = obj[paths[i]];
    }
    return obj;
}
/**
 * @param callback
 * @private
 */
function getPromise(callback) {
    if (callback === void 0) { callback = function () { }; }
    var tryNativePromise = function () {
        if (typeof Promise === 'function' || (typeof window !== 'undefined' && window.Promise)) {
            return new Promise(function (resolve, reject) {
                callback(resolve, reject);
            });
        }
        else {
            console.error('No Promise support or polyfill found. To enable Ionic Native support, please add the es6-promise polyfill before this script, or run with a library like Angular or on a recent browser.');
        }
    };
    return tryNativePromise();
}
//# sourceMappingURL=util.js.map

/***/ }),

/***/ 5221:
/*!****************************************************************************************!*\
  !*** ./node_modules/@awesome-cordova-plugins/social-sharing/__ivy_ngcc__/ngx/index.js ***!
  \****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SocialSharing": () => (/* binding */ SocialSharing)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _awesome_cordova_plugins_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @awesome-cordova-plugins/core */ 6887);




var SocialSharing = /** @class */ (function (_super) {
    (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__extends)(SocialSharing, _super);
    function SocialSharing() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    SocialSharing.prototype.share = function (message, subject, file, url) { return (0,_awesome_cordova_plugins_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "share", { "successIndex": 4, "errorIndex": 5 }, arguments); };
    SocialSharing.prototype.shareWithOptions = function (options) { return (0,_awesome_cordova_plugins_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "shareWithOptions", { "platforms": ["iOS", "Android"] }, arguments); };
    SocialSharing.prototype.canShareVia = function (appName, message, subject, image, url) { return (0,_awesome_cordova_plugins_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "canShareVia", { "successIndex": 5, "errorIndex": 6, "platforms": ["iOS", "Android"] }, arguments); };
    SocialSharing.prototype.shareViaTwitter = function (message, image, url) { return (0,_awesome_cordova_plugins_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "shareViaTwitter", { "successIndex": 3, "errorIndex": 4, "platforms": ["iOS", "Android"] }, arguments); };
    SocialSharing.prototype.shareViaFacebook = function (message, image, url) { return (0,_awesome_cordova_plugins_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "shareViaFacebook", { "successIndex": 3, "errorIndex": 4, "platforms": ["iOS", "Android"] }, arguments); };
    SocialSharing.prototype.shareViaFacebookWithPasteMessageHint = function (message, image, url, pasteMessageHint) { return (0,_awesome_cordova_plugins_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "shareViaFacebookWithPasteMessageHint", { "successIndex": 4, "errorIndex": 5, "platforms": ["iOS", "Android"] }, arguments); };
    SocialSharing.prototype.shareViaInstagram = function (message, image) { return (0,_awesome_cordova_plugins_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "shareViaInstagram", { "platforms": ["iOS", "Android"] }, arguments); };
    SocialSharing.prototype.shareViaWhatsApp = function (message, image, url) { return (0,_awesome_cordova_plugins_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "shareViaWhatsApp", { "successIndex": 3, "errorIndex": 4, "platforms": ["iOS", "Android"] }, arguments); };
    SocialSharing.prototype.shareViaWhatsAppToReceiver = function (receiver, message, image, url) { return (0,_awesome_cordova_plugins_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "shareViaWhatsAppToReceiver", { "successIndex": 4, "errorIndex": 5, "platforms": ["iOS", "Android"] }, arguments); };
    SocialSharing.prototype.shareViaSMS = function (messge, phoneNumber) { return (0,_awesome_cordova_plugins_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "shareViaSMS", { "platforms": ["iOS", "Android"] }, arguments); };
    SocialSharing.prototype.canShareViaEmail = function () { return (0,_awesome_cordova_plugins_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "canShareViaEmail", { "platforms": ["iOS", "Android"] }, arguments); };
    SocialSharing.prototype.shareViaEmail = function (message, subject, to, cc, bcc, files) { return (0,_awesome_cordova_plugins_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "shareViaEmail", { "platforms": ["iOS", "Android"], "successIndex": 6, "errorIndex": 7 }, arguments); };
    SocialSharing.prototype.shareVia = function (appName, message, subject, image, url) { return (0,_awesome_cordova_plugins_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "shareVia", { "successIndex": 5, "errorIndex": 6, "platforms": ["iOS", "Android"] }, arguments); };
    SocialSharing.prototype.setIPadPopupCoordinates = function (targetBounds) { return (0,_awesome_cordova_plugins_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "setIPadPopupCoordinates", { "sync": true, "platforms": ["iOS"] }, arguments); };
    SocialSharing.prototype.saveToPhotoAlbum = function (fileOrFileArray) { return (0,_awesome_cordova_plugins_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "saveToPhotoAlbum", { "platforms": ["iOS"] }, arguments); };
    SocialSharing.prototype.shareViaWhatsAppToPhone = function (phone, message, fileOrFileArray, url) { return (0,_awesome_cordova_plugins_core__WEBPACK_IMPORTED_MODULE_0__.cordova)(this, "shareViaWhatsAppToPhone", { "successIndex": 5, "errorIndex": 6, "platforms": ["iOS", "Android"] }, arguments); };
    SocialSharing.pluginName = "SocialSharing";
    SocialSharing.plugin = "cordova-plugin-x-socialsharing";
    SocialSharing.pluginRef = "plugins.socialsharing";
    SocialSharing.repo = "https://github.com/EddyVerbruggen/SocialSharing-PhoneGap-Plugin";
    SocialSharing.platforms = ["Android", "Browser", "iOS", "Windows", "Windows Phone"];
SocialSharing.ɵfac = /*@__PURE__*/ function () { var ɵSocialSharing_BaseFactory; return function SocialSharing_Factory(t) { return (ɵSocialSharing_BaseFactory || (ɵSocialSharing_BaseFactory = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵgetInheritedFactory"](SocialSharing)))(t || SocialSharing); }; }();
SocialSharing.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjectable"]({ token: SocialSharing, factory: function (t) { return SocialSharing.ɵfac(t); } });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵsetClassMetadata"](SocialSharing, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable
    }], null, null); })();
    return SocialSharing;
}(_awesome_cordova_plugins_core__WEBPACK_IMPORTED_MODULE_0__.AwesomeCordovaNativePlugin));


//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9AYXdlc29tZS1jb3Jkb3ZhLXBsdWdpbnMvcGx1Z2lucy9zb2NpYWwtc2hhcmluZy9uZ3gvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyx1Q0FBK0MsTUFBTSwrQkFBK0IsQ0FBQzs7QUFDNUY7QUFHMEIsSUFvQ1MsaUNBQTBCO0FBQUM7QUFFdkM7QUFFa0M7QUFBTSxJQVU3RCw2QkFBSyxhQUFDLE9BQWdCLEVBQUUsT0FBZ0IsRUFBRSxJQUF3QixFQUFFLEdBQVk7QUFLbEMsSUFhOUMsd0NBQWdCLGFBQUMsT0FNaEI7QUFPRCxJQVlBLG1DQUFXLGFBQUMsT0FBZSxFQUFFLE9BQWdCLEVBQUUsT0FBZ0IsRUFBRSxLQUFjLEVBQUUsR0FBWTtBQVExRSxJQVNuQix1Q0FBZSxhQUFDLE9BQWUsRUFBRSxLQUFjLEVBQUUsR0FBWTtBQVF2QyxJQVN0Qix3Q0FBZ0IsYUFBQyxPQUFlLEVBQUUsS0FBYyxFQUFFLEdBQVk7QUFPcEMsSUFXMUIsNERBQW9DLGFBQ2xDLE9BQWUsRUFDZixLQUFjLEVBQ2QsR0FBWSxFQUNaLGdCQUF5QjtBQVVmLElBS1oseUNBQWlCLGFBQUMsT0FBZSxFQUFFLEtBQWE7QUFPL0IsSUFVakIsd0NBQWdCLGFBQUMsT0FBZSxFQUFFLEtBQWMsRUFBRSxHQUFZO0FBT3BCLElBVzFDLGtEQUEwQixhQUFDLFFBQWdCLEVBQUUsT0FBZSxFQUFFLEtBQWMsRUFBRSxHQUFZO0FBUTFELElBTWhDLG1DQUFXLGFBQUMsTUFBYyxFQUFFLFdBQW1CO0FBTzFDLElBS0wsd0NBQWdCO0FBUUwsSUFZWCxxQ0FBYSxhQUNYLE9BQWUsRUFDZixPQUFlLEVBQ2YsRUFBWSxFQUNaLEVBQWEsRUFDYixHQUFjLEVBQ2QsS0FBeUI7QUFRNkIsSUFZeEQsZ0NBQVEsYUFBQyxPQUFlLEVBQUUsT0FBZSxFQUFFLE9BQWdCLEVBQUUsS0FBYyxFQUFFLEdBQVk7QUFPMUUsSUFNZiwrQ0FBdUIsYUFBQyxZQUFvQjtBQUtWLElBTWxDLHdDQUFnQixhQUFDLGVBQWtDO0FBS1IsSUFhM0MsK0NBQXVCLGFBQ3JCLEtBQWEsRUFDYixPQUFlLEVBQ2YsZUFBa0MsRUFDbEMsR0FBWTtBQUtxRjtBQUFnRDtBQUE2RDtBQUF1RDtBQUE0RjtpREEvUnBXLFVBQVU7Ozs7MEJBQ0w7QUFBQyx3QkF6Q1A7QUFBRSxFQXlDaUMsMEJBQTBCO0FBQzVELFNBRFksYUFBYTtBQUFJIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ29yZG92YSwgQXdlc29tZUNvcmRvdmFOYXRpdmVQbHVnaW4sIFBsdWdpbiB9IGZyb20gJ0Bhd2Vzb21lLWNvcmRvdmEtcGx1Z2lucy9jb3JlJztcblxuLyoqXG4gKiBAbmFtZSBTb2NpYWwgU2hhcmluZ1xuICogQHByZW1pZXIgc29jaWFsLXNoYXJpbmdcbiAqIEBkZXNjcmlwdGlvblxuICogU2hhcmUgdGV4dCwgZmlsZXMsIGltYWdlcywgYW5kIGxpbmtzIHZpYSBzb2NpYWwgbmV0d29ya3MsIHNtcywgYW5kIGVtYWlsLlxuICpcbiAqIEZvciBCcm93c2VyIHVzYWdlIGNoZWNrIG91dCB0aGUgV2ViIFNoYXJlIEFQSSBkb2NzOiBodHRwczovL2dpdGh1Yi5jb20vRWRkeVZlcmJydWdnZW4vU29jaWFsU2hhcmluZy1QaG9uZUdhcC1QbHVnaW4jNS13ZWItc2hhcmUtYXBpXG4gKiBAdXNhZ2VcbiAqIGBgYHR5cGVzY3JpcHRcbiAqIGltcG9ydCB7IFNvY2lhbFNoYXJpbmcgfSBmcm9tICdAYXdlc29tZS1jb3Jkb3ZhLXBsdWdpbnMvc29jaWFsLXNoYXJpbmcvbmd4JztcbiAqXG4gKiBjb25zdHJ1Y3Rvcihwcml2YXRlIHNvY2lhbFNoYXJpbmc6IFNvY2lhbFNoYXJpbmcpIHsgfVxuICpcbiAqIC4uLlxuICpcbiAqIC8vIENoZWNrIGlmIHNoYXJpbmcgdmlhIGVtYWlsIGlzIHN1cHBvcnRlZFxuICogdGhpcy5zb2NpYWxTaGFyaW5nLmNhblNoYXJlVmlhRW1haWwoKS50aGVuKCgpID0+IHtcbiAqICAgLy8gU2hhcmluZyB2aWEgZW1haWwgaXMgcG9zc2libGVcbiAqIH0pLmNhdGNoKCgpID0+IHtcbiAqICAgLy8gU2hhcmluZyB2aWEgZW1haWwgaXMgbm90IHBvc3NpYmxlXG4gKiB9KTtcbiAqXG4gKiAvLyBTaGFyZSB2aWEgZW1haWxcbiAqIHRoaXMuc29jaWFsU2hhcmluZy5zaGFyZVZpYUVtYWlsKCdCb2R5JywgJ1N1YmplY3QnLCBbJ3JlY2lwaWVudEBleGFtcGxlLm9yZyddKS50aGVuKCgpID0+IHtcbiAqICAgLy8gU3VjY2VzcyFcbiAqIH0pLmNhdGNoKCgpID0+IHtcbiAqICAgLy8gRXJyb3IhXG4gKiB9KTtcbiAqIGBgYFxuICovXG5AUGx1Z2luKHtcbiAgcGx1Z2luTmFtZTogJ1NvY2lhbFNoYXJpbmcnLFxuICBwbHVnaW46ICdjb3Jkb3ZhLXBsdWdpbi14LXNvY2lhbHNoYXJpbmcnLFxuICBwbHVnaW5SZWY6ICdwbHVnaW5zLnNvY2lhbHNoYXJpbmcnLFxuICByZXBvOiAnaHR0cHM6Ly9naXRodWIuY29tL0VkZHlWZXJicnVnZ2VuL1NvY2lhbFNoYXJpbmctUGhvbmVHYXAtUGx1Z2luJyxcbiAgcGxhdGZvcm1zOiBbJ0FuZHJvaWQnLCAnQnJvd3NlcicsICdpT1MnLCAnV2luZG93cycsICdXaW5kb3dzIFBob25lJ10sXG59KVxuQEluamVjdGFibGUoKVxuZXhwb3J0IGNsYXNzIFNvY2lhbFNoYXJpbmcgZXh0ZW5kcyBBd2Vzb21lQ29yZG92YU5hdGl2ZVBsdWdpbiB7XG4gIC8qKlxuICAgKiBTaGFyZXMgdXNpbmcgdGhlIHNoYXJlIHNoZWV0XG4gICAqXG4gICAqIEBwYXJhbSBtZXNzYWdlIHtzdHJpbmd9IFRoZSBtZXNzYWdlIHlvdSB3b3VsZCBsaWtlIHRvIHNoYXJlLlxuICAgKiBAcGFyYW0gc3ViamVjdCB7c3RyaW5nfSBUaGUgc3ViamVjdFxuICAgKiBAcGFyYW0gZmlsZSB7c3RyaW5nfHN0cmluZ1tdfSBVUkwocykgdG8gZmlsZShzKSBvciBpbWFnZShzKSwgbG9jYWwgcGF0aChzKSB0byBmaWxlKHMpIG9yIGltYWdlKHMpLCBvciBiYXNlNjQgZGF0YSBvZiBhbiBpbWFnZS4gT25seSB0aGUgZmlyc3QgZmlsZS9pbWFnZSB3aWxsIGJlIHVzZWQgb24gV2luZG93cyBQaG9uZS5cbiAgICogQHBhcmFtIHVybCB7c3RyaW5nfSBBIFVSTCB0byBzaGFyZVxuICAgKiBAcmV0dXJucyB7UHJvbWlzZTxhbnk+fVxuICAgKi9cbiAgQENvcmRvdmEoe1xuICAgIHN1Y2Nlc3NJbmRleDogNCxcbiAgICBlcnJvckluZGV4OiA1LFxuICB9KVxuICBzaGFyZShtZXNzYWdlPzogc3RyaW5nLCBzdWJqZWN0Pzogc3RyaW5nLCBmaWxlPzogc3RyaW5nIHwgc3RyaW5nW10sIHVybD86IHN0cmluZyk6IFByb21pc2U8YW55PiB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgLyoqXG4gICAqIFNoYXJlcyB1c2luZyB0aGUgc2hhcmUgc2hlZXQgd2l0aCBhZGRpdGlvbmFsIG9wdGlvbnMgYW5kIHJldHVybnMgYSByZXN1bHQgb2JqZWN0IG9yIGFuIGVycm9yIG1lc3NhZ2UgKHJlcXVpcmVzIHBsdWdpbiB2ZXJzaW9uIDUuMS4wKylcbiAgICpcbiAgICogQHBhcmFtIG9wdGlvbnMge29iamVjdH0gVGhlIG9wdGlvbnMgb2JqZWN0IHdpdGggdGhlIG1lc3NhZ2UsIHN1YmplY3QsIGZpbGVzLCB1cmwgYW5kIGNob29zZXJUaXRsZSBwcm9wZXJ0aWVzLlxuICAgKiBAcGFyYW0gb3B0aW9ucy5tZXNzYWdlXG4gICAqIEBwYXJhbSBvcHRpb25zLnN1YmplY3RcbiAgICogQHBhcmFtIG9wdGlvbnMuZmlsZXNcbiAgICogQHBhcmFtIG9wdGlvbnMudXJsXG4gICAqIEBwYXJhbSBvcHRpb25zLmNob29zZXJUaXRsZVxuICAgKiBAcmV0dXJucyB7UHJvbWlzZTxhbnk+fVxuICAgKi9cbiAgQENvcmRvdmEoe1xuICAgIHBsYXRmb3JtczogWydpT1MnLCAnQW5kcm9pZCddLFxuICB9KVxuICBzaGFyZVdpdGhPcHRpb25zKG9wdGlvbnM6IHtcbiAgICBtZXNzYWdlPzogc3RyaW5nO1xuICAgIHN1YmplY3Q/OiBzdHJpbmc7XG4gICAgZmlsZXM/OiBzdHJpbmdbXTtcbiAgICB1cmw/OiBzdHJpbmc7XG4gICAgY2hvb3NlclRpdGxlPzogc3RyaW5nO1xuICB9KTogUHJvbWlzZTxhbnk+IHtcbiAgICByZXR1cm47XG4gIH1cblxuICAvKipcbiAgICogQ2hlY2tzIGlmIHlvdSBjYW4gc2hhcmUgdmlhIGEgc3BlY2lmaWMgYXBwLlxuICAgKlxuICAgKiBAcGFyYW0gYXBwTmFtZSB7c3RyaW5nfSBBcHAgbmFtZSBvciBwYWNrYWdlIG5hbWUuIEV4YW1wbGVzOiBpbnN0YWdyYW0gb3IgY29tLmFwcGxlLnNvY2lhbC5mYWNlYm9va1xuICAgKiBAcGFyYW0gbWVzc2FnZSB7c3RyaW5nfVxuICAgKiBAcGFyYW0gc3ViamVjdCB7c3RyaW5nfVxuICAgKiBAcGFyYW0gaW1hZ2Uge3N0cmluZ31cbiAgICogQHBhcmFtIHVybCB7c3RyaW5nfVxuICAgKiBAcmV0dXJucyB7UHJvbWlzZTxhbnk+fVxuICAgKi9cbiAgQENvcmRvdmEoe1xuICAgIHN1Y2Nlc3NJbmRleDogNSxcbiAgICBlcnJvckluZGV4OiA2LFxuICAgIHBsYXRmb3JtczogWydpT1MnLCAnQW5kcm9pZCddLFxuICB9KVxuICBjYW5TaGFyZVZpYShhcHBOYW1lOiBzdHJpbmcsIG1lc3NhZ2U/OiBzdHJpbmcsIHN1YmplY3Q/OiBzdHJpbmcsIGltYWdlPzogc3RyaW5nLCB1cmw/OiBzdHJpbmcpOiBQcm9taXNlPGFueT4ge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8qKlxuICAgKiBTaGFyZXMgZGlyZWN0bHkgdG8gVHdpdHRlclxuICAgKlxuICAgKiBAcGFyYW0gbWVzc2FnZSB7c3RyaW5nfVxuICAgKiBAcGFyYW0gaW1hZ2Uge3N0cmluZ31cbiAgICogQHBhcmFtIHVybCB7c3RyaW5nfVxuICAgKiBAcmV0dXJucyB7UHJvbWlzZTxhbnk+fVxuICAgKi9cbiAgQENvcmRvdmEoe1xuICAgIHN1Y2Nlc3NJbmRleDogMyxcbiAgICBlcnJvckluZGV4OiA0LFxuICAgIHBsYXRmb3JtczogWydpT1MnLCAnQW5kcm9pZCddLFxuICB9KVxuICBzaGFyZVZpYVR3aXR0ZXIobWVzc2FnZTogc3RyaW5nLCBpbWFnZT86IHN0cmluZywgdXJsPzogc3RyaW5nKTogUHJvbWlzZTxhbnk+IHtcbiAgICByZXR1cm47XG4gIH1cblxuICAvKipcbiAgICogU2hhcmVzIGRpcmVjdGx5IHRvIEZhY2Vib29rXG4gICAqXG4gICAqIEBwYXJhbSBtZXNzYWdlIHtzdHJpbmd9XG4gICAqIEBwYXJhbSBpbWFnZSB7c3RyaW5nfVxuICAgKiBAcGFyYW0gdXJsIHtzdHJpbmd9XG4gICAqIEByZXR1cm5zIHtQcm9taXNlPGFueT59XG4gICAqL1xuICBAQ29yZG92YSh7XG4gICAgc3VjY2Vzc0luZGV4OiAzLFxuICAgIGVycm9ySW5kZXg6IDQsXG4gICAgcGxhdGZvcm1zOiBbJ2lPUycsICdBbmRyb2lkJ10sXG4gIH0pXG4gIHNoYXJlVmlhRmFjZWJvb2sobWVzc2FnZTogc3RyaW5nLCBpbWFnZT86IHN0cmluZywgdXJsPzogc3RyaW5nKTogUHJvbWlzZTxhbnk+IHtcbiAgICByZXR1cm47XG4gIH1cblxuICAvKipcbiAgICogU2hhcmVzIGRpcmVjdGx5IHRvIEZhY2Vib29rIHdpdGggYSBwYXN0ZSBtZXNzYWdlIGhpbnRcbiAgICpcbiAgICogQHBhcmFtIG1lc3NhZ2Uge3N0cmluZ31cbiAgICogQHBhcmFtIGltYWdlIHtzdHJpbmd9XG4gICAqIEBwYXJhbSB1cmwge3N0cmluZ31cbiAgICogQHBhcmFtIHBhc3RlTWVzc2FnZUhpbnQge3N0cmluZ31cbiAgICogQHJldHVybnMge1Byb21pc2U8YW55Pn1cbiAgICovXG4gIEBDb3Jkb3ZhKHtcbiAgICBzdWNjZXNzSW5kZXg6IDQsXG4gICAgZXJyb3JJbmRleDogNSxcbiAgICBwbGF0Zm9ybXM6IFsnaU9TJywgJ0FuZHJvaWQnXSxcbiAgfSlcbiAgc2hhcmVWaWFGYWNlYm9va1dpdGhQYXN0ZU1lc3NhZ2VIaW50KFxuICAgIG1lc3NhZ2U6IHN0cmluZyxcbiAgICBpbWFnZT86IHN0cmluZyxcbiAgICB1cmw/OiBzdHJpbmcsXG4gICAgcGFzdGVNZXNzYWdlSGludD86IHN0cmluZ1xuICApOiBQcm9taXNlPGFueT4ge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8qKlxuICAgKiBTaGFyZXMgZGlyZWN0bHkgdG8gSW5zdGFncmFtXG4gICAqXG4gICAqIEBwYXJhbSBtZXNzYWdlIHtzdHJpbmd9XG4gICAqIEBwYXJhbSBpbWFnZSB7c3RyaW5nfVxuICAgKiBAcmV0dXJucyB7UHJvbWlzZTxhbnk+fVxuICAgKi9cbiAgQENvcmRvdmEoe1xuICAgIHBsYXRmb3JtczogWydpT1MnLCAnQW5kcm9pZCddLFxuICB9KVxuICBzaGFyZVZpYUluc3RhZ3JhbShtZXNzYWdlOiBzdHJpbmcsIGltYWdlOiBzdHJpbmcpOiBQcm9taXNlPGFueT4ge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8qKlxuICAgKiBTaGFyZXMgZGlyZWN0bHkgdG8gV2hhdHNBcHBcbiAgICpcbiAgICogQHBhcmFtIG1lc3NhZ2Uge3N0cmluZ31cbiAgICogQHBhcmFtIGltYWdlIHtzdHJpbmd9XG4gICAqIEBwYXJhbSB1cmwge3N0cmluZ31cbiAgICogQHJldHVybnMge1Byb21pc2U8YW55Pn1cbiAgICovXG4gIEBDb3Jkb3ZhKHtcbiAgICBzdWNjZXNzSW5kZXg6IDMsXG4gICAgZXJyb3JJbmRleDogNCxcbiAgICBwbGF0Zm9ybXM6IFsnaU9TJywgJ0FuZHJvaWQnXSxcbiAgfSlcbiAgc2hhcmVWaWFXaGF0c0FwcChtZXNzYWdlOiBzdHJpbmcsIGltYWdlPzogc3RyaW5nLCB1cmw/OiBzdHJpbmcpOiBQcm9taXNlPGFueT4ge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8qKlxuICAgKiBTaGFyZXMgZGlyZWN0bHkgdG8gYSBXaGF0c0FwcCBDb250YWN0XG4gICAqXG4gICAqIEBwYXJhbSByZWNlaXZlciB7c3RyaW5nfSBQYXNzIHBob25lIG51bWJlciBvbiBBbmRyb2lkLCBhbmQgQWRkcmVzc2Jvb2sgSUQgKGFiaWQpIG9uIGlPU1xuICAgKiBAcGFyYW0gbWVzc2FnZSB7c3RyaW5nfSBNZXNzYWdlIHRvIHNlbmRcbiAgICogQHBhcmFtIGltYWdlIHtzdHJpbmd9IEltYWdlIHRvIHNlbmQgKGRvZXMgbm90IHdvcmsgb24gaU9TXG4gICAqIEBwYXJhbSB1cmwge3N0cmluZ30gTGluayB0byBzZW5kXG4gICAqIEByZXR1cm5zIHtQcm9taXNlPGFueT59XG4gICAqL1xuICBAQ29yZG92YSh7XG4gICAgc3VjY2Vzc0luZGV4OiA0LFxuICAgIGVycm9ySW5kZXg6IDUsXG4gICAgcGxhdGZvcm1zOiBbJ2lPUycsICdBbmRyb2lkJ10sXG4gIH0pXG4gIHNoYXJlVmlhV2hhdHNBcHBUb1JlY2VpdmVyKHJlY2VpdmVyOiBzdHJpbmcsIG1lc3NhZ2U6IHN0cmluZywgaW1hZ2U/OiBzdHJpbmcsIHVybD86IHN0cmluZyk6IFByb21pc2U8YW55PiB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgLyoqXG4gICAqIFNoYXJlIHZpYSBTTVNcbiAgICpcbiAgICogQHBhcmFtIG1lc3NnZSB7c3RyaW5nfSBtZXNzYWdlIHRvIHNlbmRcbiAgICogQHBhcmFtIHBob25lTnVtYmVyIHtzdHJpbmd9IE51bWJlciBvciBtdWx0aXBsZSBudW1iZXJzIHNlcGVyYXRlZCBieSBjb21tYXNcbiAgICogQHJldHVybnMge1Byb21pc2U8YW55Pn1cbiAgICovXG4gIEBDb3Jkb3ZhKHtcbiAgICBwbGF0Zm9ybXM6IFsnaU9TJywgJ0FuZHJvaWQnXSxcbiAgfSlcbiAgc2hhcmVWaWFTTVMobWVzc2dlOiBzdHJpbmcsIHBob25lTnVtYmVyOiBzdHJpbmcpOiBQcm9taXNlPGFueT4ge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8qKlxuICAgKiBDaGVja3MgaWYgeW91IGNhbiBzaGFyZSB2aWEgZW1haWxcbiAgICpcbiAgICogQHJldHVybnMge1Byb21pc2U8YW55Pn1cbiAgICovXG4gIEBDb3Jkb3ZhKHtcbiAgICBwbGF0Zm9ybXM6IFsnaU9TJywgJ0FuZHJvaWQnXSxcbiAgfSlcbiAgY2FuU2hhcmVWaWFFbWFpbCgpOiBQcm9taXNlPGFueT4ge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8qKlxuICAgKiBTaGFyZSB2aWEgRW1haWxcbiAgICpcbiAgICogQHBhcmFtIG1lc3NhZ2Uge3N0cmluZ31cbiAgICogQHBhcmFtIHN1YmplY3Qge3N0cmluZ31cbiAgICogQHBhcmFtIHRvIHtzdHJpbmdbXX1cbiAgICogQHBhcmFtIGNjIHtzdHJpbmdbXX0gT3B0aW9uYWxcbiAgICogQHBhcmFtIGJjYyB7c3RyaW5nW119IE9wdGlvbmFsXG4gICAqIEBwYXJhbSBmaWxlcyB7c3RyaW5nfHN0cmluZ1tdfSBPcHRpb25hbCBVUkwgb3IgbG9jYWwgcGF0aCB0byBmaWxlKHMpIHRvIGF0dGFjaFxuICAgKiBAcmV0dXJucyB7UHJvbWlzZTxhbnk+fVxuICAgKi9cbiAgQENvcmRvdmEoe1xuICAgIHBsYXRmb3JtczogWydpT1MnLCAnQW5kcm9pZCddLFxuICAgIHN1Y2Nlc3NJbmRleDogNixcbiAgICBlcnJvckluZGV4OiA3LFxuICB9KVxuICBzaGFyZVZpYUVtYWlsKFxuICAgIG1lc3NhZ2U6IHN0cmluZyxcbiAgICBzdWJqZWN0OiBzdHJpbmcsXG4gICAgdG86IHN0cmluZ1tdLFxuICAgIGNjPzogc3RyaW5nW10sXG4gICAgYmNjPzogc3RyaW5nW10sXG4gICAgZmlsZXM/OiBzdHJpbmcgfCBzdHJpbmdbXVxuICApOiBQcm9taXNlPGFueT4ge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8qKlxuICAgKiBTaGFyZSB2aWEgQXBwTmFtZVxuICAgKlxuICAgKiBAcGFyYW0gYXBwTmFtZSB7c3RyaW5nfSBBcHAgbmFtZSBvciBwYWNrYWdlIG5hbWUuIEV4YW1wbGVzOiBpbnN0YWdyYW0gb3IgY29tLmFwcGxlLnNvY2lhbC5mYWNlYm9va1xuICAgKiBAcGFyYW0gbWVzc2FnZSB7c3RyaW5nfVxuICAgKiBAcGFyYW0gc3ViamVjdCB7c3RyaW5nfVxuICAgKiBAcGFyYW0gaW1hZ2Uge3N0cmluZ31cbiAgICogQHBhcmFtIHVybCB7c3RyaW5nfVxuICAgKiBAcmV0dXJucyB7UHJvbWlzZTxhbnk+fVxuICAgKi9cbiAgQENvcmRvdmEoe1xuICAgIHN1Y2Nlc3NJbmRleDogNSxcbiAgICBlcnJvckluZGV4OiA2LFxuICAgIHBsYXRmb3JtczogWydpT1MnLCAnQW5kcm9pZCddLFxuICB9KVxuICBzaGFyZVZpYShhcHBOYW1lOiBzdHJpbmcsIG1lc3NhZ2U6IHN0cmluZywgc3ViamVjdD86IHN0cmluZywgaW1hZ2U/OiBzdHJpbmcsIHVybD86IHN0cmluZyk6IFByb21pc2U8YW55PiB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgLyoqXG4gICAqIGRlZmluZXMgdGhlIHBvcHVwIHBvc2l0aW9uIGJlZm9yZSBjYWxsIHRoZSBzaGFyZSBtZXRob2QuXG4gICAqXG4gICAqIEBwYXJhbSB0YXJnZXRCb3VuZHMge3N0cmluZ30gbGVmdCwgdG9wLCB3aWR0aCwgaGVpZ2h0XG4gICAqL1xuICBAQ29yZG92YSh7XG4gICAgc3luYzogdHJ1ZSxcbiAgICBwbGF0Zm9ybXM6IFsnaU9TJ10sXG4gIH0pXG4gIHNldElQYWRQb3B1cENvb3JkaW5hdGVzKHRhcmdldEJvdW5kczogc3RyaW5nKTogdm9pZCB7fVxuXG4gIC8qKlxuICAgKiBTYXZlIGFuIGFycmF5IG9mIGltYWdlcyB0byB0aGUgY2FtZXJhIHJvbGxcbiAgICpcbiAgICogQHBhcmFtICB7c3RyaW5nfHN0cmluZ1tdfSBmaWxlT3JGaWxlQXJyYXkgU2luZ2xlIG9yIG11bHRpcGxlIGZpbGVzXG4gICAqIEByZXR1cm5zIHtQcm9taXNlPGFueT4gfVxuICAgKi9cbiAgQENvcmRvdmEoe1xuICAgIHBsYXRmb3JtczogWydpT1MnXSxcbiAgfSlcbiAgc2F2ZVRvUGhvdG9BbGJ1bShmaWxlT3JGaWxlQXJyYXk6IHN0cmluZyB8IHN0cmluZ1tdKTogUHJvbWlzZTxhbnk+IHtcbiAgICByZXR1cm47XG4gIH1cblxuICAvKipcbiAgICogU2hhcmVzIGRpcmVjdGx5IHRvIGEgV2hhdHNBcHAgQ29udGFjdCB3aXRoIHBob25lIG51bWJlci5cbiAgICpcbiAgICogQHBhcmFtIHBob25lIHtzdHJpbmd9IFBhc3MgcGhvbmUgbnVtYmVyXG4gICAqIEBwYXJhbSBtZXNzYWdlIHtzdHJpbmd9IE1lc3NhZ2UgdG8gc2VuZFxuICAgKiBAcGFyYW0gZmlsZU9yRmlsZUFycmF5IGZpbGVPckZpbGVBcnJheSBTaW5nbGUgb3IgbXVsdGlwbGUgZmlsZXNcbiAgICogQHBhcmFtIHVybCB7c3RyaW5nfSBMaW5rIHRvIHNlbmRcbiAgICogQHJldHVybnMge1Byb21pc2U8YW55Pn1cbiAgICovXG4gIEBDb3Jkb3ZhKHtcbiAgICBzdWNjZXNzSW5kZXg6IDUsXG4gICAgZXJyb3JJbmRleDogNixcbiAgICBwbGF0Zm9ybXM6IFsnaU9TJywgJ0FuZHJvaWQnXSxcbiAgfSlcbiAgc2hhcmVWaWFXaGF0c0FwcFRvUGhvbmUoXG4gICAgcGhvbmU6IHN0cmluZyxcbiAgICBtZXNzYWdlOiBzdHJpbmcsXG4gICAgZmlsZU9yRmlsZUFycmF5OiBzdHJpbmcgfCBzdHJpbmdbXSxcbiAgICB1cmw/OiBzdHJpbmdcbiAgKTogUHJvbWlzZTxhbnk+IHtcbiAgICByZXR1cm47XG4gIH1cbn1cbiJdfQ==

/***/ }),

/***/ 6977:
/*!*******************************************************************!*\
  !*** ./node_modules/@ionic-native/core/__ivy_ngcc__/bootstrap.js ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "checkReady": () => (/* binding */ checkReady)
/* harmony export */ });
function checkReady() {
    if (typeof process === 'undefined') {
        var win_1 = typeof window !== 'undefined' ? window : {};
        var DEVICE_READY_TIMEOUT_1 = 5000;
        // To help developers using cordova, we listen for the device ready event and
        // log an error if it didn't fire in a reasonable amount of time. Generally,
        // when this happens, developers should remove and reinstall plugins, since
        // an inconsistent plugin is often the culprit.
        var before_1 = Date.now();
        var didFireReady_1 = false;
        win_1.document.addEventListener('deviceready', function () {
            console.log("Ionic Native: deviceready event fired after " + (Date.now() - before_1) + " ms");
            didFireReady_1 = true;
        });
        setTimeout(function () {
            if (!didFireReady_1 && win_1.cordova) {
                console.warn("Ionic Native: deviceready did not fire within " + DEVICE_READY_TIMEOUT_1 + "ms. This can happen when plugins are in an inconsistent state. Try removing plugins from plugins/ and reinstalling them.");
            }
        }, DEVICE_READY_TIMEOUT_1);
    }
}
//# sourceMappingURL=bootstrap.js.map

/***/ }),

/***/ 9870:
/*!***************************************************************************!*\
  !*** ./node_modules/@ionic-native/core/__ivy_ngcc__/decorators/common.js ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ERR_CORDOVA_NOT_AVAILABLE": () => (/* binding */ ERR_CORDOVA_NOT_AVAILABLE),
/* harmony export */   "ERR_PLUGIN_NOT_INSTALLED": () => (/* binding */ ERR_PLUGIN_NOT_INSTALLED),
/* harmony export */   "getPromise": () => (/* binding */ getPromise),
/* harmony export */   "wrapPromise": () => (/* binding */ wrapPromise),
/* harmony export */   "checkAvailability": () => (/* binding */ checkAvailability),
/* harmony export */   "instanceAvailability": () => (/* binding */ instanceAvailability),
/* harmony export */   "setIndex": () => (/* binding */ setIndex),
/* harmony export */   "callCordovaPlugin": () => (/* binding */ callCordovaPlugin),
/* harmony export */   "callInstance": () => (/* binding */ callInstance),
/* harmony export */   "getPlugin": () => (/* binding */ getPlugin),
/* harmony export */   "get": () => (/* binding */ get),
/* harmony export */   "pluginWarn": () => (/* binding */ pluginWarn),
/* harmony export */   "cordovaWarn": () => (/* binding */ cordovaWarn),
/* harmony export */   "wrap": () => (/* binding */ wrap),
/* harmony export */   "wrapInstance": () => (/* binding */ wrapInstance)
/* harmony export */ });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ 9165);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ 2759);

var ERR_CORDOVA_NOT_AVAILABLE = { error: 'cordova_not_available' };
var ERR_PLUGIN_NOT_INSTALLED = { error: 'plugin_not_installed' };
function getPromise(callback) {
    var tryNativePromise = function () {
        if (Promise) {
            return new Promise(function (resolve, reject) {
                callback(resolve, reject);
            });
        }
        else {
            console.error('No Promise support or polyfill found. To enable Ionic Native support, please add the es6-promise polyfill before this script, or run with a library like Angular or on a recent browser.');
        }
    };
    if (typeof window !== 'undefined' && window.angular) {
        var doc = window.document;
        var injector = window.angular.element(doc.querySelector('[ng-app]') || doc.body).injector();
        if (injector) {
            var $q = injector.get('$q');
            return $q(function (resolve, reject) {
                callback(resolve, reject);
            });
        }
        console.warn("Angular 1 was detected but $q couldn't be retrieved. This is usually when the app is not bootstrapped on the html or body tag. Falling back to native promises which won't trigger an automatic digest when promises resolve.");
    }
    return tryNativePromise();
}
function wrapPromise(pluginObj, methodName, args, opts) {
    if (opts === void 0) { opts = {}; }
    var pluginResult, rej;
    var p = getPromise(function (resolve, reject) {
        if (opts.destruct) {
            pluginResult = callCordovaPlugin(pluginObj, methodName, args, opts, function () {
                var args = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    args[_i] = arguments[_i];
                }
                return resolve(args);
            }, function () {
                var args = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    args[_i] = arguments[_i];
                }
                return reject(args);
            });
        }
        else {
            pluginResult = callCordovaPlugin(pluginObj, methodName, args, opts, resolve, reject);
        }
        rej = reject;
    });
    // Angular throws an error on unhandled rejection, but in this case we have already printed
    // a warning that Cordova is undefined or the plugin is uninstalled, so there is no reason
    // to error
    if (pluginResult && pluginResult.error) {
        p.catch(function () { });
        typeof rej === 'function' && rej(pluginResult.error);
    }
    return p;
}
function wrapOtherPromise(pluginObj, methodName, args, opts) {
    if (opts === void 0) { opts = {}; }
    return getPromise(function (resolve, reject) {
        var pluginResult = callCordovaPlugin(pluginObj, methodName, args, opts);
        if (pluginResult) {
            if (pluginResult.error) {
                reject(pluginResult.error);
            }
            else if (pluginResult.then) {
                pluginResult.then(resolve).catch(reject);
            }
        }
        else {
            reject({ error: 'unexpected_error' });
        }
    });
}
function wrapObservable(pluginObj, methodName, args, opts) {
    if (opts === void 0) { opts = {}; }
    return new rxjs__WEBPACK_IMPORTED_MODULE_0__.Observable(function (observer) {
        var pluginResult;
        if (opts.destruct) {
            pluginResult = callCordovaPlugin(pluginObj, methodName, args, opts, function () {
                var args = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    args[_i] = arguments[_i];
                }
                return observer.next(args);
            }, function () {
                var args = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    args[_i] = arguments[_i];
                }
                return observer.error(args);
            });
        }
        else {
            pluginResult = callCordovaPlugin(pluginObj, methodName, args, opts, observer.next.bind(observer), observer.error.bind(observer));
        }
        if (pluginResult && pluginResult.error) {
            observer.error(pluginResult.error);
            observer.complete();
        }
        return function () {
            try {
                if (opts.clearFunction) {
                    if (opts.clearWithArgs) {
                        return callCordovaPlugin(pluginObj, opts.clearFunction, args, opts, observer.next.bind(observer), observer.error.bind(observer));
                    }
                    return callCordovaPlugin(pluginObj, opts.clearFunction, []);
                }
            }
            catch (e) {
                console.warn('Unable to clear the previous observable watch for', pluginObj.constructor.getPluginName(), methodName);
                console.warn(e);
            }
        };
    });
}
/**
 * Wrap the event with an observable
 * @private
 * @param event event name
 * @param element The element to attach the event listener to
 * @returns {Observable}
 */
function wrapEventObservable(event, element) {
    element =
        typeof window !== 'undefined' && element
            ? get(window, element)
            : element || (typeof window !== 'undefined' ? window : {});
    return (0,rxjs__WEBPACK_IMPORTED_MODULE_1__.fromEvent)(element, event);
}
function checkAvailability(plugin, methodName, pluginName) {
    var pluginRef, pluginInstance, pluginPackage;
    if (typeof plugin === 'string') {
        pluginRef = plugin;
    }
    else {
        pluginRef = plugin.constructor.getPluginRef();
        pluginName = plugin.constructor.getPluginName();
        pluginPackage = plugin.constructor.getPluginInstallName();
    }
    pluginInstance = getPlugin(pluginRef);
    if (!pluginInstance || (!!methodName && typeof pluginInstance[methodName] === 'undefined')) {
        if (typeof window === 'undefined' || !window.cordova) {
            cordovaWarn(pluginName, methodName);
            return ERR_CORDOVA_NOT_AVAILABLE;
        }
        pluginWarn(pluginName, pluginPackage, methodName);
        return ERR_PLUGIN_NOT_INSTALLED;
    }
    return true;
}
/**
 * Checks if _objectInstance exists and has the method/property
 * @private
 */
function instanceAvailability(pluginObj, methodName) {
    return pluginObj._objectInstance && (!methodName || typeof pluginObj._objectInstance[methodName] !== 'undefined');
}
function setIndex(args, opts, resolve, reject) {
    if (opts === void 0) { opts = {}; }
    // ignore resolve and reject in case sync
    if (opts.sync) {
        return args;
    }
    // If the plugin method expects myMethod(success, err, options)
    if (opts.callbackOrder === 'reverse') {
        // Get those arguments in the order [resolve, reject, ...restOfArgs]
        args.unshift(reject);
        args.unshift(resolve);
    }
    else if (opts.callbackStyle === 'node') {
        args.push(function (err, result) {
            if (err) {
                reject(err);
            }
            else {
                resolve(result);
            }
        });
    }
    else if (opts.callbackStyle === 'object' && opts.successName && opts.errorName) {
        var obj = {};
        obj[opts.successName] = resolve;
        obj[opts.errorName] = reject;
        args.push(obj);
    }
    else if (typeof opts.successIndex !== 'undefined' || typeof opts.errorIndex !== 'undefined') {
        var setSuccessIndex = function () {
            // If we've specified a success/error index
            if (opts.successIndex > args.length) {
                args[opts.successIndex] = resolve;
            }
            else {
                args.splice(opts.successIndex, 0, resolve);
            }
        };
        var setErrorIndex = function () {
            // We don't want that the reject cb gets spliced into the position of an optional argument that has not been
            // defined and thus causing non expected behavior.
            if (opts.errorIndex > args.length) {
                args[opts.errorIndex] = reject; // insert the reject fn at the correct specific index
            }
            else {
                args.splice(opts.errorIndex, 0, reject); // otherwise just splice it into the array
            }
        };
        if (opts.successIndex > opts.errorIndex) {
            setErrorIndex();
            setSuccessIndex();
        }
        else {
            setSuccessIndex();
            setErrorIndex();
        }
    }
    else {
        // Otherwise, let's tack them on to the end of the argument list
        // which is 90% of cases
        args.push(resolve);
        args.push(reject);
    }
    return args;
}
function callCordovaPlugin(pluginObj, methodName, args, opts, resolve, reject) {
    if (opts === void 0) { opts = {}; }
    // Try to figure out where the success/error callbacks need to be bound
    // to our promise resolve/reject handlers.
    args = setIndex(args, opts, resolve, reject);
    var availabilityCheck = checkAvailability(pluginObj, methodName);
    if (availabilityCheck === true) {
        var pluginInstance = getPlugin(pluginObj.constructor.getPluginRef());
        return pluginInstance[methodName].apply(pluginInstance, args);
    }
    else {
        return availabilityCheck;
    }
}
function callInstance(pluginObj, methodName, args, opts, resolve, reject) {
    if (opts === void 0) { opts = {}; }
    args = setIndex(args, opts, resolve, reject);
    if (instanceAvailability(pluginObj, methodName)) {
        return pluginObj._objectInstance[methodName].apply(pluginObj._objectInstance, args);
    }
}
function getPlugin(pluginRef) {
    if (typeof window !== 'undefined') {
        return get(window, pluginRef);
    }
    return null;
}
function get(element, path) {
    var paths = path.split('.');
    var obj = element;
    for (var i = 0; i < paths.length; i++) {
        if (!obj) {
            return null;
        }
        obj = obj[paths[i]];
    }
    return obj;
}
function pluginWarn(pluginName, plugin, method) {
    if (method) {
        console.warn('Native: tried calling ' + pluginName + '.' + method + ', but the ' + pluginName + ' plugin is not installed.');
    }
    else {
        console.warn("Native: tried accessing the " + pluginName + " plugin but it's not installed.");
    }
    if (plugin) {
        console.warn("Install the " + pluginName + " plugin: 'ionic cordova plugin add " + plugin + "'");
    }
}
/**
 * @private
 * @param pluginName
 * @param method
 */
function cordovaWarn(pluginName, method) {
    if (typeof process === 'undefined') {
        if (method) {
            console.warn('Native: tried calling ' +
                pluginName +
                '.' +
                method +
                ', but Cordova is not available. Make sure to include cordova.js or run in a device/simulator');
        }
        else {
            console.warn('Native: tried accessing the ' +
                pluginName +
                ' plugin but Cordova is not available. Make sure to include cordova.js or run in a device/simulator');
        }
    }
}
/**
 * @private
 */
var wrap = function (pluginObj, methodName, opts) {
    if (opts === void 0) { opts = {}; }
    return function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        if (opts.sync) {
            // Sync doesn't wrap the plugin with a promise or observable, it returns the result as-is
            return callCordovaPlugin(pluginObj, methodName, args, opts);
        }
        else if (opts.observable) {
            return wrapObservable(pluginObj, methodName, args, opts);
        }
        else if (opts.eventObservable && opts.event) {
            return wrapEventObservable(opts.event, opts.element);
        }
        else if (opts.otherPromise) {
            return wrapOtherPromise(pluginObj, methodName, args, opts);
        }
        else {
            return wrapPromise(pluginObj, methodName, args, opts);
        }
    };
};
/**
 * @private
 */
function wrapInstance(pluginObj, methodName, opts) {
    if (opts === void 0) { opts = {}; }
    return function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        if (opts.sync) {
            return callInstance(pluginObj, methodName, args, opts);
        }
        else if (opts.observable) {
            return new rxjs__WEBPACK_IMPORTED_MODULE_0__.Observable(function (observer) {
                var pluginResult;
                if (opts.destruct) {
                    pluginResult = callInstance(pluginObj, methodName, args, opts, function () {
                        var args = [];
                        for (var _i = 0; _i < arguments.length; _i++) {
                            args[_i] = arguments[_i];
                        }
                        return observer.next(args);
                    }, function () {
                        var args = [];
                        for (var _i = 0; _i < arguments.length; _i++) {
                            args[_i] = arguments[_i];
                        }
                        return observer.error(args);
                    });
                }
                else {
                    pluginResult = callInstance(pluginObj, methodName, args, opts, observer.next.bind(observer), observer.error.bind(observer));
                }
                if (pluginResult && pluginResult.error) {
                    observer.error(pluginResult.error);
                }
                return function () {
                    try {
                        if (opts.clearWithArgs) {
                            return callInstance(pluginObj, opts.clearFunction, args, opts, observer.next.bind(observer), observer.error.bind(observer));
                        }
                        return callInstance(pluginObj, opts.clearFunction, []);
                    }
                    catch (e) {
                        console.warn('Unable to clear the previous observable watch for', pluginObj.constructor.getPluginName(), methodName);
                        console.warn(e);
                    }
                };
            });
        }
        else if (opts.otherPromise) {
            return getPromise(function (resolve, reject) {
                var result;
                if (opts.destruct) {
                    result = callInstance(pluginObj, methodName, args, opts, function () {
                        var args = [];
                        for (var _i = 0; _i < arguments.length; _i++) {
                            args[_i] = arguments[_i];
                        }
                        return resolve(args);
                    }, function () {
                        var args = [];
                        for (var _i = 0; _i < arguments.length; _i++) {
                            args[_i] = arguments[_i];
                        }
                        return reject(args);
                    });
                }
                else {
                    result = callInstance(pluginObj, methodName, args, opts, resolve, reject);
                }
                if (result && result.then) {
                    result.then(resolve, reject);
                }
                else {
                    reject();
                }
            });
        }
        else {
            var pluginResult_1, rej_1;
            var p = getPromise(function (resolve, reject) {
                if (opts.destruct) {
                    pluginResult_1 = callInstance(pluginObj, methodName, args, opts, function () {
                        var args = [];
                        for (var _i = 0; _i < arguments.length; _i++) {
                            args[_i] = arguments[_i];
                        }
                        return resolve(args);
                    }, function () {
                        var args = [];
                        for (var _i = 0; _i < arguments.length; _i++) {
                            args[_i] = arguments[_i];
                        }
                        return reject(args);
                    });
                }
                else {
                    pluginResult_1 = callInstance(pluginObj, methodName, args, opts, resolve, reject);
                }
                rej_1 = reject;
            });
            // Angular throws an error on unhandled rejection, but in this case we have already printed
            // a warning that Cordova is undefined or the plugin is uninstalled, so there is no reason
            // to error
            if (pluginResult_1 && pluginResult_1.error) {
                p.catch(function () { });
                typeof rej_1 === 'function' && rej_1(pluginResult_1.error);
            }
            return p;
        }
    };
}
//# sourceMappingURL=common.js.map

/***/ }),

/***/ 9849:
/*!**********************************************************************************************!*\
  !*** ./node_modules/@ionic-native/core/__ivy_ngcc__/decorators/cordova-function-override.js ***!
  \**********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "cordovaFunctionOverride": () => (/* binding */ cordovaFunctionOverride)
/* harmony export */ });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ 9165);
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./common */ 9870);


function overrideFunction(pluginObj, methodName) {
    return new rxjs__WEBPACK_IMPORTED_MODULE_1__.Observable(function (observer) {
        var availabilityCheck = (0,_common__WEBPACK_IMPORTED_MODULE_0__.checkAvailability)(pluginObj, methodName);
        if (availabilityCheck === true) {
            var pluginInstance_1 = (0,_common__WEBPACK_IMPORTED_MODULE_0__.getPlugin)(pluginObj.constructor.getPluginRef());
            pluginInstance_1[methodName] = observer.next.bind(observer);
            return function () { return (pluginInstance_1[methodName] = function () { }); };
        }
        else {
            observer.error(availabilityCheck);
            observer.complete();
        }
    });
}
function cordovaFunctionOverride(pluginObj, methodName, args) {
    if (args === void 0) { args = []; }
    return overrideFunction(pluginObj, methodName);
}
//# sourceMappingURL=cordova-function-override.js.map

/***/ }),

/***/ 1784:
/*!*************************************************************************************!*\
  !*** ./node_modules/@ionic-native/core/__ivy_ngcc__/decorators/cordova-instance.js ***!
  \*************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "cordovaInstance": () => (/* binding */ cordovaInstance)
/* harmony export */ });
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./common */ 9870);

function cordovaInstance(pluginObj, methodName, config, args) {
    args = Array.from(args);
    return (0,_common__WEBPACK_IMPORTED_MODULE_0__.wrapInstance)(pluginObj, methodName, config).apply(this, args);
}
//# sourceMappingURL=cordova-instance.js.map

/***/ }),

/***/ 9418:
/*!*************************************************************************************!*\
  !*** ./node_modules/@ionic-native/core/__ivy_ngcc__/decorators/cordova-property.js ***!
  \*************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "cordovaPropertyGet": () => (/* binding */ cordovaPropertyGet),
/* harmony export */   "cordovaPropertySet": () => (/* binding */ cordovaPropertySet)
/* harmony export */ });
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./common */ 9870);

function cordovaPropertyGet(pluginObj, key) {
    if ((0,_common__WEBPACK_IMPORTED_MODULE_0__.checkAvailability)(pluginObj, key) === true) {
        return (0,_common__WEBPACK_IMPORTED_MODULE_0__.getPlugin)(pluginObj.constructor.getPluginRef())[key];
    }
    return null;
}
function cordovaPropertySet(pluginObj, key, value) {
    if ((0,_common__WEBPACK_IMPORTED_MODULE_0__.checkAvailability)(pluginObj, key) === true) {
        (0,_common__WEBPACK_IMPORTED_MODULE_0__.getPlugin)(pluginObj.constructor.getPluginRef())[key] = value;
    }
}
//# sourceMappingURL=cordova-property.js.map

/***/ }),

/***/ 4234:
/*!****************************************************************************!*\
  !*** ./node_modules/@ionic-native/core/__ivy_ngcc__/decorators/cordova.js ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "cordova": () => (/* binding */ cordova)
/* harmony export */ });
/* harmony import */ var _common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./common */ 9870);

function cordova(pluginObj, methodName, config, args) {
    return (0,_common__WEBPACK_IMPORTED_MODULE_0__.wrap)(pluginObj, methodName, config).apply(this, args);
}
//# sourceMappingURL=cordova.js.map

/***/ }),

/***/ 5938:
/*!**************************************************************************************!*\
  !*** ./node_modules/@ionic-native/core/__ivy_ngcc__/decorators/instance-property.js ***!
  \**************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "instancePropertyGet": () => (/* binding */ instancePropertyGet),
/* harmony export */   "instancePropertySet": () => (/* binding */ instancePropertySet)
/* harmony export */ });
function instancePropertyGet(pluginObj, key) {
    if (pluginObj._objectInstance && pluginObj._objectInstance[key]) {
        return pluginObj._objectInstance[key];
    }
    return null;
}
function instancePropertySet(pluginObj, key, value) {
    if (pluginObj._objectInstance) {
        pluginObj._objectInstance[key] = value;
    }
}
//# sourceMappingURL=instance-property.js.map

/***/ }),

/***/ 6264:
/*!*******************************************************************************!*\
  !*** ./node_modules/@ionic-native/core/__ivy_ngcc__/decorators/interfaces.js ***!
  \*******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);

//# sourceMappingURL=interfaces.js.map

/***/ }),

/***/ 399:
/*!***************************************************************!*\
  !*** ./node_modules/@ionic-native/core/__ivy_ngcc__/index.js ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "IonicNativePlugin": () => (/* reexport safe */ _ionic_native_plugin__WEBPACK_IMPORTED_MODULE_1__.IonicNativePlugin),
/* harmony export */   "checkAvailability": () => (/* reexport safe */ _decorators_common__WEBPACK_IMPORTED_MODULE_2__.checkAvailability),
/* harmony export */   "instanceAvailability": () => (/* reexport safe */ _decorators_common__WEBPACK_IMPORTED_MODULE_2__.instanceAvailability),
/* harmony export */   "wrap": () => (/* reexport safe */ _decorators_common__WEBPACK_IMPORTED_MODULE_2__.wrap),
/* harmony export */   "getPromise": () => (/* reexport safe */ _decorators_common__WEBPACK_IMPORTED_MODULE_2__.getPromise),
/* harmony export */   "cordova": () => (/* reexport safe */ _decorators_cordova__WEBPACK_IMPORTED_MODULE_3__.cordova),
/* harmony export */   "cordovaFunctionOverride": () => (/* reexport safe */ _decorators_cordova_function_override__WEBPACK_IMPORTED_MODULE_4__.cordovaFunctionOverride),
/* harmony export */   "cordovaInstance": () => (/* reexport safe */ _decorators_cordova_instance__WEBPACK_IMPORTED_MODULE_5__.cordovaInstance),
/* harmony export */   "cordovaPropertyGet": () => (/* reexport safe */ _decorators_cordova_property__WEBPACK_IMPORTED_MODULE_6__.cordovaPropertyGet),
/* harmony export */   "cordovaPropertySet": () => (/* reexport safe */ _decorators_cordova_property__WEBPACK_IMPORTED_MODULE_6__.cordovaPropertySet),
/* harmony export */   "instancePropertyGet": () => (/* reexport safe */ _decorators_instance_property__WEBPACK_IMPORTED_MODULE_7__.instancePropertyGet),
/* harmony export */   "instancePropertySet": () => (/* reexport safe */ _decorators_instance_property__WEBPACK_IMPORTED_MODULE_7__.instancePropertySet)
/* harmony export */ });
/* harmony import */ var _bootstrap__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./bootstrap */ 6977);
/* harmony import */ var _ionic_native_plugin__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ionic-native-plugin */ 2948);
/* harmony import */ var _decorators_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./decorators/common */ 9870);
/* harmony import */ var _decorators_cordova__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./decorators/cordova */ 4234);
/* harmony import */ var _decorators_cordova_function_override__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./decorators/cordova-function-override */ 9849);
/* harmony import */ var _decorators_cordova_instance__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./decorators/cordova-instance */ 1784);
/* harmony import */ var _decorators_cordova_property__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./decorators/cordova-property */ 9418);
/* harmony import */ var _decorators_instance_property__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./decorators/instance-property */ 5938);
/* harmony import */ var _decorators_interfaces__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./decorators/interfaces */ 6264);


// Decorators







(0,_bootstrap__WEBPACK_IMPORTED_MODULE_0__.checkReady)();

//# sourceMappingURL=index.js.map

/***/ }),

/***/ 2948:
/*!*****************************************************************************!*\
  !*** ./node_modules/@ionic-native/core/__ivy_ngcc__/ionic-native-plugin.js ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "IonicNativePlugin": () => (/* binding */ IonicNativePlugin)
/* harmony export */ });
/* harmony import */ var _decorators_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./decorators/common */ 9870);
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./util */ 9746);


var IonicNativePlugin = /** @class */ (function () {
    function IonicNativePlugin() {
    }
    /**
     * Returns a boolean that indicates whether the plugin is installed
     * @return {boolean}
     */
    IonicNativePlugin.installed = function () {
        var isAvailable = (0,_decorators_common__WEBPACK_IMPORTED_MODULE_0__.checkAvailability)(this.pluginRef) === true;
        return isAvailable;
    };
    /**
     * Returns the original plugin object
     */
    IonicNativePlugin.getPlugin = function () {
        if (typeof window !== 'undefined') {
            return (0,_util__WEBPACK_IMPORTED_MODULE_1__.get)(window, this.pluginRef);
        }
        return null;
    };
    /**
     * Returns the plugin's name
     */
    IonicNativePlugin.getPluginName = function () {
        var pluginName = this.pluginName;
        return pluginName;
    };
    /**
     * Returns the plugin's reference
     */
    IonicNativePlugin.getPluginRef = function () {
        var pluginRef = this.pluginRef;
        return pluginRef;
    };
    /**
     * Returns the plugin's install name
     */
    IonicNativePlugin.getPluginInstallName = function () {
        var plugin = this.plugin;
        return plugin;
    };
    /**
     * Returns the plugin's supported platforms
     */
    IonicNativePlugin.getSupportedPlatforms = function () {
        var platform = this.platforms;
        return platform;
    };
    IonicNativePlugin.pluginName = '';
    IonicNativePlugin.pluginRef = '';
    IonicNativePlugin.plugin = '';
    IonicNativePlugin.repo = '';
    IonicNativePlugin.platforms = [];
    IonicNativePlugin.install = '';
    return IonicNativePlugin;
}());

//# sourceMappingURL=ionic-native-plugin.js.map

/***/ }),

/***/ 9746:
/*!**************************************************************!*\
  !*** ./node_modules/@ionic-native/core/__ivy_ngcc__/util.js ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "get": () => (/* binding */ get),
/* harmony export */   "getPromise": () => (/* binding */ getPromise)
/* harmony export */ });
/**
 * @private
 */
function get(element, path) {
    var paths = path.split('.');
    var obj = element;
    for (var i = 0; i < paths.length; i++) {
        if (!obj) {
            return null;
        }
        obj = obj[paths[i]];
    }
    return obj;
}
/**
 * @private
 */
function getPromise(callback) {
    if (callback === void 0) { callback = function () { }; }
    var tryNativePromise = function () {
        if (typeof Promise === 'function' || (typeof window !== 'undefined' && window.Promise)) {
            return new Promise(function (resolve, reject) {
                callback(resolve, reject);
            });
        }
        else {
            console.error('No Promise support or polyfill found. To enable Ionic Native support, please add the es6-promise polyfill before this script, or run with a library like Angular or on a recent browser.');
        }
    };
    return tryNativePromise();
}
//# sourceMappingURL=util.js.map

/***/ }),

/***/ 5854:
/*!*************************************************************************!*\
  !*** ./node_modules/@ionic-native/screenshot/__ivy_ngcc__/ngx/index.js ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Screenshot": () => (/* binding */ Screenshot)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _ionic_native_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ionic-native/core */ 399);




var Screenshot = /** @class */ (function (_super) {
    (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__extends)(Screenshot, _super);
    function Screenshot() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     *  Takes screenshot and saves the image
     *
     * @param format {string} Format can take the value of either 'jpg' or 'png'
     * On ios, only 'jpg' format is supported
     * @param quality {number}  Determines the quality of the screenshot.
     *        Default quality is set to 100.
     * @param filename {string} Name of the file as stored on the storage
     * @returns {Promise<any>}
     */
    Screenshot.prototype.save = function (format, quality, filename) {
        return (0,_ionic_native_core__WEBPACK_IMPORTED_MODULE_0__.getPromise)(function (resolve, reject) {
            navigator.screenshot.save(function (error, result) {
                if (error) {
                    reject(error);
                }
                else {
                    resolve(result);
                }
            }, format, quality, filename);
        });
    };
    /**
     *  Takes screenshot and returns the image as an URI
     *
     * @param quality {number} Determines the quality of the screenshot.
     *        Default quality is set to 100.
     * @returns {Promise<any>}
     */
    Screenshot.prototype.URI = function (quality) {
        return (0,_ionic_native_core__WEBPACK_IMPORTED_MODULE_0__.getPromise)(function (resolve, reject) {
            navigator.screenshot.URI(function (error, result) {
                if (error) {
                    reject(error);
                }
                else {
                    resolve(result);
                }
            }, quality);
        });
    };
    Screenshot.pluginName = "Screenshot";
    Screenshot.plugin = "com.darktalker.cordova.screenshot";
    Screenshot.pluginRef = "navigator.screenshot";
    Screenshot.repo = "https://github.com/gitawego/cordova-screenshot";
    Screenshot.platforms = ["Android", "iOS", "macOS"];
Screenshot.ɵfac = /*@__PURE__*/ function () { var ɵScreenshot_BaseFactory; return function Screenshot_Factory(t) { return (ɵScreenshot_BaseFactory || (ɵScreenshot_BaseFactory = _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵgetInheritedFactory"](Screenshot)))(t || Screenshot); }; }();
Screenshot.ɵprov = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjectable"]({ token: Screenshot, factory: function (t) { return Screenshot.ɵfac(t); } });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵsetClassMetadata"](Screenshot, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable
    }], null, null); })();
    return Screenshot;
}(_ionic_native_core__WEBPACK_IMPORTED_MODULE_0__.IonicNativePlugin));


//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VzIjpbIi4uLy4uLy4uLy4uLy4uL3NyYy9AaW9uaWMtbmF0aXZlL3BsdWdpbnMvc2NyZWVuc2hvdC9uZ3gvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQzs7QUFDM0U7QUFJZ0IsSUF5QmdCLDhCQUFpQjtBQUFDO0FBRTlCO0FBRXlCO0FBQU0sSUFIakQ7QUFDRjtBQUNFO0FBQ0U7QUFDRTtBQUNFO0FBQ0U7QUFDRTtBQUNFO0FBRUosT0FETDtBQUNMLElBQUUseUJBQUksR0FBSixVQUFLLE1BQWUsRUFBRSxPQUFnQixFQUFFLFFBQWlCO0FBQUksUUFDM0QsT0FBTyxVQUFVLENBQU0sVUFBQyxPQUFPLEVBQUUsTUFBTTtBQUFJLFlBQ3pDLFNBQVMsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUN2QixVQUFDLEtBQVUsRUFBRSxNQUFXO0FBQUksZ0JBQzFCLElBQUksS0FBSyxFQUFFO0FBQ3JCLG9CQUFZLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztBQUMxQixpQkFBVztBQUFDLHFCQUFLO0FBQ2pCLG9CQUFZLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztBQUM1QixpQkFBVztBQUNYLFlBQVEsQ0FBQyxFQUNELE1BQU0sRUFDTixPQUFPLEVBQ1AsUUFBUSxDQUNULENBQUM7QUFDUixRQUFJLENBQUMsQ0FBQyxDQUFDO0FBQ1AsSUFBRSxDQUFDO0FBRUgsSUFBRTtBQUNGO0FBQ0U7QUFDRTtBQUNFO0FBQ0U7QUFFSixPQURDO0FBQ0wsSUFBRSx3QkFBRyxHQUFILFVBQUksT0FBZ0I7QUFBSSxRQUN0QixPQUFPLFVBQVUsQ0FBTSxVQUFDLE9BQU8sRUFBRSxNQUFNO0FBQUksWUFDekMsU0FBUyxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsVUFBQyxLQUFVLEVBQUUsTUFBVztBQUFJLGdCQUNuRCxJQUFJLEtBQUssRUFBRTtBQUNuQixvQkFBVSxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7QUFDeEIsaUJBQVM7QUFBQyxxQkFBSztBQUNmLG9CQUFVLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztBQUMxQixpQkFBUztBQUNULFlBQU0sQ0FBQyxFQUFFLE9BQU8sQ0FBQyxDQUFDO0FBQ2xCLFFBQUksQ0FBQyxDQUFDLENBQUM7QUFDUCxJQUFFLENBQUM7QUFDRjtBQUN3QztBQUE2RDtBQUFtRDtBQUF3RTs4Q0FoRGhPLFVBQVU7Ozs7MEJBQ0w7QUFBQyxxQkEvQlA7QUFBRSxFQStCOEIsaUJBQWlCO0FBQ2hELFNBRFksVUFBVTtBQUFJIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgSW9uaWNOYXRpdmVQbHVnaW4sIFBsdWdpbiwgZ2V0UHJvbWlzZSB9IGZyb20gJ0Bpb25pYy1uYXRpdmUvY29yZSc7XG5cbmRlY2xhcmUgY29uc3QgbmF2aWdhdG9yOiBhbnk7XG5cbi8qKlxuICogQG5hbWUgU2NyZWVuc2hvdFxuICogQGRlc2NyaXB0aW9uIENhcHR1cmVzIGEgc2NyZWVuIHNob3RcbiAqIEB1c2FnZVxuICogYGBgdHlwZXNjcmlwdFxuICogaW1wb3J0IHsgU2NyZWVuc2hvdCB9IGZyb20gJ0Bpb25pYy1uYXRpdmUvc2NyZWVuc2hvdC9uZ3gnO1xuICpcbiAqIGNvbnN0cnVjdG9yKHByaXZhdGUgc2NyZWVuc2hvdDogU2NyZWVuc2hvdCkgeyB9XG4gKlxuICogLi4uXG4gKlxuICogLy8gVGFrZSBhIHNjcmVlbnNob3QgYW5kIHNhdmUgdG8gZmlsZVxuICogdGhpcy5zY3JlZW5zaG90LnNhdmUoJ2pwZycsIDgwLCAnbXlzY3JlZW5zaG90LmpwZycpLnRoZW4ob25TdWNjZXNzLCBvbkVycm9yKTtcbiAqXG4gKiAvLyBUYWtlIGEgc2NyZWVuc2hvdCBhbmQgZ2V0IHRlbXBvcmFyeSBmaWxlIFVSSVxuICogdGhpcy5zY3JlZW5zaG90LlVSSSg4MCkudGhlbihvblN1Y2Nlc3MsIG9uRXJyb3IpO1xuICogYGBgXG4gKi9cbkBQbHVnaW4oe1xuICBwbHVnaW5OYW1lOiAnU2NyZWVuc2hvdCcsXG4gIHBsdWdpbjogJ2NvbS5kYXJrdGFsa2VyLmNvcmRvdmEuc2NyZWVuc2hvdCcsXG4gIHBsdWdpblJlZjogJ25hdmlnYXRvci5zY3JlZW5zaG90JyxcbiAgcmVwbzogJ2h0dHBzOi8vZ2l0aHViLmNvbS9naXRhd2Vnby9jb3Jkb3ZhLXNjcmVlbnNob3QnLFxuICBwbGF0Zm9ybXM6IFsnQW5kcm9pZCcsICdpT1MnLCAnbWFjT1MnXSxcbn0pXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgU2NyZWVuc2hvdCBleHRlbmRzIElvbmljTmF0aXZlUGx1Z2luIHtcbiAgLyoqXG4gICAqICBUYWtlcyBzY3JlZW5zaG90IGFuZCBzYXZlcyB0aGUgaW1hZ2VcbiAgICpcbiAgICogQHBhcmFtIGZvcm1hdCB7c3RyaW5nfSBGb3JtYXQgY2FuIHRha2UgdGhlIHZhbHVlIG9mIGVpdGhlciAnanBnJyBvciAncG5nJ1xuICAgKiBPbiBpb3MsIG9ubHkgJ2pwZycgZm9ybWF0IGlzIHN1cHBvcnRlZFxuICAgKiBAcGFyYW0gcXVhbGl0eSB7bnVtYmVyfSAgRGV0ZXJtaW5lcyB0aGUgcXVhbGl0eSBvZiB0aGUgc2NyZWVuc2hvdC5cbiAgICogICAgICAgIERlZmF1bHQgcXVhbGl0eSBpcyBzZXQgdG8gMTAwLlxuICAgKiBAcGFyYW0gZmlsZW5hbWUge3N0cmluZ30gTmFtZSBvZiB0aGUgZmlsZSBhcyBzdG9yZWQgb24gdGhlIHN0b3JhZ2VcbiAgICogQHJldHVybnMge1Byb21pc2U8YW55Pn1cbiAgICovXG4gIHNhdmUoZm9ybWF0Pzogc3RyaW5nLCBxdWFsaXR5PzogbnVtYmVyLCBmaWxlbmFtZT86IHN0cmluZyk6IFByb21pc2U8YW55PiB7XG4gICAgcmV0dXJuIGdldFByb21pc2U8YW55PigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICBuYXZpZ2F0b3Iuc2NyZWVuc2hvdC5zYXZlKFxuICAgICAgICAoZXJyb3I6IGFueSwgcmVzdWx0OiBhbnkpID0+IHtcbiAgICAgICAgICBpZiAoZXJyb3IpIHtcbiAgICAgICAgICAgIHJlamVjdChlcnJvcik7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHJlc29sdmUocmVzdWx0KTtcbiAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGZvcm1hdCxcbiAgICAgICAgcXVhbGl0eSxcbiAgICAgICAgZmlsZW5hbWVcbiAgICAgICk7XG4gICAgfSk7XG4gIH1cblxuICAvKipcbiAgICogIFRha2VzIHNjcmVlbnNob3QgYW5kIHJldHVybnMgdGhlIGltYWdlIGFzIGFuIFVSSVxuICAgKlxuICAgKiBAcGFyYW0gcXVhbGl0eSB7bnVtYmVyfSBEZXRlcm1pbmVzIHRoZSBxdWFsaXR5IG9mIHRoZSBzY3JlZW5zaG90LlxuICAgKiAgICAgICAgRGVmYXVsdCBxdWFsaXR5IGlzIHNldCB0byAxMDAuXG4gICAqIEByZXR1cm5zIHtQcm9taXNlPGFueT59XG4gICAqL1xuICBVUkkocXVhbGl0eT86IG51bWJlcik6IFByb21pc2U8YW55PiB7XG4gICAgcmV0dXJuIGdldFByb21pc2U8YW55PigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICBuYXZpZ2F0b3Iuc2NyZWVuc2hvdC5VUkkoKGVycm9yOiBhbnksIHJlc3VsdDogYW55KSA9PiB7XG4gICAgICAgIGlmIChlcnJvcikge1xuICAgICAgICAgIHJlamVjdChlcnJvcik7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgcmVzb2x2ZShyZXN1bHQpO1xuICAgICAgICB9XG4gICAgICB9LCBxdWFsaXR5KTtcbiAgICB9KTtcbiAgfVxufVxuIl19

/***/ }),

/***/ 8932:
/*!***************************************************************!*\
  !*** ./src/app/components/expandable/expandable.component.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ExpandableComponent": () => (/* binding */ ExpandableComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_expandable_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./expandable.component.html */ 3032);
/* harmony import */ var _expandable_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./expandable.component.scss */ 3680);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);



/* eslint-disable @angular-eslint/no-input-rename */
/* eslint-disable @typescript-eslint/no-inferrable-types */
/* eslint-disable @typescript-eslint/quotes */

let ExpandableComponent = class ExpandableComponent {
    constructor(renderer) {
        this.renderer = renderer;
        // eslint-disable-next-line @angular-eslint/no-input-rename
        this.expanded = false;
        this.expandHeight = "150px";
    }
    ngAfterViewInit() {
        this.renderer.setStyle(this.expandWrapper.nativeElement, "max-height", this.expandHeight);
    }
};
ExpandableComponent.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Renderer2 }
];
ExpandableComponent.propDecorators = {
    expandWrapper: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.ViewChild, args: ["expandWrapper", { read: _angular_core__WEBPACK_IMPORTED_MODULE_2__.ElementRef },] }],
    expanded: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input, args: ["expanded",] }],
    expandHeight: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input, args: ["expandHeight",] }]
};
ExpandableComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Component)({
        selector: "app-expandable",
        template: _raw_loader_expandable_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_expandable_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], ExpandableComponent);



/***/ }),

/***/ 9433:
/*!************************************************************************!*\
  !*** ./src/app/pages/time-machine/details/time-machine-detail.page.ts ***!
  \************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TimeMachineDetailPage": () => (/* binding */ TimeMachineDetailPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_time_machine_detail_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./time-machine-detail.page.html */ 7957);
/* harmony import */ var _time_machine_detail_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./time-machine-detail.page.scss */ 5877);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var src_app_globals_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/globals.service */ 3221);
/* harmony import */ var _awesome_cordova_plugins_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @awesome-cordova-plugins/social-sharing/ngx */ 5221);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _ionic_native_screenshot_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/screenshot/ngx */ 5854);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ 9895);



/* eslint-disable arrow-body-style */
/* eslint-disable @typescript-eslint/no-inferrable-types */
/* eslint-disable max-len */
/* eslint-disable no-var */
/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable @typescript-eslint/type-annotation-spacing */
/* eslint-disable no-trailing-spaces */
/* eslint-disable @typescript-eslint/dot-notation */
/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable @typescript-eslint/semi */
/* eslint-disable quote-props */
/* eslint-disable @typescript-eslint/quotes */
/* eslint-disable @typescript-eslint/prefer-for-of */







let TimeMachineDetailPage = class TimeMachineDetailPage {
    constructor(globalService, socialSharing, loadingController, screenShot, platform, toastCtrl, navController, route) {
        this.globalService = globalService;
        this.socialSharing = socialSharing;
        this.loadingController = loadingController;
        this.screenShot = screenShot;
        this.platform = platform;
        this.toastCtrl = toastCtrl;
        this.navController = navController;
        this.route = route;
        this.imagePrint = false;
        this.isMobilePlatform = !(this.platform.is('desktop') || this.platform.is('mobileweb'));
        this.pointSubscription = this.globalService.selectedPoint.subscribe((point) => {
            console.log('point', point);
            if (point == '') {
                this.toastCtrl
                    .create({
                    message: 'Error occured. something went wrong!!',
                    position: 'bottom',
                    duration: 4000,
                    color: 'danger',
                })
                    .then((toast) => {
                    toast.present();
                    this.route.navigate(['/time-machine']);
                });
                return '';
            }
            this.selectedPoint = point;
            let options = {
                weekday: 'short',
                year: 'numeric',
                month: 'short',
                day: 'numeric',
                hour: 'numeric',
                minute: 'numeric',
                hour12: true,
            };
            this.selectedPoint.dateTime = new Date(this.selectedPoint.timestamp.replace(' ', 'T')).toLocaleDateString('en-US', options);
            console.log(this.selectedPoint);
        });
    }
    ionViewWillEnter() {
        this.pointSubscription = this.globalService.selectedPoint.subscribe((point) => {
            console.log('point', point);
            if (point == '') {
                this.toastCtrl
                    .create({
                    message: 'Error occured. something went wrong!!',
                    position: 'bottom',
                    duration: 4000,
                    color: 'danger',
                })
                    .then((toast) => {
                    toast.present();
                    this.route.navigate(['/time-machine']);
                });
                return '';
            }
            else {
                this.selectedPoint = point;
                let options = {
                    weekday: 'short',
                    year: 'numeric',
                    month: 'short',
                    day: 'numeric',
                    hour: 'numeric',
                    minute: 'numeric',
                    hour12: true,
                };
                this.selectedPoint.dateTime = new Date(this.selectedPoint.timestamp.replace(' ', 'T')).toLocaleDateString('en-US', options);
                console.log(this.selectedPoint);
                this.renderMap();
            }
        });
        console.log(this.selectedPoint);
        this.isMobilePlatform = !(this.platform.is('desktop') || this.platform.is('mobileweb'));
    }
    renderMap() {
        this.map2 = new google.maps.Map(document.getElementById('mapDiv'), {
            mapTypeId: google.maps.MapTypeId.HYBRID,
            mapTypeControl: false,
            streetViewControl: false,
            keyboardShortcuts: false,
            center: {
                lat: parseFloat(this.selectedPoint.latitude),
                lng: parseFloat(this.selectedPoint.longitude),
            },
            zoom: 18,
            zoomControl: false,
            fullscreenControl: false,
        });
        // var bounds = new google.maps.LatLngBounds();
        var pos = new google.maps.LatLng(this.selectedPoint.latitude, this.selectedPoint.longitude);
        // bounds.extend(pos);
        new google.maps.Marker({
            position: pos,
            map: this.map2,
            title: this.selectedPoint.formatted_address,
            clickable: true,
            animation: google.maps.Animation.DROP,
            icon: {
                url: this.selectedPoint.tracker_icon_url,
                fillOpacity: 1,
                strokeWeight: 0,
                size: new google.maps.Size(30, 30),
                scaledSize: new google.maps.Size(30, 30),
                anchor: new google.maps.Point(0, 32),
            },
        });
    }
    getDirection() {
        let url = `https://www.google.com/maps/search/?api=1&query=${this.selectedPoint.latitude},${this.selectedPoint.longitude}`;
        window.open(url, '_system');
    }
    shareLocation() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            console.log('is mobile platform  => ', this.isMobilePlatform);
            if (this.isMobilePlatform) {
                this.imagePrint = true;
                this.loadingGbl = yield this.loadingController.create({
                    cssClass: 'my-custom-class',
                    message: 'Please wait...',
                });
                yield this.loadingGbl.present();
                yield this.loadingGbl.dismiss();
                this.screenShot.URI(100).then((imageBase64) => (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
                    this.imagePrint = false;
                    let prepareText = `${this.selectedPoint.trackerName} was near ${this.selectedPoint.formatted_address} on ${this.selectedPoint.dateTime}`;
                    yield this.socialSharing.share(prepareText, null, imageBase64.URI, null);
                }), (error) => (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
                    this.imagePrint = false;
                    console.log(error);
                }));
            }
            else {
                console.log('this feature only supported in mobile application');
            }
        });
    }
    blobToBase64(blob) {
        return new Promise((resolve, _) => {
            const reader = new FileReader();
            reader.onloadend = () => resolve(reader.result);
            reader.readAsDataURL(blob);
        });
    }
    ionViewDidLeave() {
        if (this.pointSubscription) {
            this.pointSubscription.unsubscribe();
        }
    }
};
TimeMachineDetailPage.ctorParameters = () => [
    { type: src_app_globals_service__WEBPACK_IMPORTED_MODULE_2__.GlobalService },
    { type: _awesome_cordova_plugins_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_3__.SocialSharing },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.LoadingController },
    { type: _ionic_native_screenshot_ngx__WEBPACK_IMPORTED_MODULE_4__.Screenshot },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.Platform },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.ToastController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.NavController },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__.Router }
];
TimeMachineDetailPage.propDecorators = {
    mapRef: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.ViewChild, args: ['mapDiv', { read: _angular_core__WEBPACK_IMPORTED_MODULE_8__.ElementRef, static: false },] }]
};
TimeMachineDetailPage = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Component)({
        selector: 'app-time-machine-detail',
        template: _raw_loader_time_machine_detail_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_time_machine_detail_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], TimeMachineDetailPage);



/***/ }),

/***/ 6508:
/*!*******************************************************************!*\
  !*** ./src/app/pages/time-machine/time-machine-routing.module.ts ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TimeMachinePageRoutingModule": () => (/* binding */ TimeMachinePageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _details_time_machine_detail_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./details/time-machine-detail.page */ 9433);
/* harmony import */ var _time_machine_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./time-machine.page */ 9521);





const routes = [
    {
        path: '',
        component: _time_machine_page__WEBPACK_IMPORTED_MODULE_1__.TimeMachinePage
    },
    {
        path: 'detail',
        component: _details_time_machine_detail_page__WEBPACK_IMPORTED_MODULE_0__.TimeMachineDetailPage
    }
];
let TimeMachinePageRoutingModule = class TimeMachinePageRoutingModule {
};
TimeMachinePageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__.RouterModule],
    })
], TimeMachinePageRoutingModule);



/***/ }),

/***/ 6803:
/*!***********************************************************!*\
  !*** ./src/app/pages/time-machine/time-machine.module.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TimeMachinePageModule": () => (/* binding */ TimeMachinePageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _time_machine_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./time-machine-routing.module */ 6508);
/* harmony import */ var _time_machine_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./time-machine.page */ 9521);
/* harmony import */ var _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @fortawesome/angular-fontawesome */ 5002);
/* harmony import */ var _components_expandable_expandable_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../components/expandable/expandable.component */ 8932);
/* harmony import */ var ngx_owl_carousel__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-owl-carousel */ 3189);
/* harmony import */ var ngx_owl_carousel__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(ngx_owl_carousel__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _details_time_machine_detail_page__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./details/time-machine-detail.page */ 9433);
/* harmony import */ var _awesome_cordova_plugins_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @awesome-cordova-plugins/social-sharing/ngx */ 5221);
/* harmony import */ var _ionic_native_screenshot_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/screenshot/ngx */ 5854);
/* harmony import */ var src_app_PipesModule_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/PipesModule.module */ 9024);














let TimeMachinePageModule = class TimeMachinePageModule {
};
TimeMachinePageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_9__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_10__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_11__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_12__.IonicModule,
            _time_machine_routing_module__WEBPACK_IMPORTED_MODULE_0__.TimeMachinePageRoutingModule,
            _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_13__.FontAwesomeModule,
            ngx_owl_carousel__WEBPACK_IMPORTED_MODULE_3__.OwlModule,
            src_app_PipesModule_module__WEBPACK_IMPORTED_MODULE_7__.PipesModule,
        ],
        declarations: [_time_machine_page__WEBPACK_IMPORTED_MODULE_1__.TimeMachinePage, _components_expandable_expandable_component__WEBPACK_IMPORTED_MODULE_2__.ExpandableComponent, _details_time_machine_detail_page__WEBPACK_IMPORTED_MODULE_4__.TimeMachineDetailPage],
        providers: [_awesome_cordova_plugins_social_sharing_ngx__WEBPACK_IMPORTED_MODULE_5__.SocialSharing, _ionic_native_screenshot_ngx__WEBPACK_IMPORTED_MODULE_6__.Screenshot],
    })
], TimeMachinePageModule);



/***/ }),

/***/ 9521:
/*!*********************************************************!*\
  !*** ./src/app/pages/time-machine/time-machine.page.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TimeMachinePage": () => (/* binding */ TimeMachinePage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_time_machine_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./time-machine.page.html */ 8587);
/* harmony import */ var _time_machine_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./time-machine.page.scss */ 1334);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var src_app_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/rest.service */ 1881);
/* harmony import */ var src_app_globals_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/globals.service */ 3221);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ 5257);



/* eslint-disable max-len */
/* eslint-disable no-var */
/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable @typescript-eslint/type-annotation-spacing */
/* eslint-disable no-trailing-spaces */
/* eslint-disable @typescript-eslint/dot-notation */
/* eslint-disable eqeqeq */
/* eslint-disable prefer-const */
/* eslint-disable @typescript-eslint/semi */
/* eslint-disable quote-props */
/* eslint-disable @typescript-eslint/quotes */
/* eslint-disable @typescript-eslint/prefer-for-of */






let TimeMachinePage = class TimeMachinePage {
    constructor(rest, global, router, loadingController) {
        this.rest = rest;
        this.global = global;
        this.router = router;
        this.loadingController = loadingController;
        this.dataLoad = false;
        this.filteredTrackerResult = [];
        this.items = [];
        this.defaultMapLocation = {
            last_location: { latitude: 32.3143016666667, longitude: -84.5017333333333 },
        }; // LAT LONG POINTS TO DAVAO, PHILIPPINES (Subject to change)
        this.mapStyle = '0'; // map style by default is set to 0 (ROADMAP style)
        this.slideOptions = { items: 2, dots: false };
        this.carouselOptions = { items: 3, dots: true, nav: true };
        this.getUserSettings();
        this.displayMap = false;
        this.globalDataMessageSubscription = this.global.currentMessage.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_4__.take)(1)).subscribe(message => this.globalData = message);
        this.items = [
            { expanded: true },
        ];
        // this.flightPath = [];
        // this.selectedTracker = {
        //   "imei": 4900104424,
        //   "date": "2022/2/24",
        //   "starttime": "8:41:15",
        //   "endtime": "20:41:15"
        // };
        // this.selectedDate = "2022-02-24T03:11:15.395-00:00";
        // this.selectedStartTime = "2022-04-09T08:41:15.397+05:30";
        // this.selectedEndTime = "2022-04-09T12:41:15.397+05:30";
        // this.selectedTracker = {
        //   "imei": 4900104424,
        //   "date": "2022/2/24",
        //   "starttime": "8:41:15",
        //   "endtime": "20:41:15"
        // };
        this.items[0].expanded = true;
    }
    ionViewDidEnter() {
        //this.getUserSettings();
        //this.displayMap = false;
        //this.globalDataMessageSubscription = this.global.currentMessage.pipe(take(1)).subscribe(message => this.globalData = message);
        // this.items = [
        //   { expanded: true },      
        // ];
        var _a;
        // this.displayMap = false;
        // this.items[0].expanded = true;
        //this.global.currentMessage.subscribe(message => this.globalData = message)
        this.slideOptions = { items: 2, dots: false };
        if (this.owlElement) {
            (_a = this.owlElement) === null || _a === void 0 ? void 0 : _a.reInit();
        }
        this.getUserTracker();
        this.presentLoading();
    }
    getUserTracker() {
        this.rest.get('thorton/trackers/user', '', '').subscribe((resp) => {
            if (resp.length != 0) {
                this.userTrackers = resp;
                this.dataLoad = true;
            }
            else {
                this.dataLoad = true;
            }
        });
    }
    selectTracker(tracker) {
        if (tracker != 'All') {
            this.selectedTracker = tracker;
        }
        else {
            this.selectedTracker = tracker;
        }
    }
    presentLoading() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            const loading = yield this.loadingController.create({
                cssClass: 'my-custom-class',
                message: 'Please wait...',
                duration: 4000
            });
            yield loading.present();
            const { role, data } = yield loading.onDidDismiss();
        });
    }
    applyFilter() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            this.loadingGbl = yield this.loadingController.create({
                cssClass: 'my-custom-class',
                message: 'Please wait...'
            });
            yield this.loadingGbl.present();
            let selectedTracker = JSON.parse(localStorage.getItem('selectedUserTrackers'));
            let date = new Date(this.selectedDate);
            let startTime = new Date(this.selectedStartTime);
            let endTime = new Date(this.selectedEndTime);
            let dataToSend;
            if (this.selectedTracker == 'All') {
                dataToSend = {
                    "date": date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate(),
                    "starttime": startTime.getHours() + ':' + startTime.getMinutes() + ':' + startTime.getSeconds(),
                    "endtime": endTime.getHours() + ':' + endTime.getMinutes() + ':' + endTime.getSeconds(),
                };
            }
            else {
                dataToSend = {
                    "imei": this.selectedTracker.imei,
                    "date": date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate(),
                    "starttime": startTime.getHours() + ':' + startTime.getMinutes() + ':' + startTime.getSeconds(),
                    "endtime": endTime.getHours() + ':' + endTime.getMinutes() + ':' + endTime.getSeconds(),
                };
            }
            this.rest.post(dataToSend, 'thorton/time/machine').subscribe((resp) => (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
                if ((resp === null || resp === void 0 ? void 0 : resp.length) == 0) {
                    yield this.loadingGbl.dismiss();
                    this.displayMap = true;
                }
                else {
                    for (let x = 0; x < resp.length; x++) {
                        resp[x].tracker_icon_url = this.global.getMarkerLocation(resp[x].tracker_icon, resp[x].tracker_icon_color);
                    }
                    // let uniqueLocations = this.removeDuplicatesLatLong(resp);
                    // let response = resp;
                    // response[0].locations = uniqueLocations;
                    this.filteredTrackerResult = resp;
                    if (this.owlElement) {
                        this.owlElement.reInit();
                    }
                    else {
                        setTimeout(() => {
                            var _a;
                            (_a = this.owlElement) === null || _a === void 0 ? void 0 : _a.reInit();
                        }, 0);
                    }
                    if (this.filteredTrackerResult.length > 0) {
                        this.selectedTrackerData = resp[0];
                        this.displayMap = true;
                        //this.owlElement.reInit();
                        this.drawMap();
                    }
                    else {
                        this.displayMap = true;
                        yield this.loadingGbl.dismiss();
                    }
                }
                // this.newMessage(JSON.stringify(resp));
                // this.router.navigate(['/live-tracking']);
            }));
        });
    }
    viewInLiveTracking(trackerDetails, location) {
        // console.log(trackerDetails);
        // console.log(location);
        // trackerDetails.locations = [];
        // let a = trackerDetails['tracker_icon'];
        // let output = a;
        // trackerDetails['tracker_icon'] = output;
        // trackerDetails.locations.push(location);
        // console.log(trackerDetails);
        // this.newMessage(JSON.stringify(trackerDetails));
        // console.log(trackerDetails);
        // this.router.navigate(['/live-tracking']);
        let requiredData = Object.assign({ trackerName: trackerDetails.tracker_name, tracker_icon_url: trackerDetails.tracker_icon_url }, location);
        this.global.userSelectedNewPoint(requiredData);
        this.router.navigate(['/time-machine/detail']);
    }
    newMessage(locations) {
        this.global.changeMessage(locations);
    }
    drawMap(selectedLocation) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            if (this.selectedTrackerData.locations.length > 0) {
                let currentTrackerLocation = this.selectedTrackerData.locations;
                let latu = selectedLocation ? selectedLocation.latitude : currentTrackerLocation[0].latitude;
                let long = selectedLocation ? selectedLocation.longitude : currentTrackerLocation[0].longitude;
                this.map = null;
                this.flightPath = [];
                this.map = new google.maps.Map(document.getElementById("tmmap"), {
                    mapTypeId: google.maps.MapTypeId[(this.userSettings.map_style).toUpperCase()],
                    mapTypeControl: false,
                    streetViewControl: false,
                    keyboardShortcuts: false,
                    zoom: 0
                });
                let bounds = new google.maps.LatLngBounds();
                var poly = [];
                let index = 0;
                for (let location of currentTrackerLocation) {
                    const latLong = { lat: parseFloat(location.latitude), lng: parseFloat(location.longitude) };
                    if (selectedLocation) {
                        if (location.latitude == selectedLocation.latitude && location.longitude == selectedLocation.longitude) {
                            this.currentMarker = new google.maps.Marker({
                                position: { lat: parseFloat(selectedLocation.latitude), lng: parseFloat(selectedLocation.longitude) },
                                map: this.map,
                                title: location.formatted_address,
                                clickable: true,
                                icon: {
                                    url: this.selectedTrackerData.tracker_icon_url,
                                    fillOpacity: 1,
                                    strokeWeight: 0,
                                    size: new google.maps.Size(30, 30),
                                    scaledSize: new google.maps.Size(30, 30),
                                    anchor: new google.maps.Point(0, 32)
                                },
                            });
                        }
                    }
                    else {
                        if (index == 0) {
                            this.currentMarker = new google.maps.Marker({
                                position: latLong,
                                map: this.map,
                                title: location.formatted_address,
                                clickable: true,
                                animation: google.maps.Animation.DROP,
                                icon: {
                                    url: this.selectedTrackerData.tracker_icon_url,
                                    fillOpacity: 1,
                                    strokeWeight: 0,
                                    size: new google.maps.Size(30, 30),
                                    scaledSize: new google.maps.Size(30, 30),
                                    anchor: new google.maps.Point(0, 32)
                                }
                            });
                        }
                    }
                    index++;
                }
                let color = '#lightgray';
                if (selectedLocation) {
                    color = this.selectedTrackerData.tracker_icon_color;
                }
                for (var i = 0; i < currentTrackerLocation.length; i = i + 1) {
                    let j = i + 1;
                    if (selectedLocation && currentTrackerLocation[i].latitude == selectedLocation.latitude && currentTrackerLocation[i].longitude == selectedLocation.longitude) {
                        color = "#lightgray";
                    }
                    if (currentTrackerLocation[i] && currentTrackerLocation[j]) {
                        var pos = new google.maps.LatLng(currentTrackerLocation[i].latitude, currentTrackerLocation[i].longitude);
                        var pos2 = new google.maps.LatLng(currentTrackerLocation[j].latitude, currentTrackerLocation[j].longitude);
                        //bounds.extend(pos);
                        bounds.extend(pos2);
                        poly.push(pos);
                        this.flightPath.push(new google.maps.Polyline({
                            path: [pos, pos2],
                            geodesic: true,
                            strokeColor: "lightgray",
                            strokeOpacity: 1.0,
                            strokeWeight: 8,
                            map: this.map,
                        }));
                        this.flightPath.push(new google.maps.Polyline({
                            path: [pos, pos2],
                            geodesic: true,
                            strokeColor: color,
                            strokeOpacity: 1.0,
                            strokeWeight: 5,
                            map: this.map,
                        }));
                    }
                }
                setTimeout(() => (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
                    this.displayMap = true;
                    this.map.fitBounds(bounds);
                    this.map.panToBounds(bounds);
                    this.map.panTo({
                        lat: parseFloat(latu),
                        lng: parseFloat(long)
                    });
                    yield this.loadingGbl.dismiss();
                }), 100);
            }
            else {
                //this.displayMap = false;
                yield this.loadingGbl.dismiss();
            }
        });
    }
    drawPathInToMap(selectedLocation) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            let latu = selectedLocation.latitude;
            let long = selectedLocation.longitude;
            let currentTrackerLocation = this.selectedTrackerData.locations;
            let bounds = new google.maps.LatLngBounds();
            let poly = [];
            this.currentMarker.setMap(null);
            for (let existingPoly of this.flightPath) {
                existingPoly.setMap(null);
            }
            this.flightPath = [];
            let index = 0;
            for (let location of currentTrackerLocation) {
                const latLong = { lat: parseFloat(location.latitude), lng: parseFloat(location.longitude) };
                if (selectedLocation) {
                    if (location.latitude == selectedLocation.latitude && location.longitude == selectedLocation.longitude) {
                        this.currentMarker = new google.maps.Marker({
                            position: { lat: parseFloat(selectedLocation.latitude), lng: parseFloat(selectedLocation.longitude) },
                            map: this.map,
                            title: location.formatted_address,
                            clickable: true,
                            icon: {
                                url: this.selectedTrackerData.tracker_icon_url,
                                fillOpacity: 1,
                                strokeWeight: 0,
                                size: new google.maps.Size(30, 30),
                                scaledSize: new google.maps.Size(30, 30),
                                anchor: new google.maps.Point(0, 32)
                            },
                        });
                    }
                }
                else {
                    if (index == 0) {
                        this.currentMarker = new google.maps.Marker({
                            position: latLong,
                            map: this.map,
                            title: location.formatted_address,
                            clickable: true,
                            animation: google.maps.Animation.DROP,
                            icon: {
                                url: this.selectedTrackerData.tracker_icon_url,
                                fillOpacity: 1,
                                strokeWeight: 0,
                                size: new google.maps.Size(30, 30),
                                scaledSize: new google.maps.Size(30, 30),
                                anchor: new google.maps.Point(0, 32)
                            }
                        });
                    }
                }
                index++;
            }
            let color = 'lightgray';
            if (selectedLocation) {
                color = this.selectedTrackerData.tracker_icon_color;
            }
            for (var i = 0; i < currentTrackerLocation.length; i = i + 1) {
                let j = i + 1;
                if (selectedLocation && currentTrackerLocation[i].latitude == selectedLocation.latitude && currentTrackerLocation[i].longitude == selectedLocation.longitude) {
                    color = "lightgray";
                }
                if (currentTrackerLocation[i] && currentTrackerLocation[j]) {
                    var pos = new google.maps.LatLng(currentTrackerLocation[i].latitude, currentTrackerLocation[i].longitude);
                    var pos2 = new google.maps.LatLng(currentTrackerLocation[j].latitude, currentTrackerLocation[j].longitude);
                    //bounds.extend(pos);
                    bounds.extend(pos2);
                    poly.push(pos);
                    this.flightPath.push(new google.maps.Polyline({
                        path: [pos, pos2],
                        geodesic: true,
                        strokeColor: "lightgray",
                        strokeOpacity: 1.0,
                        strokeWeight: 8,
                        map: this.map,
                    }));
                    this.flightPath.push(new google.maps.Polyline({
                        path: [pos, pos2],
                        geodesic: true,
                        strokeColor: color,
                        strokeOpacity: 1.0,
                        strokeWeight: 5,
                        map: this.map,
                    }));
                }
            }
            this.displayMap = true;
            this.map.panTo({ lat: parseFloat(latu), lng: parseFloat(long) });
            //this.map.fitBounds(bounds);
            //this.map.panToBounds(bounds);
            // this.map.panTo({ 
            //     lat: parseFloat(latu) , 
            //     lng: parseFloat(long) 
            //   });
        });
    }
    viewInCurrentMap(trackerDetails, location, index) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            // console.log(trackerDetails, location,index)
            // this.loadingGbl = await this.loadingController.create({
            //   cssClass: 'my-custom-class',
            //   message: 'Please wait...'
            // });
            // await this.loadingGbl.present();
            //this.map.setCenter({lat:parseFloat(location.latitude),lng:parseFloat(location.longitude)});
            this.drawPathInToMap(location);
        });
    }
    expandItem(item) {
        if (item.expanded) {
            item.expanded = false;
        }
        else {
            this.items.map(listItem => {
                if (item == listItem) {
                    listItem.expanded = !listItem.expanded;
                }
                else {
                    listItem.expanded = false;
                }
                return listItem;
            });
        }
    }
    changeSelectedTracker(index) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            this.loadingGbl = yield this.loadingController.create({
                cssClass: 'my-custom-class',
                message: 'Please wait...'
            });
            yield this.loadingGbl.present();
            this.selectedTrackerData = this.filteredTrackerResult[index];
            this.displayMap = true;
            this.drawMap();
        });
    }
    getUserSettings() {
        this.userSettings = JSON.parse(localStorage.getItem('userSettings'));
    }
    ionViewDidLeave() {
        // this.displayMap = false;
        // this.items[0].expanded = true;
        // this.selectedDate = undefined;
        // this.selectedStartTime = undefined;
        // this.selectedEndTime = undefined;
        // this.selectedTracker = null;
        if (this.globalDataMessageSubscription) {
            this.globalDataMessageSubscription.unsubscribe();
        }
    }
};
TimeMachinePage.ctorParameters = () => [
    { type: src_app_rest_service__WEBPACK_IMPORTED_MODULE_2__.RestService },
    { type: src_app_globals_service__WEBPACK_IMPORTED_MODULE_3__.GlobalService },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__.Router },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.LoadingController }
];
TimeMachinePage.propDecorators = {
    mapRef: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.ViewChild, args: ['tmmap', { read: _angular_core__WEBPACK_IMPORTED_MODULE_8__.ElementRef, static: false },] }],
    owlElement: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.ViewChild, args: ['owlElement',] }]
};
TimeMachinePage = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Component)({
        selector: 'app-time-machine',
        template: _raw_loader_time_machine_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_time_machine_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], TimeMachinePage);



/***/ }),

/***/ 3680:
/*!*****************************************************************!*\
  !*** ./src/app/components/expandable/expandable.component.scss ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".expand-wrapper {\n  transition: max-height 0.4s ease-in-out;\n  overflow: hidden;\n  height: auto;\n}\n\n.collapsed {\n  max-height: 0 !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImV4cGFuZGFibGUuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSx1Q0FBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtBQUNKOztBQUNFO0VBQ0Usd0JBQUE7QUFFSiIsImZpbGUiOiJleHBhbmRhYmxlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmV4cGFuZC13cmFwcGVyIHtcbiAgICB0cmFuc2l0aW9uOiBtYXgtaGVpZ2h0IDAuNHMgZWFzZS1pbi1vdXQ7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICBoZWlnaHQ6IGF1dG87XG4gIH1cbiAgLmNvbGxhcHNlZCB7XG4gICAgbWF4LWhlaWdodDogMCAhaW1wb3J0YW50O1xuICB9Il19 */");

/***/ }),

/***/ 5877:
/*!**************************************************************************!*\
  !*** ./src/app/pages/time-machine/details/time-machine-detail.page.scss ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".header-div {\n  width: 100%;\n  height: 48px;\n}\n\n.header-div span {\n  display: inline-block;\n}\n\n.header-div > span {\n  float: left;\n}\n\n.header-title {\n  width: 70%;\n  display: inline-block;\n  text-align: center;\n  vertical-align: middle;\n  padding: 10px 0px;\n}\n\n.back-span > ion-buttons {\n  float: left;\n  display: inline-block;\n}\n\n.mapDiv {\n  height: 280px;\n  margin: 0 10px;\n}\n\n.direction-btn-text {\n  margin-left: 5px;\n}\n\n.main-details-button-div ion-button {\n  text-transform: none !important;\n  margin-right: 12px;\n}\n\n.main-details-div {\n  width: 100%;\n  text-align: center;\n}\n\n.avtar-div {\n  display: inline-block;\n}\n\n.tracker-information-list {\n  width: 100%;\n}\n\n.tracker-information-list .tracker-locations-list-item {\n  width: 100%;\n}\n\n.tracker-locations-list-item .item-div {\n  width: 100%;\n}\n\n.tracker-locations-list-item .name {\n  float: left;\n}\n\n.tracker-locations-list-item .value {\n  float: right;\n}\n\n.f-left {\n  text-align: left;\n}\n\n.details-information {\n  width: 80%;\n  margin: 0 auto;\n  white-space: normal;\n}\n\n.avtar-div {\n  margin: 5px 0px;\n}\n\n.ios.button.show-back-button {\n  margin-top: 5px;\n}\n\nion-header.ios.header-ios.header-collapse-none.hydrated {\n  margin-top: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInRpbWUtbWFjaGluZS1kZXRhaWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUFDRjs7QUFDQTtFQUNFLHFCQUFBO0FBRUY7O0FBQ0E7RUFDRSxXQUFBO0FBRUY7O0FBQ0E7RUFDRSxVQUFBO0VBQ0EscUJBQUE7RUFDQSxrQkFBQTtFQUNBLHNCQUFBO0VBQ0EsaUJBQUE7QUFFRjs7QUFDQTtFQUNFLFdBQUE7RUFDQSxxQkFBQTtBQUVGOztBQUNBO0VBQ0UsYUFBQTtFQUNBLGNBQUE7QUFFRjs7QUFDQTtFQUNFLGdCQUFBO0FBRUY7O0FBQ0E7RUFDRSwrQkFBQTtFQUNBLGtCQUFBO0FBRUY7O0FBQ0E7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7QUFFRjs7QUFLQTtFQUNFLHFCQUFBO0FBRkY7O0FBS0E7RUFDRSxXQUFBO0FBRkY7O0FBS0E7RUFDRSxXQUFBO0FBRkY7O0FBS0E7RUFDRSxXQUFBO0FBRkY7O0FBS0E7RUFDRSxXQUFBO0FBRkY7O0FBS0E7RUFDRSxZQUFBO0FBRkY7O0FBS0E7RUFDRSxnQkFBQTtBQUZGOztBQUtBO0VBQ0UsVUFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtBQUZGOztBQUtBO0VBQ0UsZUFBQTtBQUZGOztBQUtBO0VBQ0UsZUFBQTtBQUZGOztBQUtBO0VBQ0UsZ0JBQUE7QUFGRiIsImZpbGUiOiJ0aW1lLW1hY2hpbmUtZGV0YWlsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oZWFkZXItZGl2IHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogNDhweDtcbn1cbi5oZWFkZXItZGl2IHNwYW4ge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG59XG5cbi5oZWFkZXItZGl2ID4gc3BhbiB7XG4gIGZsb2F0OiBsZWZ0O1xufVxuXG4uaGVhZGVyLXRpdGxlIHtcbiAgd2lkdGg6IDcwJTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG4gIHBhZGRpbmc6IDEwcHggMHB4O1xufVxuXG4uYmFjay1zcGFuID4gaW9uLWJ1dHRvbnMge1xuICBmbG9hdDogbGVmdDtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xufVxuXG4ubWFwRGl2IHtcbiAgaGVpZ2h0OiAyODBweDtcbiAgbWFyZ2luOiAwIDEwcHg7XG59XG5cbi5kaXJlY3Rpb24tYnRuLXRleHQge1xuICBtYXJnaW4tbGVmdDogNXB4O1xufVxuXG4ubWFpbi1kZXRhaWxzLWJ1dHRvbi1kaXYgaW9uLWJ1dHRvbiB7XG4gIHRleHQtdHJhbnNmb3JtOiBub25lICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi1yaWdodDogMTJweDtcbn1cblxuLm1haW4tZGV0YWlscy1kaXYge1xuICB3aWR0aDogMTAwJTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4vLyAuZGV0YWlscy1kaXZ7XG4vLyAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuLy8gfVxuXG4uYXZ0YXItZGl2IHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xufVxuXG4udHJhY2tlci1pbmZvcm1hdGlvbi1saXN0IHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi50cmFja2VyLWluZm9ybWF0aW9uLWxpc3QgLnRyYWNrZXItbG9jYXRpb25zLWxpc3QtaXRlbSB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4udHJhY2tlci1sb2NhdGlvbnMtbGlzdC1pdGVtIC5pdGVtLWRpdiB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4udHJhY2tlci1sb2NhdGlvbnMtbGlzdC1pdGVtIC5uYW1lIHtcbiAgZmxvYXQ6IGxlZnQ7XG59XG5cbi50cmFja2VyLWxvY2F0aW9ucy1saXN0LWl0ZW0gLnZhbHVlIHtcbiAgZmxvYXQ6IHJpZ2h0O1xufVxuXG4uZi1sZWZ0IHtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbn1cblxuLmRldGFpbHMtaW5mb3JtYXRpb24ge1xuICB3aWR0aDogODAlO1xuICBtYXJnaW46IDAgYXV0bztcbiAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcbn1cblxuLmF2dGFyLWRpdiB7XG4gIG1hcmdpbjogNXB4IDBweDtcbn1cblxuLmlvcy5idXR0b24uc2hvdy1iYWNrLWJ1dHRvbiB7XG4gIG1hcmdpbi10b3A6IDVweDtcbn1cblxuaW9uLWhlYWRlci5pb3MuaGVhZGVyLWlvcy5oZWFkZXItY29sbGFwc2Utbm9uZS5oeWRyYXRlZCB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59XG4iXX0= */");

/***/ }),

/***/ 1334:
/*!***********************************************************!*\
  !*** ./src/app/pages/time-machine/time-machine.page.scss ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("#date-selection {\n  width: 100%;\n  border: 1px solid grey;\n  border-radius: 18px;\n}\n\n.tracker-sample-view {\n  width: 10%;\n}\n\n.tracker-icon-view {\n  width: 25%;\n}\n\n#tmmap {\n  height: 360px;\n}\n\n.tracker-locations-list {\n  height: 300px;\n  overflow-y: scroll;\n  width: 100%;\n  margin-top: 0px;\n}\n\n.tracker-section {\n  padding: 3px;\n}\n\n.tracker-list {\n  width: 100%;\n  overflow: auto;\n  padding: 0px;\n  white-space: nowrap;\n  margin: 0px;\n}\n\n.tracker-list-item {\n  display: inline;\n  margin: 0 auto;\n  float: left;\n  padding: 0px 16px;\n}\n\n.tracker-name-information-div {\n  text-align: center;\n}\n\n.custom-owl-item {\n  text-align: center !important;\n}\n\nhr {\n  margin: 0.2rem 0;\n}\n\nion-card.filter-content.ios.hydrated {\n  margin: 5px 5px;\n}\n\nion-card-content.filters-card-content.ios.card-content-ios.hydrated {\n  padding-bottom: 13px !important;\n  padding-top: 13px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInRpbWUtbWFjaGluZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxXQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtBQUNKOztBQUNBO0VBT0ksVUFBQTtBQUpKOztBQVFBO0VBUUksVUFBQTtBQVpKOztBQWVBO0VBQ0ksYUFBQTtBQVpKOztBQWVBO0VBQ0ksYUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7QUFaSjs7QUFlQTtFQUNJLFlBQUE7QUFaSjs7QUFlQTtFQUNJLFdBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtBQVpKOztBQWVBO0VBQ0ksZUFBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7QUFaSjs7QUFlQTtFQUNJLGtCQUFBO0FBWko7O0FBZUE7RUFDSSw2QkFBQTtBQVpKOztBQWVBO0VBQ0ksZ0JBQUE7QUFaSjs7QUFlQTtFQUNJLGVBQUE7QUFaSjs7QUFlQTtFQUNJLCtCQUFBO0VBQ0EsNEJBQUE7QUFaSiIsImZpbGUiOiJ0aW1lLW1hY2hpbmUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI2RhdGUtc2VsZWN0aW9uIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBib3JkZXI6IDFweCBzb2xpZCBncmV5O1xuICAgIGJvcmRlci1yYWRpdXM6IDE4cHg7XG59XG4udHJhY2tlci1zYW1wbGUtdmlldyB7XG4gICAgLy8gY29sb3I6IHdoaXRlO1xuICAgIC8vIGZvbnQtc2l6ZTogMTBweDtcbiAgICAvLyBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgLy8gYm9yZGVyLXJhZGl1czogNjBweDtcbiAgICAvLyBib3gtc2hhZG93OiAwIDAgMnB4ICM4ODg7XG4gICAgLy8gcGFkZGluZzogMC41ZW0gMC42ZW07IFxuICAgIHdpZHRoOiAxMCU7XG5cbn1cblxuLnRyYWNrZXItaWNvbi12aWV3IHtcbiAgICAvLyBjb2xvcjogd2hpdGU7XG4gICAgLy8gZm9udC1zaXplOiA1MHB4O1xuICAgIC8vIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAvLyBib3JkZXItcmFkaXVzOiA2MHB4O1xuICAgIC8vIGJveC1zaGFkb3c6IDAgMCAycHggIzg4ODtcbiAgICAvLyBwYWRkaW5nOiAwLjJlbSAwLjM1ZW07IFxuICAgIC8vIGZvbnQtc2l6ZTogMTJweDsgXG4gICAgd2lkdGg6IDI1JTtcbn1cblxuI3RtbWFwe1xuICAgIGhlaWdodDogMzYwcHg7XG59XG5cbi50cmFja2VyLWxvY2F0aW9ucy1saXN0IHtcbiAgICBoZWlnaHQ6IDMwMHB4O1xuICAgIG92ZXJmbG93LXk6IHNjcm9sbDtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBtYXJnaW4tdG9wOiAwcHg7XG59XG5cbi50cmFja2VyLXNlY3Rpb24ge1xuICAgIHBhZGRpbmc6IDNweDtcbn1cblxuLnRyYWNrZXItbGlzdCB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgb3ZlcmZsb3c6IGF1dG87XG4gICAgcGFkZGluZzogMHB4O1xuICAgIHdoaXRlLXNwYWNlOm5vd3JhcDtcbiAgICBtYXJnaW46IDBweDtcbn1cblxuLnRyYWNrZXItbGlzdC1pdGVtIHtcbiAgICBkaXNwbGF5OmlubGluZTtcbiAgICBtYXJnaW46IDAgYXV0bztcbiAgICBmbG9hdDogbGVmdDtcbiAgICBwYWRkaW5nOiAwcHggMTZweDtcbn1cblxuLnRyYWNrZXItbmFtZS1pbmZvcm1hdGlvbi1kaXYge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmN1c3RvbS1vd2wtaXRlbSB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyICFpbXBvcnRhbnQ7XG59XG5cbmhyIHtcbiAgICBtYXJnaW46IDAuMnJlbSAwO1xufVxuXG5pb24tY2FyZC5maWx0ZXItY29udGVudC5pb3MuaHlkcmF0ZWQge1xuICAgIG1hcmdpbjogNXB4IDVweDtcbn1cblxuaW9uLWNhcmQtY29udGVudC5maWx0ZXJzLWNhcmQtY29udGVudC5pb3MuY2FyZC1jb250ZW50LWlvcy5oeWRyYXRlZCB7XG4gICAgcGFkZGluZy1ib3R0b206IDEzcHggIWltcG9ydGFudDtcbiAgICBwYWRkaW5nLXRvcDogMTNweCAhaW1wb3J0YW50O1xufVxuIl19 */");

/***/ }),

/***/ 3032:
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/expandable/expandable.component.html ***!
  \*******************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<div #expandWrapper class=\"expand-wrapper\" [class.collapsed]=\"!expanded\">\n  <ng-content></ng-content>\n</div>");

/***/ }),

/***/ 7957:
/*!****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/time-machine/details/time-machine-detail.page.html ***!
  \****************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\n  <ion-row class=\"header-row\">\n    <div class=\"header-div\">\n      <span class=\"back-span\">\n        <ion-buttons>\n          <ion-back-button defaultHref=\"time-machine\"></ion-back-button>\n        </ion-buttons>\n      </span>\n      <span class=\"header-title\" *ngIf=\"selectedPoint\">\n        {{selectedPoint.dateTime}}\n      </span>\n      <span class=\"back-span\"></span>\n    </div>\n  </ion-row>\n</ion-header>\n\n<ion-content>\n  <!-- <ion-row class=\"header-row\">\n    <div class=\"header-div\">\n      <span class=\"back-span\">\n        <ion-buttons>\n          <ion-back-button defaultHref=\"time-machine\"></ion-back-button>\n        </ion-buttons>\n      </span>\n      <span class=\"header-title\" *ngIf=\"selectedPoint\">\n        {{selectedPoint.dateTime}}\n      </span>\n      <span class=\"back-span\"></span>\n    </div>\n  </ion-row> -->\n\n  <ion-row class=\"ion-padding\" *ngIf=\"selectedPoint\">\n    <div class=\"main-details-div\" id=\"mainDivHtml\">\n      <div class=\"mapDiv\" id=\"mapDiv\">\n        map will be loaded here\n      </div>\n      <div id=\"detailDiv\" class=\"details-div\" [style.display]=\"imagePrint ? 'block' : 'none'\"  style=\"background: white;\">\n        <ion-avatar slot=\"start\" class=\"avtar-div\">\n          <img class=\"loc-avtar\" [src]=\"selectedPoint.tracker_icon_url\" />\n        </ion-avatar>\n        <ion-label class=\"details-information\" style=\"white-space: normal;\">\n          <div class=\"tracker-name\">\n            <strong>tracker Name</strong> : <span> {{selectedPoint.trackerName}}</span>\n          </div>\n          <div>\n            <span style=\"color: green\">Moving at 0 mph</span> \n          </div>\n          <div>\n            <span class=\"tracker-name\">Address</span> : <span> {{selectedPoint.formatted_address}} </span>\n          </div>\n          <div>\n            <span class=\"tracker-name\">Time</span> : <span>  {{selectedPoint.dateTime}} </span>\n          </div>\n        </ion-label>\n      </div>\n    </div>\n    <br/>\n    <div class=\"details-div\" [style.display]=\"imagePrint ? 'none' : 'block'\"  style=\"text-align: center; width: 100%;\">\n      <ion-avatar slot=\"start\" class=\"avtar-div\">\n        <img class=\"loc-avtar\" [src]=\"selectedPoint.tracker_icon_url\" />\n      </ion-avatar>\n      <div class=\"details-information\">\n        <div class=\"f-left\">\n          <strong>tracker Name</strong> : <span> {{selectedPoint.trackerName}}</span>\n        </div>\n        <div  class=\"f-left\">\n          <strong>Status</strong> : <span> {{selectedPoint.location_type}} </span>\n        </div>\n        <div class=\"f-left\">\n          <strong>Address</strong> : <span> {{selectedPoint.formatted_address}} </span>\n        </div>\n      </div>\n    </div>\n    <div class=\"main-details-button-div\" [style.display]=\"imagePrint ? 'none' : 'block'\"  style=\"text-align: center; width: 100%;\" style=\"text-align: center; width: 100%;\">\n\n      <ion-button color=\"dark\" (click)=\"getDirection()\"> \n        <fa-icon  class=\"tracker-sample-view\"  icon=\"location-dot\" style=\"font-size: 20px; margin-right: 7px;\"></fa-icon>\n        <span calss=\"direction-btn-text\" >Get Direction </span>\n      </ion-button>\n      <ion-button color=\"dark\" *ngIf=\"isMobilePlatform\" (click)=\"shareLocation()\"> \n        <fa-icon class=\"tracker-sample-view\" icon=\"share-square\" style=\"font-size: 20px;\"></fa-icon> \n      </ion-button>\n    </div>\n  </ion-row>\n  <hr style=\"margin: 0px;\"/>\n  <ion-row *ngIf=\"selectedPoint\" class=\"ion-padding\" [style.display]=\"imagePrint ? 'none' : 'block'\"  style=\"text-align: center; width: 100%; padding-top: 5px;\">\n    <ion-list class=\"tracker-information-list\">\n      <ion-item class=\"tracker-locations-list-item\">\n            <div class=\"item-div\">\n              <span class=\"name\">Speed</span>\n              <span class=\"value\">{{selectedPoint.moving_at}}</span>\n            </div>\n        </ion-item>\n        <ion-item class=\"tracker-locations-list-item\">\n          <div class=\"item-div\">\n            <span class=\"name\">Battery</span>\n            <span class=\"value\">{{selectedPoint.battery_level}}% <fa-icon class=\"tracker-sample-view\" icon=\"battery-half\" style=\"font-size: 20px;\"></fa-icon> </span>\n            <span></span>\n          </div>\n        </ion-item>\n      </ion-list>\n  </ion-row>\n</ion-content>\n");

/***/ }),

/***/ 8587:
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/time-machine/time-machine.page.html ***!
  \*************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\n  <ion-toolbar>\n    <ion-title><strong>Time Machine</strong></ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button menu=\"main-menu\"></ion-menu-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ng-container *ngIf=\"dataLoad && (!userTrackers || userTrackers?.length === 0); else hasTracker\">\n    <ion-grid>\n      <ion-row>\n        <div style=\"text-align: center;\">You need atleast one tracker to check time machine</div>\n      </ion-row>\n    </ion-grid>\n  </ng-container>\n  <ng-template #hasTracker>\n    <ion-card class=\"filter-content\" *ngFor=\"let item of items\">\n      <ion-card-content class=\"filters-card-content\">\n        <ion-card-title (click)=\"expandItem(item)\">Filters</ion-card-title>\n  \n        <app-expandable expandHeight=\"370px\" [expanded]=\"item.expanded\">\n          <br />\n          <ion-row>\n            <ion-datetime\n              placeholder=\"Select Date\"\n              display-timezone=\"utc\"\n              id=\"date-selection\"\n              [(ngModel)]=\"selectedDate\"\n            >\n            </ion-datetime>\n          </ion-row>\n          <ion-row>\n            <ion-col style=\"padding-left: 0px\">\n              <ion-datetime\n                displayFormat=\"h:mm a\"\n                placeholder=\"Select Start Time\"\n                id=\"date-selection\"\n                [(ngModel)]=\"selectedStartTime\"\n              ></ion-datetime>\n            </ion-col>\n            <ion-col style=\"padding-right: 0px\">\n              <ion-datetime\n                displayFormat=\"h:mm a\"\n                placeholder=\"Select End Time\"\n                id=\"date-selection\"\n                [(ngModel)]=\"selectedEndTime\"\n              ></ion-datetime>\n            </ion-col>\n          </ion-row>\n          <ion-list>\n            <ion-radio-group mode=\"md\">\n              <ion-item>\n                <ion-label><p>All</p></ion-label>\n                <ion-radio\n                  slot=\"start\"\n                  value=\"All\"\n                  (ionFocus)=\"selectTracker('All')\"\n                ></ion-radio>\n              </ion-item>\n              <ion-item *ngFor=\"let tracker of userTrackers\">\n                <ion-label><p>{{ tracker.tracker_name }}</p></ion-label>\n                <ion-radio\n                  slot=\"start\"\n                  [value]=\"tracker\"\n                  (ionFocus)=\"selectTracker(tracker)\"\n                ></ion-radio>\n              </ion-item>\n            </ion-radio-group>\n          </ion-list>\n          <br />\n          <ion-row>\n            <ion-button\n              (click)=\"expandItem(item)\"\n              color=\"dark\"\n              expand=\"block\"\n              style=\"width: 100%\"\n              (click)=\"applyFilter()\"\n              [(disabled)]=\"!selectedDate || !selectedStartTime || !selectedEndTime || !selectedTracker\"\n              >Go back in time</ion-button\n            >\n          </ion-row>\n        </app-expandable>\n      </ion-card-content>\n    </ion-card>\n    <ion-card>\n      <div #tmmap id=\"tmmap\" [style.display]=\"displayMap ? 'block' : 'none'\">\n        No location found for this tracker\n      </div>\n    </ion-card>\n    <div>\n      <div></div>\n      <ion-row [style.display]=\"displayMap ? 'block' : 'none'\">\n        <ion-card>\n          <ion-card-content class=\"tracker-section\">\n            <ion-row>\n              <owl-carousel\n                #owlElement\n                [options]=\"slideOptions\"\n                [carouselClasses]=\"['owl-theme', 'sliding']\"\n                *ngIf=\"filteredTrackerResult.length !== 0; else noTrackers\"\n              >\n                <div\n                  class=\"item custom-owl-item\"\n                  *ngFor=\"let filteredTracker of filteredTrackerResult; let index = index\"\n                  (click)=\"changeSelectedTracker(index)\"\n                >\n                  <div style=\"align-content: center\">\n                    <div>\n                      <img\n                        class=\"tracker-icon-view\"\n                        [src]=\"filteredTracker.tracker_icon_url\"\n                        style=\"width: 20%; margin: 0 auto\"\n                      />\n                    </div>\n                    {{ filteredTracker.tracker_name }}\n                  </div>\n                </div>\n              </owl-carousel>\n            </ion-row>\n            <hr />\n            <ion-row\n              *ngIf=\"selectedTrackerData && selectedTrackerData.locations; else noTrackerActivity\"\n            >\n              <ion-list class=\"tracker-locations-list\">\n                <ion-item\n                  class=\"tracker-locations-list-items\"\n                  *ngFor=\"let locations of selectedTrackerData.locations; let index = index\"\n                >\n                  <ion-avatar slot=\"start\" *ngIf=\"locations.length !== 0\">\n                    <img\n                      *ngIf=\"locations.length !== 0\"\n                      [src]=\"selectedTrackerData.tracker_icon_url\"\n                    />\n                  </ion-avatar>\n                  <ion-label\n                    *ngIf=\"locations.length !== 0\"\n                    style=\"white-space: normal\"\n                  >\n                    <div>\n                      <strong>{{ selectedTrackerData.description }}</strong>\n                    </div>\n                    <div>\n                      <span>{{ locations.timestamp | dateTras | date : 'yyyy-MM-dd HH:mm:ss'}}</span>\n                    </div>\n                    <div>\n                      <span> {{locations.formatted_address}} </span>\n                    </div>\n                    <p>{{ locations.latitude }}, {{ locations.longitude }}</p>\n                    <span\n                      (click)=\"viewInLiveTracking(selectedTrackerData, locations)\"\n                    >\n                      <ion-button color=\"dark\">\n                        <fa-icon\n                          class=\"tracker-sample-view\"\n                          icon=\"map-location-dot\"\n                          style=\"margin-right: 10px\"\n                        ></fa-icon>\n                        Details\n                      </ion-button>\n                    </span>\n                    <span\n                      (click)=\"viewInCurrentMap(selectedTrackerData,locations, index)\"\n                    >\n                      <ion-button color=\"dark\">\n                        <fa-icon\n                          class=\"tracker-sample-view\"\n                          icon=\"wave-square\"\n                          style=\"margin-right: 10px\"\n                        ></fa-icon>\n                        Route\n                      </ion-button>\n                    </span>\n                  </ion-label>\n                </ion-item>\n                <p\n                  *ngIf=\"selectedTrackerData.locations.length === 0\"\n                  style=\"text-align: center; font-size: 18px\"\n                ></p>\n              </ion-list>\n            </ion-row>\n            <ng-template #noTrackers>\n              <p>No tracker found for this filters.</p>\n            </ng-template>\n            <ng-template #noTrackerActivity>\n              <p>No activity found for this tracker.</p>\n            </ng-template>\n          </ion-card-content>\n        </ion-card>\n      </ion-row>\n    </div>\n  </ng-template>\n  \n\n</ion-content>\n");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_time-machine_time-machine_module_ts.js.map