(self["webpackChunkspytrack"] = self["webpackChunkspytrack"] || []).push([["src_app_pages_preferences_preferences_module_ts"],{

/***/ 8092:
/*!*********************************************************************************************!*\
  !*** ./src/app/pages/preferences/modals/alert-notification/alert-notification.component.ts ***!
  \*********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AlertNotificationComponent": () => (/* binding */ AlertNotificationComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_alert_notification_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./alert-notification.component.html */ 6281);
/* harmony import */ var _alert_notification_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./alert-notification.component.scss */ 8884);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ 5257);
/* harmony import */ var src_app_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/rest.service */ 1881);







let AlertNotificationComponent = class AlertNotificationComponent {
    constructor(modalController, restService, toastCtrl, loader) {
        this.modalController = modalController;
        this.restService = restService;
        this.toastCtrl = toastCtrl;
        this.loader = loader;
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.getAlertData();
    }
    closeModal() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            const onClosedData = 'Wrapped Up!';
            yield this.modalController.dismiss(onClosedData);
        });
    }
    getAlertData() {
        this.showLoader();
        this.restService
            .get('thorton/user/alerts', '', '')
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_4__.take)(1))
            .subscribe((resp) => {
            this.hideLoader();
            if (resp) {
                this.isChecked = resp['disturb'];
                this.respDist = resp['disturb'];
            }
            else {
            }
        });
    }
    showLoader() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            this.globalLoader = yield this.loader.create({
                cssClass: 'my-custom-class',
                message: 'Please wait...',
                duration: 4000,
            });
            this.globalLoader.present();
        });
    }
    hideLoader() {
        this.globalLoader.dismiss();
    }
    saveChange() {
        this.showLoader();
        const objdata = {
            disturb: this.isChecked ? 1 : 0,
        };
        this.restService
            .put(JSON.stringify(objdata), 'thorton/user/alerts/update')
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_4__.take)(1))
            .subscribe((resp) => (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            this.hideLoader();
            if (resp.isSuccess) {
                const toast = yield this.toastCtrl.create({
                    message: 'notification data set.',
                    position: 'bottom',
                    duration: 1500,
                    color: 'success',
                });
                yield toast.present();
                this.closeModal();
            }
            else {
                const toast = yield this.toastCtrl.create({
                    message: resp.error.message || resp.message,
                    position: 'bottom',
                    duration: 1500,
                    color: 'danger',
                });
                yield toast.present();
            }
        }), (error) => (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            this.hideLoader();
            const toast = yield this.toastCtrl.create({
                message: 'Something went wrong while updating the password.',
                position: 'bottom',
                duration: 1500,
                color: 'error',
            });
            toast.present();
        }));
    }
    resetForm() {
        this.isChecked = this.respDist;
    }
};
AlertNotificationComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.ModalController },
    { type: src_app_rest_service__WEBPACK_IMPORTED_MODULE_2__.RestService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.ToastController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.LoadingController }
];
AlertNotificationComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-alert-notification',
        template: _raw_loader_alert_notification_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_alert_notification_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], AlertNotificationComponent);



/***/ }),

/***/ 860:
/*!***************************************************************************************!*\
  !*** ./src/app/pages/preferences/modals/change-password/change-password.component.ts ***!
  \***************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ChangePasswordComponent": () => (/* binding */ ChangePasswordComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_change_password_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./change-password.component.html */ 4119);
/* harmony import */ var _change_password_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./change-password.component.scss */ 7384);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var src_app_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/rest.service */ 1881);







let ChangePasswordComponent = class ChangePasswordComponent {
    constructor(modalController, formBuilder, restService, toastCtrl, loader) {
        this.modalController = modalController;
        this.formBuilder = formBuilder;
        this.restService = restService;
        this.toastCtrl = toastCtrl;
        this.loader = loader;
        this.passwordVal = {
            minLen: false,
            number: false,
            symbol: false,
            upperCase: false,
            lowerCase: false
        };
        this.cPasswordVal = false;
        this.formInit();
    }
    ngOnInit() { }
    ionViewWillEnter() {
    }
    formInit() {
        this.changePasswordForm = this.formBuilder.group({
            opassword: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__.FormControl('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&]).{8,}')]),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__.FormControl('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&]).{8,}')]),
            cpassword: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__.FormControl('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&]).{8,}')]),
        });
    }
    passwordInputValidation(event) {
        const e = event.target.value;
        const number = /\d/;
        const symbol = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
        const upperCase = /[A-Z]/;
        const lowerCase = /[a-z]/;
        this.changePasswordForm.controls.password.setValue(e);
        if (this.changePasswordForm.controls.password.value == '') {
            this.passwordVal = {
                minLen: false,
                number: false,
                symbol: false,
                upperCase: false,
                lowerCase: false
            };
        }
        else {
            if (this.changePasswordForm.controls.password.value.length >= 8) {
                this.passwordVal.minLen = true;
            }
            else {
                this.passwordVal.minLen = false;
            }
            if (number.test(this.changePasswordForm.controls.password.value)) {
                this.passwordVal.number = true;
            }
            else {
                this.passwordVal.number = false;
            }
            if (symbol.test(this.changePasswordForm.controls.password.value)) {
                this.passwordVal.symbol = true;
            }
            else {
                this.passwordVal.symbol = false;
            }
            if (upperCase.test(this.changePasswordForm.controls.password.value)) {
                this.passwordVal.upperCase = true;
            }
            else {
                this.passwordVal.upperCase = false;
            }
            if (lowerCase.test(this.changePasswordForm.controls.password.value)) {
                this.passwordVal.lowerCase = true;
            }
            else {
                this.passwordVal.lowerCase = false;
            }
        }
    }
    cpasswordInputValidation(event) {
        const e = event.target.value;
        this.changePasswordForm.controls.cpassword.setValue(e);
        if (this.changePasswordForm.controls.password.value === this.changePasswordForm.controls.cpassword.value) {
            this.cPasswordVal = true;
        }
        else {
            this.cPasswordVal = false;
        }
    }
    changePasswordInitiate() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            console.log(this.changePasswordForm);
            const sendData = {
                old_password: this.changePasswordForm.controls.opassword.value,
                new_password: this.changePasswordForm.controls.password.value
            };
            this.globalLoader = yield this.loader.create({
                cssClass: 'my-custom-class',
                message: 'Please wait...',
                duration: 4000
            });
            this.globalLoader.present();
            this.restService.post(JSON.stringify(sendData), 'thorton/user/change_password').subscribe((resp) => (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
                console.log(resp);
                if (resp.isSuccess) {
                    this.globalLoader.dismiss();
                    const toast = yield this.toastCtrl.create({
                        message: 'Password has been changed successfuly updated.',
                        position: 'bottom',
                        duration: 1500,
                        color: 'success'
                    });
                    yield toast.present();
                    this.closeModal();
                }
                else {
                    this.globalLoader.dismiss();
                    const toast = yield this.toastCtrl.create({
                        message: resp.error.message || resp.message,
                        position: 'bottom',
                        duration: 1500,
                        color: 'danger'
                    });
                    yield toast.present();
                }
            }), (error) => (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
                const toast = yield this.toastCtrl.create({
                    message: 'Something went wrong while updating the password.',
                    position: 'bottom',
                    duration: 1500,
                    color: 'error'
                });
                toast.present();
            }));
        });
    }
    closeModal() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            const onClosedData = 'Wrapped Up!';
            yield this.modalController.dismiss(onClosedData);
        });
    }
    resetForm() {
        this.formInit();
    }
};
ChangePasswordComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.ModalController },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__.FormBuilder },
    { type: src_app_rest_service__WEBPACK_IMPORTED_MODULE_2__.RestService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.ToastController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.LoadingController }
];
ChangePasswordComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-change-password',
        template: _raw_loader_change_password_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_change_password_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], ChangePasswordComponent);



/***/ }),

/***/ 54:
/*!*********************************************************************************!*\
  !*** ./src/app/pages/preferences/modals/edit-account/edit-account.component.ts ***!
  \*********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "EditAccountComponent": () => (/* binding */ EditAccountComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_edit_account_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./edit-account.component.html */ 4573);
/* harmony import */ var _edit_account_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./edit-account.component.scss */ 6844);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ 5257);
/* harmony import */ var src_app_globals_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/globals.service */ 3221);
/* harmony import */ var src_app_rest_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/rest.service */ 1881);










let EditAccountComponent = class EditAccountComponent {
    constructor(modalController, router, global, _formBuilder, httpRequest, toastCtrl, loader) {
        this.modalController = modalController;
        this.router = router;
        this.global = global;
        this._formBuilder = _formBuilder;
        this.httpRequest = httpRequest;
        this.toastCtrl = toastCtrl;
        this.loader = loader;
        this.aa = 'asdsad';
    }
    ngOnInit() {
        this.global.currentMessageSubscriberSummary.subscribe((data) => {
            if (data.isRefresh) {
                this.userDetails = this.global.userDetails;
            }
            else {
            }
        });
        this.formInit(this.userDetails);
    }
    formInit(userDetails) {
        this.accountInformationForm = this._formBuilder.group({
            firstname: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormControl(userDetails.firstname, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required),
            lastname: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormControl(userDetails.lastname, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormControl(userDetails.email, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required),
            phone: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormControl(userDetails.phone, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required),
            address: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormControl(userDetails.address, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required),
            city: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormControl(userDetails.city, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required),
            state: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormControl(userDetails.state, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required),
            zipcode: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormControl(userDetails.zipcode, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required),
            country: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormControl(userDetails.country, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required),
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormControl(userDetails.username, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required),
            companyName: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormControl(userDetails.companyName),
        });
    }
    closeModal() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            const onClosedData = 'Wrapped Up!';
            yield this.modalController.dismiss(onClosedData);
        });
    }
    updateAccountInformation() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            this.globalLoader = yield this.loader.create({
                cssClass: 'my-custom-class',
                message: 'Please wait...',
                duration: 4000,
            });
            this.globalLoader.present();
            this.httpRequest
                .put(JSON.stringify(this.accountInformationForm.value), 'thorton/user/update')
                .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_6__.take)(1))
                .subscribe((resp) => {
                if (resp.isUpdated) {
                    const oldUserData = Object.assign({}, this.global.userDetails);
                    const newUserData = Object.assign(Object.assign(Object.assign({}, oldUserData), resp.Details), resp.User);
                    this.global.userDetails = newUserData;
                    this.global.notifySummary({ isRefresh: true });
                    this.closeModal();
                    this.globalLoader.dismiss();
                    this.openToastSuccess();
                }
                else {
                    this.openToastError();
                }
            }, (error) => {
                this.globalLoader.dismiss();
                console.log(error);
                this.openToastError();
            });
        });
    }
    openToastSuccess() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: 'Account information has been successfuly updated.',
                position: 'bottom',
                duration: 1500,
                color: 'success',
            });
            toast.present();
        });
    }
    openToastError() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: 'Error occured. Please try again later.',
                position: 'bottom',
                duration: 4000,
                color: 'danger',
            });
            toast.present();
        });
    }
    resetForm() {
        this.formInit(this.userDetails);
    }
};
EditAccountComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.ModalController },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__.Router },
    { type: src_app_globals_service__WEBPACK_IMPORTED_MODULE_2__.GlobalService },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormBuilder },
    { type: src_app_rest_service__WEBPACK_IMPORTED_MODULE_3__.RestService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.ToastController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.LoadingController }
];
EditAccountComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_9__.Component)({
        selector: 'app-edit-account',
        template: _raw_loader_edit_account_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_edit_account_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], EditAccountComponent);



/***/ }),

/***/ 3608:
/*!*************************************************************************!*\
  !*** ./src/app/pages/preferences/modals/settings/settings.component.ts ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SettingsComponent": () => (/* binding */ SettingsComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_settings_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./settings.component.html */ 1781);
/* harmony import */ var _settings_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./settings.component.scss */ 7581);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ 5257);
/* harmony import */ var src_app_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/rest.service */ 1881);
/* harmony import */ var _time_zones__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./time-zones */ 250);









let SettingsComponent = class SettingsComponent {
    constructor(modalController, rest, toastCtrl, pickerController, loader) {
        this.modalController = modalController;
        this.rest = rest;
        this.toastCtrl = toastCtrl;
        this.pickerController = pickerController;
        this.loader = loader;
        this.animals = ['Tiger', 'Lion', 'Elephant', 'Fox', 'Wolf'];
        this.selectedTimeZone = 'US/Eastern';
        this.allTimeZones = _time_zones__WEBPACK_IMPORTED_MODULE_3__.timeZones;
    }
    ngOnInit() { }
    ionViewDidEnter() {
        this.getUserSettings();
        this.defaultMapStyle = this.userSettings.map_style;
        this.selectedTimeZone = this.userSettings.timezone;
        this.defaultDistanceUnit = this.userSettings.distance_unit;
        this.defaultSpeedUnit = this.userSettings.speed_unit;
    }
    closeModal() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            const onClosedData = 'Wrapped Up!';
            yield this.modalController.dismiss(onClosedData);
        });
    }
    showPicker() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            let options = {
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                    },
                    {
                        text: 'Ok',
                        handler: (value) => {
                            console.log(value);
                            this.selectedTimeZone = value.Timezones.text;
                        },
                    },
                ],
                columns: [
                    {
                        name: 'Timezones',
                        options: this.getColumnOptions(),
                    },
                ],
            };
            let picker = yield this.pickerController.create(options);
            picker.present();
        });
    }
    getColumnOptions() {
        let options = [];
        this.allTimeZones.forEach((x) => {
            options.push({
                text: x.offset + ' ' + x.name,
                value: x.offset,
            });
        });
        return options;
    }
    getUserSettings() {
        this.userSettings = JSON.parse(localStorage.getItem('userSettings'));
    }
    selectedMapType(data) {
        console.log(data);
        this.defaultMapStyle = data;
    }
    selectedDistanceUnit(data) {
        console.log(data);
        this.defaultDistanceUnit = data;
    }
    selectedSpeedUnit(data) {
        console.log(data);
        this.defaultSpeedUnit = data;
    }
    applyChanges() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.globalLoader = yield this.loader.create({
                cssClass: 'my-custom-class',
                message: 'Please wait...',
            });
            this.globalLoader.present();
            let dataToSend = {
                timezone: this.selectedTimeZone,
                map_style: this.defaultMapStyle,
                speed_unit: this.defaultSpeedUnit,
                distance_unit: this.defaultDistanceUnit,
            };
            console.log(dataToSend);
            this.rest
                .put(dataToSend, 'thorton/user/settings')
                .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.take)(1))
                .subscribe((resp) => {
                this.globalLoader.dismiss();
                console.log(resp);
                localStorage.setItem('userSettings', JSON.stringify(resp));
                this.closeModal();
            }, (error) => {
                this.globalLoader.dismiss();
                this.errorToast();
                this.closeModal();
            });
        });
    }
    resetForm() {
        this.defaultMapStyle = this.userSettings.map_style;
        this.selectedTimeZone = this.userSettings.timezone;
        this.defaultDistanceUnit = this.userSettings.distance_unit;
        this.defaultSpeedUnit = this.userSettings.speed_unit;
        //this.closeModal();
    }
    errorToast() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: 'Error in saving the data, please try again later.',
                position: 'bottom',
                duration: 4000,
                color: 'danger',
            });
            toast.present();
        });
    }
};
SettingsComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.ModalController },
    { type: src_app_rest_service__WEBPACK_IMPORTED_MODULE_2__.RestService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.ToastController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.PickerController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.LoadingController }
];
SettingsComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-settings',
        template: _raw_loader_settings_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_settings_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], SettingsComponent);



/***/ }),

/***/ 250:
/*!*****************************************************************!*\
  !*** ./src/app/pages/preferences/modals/settings/time-zones.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "timeZones": () => (/* binding */ timeZones)
/* harmony export */ });
const sampleData = [
    {
        "offset": "GMT-12:00",
        "name": "Etc/GMT-12"
    },
    {
        "offset": "GMT-11:00",
        "name": "Etc/GMT-11"
    },
    {
        "offset": "GMT-11:00",
        "name": "Pacific/Midway"
    },
    {
        "offset": "GMT-10:00",
        "name": "America/Adak"
    },
    {
        "offset": "GMT-09:00",
        "name": "America/Anchorage"
    },
    {
        "offset": "GMT-09:00",
        "name": "Pacific/Gambier"
    },
    {
        "offset": "GMT-08:00",
        "name": "America/Dawson_Creek"
    },
    {
        "offset": "GMT-08:00",
        "name": "America/Ensenada"
    },
    {
        "offset": "GMT-08:00",
        "name": "America/Los_Angeles"
    },
    {
        "offset": "GMT-07:00",
        "name": "America/Chihuahua"
    },
    {
        "offset": "GMT-07:00",
        "name": "America/Denver"
    },
    {
        "offset": "GMT-06:00",
        "name": "America/Belize"
    },
    {
        "offset": "GMT-06:00",
        "name": "America/Cancun"
    },
    {
        "offset": "GMT-06:00",
        "name": "America/Chicago"
    },
    {
        "offset": "GMT-06:00",
        "name": "Chile/EasterIsland"
    },
    {
        "offset": "GMT-05:00",
        "name": "America/Bogota"
    },
    {
        "offset": "GMT-05:00",
        "name": "America/Havana"
    },
    {
        "offset": "GMT-05:00",
        "name": "America/New_York"
    },
    {
        "offset": "GMT-04:30",
        "name": "America/Caracas"
    },
    {
        "offset": "GMT-04:00",
        "name": "America/Campo_Grande"
    },
    {
        "offset": "GMT-04:00",
        "name": "America/Glace_Bay"
    },
    {
        "offset": "GMT-04:00",
        "name": "America/Goose_Bay"
    },
    {
        "offset": "GMT-04:00",
        "name": "America/Santiago"
    },
    {
        "offset": "GMT-04:00",
        "name": "America/La_Paz"
    },
    {
        "offset": "GMT-03:00",
        "name": "America/Argentina/Buenos_Aires"
    },
    {
        "offset": "GMT-03:00",
        "name": "America/Montevideo"
    },
    {
        "offset": "GMT-03:00",
        "name": "America/Araguaina"
    },
    {
        "offset": "GMT-03:00",
        "name": "America/Godthab"
    },
    {
        "offset": "GMT-03:00",
        "name": "America/Miquelon"
    },
    {
        "offset": "GMT-03:00",
        "name": "America/Sao_Paulo"
    },
    {
        "offset": "GMT-03:30",
        "name": "America/St_Johns"
    },
    {
        "offset": "GMT-02:00",
        "name": "America/Noronha"
    },
    {
        "offset": "GMT-01:00",
        "name": "Atlantic/Cape_Verde"
    },
    {
        "offset": "GMT",
        "name": "Europe/Belfast"
    },
    {
        "offset": "GMT",
        "name": "Africa/Abidjan"
    },
    {
        "offset": "GMT",
        "name": "Europe/Dublin"
    },
    {
        "offset": "GMT",
        "name": "Europe/Lisbon"
    },
    {
        "offset": "GMT",
        "name": "Europe/London"
    },
    {
        "offset": "UTC",
        "name": "UTC"
    },
    {
        "offset": "GMT+01:00",
        "name": "Africa/Algiers"
    },
    {
        "offset": "GMT+01:00",
        "name": "Africa/Windhoek"
    },
    {
        "offset": "GMT+01:00",
        "name": "Atlantic/Azores"
    },
    {
        "offset": "GMT+01:00",
        "name": "Atlantic/Stanley"
    },
    {
        "offset": "GMT+01:00",
        "name": "Europe/Amsterdam"
    },
    {
        "offset": "GMT+01:00",
        "name": "Europe/Belgrade"
    },
    {
        "offset": "GMT+01:00",
        "name": "Europe/Brussels"
    },
    {
        "offset": "GMT+02:00",
        "name": "Africa/Cairo"
    },
    {
        "offset": "GMT+02:00",
        "name": "Africa/Blantyre"
    },
    {
        "offset": "GMT+02:00",
        "name": "Asia/Beirut"
    },
    {
        "offset": "GMT+02:00",
        "name": "Asia/Damascus"
    },
    {
        "offset": "GMT+02:00",
        "name": "Asia/Gaza"
    },
    {
        "offset": "GMT+02:00",
        "name": "Asia/Jerusalem"
    },
    {
        "offset": "GMT+03:00",
        "name": "Africa/Addis_Ababa"
    },
    {
        "offset": "GMT+03:00",
        "name": "Asia/Riyadh89"
    },
    {
        "offset": "GMT+03:00",
        "name": "Europe/Minsk"
    },
    {
        "offset": "GMT+03:30",
        "name": "Asia/Tehran"
    },
    {
        "offset": "GMT+04:00",
        "name": "Asia/Dubai"
    },
    {
        "offset": "GMT+04:00",
        "name": "Asia/Yerevan"
    },
    {
        "offset": "GMT+04:00",
        "name": "Europe/Moscow"
    },
    {
        "offset": "GMT+04:30",
        "name": "Asia/Kabul"
    },
    {
        "offset": "GMT+05:00",
        "name": "Asia/Tashkent"
    },
    {
        "offset": "GMT+05:30",
        "name": "Asia/Kolkata"
    },
    {
        "offset": "GMT+05:45",
        "name": "Asia/Katmandu"
    },
    {
        "offset": "GMT+06:00",
        "name": "Asia/Dhaka"
    },
    {
        "offset": "GMT+06:00",
        "name": "Asia/Yekaterinburg"
    },
    {
        "offset": "GMT+06:30",
        "name": "Asia/Rangoon"
    },
    {
        "offset": "GMT+07:00",
        "name": "Asia/Bangkok"
    },
    {
        "offset": "GMT+07:00",
        "name": "Asia/Novosibirsk"
    },
    {
        "offset": "GMT+08:00",
        "name": "Etc/GMT+8"
    },
    {
        "offset": "GMT+08:00",
        "name": "Asia/Hong_Kong"
    },
    {
        "offset": "GMT+08:00",
        "name": "Asia/Krasnoyarsk"
    },
    {
        "offset": "GMT+08:00",
        "name": "Australia/Perth"
    },
    {
        "offset": "GMT+08:45",
        "name": "Australia/Eucla"
    },
    {
        "offset": "GMT+09:00",
        "name": "Asia/Irkutsk"
    },
    {
        "offset": "GMT+09:00",
        "name": "Asia/Seoul"
    },
    {
        "offset": "GMT+09:00",
        "name": "Asia/Tokyo"
    },
    {
        "offset": "GMT+09:30",
        "name": "Australia/Adelaide"
    },
    {
        "offset": "GMT+09:30",
        "name": "Australia/Darwin"
    },
    {
        "offset": "GMT+09:30",
        "name": "Pacific/Marquesas"
    },
    {
        "offset": "GMT+10:00",
        "name": "Etc/GMT+10"
    },
    {
        "offset": "GMT+10:00",
        "name": "Australia/Brisbane"
    },
    {
        "offset": "GMT+10:00",
        "name": "Australia/Hobart"
    },
    {
        "offset": "GMT+10:00",
        "name": "Asia/Yakutsk"
    },
    {
        "offset": "GMT+10:30",
        "name": "Australia/Lord_Howe"
    },
    {
        "offset": "GMT+11:00",
        "name": "Asia/Vladivostok"
    },
    {
        "offset": "GMT+11:30",
        "name": "Pacific/Norfolk"
    },
    {
        "offset": "GMT+12:00",
        "name": "Etc/GMT+12"
    },
    {
        "offset": "GMT+12:00",
        "name": "Asia/Anadyr"
    },
    {
        "offset": "GMT+12:00",
        "name": "Asia/Magadan"
    },
    {
        "offset": "GMT+12:00",
        "name": "Pacific/Auckland"
    },
    {
        "offset": "GMT+12:45",
        "name": "Pacific/Chatham"
    },
    {
        "offset": "GMT+13:00",
        "name": "Pacific/Tongatapu"
    },
    {
        "offset": "GMT+14:00",
        "name": "Pacific/Kiritimati"
    }
];
const timeZones = sampleData.map(dataItem => ({
    offset: dataItem.offset,
    name: dataItem.name
}));


/***/ }),

/***/ 4695:
/*!*************************************************************************!*\
  !*** ./src/app/pages/preferences/modals/trackers/trackers.component.ts ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TrackersComponent": () => (/* binding */ TrackersComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_trackers_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./trackers.component.html */ 6066);
/* harmony import */ var _trackers_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./trackers.component.scss */ 7286);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var src_app_rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/rest.service */ 1881);







// import { InAppBrowser } from '@ionic-native/in-app-browser';
let TrackersComponent = class TrackersComponent {
    constructor(modalController, _formBuilder, rest, toastCtrl, loadingController
    // private inAppBrowser: InAppBrowser
    ) {
        this.modalController = modalController;
        this._formBuilder = _formBuilder;
        this.rest = rest;
        this.toastCtrl = toastCtrl;
        this.loadingController = loadingController;
        this.activateTracker = false;
        this.userTrackers = [];
        this.selectedTracker = '';
        this.dataLoad = false;
        this.iconClass = [
            { icon: "location-arrow" },
            { icon: "map-marker" },
            { icon: "map-marker-alt" },
            { icon: "car" },
            { icon: "motorcycle" },
            { icon: "shuttle-van" },
            { icon: "truck" },
            { icon: "truck-pickup" },
            { icon: "caravan" },
            { icon: "user" },
            { icon: "male" },
            { icon: "female" },
            { icon: "child" },
            { icon: "baby" },
            { icon: "baby-carriage" },
        ];
        this.colorClass = [
            { color: "#229954" },
            { color: "#CD6155" },
            { color: "#A569BD" },
            { color: "#85C1E9" },
            { color: "#45B39D " },
            { color: "#F4D03F " },
            { color: "#DC7633 " },
            { color: "#CCCCFF " },
            { color: "#6495ED " },
            { color: "#40E0D0 " },
            { color: "#9FE2BF " },
            { color: "#DE3163 " },
            { color: "#DFFF00 " },
            { color: "#FFBF00 " },
            { color: "#FF7F50 " },
        ];
    }
    ngOnInit() {
    }
    ionViewDidEnter() {
        this.getUserTracker();
        this.presentLoading();
    }
    getUserTracker() {
        this.rest.get('thorton/trackers/user', '', '').subscribe((resp) => {
            console.log('all user tracker', resp);
            if (resp.length != 0) {
                this.trackerResp = resp.slice(0);
                for (let x = 0; x < resp.length; x++) {
                    if (resp[x].tracker_icon) {
                        resp[x].tracker_icon = resp[x].tracker_icon.replace('fa-', '');
                        this.userTrackers.push(resp[x]);
                    }
                    else {
                        this.userTrackers.push(resp[x]);
                    }
                }
                console.log(this.userTrackers);
                this.dataLoad = true;
            }
            else {
                this.dataLoad = true;
            }
        });
    }
    presentLoading() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            const loading = yield this.loadingController.create({
                cssClass: 'my-custom-class',
                message: 'Please wait...',
                duration: 5000
            });
            yield loading.present();
            const { role, data } = yield loading.onDidDismiss();
            console.log('Loading dismissed!');
        });
    }
    formInitTracker(tracker) {
        console.log(tracker);
        this.trackerForm = this._formBuilder.group({
            trackerName: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormControl(tracker.tracker_name, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required),
            trackerDescription: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormControl(tracker.description, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required),
            imei: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormControl(tracker.imei, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required)
        });
    }
    closeModal() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            const onClosedData = "Wrapped Up!";
            yield this.modalController.dismiss(onClosedData);
        });
    }
    selectTracker(event, tracker) {
        this.formInitTracker(tracker);
        this.selectedTracker = tracker;
        this.selectedTrackerColor = tracker.tracker_icon_color;
        this.selectedTrackerIcon = tracker.tracker_icon.replace('fa-', '');
    }
    colorChange(event, color) {
        this.selectedTrackerColor = color;
    }
    iconChange(event, icon) {
        console.log(icon);
        this.selectedTrackerIcon = icon;
    }
    updateTrackerSetting() {
        let dataToSend = {
            tracker_name: this.trackerForm.get('trackerName').value,
            description: this.trackerForm.get('trackerDescription').value,
            imei: this.trackerForm.get('imei').value,
            tracker_icon: 'fa-' + this.selectedTrackerIcon,
            tracker_icon_color: this.selectedTrackerColor
        };
        console.log(dataToSend);
        this.rest.put(dataToSend, 'thorton/tracker/preferences/update').subscribe((resp) => {
            this.closeModal();
        }, error => {
            this.errorToast();
        });
    }
    resetForm() {
        this.formInitTracker(this.selectedTracker);
    }
    errorToast() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: 'Error on updating the tracker details.',
                position: 'bottom',
                duration: 4000,
                color: 'danger'
            });
            toast.present();
        });
    }
};
TrackersComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.ModalController },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormBuilder },
    { type: src_app_rest_service__WEBPACK_IMPORTED_MODULE_2__.RestService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.ToastController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.LoadingController }
];
TrackersComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-trackers',
        template: _raw_loader_trackers_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_trackers_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], TrackersComponent);



/***/ }),

/***/ 8501:
/*!*******************************************************************!*\
  !*** ./src/app/pages/preferences/modals/users/users.component.ts ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UsersComponent": () => (/* binding */ UsersComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_users_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./users.component.html */ 6754);
/* harmony import */ var _users_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./users.component.scss */ 1379);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ 476);





let UsersComponent = class UsersComponent {
    constructor(modalController) {
        this.modalController = modalController;
    }
    ngOnInit() { }
    closeModal() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__awaiter)(this, void 0, void 0, function* () {
            const onClosedData = "Wrapped Up!";
            yield this.modalController.dismiss(onClosedData);
        });
    }
};
UsersComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__.ModalController }
];
UsersComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-users',
        template: _raw_loader_users_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_users_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], UsersComponent);



/***/ }),

/***/ 4527:
/*!*****************************************************************!*\
  !*** ./src/app/pages/preferences/preferences-routing.module.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PreferencesPageRoutingModule": () => (/* binding */ PreferencesPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _preferences_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./preferences.page */ 7375);




const routes = [
    {
        path: '',
        component: _preferences_page__WEBPACK_IMPORTED_MODULE_0__.PreferencesPage
    },
];
let PreferencesPageRoutingModule = class PreferencesPageRoutingModule {
};
PreferencesPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], PreferencesPageRoutingModule);



/***/ }),

/***/ 855:
/*!*********************************************************!*\
  !*** ./src/app/pages/preferences/preferences.module.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PreferencesPageModule": () => (/* binding */ PreferencesPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _preferences_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./preferences-routing.module */ 4527);
/* harmony import */ var _preferences_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./preferences.page */ 7375);
/* harmony import */ var _modals_edit_account_edit_account_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./modals/edit-account/edit-account.component */ 54);
/* harmony import */ var _modals_trackers_trackers_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./modals/trackers/trackers.component */ 4695);
/* harmony import */ var _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @fortawesome/angular-fontawesome */ 5002);
/* harmony import */ var _modals_alert_notification_alert_notification_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./modals/alert-notification/alert-notification.component */ 8092);
/* harmony import */ var _modals_settings_settings_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./modals/settings/settings.component */ 3608);
/* harmony import */ var _modals_users_users_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./modals/users/users.component */ 8501);
/* harmony import */ var _modals_change_password_change_password_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./modals/change-password/change-password.component */ 860);














let PreferencesPageModule = class PreferencesPageModule {
};
PreferencesPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_9__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_10__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_11__.FormsModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_11__.ReactiveFormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_12__.IonicModule,
            _preferences_routing_module__WEBPACK_IMPORTED_MODULE_0__.PreferencesPageRoutingModule,
            _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_13__.FontAwesomeModule,
        ],
        declarations: [
            _preferences_page__WEBPACK_IMPORTED_MODULE_1__.PreferencesPage,
            _modals_edit_account_edit_account_component__WEBPACK_IMPORTED_MODULE_2__.EditAccountComponent,
            _modals_trackers_trackers_component__WEBPACK_IMPORTED_MODULE_3__.TrackersComponent,
            _modals_alert_notification_alert_notification_component__WEBPACK_IMPORTED_MODULE_4__.AlertNotificationComponent,
            _modals_settings_settings_component__WEBPACK_IMPORTED_MODULE_5__.SettingsComponent,
            _modals_users_users_component__WEBPACK_IMPORTED_MODULE_6__.UsersComponent,
            _modals_change_password_change_password_component__WEBPACK_IMPORTED_MODULE_7__.ChangePasswordComponent
        ]
    })
], PreferencesPageModule);



/***/ }),

/***/ 7375:
/*!*******************************************************!*\
  !*** ./src/app/pages/preferences/preferences.page.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PreferencesPage": () => (/* binding */ PreferencesPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_preferences_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./preferences.page.html */ 7452);
/* harmony import */ var _preferences_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./preferences.page.scss */ 4493);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _modals_edit_account_edit_account_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./modals/edit-account/edit-account.component */ 54);
/* harmony import */ var _modals_alert_notification_alert_notification_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./modals/alert-notification/alert-notification.component */ 8092);
/* harmony import */ var _modals_settings_settings_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./modals/settings/settings.component */ 3608);
/* harmony import */ var _modals_users_users_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./modals/users/users.component */ 8501);
/* harmony import */ var _modals_trackers_trackers_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./modals/trackers/trackers.component */ 4695);
/* harmony import */ var src_app_globals_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/globals.service */ 3221);
/* harmony import */ var src_app_rest_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/rest.service */ 1881);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! rxjs/operators */ 5257);
/* harmony import */ var _modals_change_password_change_password_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./modals/change-password/change-password.component */ 860);














let PreferencesPage = class PreferencesPage {
    constructor(modalController, global, rest, loadingController) {
        this.modalController = modalController;
        this.global = global;
        this.rest = rest;
        this.loadingController = loadingController;
    }
    ngOnInit() {
        this.currentMessageSubscription = this.global.currentMessageSubscriberSummary.subscribe((data) => {
            if (data.isRefresh) {
                this.user = this.global.userDetails;
            }
            else {
                console.log(this.global.userDetails);
                if (this.global.userDetails) {
                    this.getUser();
                }
                else {
                    this.user = {
                        address: "",
                        city: "",
                        companyName: "",
                        country: "",
                        created_at: "",
                        email: "",
                        email_verified_at: "",
                        firstname: "",
                        id: "",
                        lastname: "",
                        middlename: "",
                        phone: "",
                        state: "",
                        updated_at: "",
                        username: "",
                        zipcode: "",
                    };
                    this.getUser();
                }
            }
        });
    }
    openEditAccountModal() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_10__.__awaiter)(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _modals_edit_account_edit_account_component__WEBPACK_IMPORTED_MODULE_2__.EditAccountComponent,
                cssClass: 'filter-modal'
            });
            // modal.onDidDismiss().then((data: any) => {
            //   this.user = this.global.userDetails;
            // });
            return yield modal.present();
        });
    }
    openEditAlertNotificationModal() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_10__.__awaiter)(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _modals_alert_notification_alert_notification_component__WEBPACK_IMPORTED_MODULE_3__.AlertNotificationComponent,
                cssClass: 'filter-modal'
            });
            return yield modal.present();
        });
    }
    openEditSettingModal() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_10__.__awaiter)(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _modals_settings_settings_component__WEBPACK_IMPORTED_MODULE_4__.SettingsComponent,
                cssClass: 'filter-modal'
            });
            return yield modal.present();
        });
    }
    openEditUsersModal() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_10__.__awaiter)(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _modals_users_users_component__WEBPACK_IMPORTED_MODULE_5__.UsersComponent,
                cssClass: 'filter-modal'
            });
            return yield modal.present();
        });
    }
    openEditTrackersModal() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_10__.__awaiter)(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _modals_trackers_trackers_component__WEBPACK_IMPORTED_MODULE_6__.TrackersComponent,
                cssClass: 'filter-modal'
            });
            modal.onDidDismiss().then((data) => {
            });
            return yield modal.present();
        });
    }
    openEditChangePasswordModal() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_10__.__awaiter)(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _modals_change_password_change_password_component__WEBPACK_IMPORTED_MODULE_9__.ChangePasswordComponent,
                cssClass: 'filter-modal'
            });
            modal.onDidDismiss().then((data) => {
            });
            return yield modal.present();
        });
    }
    getUser() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_10__.__awaiter)(this, void 0, void 0, function* () {
            this.loadingGbl = yield this.loadingController.create({
                cssClass: 'my-custom-class',
                message: 'Please wait...'
            });
            yield this.loadingGbl.present();
            this.rest.get('user', '', '').pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_11__.take)(1)).subscribe((resp) => (0,tslib__WEBPACK_IMPORTED_MODULE_10__.__awaiter)(this, void 0, void 0, function* () {
                yield this.loadingGbl.dismiss();
                this.global.firstname = resp.firstname;
                this.global.userDetails = resp;
                console.log(this.global.userDetails);
                this.user = resp;
                this.global.notifySummary({ isRefresh: true });
            }));
        });
    }
    ngOnDestroy() {
        var _a;
        (_a = this.currentMessageSubscription) === null || _a === void 0 ? void 0 : _a.unsubscribe();
    }
};
PreferencesPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_12__.ModalController },
    { type: src_app_globals_service__WEBPACK_IMPORTED_MODULE_7__.GlobalService },
    { type: src_app_rest_service__WEBPACK_IMPORTED_MODULE_8__.RestService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_12__.LoadingController }
];
PreferencesPage = (0,tslib__WEBPACK_IMPORTED_MODULE_10__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_13__.Component)({
        selector: 'app-preferences',
        template: _raw_loader_preferences_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_preferences_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], PreferencesPage);



/***/ }),

/***/ 8884:
/*!***********************************************************************************************!*\
  !*** ./src/app/pages/preferences/modals/alert-notification/alert-notification.component.scss ***!
  \***********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("ion-card {\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFsZXJ0LW5vdGlmaWNhdGlvbi5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7QUFDSiIsImZpbGUiOiJhbGVydC1ub3RpZmljYXRpb24uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY2FyZCB7XG4gICAgd2lkdGg6IDEwMCU7XG59XG4iXX0= */");

/***/ }),

/***/ 7384:
/*!*****************************************************************************************!*\
  !*** ./src/app/pages/preferences/modals/change-password/change-password.component.scss ***!
  \*****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjaGFuZ2UtcGFzc3dvcmQuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ 6844:
/*!***********************************************************************************!*\
  !*** ./src/app/pages/preferences/modals/edit-account/edit-account.component.scss ***!
  \***********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("ion-card {\n  width: 100%;\n}\n\n.input-details {\n  width: 100%;\n  margin-bottom: 1em;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImVkaXQtYWNjb3VudC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7QUFDSjs7QUFFQTtFQUNJLFdBQUE7RUFJQSxrQkFBQTtBQUZKIiwiZmlsZSI6ImVkaXQtYWNjb3VudC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jYXJkIHtcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLmlucHV0LWRldGFpbHMge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIC8vIGJvcmRlcjogMnB4IHNvbGlkIGdyZXk7XG4gICAgLy8gYm9yZGVyLXJhZGl1czogMTVweDtcbiAgICAvLyBmb250LXNpemU6IDEycHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMWVtO1xufSJdfQ== */");

/***/ }),

/***/ 7581:
/*!***************************************************************************!*\
  !*** ./src/app/pages/preferences/modals/settings/settings.component.scss ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("img {\n  height: 48px;\n  margin-right: 1em;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNldHRpbmdzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtFQUNBLGlCQUFBO0FBQ0oiLCJmaWxlIjoic2V0dGluZ3MuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpbWcge1xuICAgIGhlaWdodDogNDhweDtcbiAgICBtYXJnaW4tcmlnaHQ6IDFlbTtcbn1cbiJdfQ== */");

/***/ }),

/***/ 7286:
/*!***************************************************************************!*\
  !*** ./src/app/pages/preferences/modals/trackers/trackers.component.scss ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".center {\n  display: flex;\n  justify-content: center;\n  align-content: center;\n  align-items: center;\n  width: 100%;\n}\n\n.tracker-sample-view {\n  color: white;\n  font-size: 50px;\n  display: inline-block;\n  border-radius: 60px;\n  box-shadow: 0 0 2px #888;\n  padding: 0.5em 0.6em;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInRyYWNrZXJzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksYUFBQTtFQUNBLHVCQUFBO0VBQ0EscUJBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7QUFDSjs7QUFDQTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0EscUJBQUE7RUFDQSxtQkFBQTtFQUNBLHdCQUFBO0VBQ0Esb0JBQUE7QUFFSiIsImZpbGUiOiJ0cmFja2Vycy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jZW50ZXIge1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgYWxpZ24tY29udGVudDogY2VudGVyO1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgd2lkdGg6IDEwMCU7XG59XG4udHJhY2tlci1zYW1wbGUtdmlldyB7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGZvbnQtc2l6ZTogNTBweDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgYm9yZGVyLXJhZGl1czogNjBweDtcbiAgICBib3gtc2hhZG93OiAwIDAgMnB4ICM4ODg7XG4gICAgcGFkZGluZzogMC41ZW0gMC42ZW07IFxufVxuIl19 */");

/***/ }),

/***/ 1379:
/*!*********************************************************************!*\
  !*** ./src/app/pages/preferences/modals/users/users.component.scss ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ1c2Vycy5jb21wb25lbnQuc2NzcyJ9 */");

/***/ }),

/***/ 4493:
/*!*********************************************************!*\
  !*** ./src/app/pages/preferences/preferences.page.scss ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("ion-card {\n  width: 100%;\n}\n\nion-card-title {\n  text-align: center;\n}\n\nion-card-content {\n  text-align: center;\n}\n\n#contact-details {\n  font-size: 12px;\n  font-weight: bold;\n}\n\n.button-icons {\n  margin-right: 1em;\n}\n\n.button-icons-desc {\n  font-size: 12px;\n}\n\nion-label {\n  font-weight: 700;\n}\n\n.username-prefrence {\n  text-transform: capitalize;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByZWZlcmVuY2VzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtBQUNKOztBQUVBO0VBQ0ksZUFBQTtFQUNBLGlCQUFBO0FBQ0o7O0FBRUE7RUFDSSxpQkFBQTtBQUNKOztBQUVBO0VBQ0ksZUFBQTtBQUNKOztBQUVBO0VBQ0ksZ0JBQUE7QUFDSjs7QUFFQTtFQUNJLDBCQUFBO0FBQ0oiLCJmaWxlIjoicHJlZmVyZW5jZXMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNhcmR7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cbmlvbi1jYXJkLXRpdGxlIHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbmlvbi1jYXJkLWNvbnRlbnQge1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuI2NvbnRhY3QtZGV0YWlscyB7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4uYnV0dG9uLWljb25zIHtcbiAgICBtYXJnaW4tcmlnaHQ6IDFlbTtcbn1cblxuLmJ1dHRvbi1pY29ucy1kZXNjIHtcbiAgICBmb250LXNpemU6IDEycHhcbn1cblxuaW9uLWxhYmVsIHtcbiAgICBmb250LXdlaWdodDogNzAwO1xufVxuXG4udXNlcm5hbWUtcHJlZnJlbmNle1xuICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xufSJdfQ== */");

/***/ }),

/***/ 6281:
/*!*************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/preferences/modals/alert-notification/alert-notification.component.html ***!
  \*************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\n  <ion-toolbar color=\"white\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button\n        defaultHref=\"preferences\"\n        (click)=\"closeModal()\"\n      ></ion-back-button>\n    </ion-buttons>\n    <div style=\"text-align: center\">\n      <span id=\"title\" style=\"margin-right: 50px\"\n        ><strong>Alerts and Notification</strong></span\n      >\n    </div>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding\">\n  <ion-grid>\n    <ion-row>\n      <ion-card>\n        <ion-card-header>\n          <!-- <ion-card-subtitle>Sort By</ion-card-subtitle> -->\n        </ion-card-header>\n        <ion-card-content>\n          <ion-list>\n            <ion-item>\n              <ion-label\n                >Do not disturb\n                <br />\n                <p style=\"font-size: 10px\">Snooze all alert notifications</p>\n              </ion-label>\n              <ion-checkbox\n                slot=\"end\"\n                color=\"dark\"\n                [(ngModel)]=\"isChecked\"\n              ></ion-checkbox>\n            </ion-item>\n          </ion-list>\n        </ion-card-content>\n      </ion-card>\n    </ion-row>\n    <ion-row>\n      <h5 style=\"width: 100%; text-align: center\">\n        Alerts and notifications are managed in the web application\n      </h5>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n<ion-footer>\n  <ion-toolbar>\n    <ion-button id=\"pref\" slot=\"start\" color=\"dark\" (click)=\"resetForm()\">\n      Reset\n    </ion-button>\n    <ion-button slot=\"end\" color=\"dark\" (click)=\"saveChange()\">\n      Save Changes\n    </ion-button>\n  </ion-toolbar>\n</ion-footer>\n");

/***/ }),

/***/ 4119:
/*!*******************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/preferences/modals/change-password/change-password.component.html ***!
  \*******************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\n  <ion-toolbar color=\"white\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button  defaultHref=\"preferences\" (click)=\"closeModal()\"></ion-back-button>\n    </ion-buttons>\n    <div style=\"text-align: center;\">      \n      <span id=\"title\" style=\"margin-right: 50px ;\" ><strong>Change Password</strong></span>\n    </div>\n  </ion-toolbar>\n</ion-header>\n<ion-content class=\"ion-padding\">\n     <ion-card>\n        <ion-card-content >\n            <form [formGroup]=\"changePasswordForm\" style=\"width: 100%;\">\n              <ion-item class=\"input-details\">\n                <ion-label position=\"floating\">Old password</ion-label>\n                <ion-input type=\"password\" formControlName=\"opassword\" ></ion-input>\n              </ion-item>\n              <ion-item class=\"input-details\">\n                <ion-label position=\"floating\">New password</ion-label>\n                <ion-input type=\"password\" formControlName=\"password\" (input)='passwordInputValidation($event)'></ion-input>\n              </ion-item>\n              <ion-item class=\"input-details\">\n                <ion-label position=\"floating\">Confirm new password</ion-label>\n                <ion-input type=\"password\" formControlName=\"cpassword\" (input)='cpasswordInputValidation($event)'></ion-input>\n              </ion-item>\n\n              <ion-list class=\"list\" style=\"max-width: 350px; margin-left: auto; margin-right: auto;\">\n                <ion-icon class=\"icon\" name=\"checkmark-circle\" style=\"color: green;\" *ngIf=\"passwordVal.minLen\"></ion-icon>\n                <ion-icon class=\"icon\" name=\"checkmark-circle\" *ngIf=\"!passwordVal.minLen\"></ion-icon>\n                <ion-label class=\"label\">Be at least 8 characters long</ion-label><br>\n    \n                <ion-icon class=\"icon\" name=\"checkmark-circle\" style=\"color: green;\" *ngIf=\"passwordVal.number\"></ion-icon>\n                <ion-icon class=\"icon\" name=\"checkmark-circle\" *ngIf=\"!passwordVal.number\"></ion-icon>\n                <ion-label class=\"label\">Have at least one number</ion-label><br>\n    \n                <ion-icon class=\"icon\" name=\"checkmark-circle\" style=\"color: green;\" *ngIf=\"passwordVal.symbol\"></ion-icon>\n                <ion-icon class=\"icon\" name=\"checkmark-circle\" *ngIf=\"!passwordVal.symbol\"></ion-icon>\n                <ion-label class=\"label\">Have at least one symbol</ion-label><br>\n    \n                <ion-icon class=\"icon\" name=\"checkmark-circle\" style=\"color: green;\" *ngIf=\"passwordVal.upperCase\"></ion-icon>\n                <ion-icon class=\"icon\" name=\"checkmark-circle\" *ngIf=\"!passwordVal.upperCase\"></ion-icon>\n                <ion-label class=\"label\">Have at least one capital letter</ion-label><br>\n                \n                <ion-icon class=\"icon\" name=\"checkmark-circle\" style=\"color: green;\" *ngIf=\"passwordVal.lowerCase\"></ion-icon>\n                <ion-icon class=\"icon\" name=\"checkmark-circle\" *ngIf=\"!passwordVal.lowerCase\"></ion-icon>\n                <ion-label class=\"label\">Have at least one small letter</ion-label><br>\n              </ion-list>\n            </form>\n        </ion-card-content>\n      </ion-card>\n</ion-content>\n\n\n<ion-footer>\n  <ion-toolbar>\n    <ion-button id=\"pref\" slot=\"start\"  color=\"dark\" (click)=\"resetForm()\">\n      Reset\n    </ion-button>\n    <ion-button slot=\"end\" color=\"dark\" [disabled]=\"!changePasswordForm.valid || !cPasswordVal\" (click)=\"changePasswordInitiate()\">\n      Change Password\n    </ion-button>\n  </ion-toolbar>\n</ion-footer>");

/***/ }),

/***/ 4573:
/*!*************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/preferences/modals/edit-account/edit-account.component.html ***!
  \*************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\n  <ion-toolbar color=\"white\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button  defaultHref=\"preferences\" (click)=\"closeModal()\"></ion-back-button>\n    </ion-buttons>\n    <div style=\"text-align: center;\">      \n      <span id=\"title\" style=\"margin-right: 50px ;\" ><strong>Edit Account</strong></span>\n    </div>\n  </ion-toolbar>\n</ion-header>\n<ion-content class=\"ion-padding\">\n  <ion-grid>\n    <ion-row>\n      <ion-card>\n        <ion-card-header>\n          <!-- <ion-card-subtitle>Sort By</ion-card-subtitle> -->\n        </ion-card-header>\n        <ion-card-content>\n          <ion-row>\n            <form [formGroup]=\"accountInformationForm\" style=\"width: 100%;\">\n              <ion-item class=\"input-details\">\n                <ion-label position=\"floating\">First Name</ion-label>\n                <ion-input type=\"text\" formControlName=\"firstname\"></ion-input>\n              </ion-item>\n              <ion-item class=\"input-details\">\n                <ion-label position=\"floating\">Last Name</ion-label>\n                <ion-input formControlName=\"lastname\"></ion-input>\n              </ion-item>\n              <ion-item class=\"input-details\">\n                <ion-label position=\"floating\">Company Name</ion-label>\n                <ion-input formControlName=\"companyName\"></ion-input>\n              </ion-item>\n              <ion-item class=\"input-details\">\n                <ion-label position=\"floating\">Username</ion-label>\n                <ion-input formControlName=\"username\"></ion-input>\n              </ion-item>\n              <ion-item class=\"input-details\">\n                <ion-label position=\"floating\">Email</ion-label>\n                <ion-input formControlName=\"email\"></ion-input>\n              </ion-item>\n              <ion-item class=\"input-details\">\n                <ion-label position=\"floating\">Phone</ion-label>\n                <ion-input formControlName=\"phone\"></ion-input>\n              </ion-item>\n              <ion-item class=\"input-details\">\n                <ion-label position=\"floating\">Address</ion-label>\n                <ion-input formControlName=\"address\"></ion-input>\n              </ion-item>\n              <ion-item class=\"input-details\">\n                <ion-label position=\"floating\">City</ion-label>\n                <ion-input formControlName=\"city\"></ion-input>\n              </ion-item>\n              <ion-item class=\"input-details\">\n                <ion-label position=\"floating\">State</ion-label>\n                <ion-input formControlName=\"state\"></ion-input>\n              </ion-item>\n              <ion-item class=\"input-details\">\n                <ion-label position=\"floating\">Zip Code</ion-label>\n                <ion-input formControlName=\"zipcode\"></ion-input>\n              </ion-item>\n              <ion-item class=\"input-details\">\n                <ion-label position=\"floating\">Country</ion-label>\n                <ion-input formControlName=\"country\"></ion-input>\n              </ion-item>\n            </form>\n          </ion-row>\n        </ion-card-content>\n      </ion-card>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n\n\n<ion-footer>\n  <ion-toolbar>\n    <ion-button id=\"pref\" slot=\"start\"  color=\"dark\" (click)=\"resetForm()\">\n      Reset\n    </ion-button>\n    <ion-button slot=\"end\" color=\"dark\" [disabled]=\"!accountInformationForm.valid\" (click)=\"updateAccountInformation()\">\n      Update Account\n    </ion-button>\n  </ion-toolbar>\n</ion-footer>");

/***/ }),

/***/ 1781:
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/preferences/modals/settings/settings.component.html ***!
  \*****************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\n  <ion-toolbar color=\"white\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button  defaultHref=\"preferences\" (click)=\"closeModal()\"></ion-back-button>\n    </ion-buttons>\n    <div style=\"text-align: center;\">      \n      <span id=\"title\" style=\"margin-right: 50px ;\" ><strong>Settings</strong></span>\n    </div>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-card>\n    <ion-card-header>\n      <ion-card-subtitle>Time Zone</ion-card-subtitle>\n    </ion-card-header>\n    <ion-card-content>\n      <ion-item detail button (click)=\"showPicker()\">\n        <ion-label>\n          {{ selectedTimeZone }}\n          <br>\n          <p class=\"button-icons-desc\">Select your time zone</p>\n        </ion-label>\n      </ion-item>\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card>\n    <ion-card-header>\n      <ion-card-subtitle>Map Style</ion-card-subtitle>\n      <p style=\"font-size: 12px;\">Choose your map style</p>\n    </ion-card-header>\n    <ion-card-content>\n      <ion-list>\n        <ion-radio-group [(value)]=\"defaultMapStyle\" mode=\"md\">\n          <ion-item style=\"margin-bottom: 1em;\">\n            <img src=\"../../../../assets/maps/light.png\"><ion-label>Roadmap</ion-label>\n            <ion-radio slot=\"start\" value=\"roadmap\" (ionFocus)=\"selectedMapType('roadmap')\"></ion-radio>\n          </ion-item>\n          <ion-item style=\"margin-bottom: 1em;\">\n            <img src=\"../../../../assets/maps/satelite.png\"><ion-label>Satellite</ion-label>\n              <ion-radio slot=\"start\" value=\"satellite\" (ionFocus)=\"selectedMapType('satellite')\"></ion-radio>\n          </ion-item>\n          <!-- <ion-item style=\"margin-bottom: 1em;\">\n            <img src=\"../../../../assets/maps/satelite.png\"><ion-label>Satelite</ion-label>\n              <ion-radio slot=\"start\" value=\"2\"></ion-radio>\n          </ion-item> -->\n          <ion-item style=\"margin-bottom: 1em;\">\n            <img src=\"../../../../assets/maps/terrain.jpg\"><ion-label>Hybrid</ion-label>\n              <ion-radio slot=\"start\" value=\"hybrid\" (ionFocus)=\"selectedMapType('hybrid')\"></ion-radio>\n          </ion-item>\n        </ion-radio-group>\n      </ion-list>\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card>\n    <ion-card-header>\n      <ion-card-subtitle>Distance Units</ion-card-subtitle>\n    </ion-card-header>\n    <ion-card-content>\n      <ion-list>\n        <ion-radio-group [(value)]=\"defaultDistanceUnit\" mode=\"md\">\n          <ion-item style=\"margin-bottom: 1em;\">\n            <ion-label>Feet</ion-label>\n            <ion-radio slot=\"start\" value=\"Feet\" (ionFocus)=\"selectedDistanceUnit('Feet')\"></ion-radio>\n          </ion-item>\n          <ion-item style=\"margin-bottom: 1em;\">\n            <ion-label>Kilometers</ion-label>\n              <ion-radio slot=\"start\" value=\"Km\" (ionFocus)=\"selectedDistanceUnit('Km')\"></ion-radio>\n          </ion-item>\n          <ion-item style=\"margin-bottom: 1em;\">\n            <ion-label>Meters</ion-label>\n              <ion-radio slot=\"start\" value=\"M\" (ionFocus)=\"selectedDistanceUnit('M')\"></ion-radio>\n          </ion-item>\n          <ion-item style=\"margin-bottom: 1em;\">\n            <ion-label>Miles</ion-label>\n              <ion-radio slot=\"start\" value=\"Miles\" (ionFocus)=\"selectedDistanceUnit('Miles')\"></ion-radio>\n          </ion-item>\n          <ion-item style=\"margin-bottom: 1em;\">\n            <ion-label>Yards</ion-label>\n              <ion-radio slot=\"start\" value=\"Yards\" (ionFocus)=\"selectedDistanceUnit('Yards')\"></ion-radio>\n          </ion-item>\n        </ion-radio-group>\n      </ion-list>\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card>\n    <ion-card-header>\n      <ion-card-subtitle>Speed Units</ion-card-subtitle>\n    </ion-card-header>\n    <ion-card-content>\n      <ion-list>\n        <ion-radio-group [(value)]=\"defaultSpeedUnit\" mode=\"md\">\n          <ion-item style=\"margin-bottom: 1em;\">\n            <ion-label>km/h</ion-label>\n            <ion-radio slot=\"start\" value=\"kmh\" (ionFocus)=\"selectedSpeedUnit('kmh')\"></ion-radio>\n          </ion-item>\n          <ion-item style=\"margin-bottom: 1em;\">\n            <ion-label>mph</ion-label>\n              <ion-radio slot=\"start\" value=\"mph\" (ionFocus)=\"selectedSpeedUnit('mph')\"></ion-radio>\n          </ion-item>\n        </ion-radio-group>\n      </ion-list>\n    </ion-card-content>\n  </ion-card>\n</ion-content>\n\n<ion-footer>\n  <ion-toolbar>\n    <ion-button id=\"pref\" slot=\"start\" color=\"dark\" (click)=\"resetForm()\">\n      Reset\n    </ion-button>\n    <ion-button slot=\"end\" color=\"dark\" (click)=\"applyChanges()\" >\n      Save Changes\n    </ion-button>\n  </ion-toolbar>\n</ion-footer>");

/***/ }),

/***/ 6066:
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/preferences/modals/trackers/trackers.component.html ***!
  \*****************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header style=\"background-color: #fff\">\n  <ion-toolbar color=\"white\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button\n        defaultHref=\"preferences\"\n        (click)=\"closeModal()\"\n      ></ion-back-button>\n    </ion-buttons>\n    <div style=\"text-align: center\">\n      <span id=\"title\" style=\"margin-right: 50px\"\n        ><strong>Trackers</strong></span\n      >\n    </div>\n  </ion-toolbar>\n</ion-header>\n<ion-content fullscreen *ngIf=\"dataLoad\">\n  <div *ngIf=\"!userTrackers || userTrackers.length === 0\" style=\"padding: 10px; text-align: center;\">\n    Please activate atlest one tracker.\n  </div>\n  <ion-radio-group allow-empty-selection mode=\"md\">\n    <div *ngIf=\"userTrackers?.length > 0\">\n      <ion-item *ngFor=\"let tracker of userTrackers\">\n        <ion-label *ngIf=\"tracker.tracker_icon\"\n          ><fa-icon\n            class=\"tracker-sample-view\"\n            [icon]=\"tracker.tracker_icon\"\n            style=\"font-size: 15px; background-color: {{\n              tracker.tracker_icon_color\n            }}\"\n          ></fa-icon>\n          {{ tracker.tracker_name }}</ion-label\n        >\n        <ion-label *ngIf=\"!tracker.tracker_icon\">{{\n          tracker.tracker_name\n        }}</ion-label>\n        <ion-radio\n          slot=\"end\"\n          color=\"dark\"\n          (ionFocus)=\"selectTracker($event, tracker)\"\n        ></ion-radio>\n      </ion-item>\n    </div>\n  </ion-radio-group>\n  <ion-row>\n    <ion-card style=\"width: 100%\" *ngIf=\"selectedTracker != ''\">\n      <ion-card-header> Tracker Details </ion-card-header>\n      <ion-card-content>\n        <form style=\"width: 100%\" [formGroup]=\"trackerForm\">\n          <ion-item class=\"input-details\">\n            <ion-label position=\"floating\">Tracker Name</ion-label>\n            <ion-input type=\"text\" formControlName=\"trackerName\"></ion-input>\n          </ion-item>\n          <ion-item class=\"input-details\">\n            <ion-label position=\"floating\">Tracker Description</ion-label>\n            <ion-input\n              type=\"text\"\n              formControlName=\"trackerDescription\"\n            ></ion-input>\n          </ion-item>\n          <p style=\"padding-top: 2%\">Select Tracker Icon</p>\n          <ion-item class=\"input-details\">\n            <ion-segment value=\"white\">\n              <ion-grid>\n                <ion-row>\n                  <ion-segment-button\n                    [value]=\"icon.icon\"\n                    *ngFor=\"let icon of iconClass; let i = index\"\n                    (click)=\"iconChange($event, icon.icon)\"\n                  >\n                    <ion-label>\n                      <fa-icon\n                        [icon]=\"icon.icon\"\n                        style=\"font-size: 30px\"\n                      ></fa-icon>\n                    </ion-label>\n                  </ion-segment-button>\n                </ion-row>\n              </ion-grid>\n            </ion-segment>\n          </ion-item>\n          <p style=\"padding-top: 2%\">Select Icon Color</p>\n          <ion-item class=\"input-details\">\n            <ion-segment value=\"white\">\n              <ion-grid>\n                <ion-row>\n                  <ion-segment-button\n                    *ngFor=\"let colors of colorClass\"\n                    style=\"border-radius: 30px; background-color: {{\n                      colors.color\n                    }}\"\n                    (click)=\"colorChange($event, colors.color)\"\n                  >\n                  </ion-segment-button>\n                </ion-row>\n              </ion-grid>\n            </ion-segment>\n          </ion-item>\n        </form>\n      </ion-card-content>\n    </ion-card>\n    <ion-card style=\"width: 100%\" *ngIf=\"selectedTracker != ''\">\n      <ion-card-header> Sample Tracker View </ion-card-header>\n      <ion-card-content class=\"center\">\n        <fa-icon\n          class=\"tracker-sample-view\"\n          [icon]=\"selectedTrackerIcon\"\n          style=\"font-size: 50px; background-color: {{ selectedTrackerColor }}\"\n        ></fa-icon>\n      </ion-card-content>\n    </ion-card>\n  </ion-row>\n  <!-- <ion-card class=\"ion-text-center\" id=\"over_map\" *ngIf=\"!activateTracker\">\n    <ion-card-header>\n      <ion-card-title>Activate a Tracker</ion-card-title>\n    </ion-card-header>\n    <ion-card-content>\n      Add a tracker to enjoy the full Spytrack GPS<br> experience.\n      <br>\n      <br>\n      <ion-grid>\n        <ion-row>\n          <ion-col (click)=\"redirectActivate()\">\n            <p style=\"color: deepskyblue;\n            font-weight: 800;\">Purchase a tracker</p>\n          </ion-col>\n          <ion-col (click)=\"redirectActivate()\">\n            <p style=\"color: darkgreen;\n            font-weight: 800;\">Activate a tracker</p>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-card-content>\n  </ion-card> -->\n  <!-- Activation -->\n  <!-- <ion-card class=\"ion-text-center\" id=\"over_map\" *ngIf=\"activateTracker\">\n    <ion-card-header>\n      <ion-card-title>Activate a Tracker</ion-card-title>\n    </ion-card-header>\n    <ion-card-content>\n      Add a tracker to enjoy the full Spytrack GPS<br> experience.\n      <br>\n      <br>\n      <ion-grid>\n        <ion-row>\n          <form [formGroup]=\"activationForm\" style=\"width: 100%;\">\n            <ion-item class=\"input-details\">\n              <ion-label position=\"floating\">IMEI Number</ion-label>\n              <ion-input type=\"text\" formControlName=\"imei\"></ion-input>\n            </ion-item>\n          </form>\n        </ion-row>\n      </ion-grid>\n    </ion-card-content>\n  </ion-card> -->\n</ion-content>\n\n<ion-footer>\n  <ion-toolbar>\n    <ion-button [disabled]=\"!(userTrackers?.length > 0)\" id=\"pref\" slot=\"start\" color=\"dark\" (click)=\"resetForm()\">\n      Reset\n    </ion-button>\n    <ion-button [disabled]=\"!(userTrackers?.length > 0)\" slot=\"end\" color=\"dark\" (click)=\"updateTrackerSetting()\">\n      Save\n    </ion-button>\n  </ion-toolbar>\n</ion-footer>\n");

/***/ }),

/***/ 6754:
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/preferences/modals/users/users.component.html ***!
  \***********************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\n  <ion-toolbar color=\"white\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"preferences\" (click)=\"closeModal()\"></ion-back-button>\n    </ion-buttons>\n    <div style=\"text-align: center;\">      \n      <span id=\"title\" style=\"margin-right: 50px ;\" ><strong>Users</strong></span>\n    </div>\n  </ion-toolbar>\n</ion-header>\n\n  <ion-grid style=\"width: 95%;\">\n    <ion-row>\n      <div style=\"margin:0 auto; text-align: center;\">Users are managed in the web application.</div>\n    </ion-row>\n  </ion-grid>");

/***/ }),

/***/ 7452:
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/preferences/preferences.page.html ***!
  \***********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>Preferences</ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button menu=\"main-menu\"></ion-menu-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid>\n    <ion-row>\n      <ion-card class=\"ion-text-center\" >\n        <ion-card-header>\n          <ion-card-title class=\"username-prefrence\">{{user.firstname}} {{user.lastname}}</ion-card-title>\n        </ion-card-header>\n      \n        <ion-card-content>\n          <p id=\"contact-details\"><ion-icon name=\"call\"></ion-icon> {{ user.phone }}</p>\n          <p id=\"contact-details\"><ion-icon name=\"mail\"></ion-icon> {{ user.email }}</p>\n          <ion-button shape=\"round\" color=\"dark\" (click)=\"openEditAccountModal()\">Edit Account</ion-button>\n        </ion-card-content>\n      </ion-card>\n    </ion-row>\n    <ion-row>\n      <ion-card class=\"ion-text-center\">\n        <ion-card-content>\n          <ion-item detail button (click)=\"openEditAlertNotificationModal()\">\n            <ion-icon class=\"button-icons\" name=\"notifications\"></ion-icon>\n            <ion-label>\n              Alert and Notification\n              <br>\n              <p class=\"button-icons-desc\">Manage your alerts and notifications</p>\n            </ion-label>\n            <br>\n          </ion-item>\n          <ion-item detail button (click)=\"openEditUsersModal()\">\n          <ion-icon class=\"button-icons\" name=\"people\"></ion-icon>\n            <ion-label>\n              Users\n              <br>\n              <p class=\"button-icons-desc\">Manage Users</p>\n            </ion-label>\n          </ion-item>\n          <ion-item detail button (click)=\"openEditTrackersModal()\">\n            <ion-icon class=\"button-icons\" name=\"people\"></ion-icon>\n            <ion-label>\n              Trackers\n              <br>\n              <p class=\"button-icons-desc\">Change your tracker name icon color</p>\n            </ion-label>\n          </ion-item>\n          <ion-item detail button (click)=\"openEditSettingModal()\">\n            <ion-icon class=\"button-icons\" name=\"settings\"></ion-icon>\n            <ion-label>\n              Settings\n              <br>\n              <p class=\"button-icons-desc\">Change your map style, time zone</p>\n            </ion-label>\n          </ion-item>\n          <ion-item detail button (click)=\"openEditChangePasswordModal()\">\n            <ion-icon class=\"button-icons\" name=\"key\"></ion-icon>\n            <ion-label>\n              Change Password\n              <br>\n              <p class=\"button-icons-desc\">Change your password</p>\n            </ion-label>\n          </ion-item>\n        </ion-card-content>\n      </ion-card>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_preferences_preferences_module_ts.js.map