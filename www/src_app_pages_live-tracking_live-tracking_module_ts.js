(self["webpackChunkspytrack"] = self["webpackChunkspytrack"] || []).push([["src_app_pages_live-tracking_live-tracking_module_ts"],{

/***/ 593:
/*!****************************************************************!*\
  !*** ./src/app/pages/live-tracking/filter/filter.component.ts ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FilterComponent": () => (/* binding */ FilterComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_filter_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./filter.component.html */ 5701);
/* harmony import */ var _filter_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./filter.component.scss */ 7240);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var src_app_globals_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/globals.service */ 3221);
/* harmony import */ var src_app_rest_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/rest.service */ 1881);



/* eslint-disable @typescript-eslint/prefer-for-of */




let FilterComponent = class FilterComponent {
    constructor(modalController, rest, globalSerice, loader) {
        this.modalController = modalController;
        this.rest = rest;
        this.globalSerice = globalSerice;
        this.loader = loader;
        this.defaultTrackerActivityFilter = '';
        this.defaultMapStyle = '0';
        this.defaultTrackerHistory = '0';
        this.userTrackers = [];
        this.selectedTrackers = [];
        this.hasTracker = false;
        this.initialSelectedTrackers = [];
    }
    ngOnInit() {
        this.initGetSettingsValue();
        this.initGetMapStyleSettingsValue();
        this.getTrackerUser();
        this.getUserSettings();
        // this.getUserTracker();
    }
    getUserSettings() {
        this.userSettings = JSON.parse(localStorage.getItem('userSettings'));
    }
    initGetSettingsValue() {
        if (localStorage.getItem('trackerHistorySetting')) {
            if (localStorage.getItem('trackerHistorySetting') === 'noHistory') {
                this.defaultTrackerHistory = '0';
            }
            else {
                this.defaultTrackerHistory = '1';
            }
        }
        if (localStorage.getItem('trackerActivityFilter')) {
            if (localStorage.getItem('trackerActivityFilter') === 'all') {
                this.defaultTrackerActivityFilter = '0';
            }
            else if (localStorage.getItem('trackerActivityFilter') === 'last hour') {
                this.defaultTrackerActivityFilter = '1';
            }
            else if (localStorage.getItem('trackerActivityFilter') === '8 hours') {
                this.defaultTrackerActivityFilter = '8';
            }
            else if (localStorage.getItem('trackerActivityFilter') === '12 hours') {
                this.defaultTrackerActivityFilter = '12';
            }
            else if (localStorage.getItem('trackerActivityFilter') === '24 hours') {
                this.defaultTrackerActivityFilter = '24';
            }
            else if (localStorage.getItem('trackerActivityFilter') === 'today') {
                this.defaultTrackerActivityFilter = 'today';
            }
        }
    }
    initSetSettingsValue() {
        if (this.defaultTrackerHistory === '0') {
            localStorage.setItem('trackerHistorySetting', 'noHistory');
        }
        else {
            localStorage.setItem('trackerHistorySetting', 'withHistory');
        }
        if (this.defaultTrackerActivityFilter === '0') {
            localStorage.setItem('trackerActivityFilter', 'all');
        }
        else if (this.defaultTrackerActivityFilter === '1') {
            localStorage.setItem('trackerActivityFilter', 'last hour');
        }
        else if (this.defaultTrackerActivityFilter === '8') {
            localStorage.setItem('trackerActivityFilter', '8 hours');
        }
        else if (this.defaultTrackerActivityFilter === '12') {
            localStorage.setItem('trackerActivityFilter', '12 hours');
        }
        else if (this.defaultTrackerActivityFilter === '24') {
            localStorage.setItem('trackerActivityFilter', '24 hours');
        }
        else if (this.defaultTrackerActivityFilter === 'today') {
            localStorage.setItem('trackerActivityFilter', 'today');
        }
    }
    initGetMapStyleSettingsValue() {
        if (localStorage.getItem('mapStyle')) {
            if (localStorage.getItem('mapStyle') === 'roadmap') {
                this.defaultMapStyle = 'roadmap';
            }
            else if (localStorage.getItem('mapStyle') === 'satellite') {
                this.defaultMapStyle = 'satellite';
            }
            else if (localStorage.getItem('mapStyle') === 'hybrid') {
                this.defaultMapStyle = 'hybrid';
            }
        }
        else {
            this.defaultMapStyle = 'roadmap';
        }
    }
    initSetMapStyleSettingsValue() {
        if (this.defaultMapStyle === 'roadmap') {
            localStorage.setItem('mapStyle', 'roadmap');
        }
        else if (this.defaultMapStyle === 'satellite') {
            localStorage.setItem('mapStyle', 'satellite');
        }
        else if (this.defaultMapStyle === 'hybrid') {
            localStorage.setItem('mapStyle', 'hybrid');
        }
    }
    closeModal(data = {}) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            console.log(data);
            yield this.modalController.dismiss(data);
        });
    }
    selectedHistory(value) {
        this.defaultTrackerHistory = value;
        this.defaultTrackerActivityFilter = '';
    }
    resetFilter() {
        this.initGetSettingsValue();
        this.initGetMapStyleSettingsValue();
        this.getTrackerUser();
        //  this.selectedTrackers = this.initialSelectedTrackers;
        //  console.log(this.initialSelectedTrackers);
        //  localStorage.setItem('selectedUserTrackers', JSON.stringify(this.selectedTrackers));
        this.getUserSettings();
    }
    applyChanges() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.globalLoader = yield this.loader.create({
                cssClass: 'my-custom-class',
                message: 'Please wait...',
            });
            this.globalLoader.present();
            this.initSetSettingsValue();
            this.initSetMapStyleSettingsValue();
            let dataToClose = {
                filterHours: '',
            };
            if (this.defaultTrackerHistory === '1') {
                if (this.defaultTrackerActivityFilter === '0') {
                    dataToClose.filterHours = 'all';
                }
                else if (this.defaultTrackerActivityFilter === '1') {
                    dataToClose.filterHours = 'last hour';
                }
                else if (this.defaultTrackerActivityFilter === '8') {
                    dataToClose.filterHours = '8 hours';
                }
                else if (this.defaultTrackerActivityFilter === '12') {
                    dataToClose.filterHours = '12 hours';
                }
                else if (this.defaultTrackerActivityFilter === '24') {
                    dataToClose.filterHours = '24 hours';
                }
                else if (this.defaultTrackerActivityFilter === 'today') {
                    dataToClose.filterHours = 'today';
                }
                else {
                    dataToClose.filterHours = '';
                }
            }
            else {
                dataToClose.filterHours = '';
            }
            // dataToClose['userSettings'] = this.
            console.log(dataToClose);
            for (let x = 0; x < this.selectedTrackers.length; x++) {
                this.selectedTrackers[x].tracker_icon =
                    'fa-' + this.selectedTrackers[x].tracker_icon;
            }
            dataToClose['selectedTracker'] = this.selectedTrackers;
            this.saveMapStyle()
                .then((response) => {
                console.log('response', response);
            })
                .catch((error) => {
                console.error(error);
            })
                .finally(() => {
                this.globalLoader.dismiss();
                this.closeModal(dataToClose);
            });
        });
    }
    saveMapStyle() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            let dataToSend = {
                timezone: this.userSettings.timezone,
                map_style: this.defaultMapStyle,
                speed_unit: this.userSettings.speed_unit,
                distance_unit: this.userSettings.distance_unit,
            };
            localStorage.setItem('userSettings', JSON.stringify(dataToSend));
            // Return a new promise object
            return new Promise((resolve, reject) => {
                // Initialise an api call
                this.rest.put(dataToSend, 'thorton/user/settings').subscribe((resp) => {
                    console.log(resp);
                    resolve(resp);
                }, (error) => {
                    reject(error);
                });
            });
        });
    }
    selectedActivityFilter(value) {
        this.defaultTrackerActivityFilter = value;
    }
    selectedMapType(value) {
        this.defaultMapStyle = value;
    }
    selectTrackerEvent(event, tracker, index) {
        if (event.detail.checked) {
            tracker['isChecked'] = true;
            this.userTrackers[index].isChecked = true;
            this.selectedTrackers.push(tracker);
            localStorage.setItem('userTrackers', JSON.stringify(this.userTrackers));
        }
        else {
            tracker['isChecked'] = false;
            this.userTrackers[index].isChecked = false;
            this.selectedTrackers = this.selectedTrackers.filter((item) => item.imei !== tracker.imei);
            localStorage.setItem('userTrackers', JSON.stringify(this.userTrackers));
        }
        localStorage.setItem('selectedUserTrackers', JSON.stringify(this.selectedTrackers));
    }
    getTrackerUser() {
        console.log(JSON.parse(localStorage.getItem('userTrackers')));
        if (localStorage.getItem('userTrackers')) {
            this.userTrackers = [];
            this.selectedTrackers = [];
            let temp = JSON.parse(localStorage.getItem('userTrackers'));
            for (let x = 0; x < temp.length; x++) {
                if (temp[x].tracker_icon) {
                    temp[x].tracker_icon_url = this.globalSerice.getMarkerLocation(temp[x].tracker_icon, temp[x].tracker_icon_color);
                    temp[x].tracker_icon = temp[x].tracker_icon.replace('fa-', '');
                }
                this.userTrackers.push(temp[x]);
            }
            for (let x = 0; x < this.userTrackers.length; x++) {
                if (this.userTrackers[x].isChecked) {
                    this.selectedTrackers.push(this.userTrackers[x]);
                }
            }
            localStorage.setItem('selectedUserTrackers', JSON.stringify(this.selectedTrackers));
            this.initialSelectedTrackers = this.selectedTrackers.slice(0);
            this.hasTracker = true;
        }
        else {
            this.hasTracker = false;
        }
    }
    getUserTracker() {
        let dataToSend = {
            trackerID: '4900112612',
            recent: 'today',
        };
        this.rest
            .get('thorton/trackerData/recent', dataToSend, '')
            .subscribe((resp) => {
            console.log(resp);
        });
    }
};
FilterComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.ModalController },
    { type: src_app_rest_service__WEBPACK_IMPORTED_MODULE_3__.RestService },
    { type: src_app_globals_service__WEBPACK_IMPORTED_MODULE_2__.GlobalService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.LoadingController }
];
FilterComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-filter',
        template: _raw_loader_filter_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_filter_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], FilterComponent);



/***/ }),

/***/ 9283:
/*!*********************************************************************!*\
  !*** ./src/app/pages/live-tracking/live-tracking-routing.module.ts ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LiveTrackingPageRoutingModule": () => (/* binding */ LiveTrackingPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _live_tracking_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./live-tracking.page */ 116);




const routes = [
    {
        path: '',
        component: _live_tracking_page__WEBPACK_IMPORTED_MODULE_0__.LiveTrackingPage
    }
];
let LiveTrackingPageRoutingModule = class LiveTrackingPageRoutingModule {
};
LiveTrackingPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], LiveTrackingPageRoutingModule);



/***/ }),

/***/ 6888:
/*!*************************************************************!*\
  !*** ./src/app/pages/live-tracking/live-tracking.module.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LiveTrackingPageModule": () => (/* binding */ LiveTrackingPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _live_tracking_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./live-tracking-routing.module */ 9283);
/* harmony import */ var _live_tracking_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./live-tracking.page */ 116);
/* harmony import */ var _filter_filter_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./filter/filter.component */ 593);
/* harmony import */ var _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @fortawesome/angular-fontawesome */ 5002);
/* harmony import */ var _fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @fortawesome/free-solid-svg-icons */ 9976);
/* harmony import */ var _fortawesome_free_regular_svg_icons__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @fortawesome/free-regular-svg-icons */ 1903);
/* harmony import */ var _fortawesome_free_brands_svg_icons__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @fortawesome/free-brands-svg-icons */ 5115);
/* harmony import */ var _get_location_url_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./get-location-url.service */ 2311);
/* harmony import */ var ngx_owl_carousel__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-owl-carousel */ 3189);
/* harmony import */ var ngx_owl_carousel__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(ngx_owl_carousel__WEBPACK_IMPORTED_MODULE_4__);














let LiveTrackingPageModule = class LiveTrackingPageModule {
    constructor(library) {
        library.addIconPacks(_fortawesome_free_solid_svg_icons__WEBPACK_IMPORTED_MODULE_5__.fas, _fortawesome_free_brands_svg_icons__WEBPACK_IMPORTED_MODULE_6__.fab, _fortawesome_free_regular_svg_icons__WEBPACK_IMPORTED_MODULE_7__.far);
    }
};
LiveTrackingPageModule.ctorParameters = () => [
    { type: _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_8__.FaIconLibrary }
];
LiveTrackingPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_10__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_11__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_12__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_13__.IonicModule,
            _live_tracking_routing_module__WEBPACK_IMPORTED_MODULE_0__.LiveTrackingPageRoutingModule,
            _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_8__.FontAwesomeModule,
            ngx_owl_carousel__WEBPACK_IMPORTED_MODULE_4__.OwlModule
        ],
        declarations: [
            _live_tracking_page__WEBPACK_IMPORTED_MODULE_1__.LiveTrackingPage,
            _filter_filter_component__WEBPACK_IMPORTED_MODULE_2__.FilterComponent
        ],
        providers: [_get_location_url_service__WEBPACK_IMPORTED_MODULE_3__.GetLocationUrlService]
    })
], LiveTrackingPageModule);



/***/ }),

/***/ 116:
/*!***********************************************************!*\
  !*** ./src/app/pages/live-tracking/live-tracking.page.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LiveTrackingPage": () => (/* binding */ LiveTrackingPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_live_tracking_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./live-tracking.page.html */ 725);
/* harmony import */ var _live_tracking_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./live-tracking.page.scss */ 8069);
/* harmony import */ var _get_location_url_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./get-location-url.service */ 2311);
/* harmony import */ var _rest_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../rest.service */ 1881);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var src_app_globals_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/globals.service */ 3221);
/* harmony import */ var _filter_filter_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./filter/filter.component */ 593);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common/http */ 1841);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ 5257);



/* eslint-disable @typescript-eslint/no-inferrable-types */
/* eslint-disable object-shorthand */
/* eslint-disable @typescript-eslint/no-shadow */
/* eslint-disable @typescript-eslint/no-unused-expressions */
/* eslint-disable curly */
/* eslint-disable @typescript-eslint/dot-notation */
/* eslint-disable quote-props */
/* eslint-disable max-len */
/* eslint-disable prefer-const */
/* eslint-disable @typescript-eslint/prefer-for-of */
/* eslint-disable @typescript-eslint/semi */
/* eslint-disable eqeqeq */
/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable no-trailing-spaces */
/* eslint-disable @typescript-eslint/quotes */
/* eslint-disable @typescript-eslint/member-ordering */










let LiveTrackingPage = class LiveTrackingPage {
    constructor(http, modalController, rest, router, global, toastCtrl, getMarkerLocation) {
        this.http = http;
        this.modalController = modalController;
        this.rest = rest;
        this.router = router;
        this.global = global;
        this.toastCtrl = toastCtrl;
        this.getMarkerLocation = getMarkerLocation;
        this.trackerHistorySetting = 'noHistory';
        this.hasTracker = false;
        this.dataLoad = false;
        this.hasTrackerActivity = false;
        this.trip = [];
        this.slideOptions = { items: 2, dots: false };
        this.months = [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December',
        ];
        // MAP variables
        this.displayMap = false;
        this.defaultMapLocation = {
            last_location: { latitude: 7.196276, longitude: 125.461807 },
        }; // LAT LONG POINTS TO DAVAO, PHILIPPINES (Subject to change)
        this.mapStyle = '0'; // map style by default is set to 0 (ROADMAP style)
        this.markers = (google.maps.Markers = []);
        this.startTracking = false;
        this.dataFilterHours = 'Now';
        this.resolvedFlag = true;
        this.selectedTrackers = [];
        if (localStorage.getItem('trackerHistorySetting')) {
            this.trackerHistorySetting = localStorage.getItem('trackerHistorySetting');
        }
        this.globalCurrentMessageSubscription =
            this.global.currentMessage.subscribe((message) => (this.globalData = message));
        this.pusher = new Pusher('b478d55d6148b087811a', {
            cluster: 'ap1',
        });
        this.liveLocations = [];
    }
    ngOnInit() {
        this.displayMap = false;
    }
    ngOnDestroy() {
        //this.timerSubscription.unsubscribe();
        clearInterval(this.liveInfoInterval);
        this.globalCurrentMessageSubscription.unsubscribe();
    }
    ionViewDidEnter() {
        this.initMap();
        this.getUserDetails();
        this.getUserSettings();
        this.getUserTracker();
        this.socketSubscriptionForActivities('asd');
        //this.loadAssetIcons();
        this.isPageFirstTimeReloaded = true;
    }
    loadAssetIcons() {
        if (localStorage) {
            if (!localStorage.getItem('firstLoad')) {
                localStorage['firstLoad'] = true;
                window.location.reload();
            }
            else
                localStorage.removeItem('firstLoad');
        }
    }
    ionViewWillEnter() {
        this.displayMap = false;
        this.dataLoad = false;
    }
    initMap() {
        this.map = new google.maps.Map(document.getElementById('map'), {
            center: { lat: -34.397, lng: 150.644 },
            zoom: 8,
        });
    }
    reInitMapPolyLine(trackers) {
        if (trackers.last_location.length == 0) {
            this.noActivityFoundToast();
        }
        else {
            let location = new google.maps.LatLng(Number(trackers.last_location[0].latitude), Number(trackers.last_location[0].longitude));
            this.map = new google.maps.Map(document.getElementById('map'), {
                center: location,
                zoom: 16,
            });
        }
    }
    getUserSettings() {
        this.rest
            .get('thorton/user/settings', '', '')
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_6__.take)(1))
            .subscribe((resp) => {
            console.log(resp);
            this.userSettings = resp;
            localStorage.setItem('mapStyle', resp.map_style);
            localStorage.setItem('userSettings', JSON.stringify(resp));
        });
    }
    getUserDetails() {
        this.rest
            .get('user', '', '')
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_6__.take)(1))
            .subscribe((resp) => {
            this.global.firstname = resp.firstname;
            this.global.userDetails = resp;
            this.global.notifySummary({ isRefresh: true });
        });
    }
    getUserTracker() {
        this.rest
            .get('thorton/trackers/user', '', '')
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_6__.take)(1))
            .subscribe((resp) => {
            if (resp.length == 0) {
                this.hasTracker = false;
                setTimeout(() => {
                    this.initMap();
                    this.dataLoad = true;
                    this.displayMap = true;
                }, 3000);
            }
            else {
                this.setDefaultTracker(resp).then((ret) => {
                    if (resp.length != 0) {
                        this.hasTracker = true;
                        this.selectedTrackers = JSON.parse(localStorage.getItem('selectedUserTrackers'));
                        for (const st of this.selectedTrackers) {
                            st.tracker_icon_url = this.global.getMarkerLocation(st.tracker_icon, st.tracker_icon_color);
                        }
                        if (ret) {
                            let trackerLocations = [];
                            if (this.trackerHistorySetting == 'noHistory') {
                                for (let x = 0; x < this.selectedTrackers.length; x++) {
                                    this.liveTrack(this.selectedTrackers[x].imei);
                                    // if(this.selectedTrackers[x].imei) {
                                    //   this.setMarkerForMap(this.selectedTrackers[x], 'single');
                                    // } else if(this.selectedTrackers[x].trackerID) {
                                    //   this.setMarkerForMap(this.selectedTrackers[x], 'single');
                                    //   // this.getTrackerLastLocation(selectedTrackers[x].trackerID);
                                    // }
                                }
                                // this.setMarkerForMap(trackerLocations);
                            }
                            else {
                                // for(let x = 0; x < selectedTrackers.length; x++) {
                                //   this.getTrackerLocations(selectedTrackers[x], localStorage.getItem('trackerActivityFilter'))
                                // }
                            }
                        }
                        else {
                            // this.setDefaultTracker(resp);
                        }
                    }
                    else {
                        this.hasTracker = false;
                        setTimeout(() => {
                            this.initMap();
                            this.dataLoad = true;
                        }, 3000);
                    }
                });
            }
        });
    }
    liveTrack(imei) {
        // this.timerSubscription = timer(0, 10000).pipe(
        //   map(() => {
        //     this.getTrackerLastLocation(imei); // load data contains the http request
        //   })
        // )
        if (this.liveInfoInterval) {
            clearInterval(this.liveInfoInterval);
        }
        console.log(this.liveInfoInterval);
        this.getTrackerLastLocation(imei);
        this.liveInfoInterval = setInterval(() => {
            this.getTrackerLastLocation(imei);
        }, 10000);
        this.socketSubscriptionForActivities(imei);
    }
    setDefaultTracker(trackerList) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__awaiter)(this, void 0, void 0, function* () {
            // index 0 in trackerlist is always the selected tracker (default or not)
            if (trackerList.length == 0) {
                return false;
            }
            else {
                let trackerToSave = [];
                for (let x = 0; x < trackerList.length; x++) {
                    let saveData = {
                        created_at: trackerList[x].created_at,
                        imei: trackerList[x].imei,
                        name: trackerList[x].name,
                        description: trackerList[x].description,
                        receive_notif: trackerList[x].receive_notif,
                        track_id: trackerList[x].track_id,
                        trackerID: trackerList[x].trackerID,
                        tracker_icon: trackerList[x].tracker_icon,
                        tracker_name: trackerList[x].tracker_name,
                        tracking_purpose: trackerList[x].tracking_purpose,
                        updated_at: trackerList[x].updated_at,
                        user_id_creator: trackerList[x].user_id_creator,
                        tracker_icon_color: trackerList[x].tracker_icon_color,
                        last_location: trackerList[x].last_location,
                        city: trackerList[x].city,
                        complete_address: trackerList[x].complete_address,
                        country: trackerList[x].country,
                        state: trackerList[x].state,
                        battery_level: trackerList[x].battery_level,
                    };
                    if (x == 0) {
                        saveData['isChecked'] = true;
                        trackerToSave.push(saveData);
                    }
                    else {
                        saveData['isChecked'] = false;
                        trackerToSave.push(saveData);
                    }
                }
                localStorage.setItem('userTrackers', JSON.stringify(trackerToSave));
                let tempArr = [];
                tempArr.push(trackerToSave[0]);
                localStorage.setItem('selectedUserTrackers', JSON.stringify(tempArr));
                this.trackerImeiSelected = tempArr[0].imei;
                this.showTrackerDetails(tempArr[0]);
                // this.timerSubscription = timer(0, 10000).pipe(
                //   map(() => {
                //     this.getTrackerLastLocation(this.trackerImeiSelected); // load data contains the http request
                //   })
                // ).subscribe();
                return true;
            }
        });
    }
    getTrackerLastLocation(imei) {
        this.rest
            .get('thorton/tracker/last/location/' + imei, '', '')
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_6__.take)(1))
            .subscribe((resp) => {
            this.setMapOnAll(null);
            this.markers = [];
            this.setMarkerForLiveMap(resp[0]);
            // if(resp.length == 0) {
            //   setTimeout(() =>{
            //     this.hasTrackerActivity = false;
            //     this.showMap(this.defaultMapLocation);
            //     this.dataLoad = true;
            //   }, 3000);
            // } else {
            //   setTimeout(() =>{
            //     this.hasTrackerActivity = true;
            //     this.showMap(resp[0]);
            //     this.dataLoad = true;
            //   }, 3000);
            // }
        });
    }
    setMarkerForLiveMap(trackers) {
        if (trackers.last_location.length == 0) {
            this.displayMap = true;
            this.dataLoad = true;
            this.hasTrackerActivity = false;
            this.initMap();
        }
        else {
            let iconR = {
                url: this.getMarkerLocation.getMarkerLocation(trackers.tracker_icon, trackers.tracker_icon_color),
                scaledSize: new google.maps.Size(40, 40),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(0, 0), // anchor
            };
            let location = new google.maps.LatLng(Number(trackers.last_location[0].latitude), Number(trackers.last_location[0].longitude));
            this.currentLocationLatandLng = location;
            this.map.setOptions(this.getMapOptions(location, this.isPageFirstTimeReloaded));
            this.isPageFirstTimeReloaded = false;
            let marker = new google.maps.Marker({
                position: {
                    lat: Number(trackers.last_location[0].latitude),
                    lng: Number(trackers.last_location[0].longitude),
                },
                map: this.map,
                title: trackers.tracker_name,
                icon: iconR,
            });
            this.markers.push(marker);
            const infowindow = new google.maps.InfoWindow({
                content: this.infoWindowContent(trackers),
            });
            google.maps.event.addListener(marker, 'click', function () {
                infowindow.open({
                    anchor: marker,
                    map: this.map,
                    shouldFocus: false,
                });
            });
            this.showTrackerDetails(trackers);
            if (!this.liveLocations[trackers.imei]) {
                this.liveLocations[trackers.imei] = [];
                this.liveLocations[trackers.imei].push({
                    lat: Number(trackers.last_location[0].latitude),
                    lng: Number(trackers.last_location[0].longitude),
                });
                console.log('new location founded!!', this.liveLocations);
            }
            else if (this.liveLocations[trackers.imei].length > 0 &&
                (this.liveLocations[trackers.imei][this.liveLocations[trackers.imei].length - 1].lat != marker.position.lat() ||
                    this.liveLocations[trackers.imei][this.liveLocations[trackers.imei].length - 1].lng != marker.position.lng())) {
                this.liveLocations[trackers.imei].push({
                    lat: Number(trackers.last_location[0].latitude),
                    lng: Number(trackers.last_location[0].longitude),
                });
                console.log('new location founded!!', this.liveLocations, this.liveLocations[trackers.imei]);
                if (this.liveLocations[trackers.imei].length > 1) {
                    this.drawPath(trackers.imei);
                }
            }
            this.displayMap = true;
            this.dataLoad = true;
            this.hasTrackerActivity = true;
        }
    }
    setMarkerForMap(trackers, type) {
        if (type == 'single') {
            if (this.liveInfoInterval) {
                clearInterval(this.liveInfoInterval);
            }
            if (trackers.last_location.length == 0) {
                this.displayMap = true;
                this.dataLoad = true;
                this.hasTrackerActivity = false;
                this.initMap();
            }
            else {
                let iconR = {
                    url: this.getMarkerLocation.getMarkerLocation(trackers.tracker_icon, trackers.tracker_icon_color),
                    scaledSize: new google.maps.Size(40, 40),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(0, 0), // anchor
                };
                let location = new google.maps.LatLng(Number(trackers.last_location[0].latitude), Number(trackers.last_location[0].longitude));
                this.map.setOptions(this.getMapOptions(location));
                let marker = new google.maps.Marker({
                    position: {
                        lat: Number(trackers.last_location[0].latitude),
                        lng: Number(trackers.last_location[0].longitude),
                    },
                    map: this.map,
                    title: trackers.tracker_name,
                    icon: iconR,
                });
                this.markers.push(marker);
                const infowindow = new google.maps.InfoWindow({
                    content: this.infoWindowContent(trackers),
                });
                google.maps.event.addListener(marker, 'click', function () {
                    infowindow.open({
                        anchor: marker,
                        map: this.map,
                        shouldFocus: false,
                    });
                });
                this.showTrackerDetails(trackers);
                if (this.liveInfoInterval) {
                    clearInterval(this.liveInfoInterval);
                    this.liveTrack(trackers.imei);
                    this.selectedLiveTrack = trackers;
                }
                else {
                    this.liveTrack(trackers.imei);
                    this.selectedLiveTrack = trackers;
                }
                // this.displayMap = true;
                // this.dataLoad = true;
                // this.hasTrackerActivity = true;
            }
        }
        else {
            for (let x = 0; x < trackers.length; x++) {
                let iconR = {
                    url: this.getMarkerLocation.getMarkerLocation(trackers[x].tracker_icon, trackers[x].tracker_icon_color),
                    scaledSize: new google.maps.Size(40, 40),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(0, 0), // anchor
                };
                let location = new google.maps.LatLng(Number(trackers[x].last_location[0].latitude), Number(trackers[x].last_location[0].longitude));
                this.map.setOptions(this.getMapOptions(location));
                let marker = new google.maps.Marker({
                    position: {
                        lat: Number(trackers[x].last_location[0].latitude),
                        lng: Number(trackers[x].last_location[0].longitude),
                    },
                    map: this.map,
                    title: trackers[x].tracker_name,
                    icon: iconR,
                });
                this.markers.push(marker);
                const infowindow = new google.maps.InfoWindow({
                    content: this.infoWindowContent(trackers[x]),
                });
                google.maps.event.addListener(marker, 'click', function () {
                    infowindow.open({
                        anchor: marker,
                        map: this.map,
                        shouldFocus: false,
                    });
                });
                this.showTrackerDetails(trackers[x]);
                if (x == trackers.length - 1) {
                    if (this.liveInfoInterval) {
                        clearInterval(this.liveInfoInterval);
                        this.liveTrack(trackers[x].imei);
                        this.selectedLiveTrack = trackers;
                    }
                    else {
                        this.liveTrack(trackers[x].imei);
                        this.selectedLiveTrack = trackers;
                    }
                }
            }
            this.displayMap = true;
            this.dataLoad = true;
            this.hasTrackerActivity = true;
        }
    }
    getTrackerLocations(tracker, filterBy) {
        let dataToSend = {
            imei: String(tracker.imei),
        };
        if (filterBy == 'last hour') {
            dataToSend['recent'] = 'last hour';
        }
        else if (filterBy == '8 hours') {
            dataToSend['recent'] = '8 hours';
        }
        else if (filterBy == '12 hours') {
            dataToSend['recent'] = '12 hours';
        }
        else if (filterBy == '24 hours') {
            dataToSend['recent'] = '24 hours';
        }
        else if (filterBy == 'today') {
            dataToSend['recent'] = 'today';
        }
        else if (filterBy == 'all') {
            dataToSend['recent'] = 'all';
        }
        this.rest
            .post(dataToSend, 'thorton/trackerData/recent')
            .subscribe((resp) => {
            this.hasTrackerActivity = true;
            this.trip = resp;
            this.trip['locations'] = this.removeIncorrectLocations(resp.locations);
            this.showMapPolyLines(this.trip);
            this.dataLoad = true;
        });
    }
    // eslint-disable-next-line @typescript-eslint/type-annotation-spacing
    showMapPolyLines(trip) {
        let flightPlanCoordinates = [];
        let path = [];
        let checkpoint = null;
        let icon = {
            url: '../assets/circle-solid.png',
            scaledSize: new google.maps.Size(10, 10),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(0, 0), // anchor
        };
        for (let x = 0; x < trip.locations.length - 1; x++) {
            flightPlanCoordinates.push({
                lat: Number(trip.locations[x].latitude),
                lng: Number(trip.locations[x].longitude),
            });
            if (x + 1 >= trip.locations.length) {
            }
            else {
                let source = {
                    lat: Number(trip.locations[x].latitude),
                    lng: Number(trip.locations[x].longitude),
                };
                if (x <= 50)
                    checkpoint = source;
                let destination = {
                    lat: Number(trip.locations[x + 1].latitude),
                    lng: Number(trip.locations[x + 1].longitude),
                };
                let distance = Number(this.calcCrow(checkpoint.lat, checkpoint.lng, source.lat, source.lng).toFixed(5)) * 1000;
                if (distance < 1) {
                    // continue;
                }
                else {
                    checkpoint = source;
                }
                let marker = new google.maps.Marker({
                    position: {
                        lat: Number(source.lat),
                        lng: Number(source.lng),
                    },
                    map: this.map,
                    title: trip.tracker_name,
                    icon: icon,
                });
                this.markers.push(marker);
                const infowindow = new google.maps.InfoWindow({
                    content: this.infoWindowContentMultipleMarker(trip, trip.locations[x]),
                });
                google.maps.event.addListener(marker, 'click', function () {
                    infowindow.open({
                        anchor: marker,
                        map: this.map,
                        shouldFocus: false,
                    });
                });
                path.push(source.lat + ',' + source.lng);
            }
            if (this.selectedTrackers.length == 1) {
                if (this.flightPath) {
                    this.flightPath.setMap(null);
                    this.flightPath.setPath(flightPlanCoordinates);
                    this.flightPath = new google.maps.Polyline({
                        path: flightPlanCoordinates,
                        geodesic: true,
                        strokeColor: trip.tracker_icon_color,
                        strokeOpacity: 3.0,
                        strokeWeight: 5,
                    });
                    this.flightPath.setMap(this.map);
                }
                else {
                    this.flightPath = new google.maps.Polyline({
                        path: flightPlanCoordinates,
                        geodesic: true,
                        strokeColor: trip.tracker_icon_color,
                        strokeOpacity: 3.0,
                        strokeWeight: 5,
                    });
                    this.flightPath.setMap(this.map);
                }
            }
            else if (this.selectedTrackers.length > 1) {
                if (this.flightPath) {
                    this.flightPath.setPath(flightPlanCoordinates);
                    this.flightPath = new google.maps.Polyline({
                        path: flightPlanCoordinates,
                        geodesic: true,
                        strokeColor: trip.tracker_icon_color,
                        strokeOpacity: 3.0,
                        strokeWeight: 5,
                    });
                    this.flightPath.setMap(this.map);
                }
                else {
                    this.flightPath = new google.maps.Polyline({
                        path: flightPlanCoordinates,
                        geodesic: true,
                        strokeColor: trip.tracker_icon_color,
                        strokeOpacity: 3.0,
                        strokeWeight: 5,
                    });
                    this.flightPath.setMap(this.map);
                }
            }
        }
    }
    removeIncorrectLocations(locations) {
        let filteredLocations = [];
        for (let x = 0; x < locations.length; x++) {
            if (!locations[x].latitude.startsWith('0') ||
                !locations[x].longitude.startsWith('0')) {
                filteredLocations.push(locations[x]);
            }
        }
        return filteredLocations;
    }
    getMapOptions(location, isZoomAndCenterRequired = true) {
        if (localStorage.getItem('mapStyle')) {
            this.mapStyle = localStorage.getItem('mapStyle');
        }
        else {
        }
        if (this.userSettings.map_style == 'roadmap') {
            this.mapOptions = {
                disableDefaultUI: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
            };
        }
        else if (this.userSettings.map_style == 'satellite') {
            this.mapOptions = {
                disableDefaultUI: true,
                mapTypeId: google.maps.MapTypeId.SATELLITE,
            };
        }
        else if (this.userSettings.map_style == 'hybrid') {
            this.mapOptions = {
                disableDefaultUI: true,
                mapTypeId: google.maps.MapTypeId.HYBRID,
            };
        }
        else {
            this.mapOptions = {
                disableDefaultUI: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
            };
        }
        if (isZoomAndCenterRequired) {
            this.mapOptions['zoom'] = 16;
            this.mapOptions['center'] = location;
        }
        return this.mapOptions;
    }
    infoWindowContent(data) {
        let dateTime = new Date(data.last_location[0].timestamp.replace(' ', 'T'));
        let contentString = '<div id="content">' +
            '<div id="siteNotice">' +
            '</div>' +
            '<h1 id="firstHeading" class="firstHeading">' +
            data.tracker_name +
            '</h1>' +
            '<div id="bodyContent">' +
            '<p><b>Date visited: </b>' +
            this.months[dateTime.getMonth()] +
            ' ' +
            dateTime.getDate() +
            ' ' +
            dateTime.getFullYear() +
            '<br><p><b>Time visited: </b>' +
            ('0' + dateTime.getHours()).slice(-2) +
            ':' +
            ('0' + dateTime.getMinutes()).slice(-2) +
            ':' +
            ('0' + dateTime.getSeconds()).slice(-2) +
            '<p><b>Position: </b>' +
            data.last_location[0].latitude +
            ',' +
            data.last_location[0].longitude;
        '</div>' + '</div>';
        return contentString;
    }
    infoWindowContentMultipleMarker(data, locations) {
        let dateTime = new Date(locations.timestamp);
        let contentString = '<div id="content">' +
            '<div id="siteNotice">' +
            '</div>' +
            '<h1 id="firstHeading" class="firstHeading">' +
            data.tracker_name +
            '</h1>' +
            '<div id="bodyContent">' +
            '<p><b>Date visited: </b>' +
            this.months[dateTime.getMonth()] +
            ' ' +
            dateTime.getDate() +
            ' ' +
            dateTime.getFullYear() +
            '<br><p><b>Time visited: </b>' +
            ('0' + dateTime.getHours()).slice(-2) +
            ':' +
            ('0' + dateTime.getMinutes()).slice(-2) +
            ':' +
            ('0' + dateTime.getSeconds()).slice(-2) +
            '<p><b>Position: </b>' +
            locations.latitude +
            ',' +
            locations.longitude;
        '</div>' + '</div>';
        return contentString;
    }
    calcCrow(latOne, lon1, latTwo, lon2) {
        let R = 6371; // km
        let dLat = this.toRad(latOne - latTwo);
        let dLon = this.toRad(lon2 - lon1);
        let lat1 = this.toRad(latOne);
        let lat2 = this.toRad(latTwo);
        let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
        let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        let d = R * c;
        return d;
    }
    // Converts numeric degrees to radians
    toRad(Value) {
        return (Value * Math.PI) / 180;
    }
    setMapOnAll(map) {
        for (let i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(map);
        }
    }
    showTrackerDetails(tracker) {
        this.trackerImeiSelected = tracker.imei;
        this.trackerDetails = tracker;
        this.trackerDetails.tracker_icon_url = this.global.getMarkerLocation(this.trackerDetails.tracker_icon, this.trackerDetails.tracker_icon_color);
        // this.liveTrack(tracker.imei)
        this.selectedLiveTrack = tracker;
        //this.liveTrack(tracker.imei)
        if (this.trackerDetails.last_location.length == 0) {
            this.initMap();
        }
        else {
            let location = new google.maps.LatLng(Number(tracker.last_location[0].latitude), Number(tracker.last_location[0].longitude));
            this.map.setCenter(location);
        }
    }
    presentModal() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__awaiter)(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _filter_filter_component__WEBPACK_IMPORTED_MODULE_5__.FilterComponent,
                cssClass: 'filter-modal',
            });
            modal.onDidDismiss().then((data) => {
                var _a, _b, _c, _d, _e;
                this.setMapOnAll(null);
                this.markers = [];
                this.userSettings = JSON.parse(localStorage.getItem('userSettings'));
                if (data.data) {
                    for (const st of data.data.selectedTracker) {
                        st.tracker_icon_url = this.global.getMarkerLocation(st.tracker_icon, st.tracker_icon_color);
                    }
                    this.selectedTrackers = data.data.selectedTracker;
                    (_a = this.owlElement) === null || _a === void 0 ? void 0 : _a.reInit();
                    if (((_b = this.selectedTrackers) === null || _b === void 0 ? void 0 : _b.length) != 0) {
                        if (data.data.filterHours == '') {
                            this.trackerHistorySetting = 'noHistory';
                            this.reInitMapPolyLine(this.selectedTrackers[this.selectedTrackers.length - 1]);
                            if (((_c = this.selectedTrackers) === null || _c === void 0 ? void 0 : _c.length) == 1) {
                                for (let x = 0; x < this.selectedTrackers.length; x++) {
                                    this.setMarkerForMap(this.selectedTrackers[x], 'single');
                                }
                            }
                            else {
                                this.setMarkerForMap(this.selectedTrackers, 'multiple');
                            }
                        }
                        else {
                            this.trackerHistorySetting = 'withHistory';
                            if (!this.selectedTrackers) {
                                console.log("no tracker found!!");
                            }
                            else if (((_d = this.selectedTrackers) === null || _d === void 0 ? void 0 : _d.length) == 1) {
                                // this.getTrackerLocations(this.selectedTrackers[0], data.data.filterHours)
                                this.reInitMapPolyLine(this.selectedTrackers[0]);
                                this.setMarkerForMap(this.selectedTrackers[0], 'single');
                            }
                            else {
                                this.reInitMapPolyLine(this.selectedTrackers[((_e = this.selectedTrackers) === null || _e === void 0 ? void 0 : _e.length) - 1]);
                                for (let x = 0; x < this.selectedTrackers.length; x++) {
                                    this.setMarkerForMap(this.selectedTrackers[x], 'single');
                                    // this.getTrackerLocations(this.selectedTrackers[x], data.data.filterHours)
                                }
                            }
                        }
                    }
                }
            });
            return yield modal.present();
        });
    }
    cantStartTrackingToast() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__awaiter)(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: 'Cannot start tracking, please try again later.',
                position: 'bottom',
                duration: 10000,
                color: 'danger',
            });
            toast.present();
        });
    }
    noActivityFoundToast() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__awaiter)(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: 'No activity found for this tracker',
                position: 'bottom',
                duration: 10000,
                color: 'danger',
            });
            toast.present();
        });
    }
    socketSubscriptionForActivities(imei) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__awaiter)(this, void 0, void 0, function* () {
            this.channelNewTrip = this.pusher.subscribe(`new-trip-${imei}`);
            this.channelNewTrip.bind('new-trip', (data) => {
                console.log('new-trip', data);
                localStorage.setItem('new-trip=================', JSON.stringify(data));
                this.toastCtrl
                    .create({
                    message: data.message,
                    position: 'bottom',
                    duration: 10000,
                    color: 'dark',
                })
                    .then((tost) => tost.present());
            });
            this.channelStop = this.pusher.subscribe(`tracker-stopped-${imei}`);
            this.channelStop.bind('tracker-stopped', (data) => {
                console.log('tracker stopped ======================', data);
                localStorage.setItem('tracker-stopped', JSON.stringify(data));
                this.toastCtrl
                    .create({
                    message: data.message,
                    position: 'bottom',
                    duration: 10000,
                    color: 'dark',
                })
                    .then((tost) => tost.present());
            });
            this.channelSpeed = this.pusher.subscribe(`speeding-event-${imei}`);
            this.channelSpeed.bind('speeding-event', (data) => {
                console.log('speeding====================', data);
                localStorage.setItem('speeding-event', JSON.stringify(data));
                this.toastCtrl
                    .create({
                    message: data.message,
                    position: 'bottom',
                    duration: 10000,
                    color: 'dark',
                })
                    .then((tost) => tost.present());
            });
            this.channelNotification = this.pusher.subscribe(`low-battery-channel-${imei}`);
            this.channelSpeed.bind('low-battery-event', (data) => {
                console.log('Low battery ===================', data);
                localStorage.setItem('low-battery-event', JSON.stringify(data));
                this.toastCtrl
                    .create({
                    message: 'Test message',
                    position: 'bottom',
                    duration: 10000,
                    color: 'danger',
                })
                    .then((tost) => tost.present());
            });
        });
    }
    ionViewDidLeave() {
        var _a, _b, _c, _d;
        clearInterval(this.liveInfoInterval);
        (_a = this.channelNewTrip) === null || _a === void 0 ? void 0 : _a.unsubscribe();
        (_b = this.channelStop) === null || _b === void 0 ? void 0 : _b.unsubscribe();
        (_c = this.channelSpeed) === null || _c === void 0 ? void 0 : _c.unsubscribe();
        (_d = this.channelNotification) === null || _d === void 0 ? void 0 : _d.unsubscribe();
    }
    recenterToMarker() {
        this.map.setOptions(this.getMapOptions(this.currentLocationLatandLng, true));
    }
    redirectActivate() {
        window.open('https://spytrack.com/', '_system');
    }
    drawPath(imei) {
        let bounds = new google.maps.LatLngBounds();
        const color = 'green';
        for (let i = 0; i < this.liveLocations[imei].length; i = i + 1) {
            let j = i + 1;
            if (this.liveLocations[imei][i] && this.liveLocations[imei][j]) {
                let pos = new google.maps.LatLng(this.liveLocations[imei][i].lat, this.liveLocations[imei][i].lng);
                let pos2 = new google.maps.LatLng(this.liveLocations[imei][j].lat, this.liveLocations[imei][j].lng);
                //bounds.extend(pos);
                bounds.extend(pos2);
                // poly.push(pos);
                new google.maps.Polyline({
                    path: [pos, pos2],
                    geodesic: true,
                    strokeColor: 'lightgray',
                    strokeOpacity: 1.0,
                    strokeWeight: 8,
                    map: this.map,
                });
                new google.maps.Polyline({
                    path: [pos, pos2],
                    geodesic: true,
                    strokeColor: color,
                    strokeOpacity: 1.0,
                    strokeWeight: 5,
                    map: this.map,
                });
            }
        }
        setTimeout(() => (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__awaiter)(this, void 0, void 0, function* () {
            this.displayMap = true;
            this.map.fitBounds(bounds);
            this.map.panToBounds(bounds);
        }), 100);
    }
};
LiveTrackingPage.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_8__.HttpClient },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__.ModalController },
    { type: _rest_service__WEBPACK_IMPORTED_MODULE_3__.RestService },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_10__.Router },
    { type: src_app_globals_service__WEBPACK_IMPORTED_MODULE_4__.GlobalService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__.ToastController },
    { type: _get_location_url_service__WEBPACK_IMPORTED_MODULE_2__.GetLocationUrlService }
];
LiveTrackingPage.propDecorators = {
    owlElement: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_11__.ViewChild, args: ['owlElement',] }],
    mapRef: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_11__.ViewChild, args: ['map', { read: _angular_core__WEBPACK_IMPORTED_MODULE_11__.ElementRef, static: false },] }]
};
LiveTrackingPage = (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_11__.Component)({
        selector: 'app-live-tracking',
        template: _raw_loader_live_tracking_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_live_tracking_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], LiveTrackingPage);



/***/ }),

/***/ 7240:
/*!******************************************************************!*\
  !*** ./src/app/pages/live-tracking/filter/filter.component.scss ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("#title {\n  width: 100%;\n  width: 100%;\n  margin: 0;\n  font-size: 24px;\n  color: black;\n  text-align: center;\n  font-weight: bolder;\n  color: white;\n}\n\n#tracker-history-card {\n  width: 100%;\n}\n\nimg {\n  height: 48px;\n  margin-right: 1em;\n}\n\n.alerts-label {\n  font-size: 12px;\n}\n\n.back-btn {\n  color: black;\n  font-size: 12px;\n  padding-left: 1em;\n}\n\nion-grid {\n  --ion-grid-padding-sm: 20px;\n  --ion-grid-padding-md: 30px;\n  --ion-grid-padding-lg: 40px;\n  --ion-grid-padding-xl: 40px;\n}\n\n.tracker-sample-view {\n  color: white;\n  font-size: 50px;\n  display: inline-block;\n  border-radius: 60px;\n  box-shadow: 0 0 2px #888;\n  padding: 0.5em 0.6em;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImZpbHRlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7RUFDQSxXQUFBO0VBQ0EsU0FBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QUFDSjs7QUFFQTtFQUNJLFdBQUE7QUFDSjs7QUFFQTtFQUNJLFlBQUE7RUFDQSxpQkFBQTtBQUNKOztBQUVBO0VBQ0ksZUFBQTtBQUNKOztBQUVBO0VBQ0ksWUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQUNKOztBQUVBO0VBQ0ksMkJBQUE7RUFDQSwyQkFBQTtFQUNBLDJCQUFBO0VBQ0EsMkJBQUE7QUFDSjs7QUFFQTtFQUNJLFlBQUE7RUFDQSxlQUFBO0VBQ0EscUJBQUE7RUFDQSxtQkFBQTtFQUNBLHdCQUFBO0VBQ0Esb0JBQUE7QUFDSiIsImZpbGUiOiJmaWx0ZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjdGl0bGUge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIG1hcmdpbjogMDtcbiAgICBmb250LXNpemU6IDI0cHg7XG4gICAgY29sb3I6IGJsYWNrO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBmb250LXdlaWdodDogYm9sZGVyO1xuICAgIGNvbG9yOiB3aGl0ZTtcbn1cblxuI3RyYWNrZXItaGlzdG9yeS1jYXJkIHtcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuaW1nIHtcbiAgICBoZWlnaHQ6IDQ4cHg7XG4gICAgbWFyZ2luLXJpZ2h0OiAxZW07XG59XG5cbi5hbGVydHMtbGFiZWwge1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbn1cblxuLmJhY2stYnRuIHtcbiAgICBjb2xvcjogYmxhY2s7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIHBhZGRpbmctbGVmdDogMWVtO1xufVxuXG5pb24tZ3JpZCB7XG4gICAgLS1pb24tZ3JpZC1wYWRkaW5nLXNtOiAyMHB4O1xuICAgIC0taW9uLWdyaWQtcGFkZGluZy1tZDogMzBweDtcbiAgICAtLWlvbi1ncmlkLXBhZGRpbmctbGc6IDQwcHg7XG4gICAgLS1pb24tZ3JpZC1wYWRkaW5nLXhsOiA0MHB4O1xufVxuXG4udHJhY2tlci1zYW1wbGUtdmlldyB7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGZvbnQtc2l6ZTogNTBweDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgYm9yZGVyLXJhZGl1czogNjBweDtcbiAgICBib3gtc2hhZG93OiAwIDAgMnB4ICM4ODg7XG4gICAgcGFkZGluZzogMC41ZW0gMC42ZW07XG59Il19 */");

/***/ }),

/***/ 8069:
/*!*************************************************************!*\
  !*** ./src/app/pages/live-tracking/live-tracking.page.scss ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("#map {\n  height: 100%;\n}\n\n#pref {\n  font-size: 1.5em;\n  padding-right: 10px;\n}\n\n#wrapper {\n  position: relative;\n}\n\n.over_map {\n  left: 10%;\n  right: 10%;\n  position: absolute;\n  z-index: 99;\n}\n\n#filterStatus {\n  font-size: 18px;\n  padding-right: 1em;\n}\n\n.new-background-color {\n  --background: #E7F7FA;\n}\n\np:hover {\n  background-color: #F3F3F3;\n}\n\n.spin {\n  position: fixed;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n}\n\nion-spinner {\n  width: 28px;\n  height: 28px;\n  stroke: #444;\n  fill: #222;\n}\n\n.over_map_below {\n  left: 10%;\n  right: 10%;\n  position: absolute;\n  z-index: 99;\n  bottom: 25px;\n  max-height: 225px;\n  height: 225px;\n  display: flex;\n  flex-direction: column;\n  margin: 0 !important;\n}\n\n.tracker-sample-view {\n  color: white;\n  font-size: 50px;\n  display: inline-block;\n  border-radius: 60px;\n  box-shadow: 0 0 2px #888;\n  padding: 0.5em 0.6em;\n}\n\n.custom-owl-item {\n  text-align: center !important;\n  box-shadow: 2px 0 5px -2px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxpdmUtdHJhY2tpbmcucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksWUFBQTtBQUNKOztBQUVBO0VBQ0ksZ0JBQUE7RUFDQSxtQkFBQTtBQUNKOztBQUNBO0VBQ0ksa0JBQUE7QUFFSjs7QUFBQTtFQUNJLFNBQUE7RUFDQSxVQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FBR0o7O0FBQUE7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7QUFHSjs7QUFBQTtFQUNJLHFCQUFBO0FBR0o7O0FBREE7RUFDRSx5QkFBQTtBQUlGOztBQURBO0VBQ0ksZUFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0VBQ0EsZ0NBQUE7QUFJSjs7QUFGQztFQUNHLFdBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7QUFLSjs7QUFGQztFQUNHLFNBQUE7RUFDQSxVQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsYUFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG9CQUFBO0FBS0o7O0FBRkM7RUFDRyxZQUFBO0VBQ0EsZUFBQTtFQUNBLHFCQUFBO0VBQ0EsbUJBQUE7RUFDQSx3QkFBQTtFQUNBLG9CQUFBO0FBS0o7O0FBREE7RUFDSSw2QkFBQTtFQUNBLDBCQUFBO0FBSUoiLCJmaWxlIjoibGl2ZS10cmFja2luZy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjbWFwIHtcbiAgICBoZWlnaHQ6IDEwMCU7XG59XG5cbiNwcmVmIHtcbiAgICBmb250LXNpemU6IDEuNWVtO1xuICAgIHBhZGRpbmctcmlnaHQ6IDEwcHg7XG59XG4jd3JhcHBlciB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLm92ZXJfbWFwIHsgXG4gICAgbGVmdDogMTAlO1xuICAgIHJpZ2h0OiAxMCU7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHotaW5kZXg6IDk5O1xufVxuXG4jZmlsdGVyU3RhdHVzIHtcbiAgICBmb250LXNpemU6IDE4cHg7XG4gICAgcGFkZGluZy1yaWdodDogMWVtO1xufVxuXG4ubmV3LWJhY2tncm91bmQtY29sb3J7XG4gICAgLS1iYWNrZ3JvdW5kOiAjRTdGN0ZBO1xufVxucDpob3ZlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNGM0YzRjM7XG59XG5cbi5zcGlue1xuICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICB0b3A6IDUwJTtcbiAgICBsZWZ0OiA1MCU7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG4gfVxuIGlvbi1zcGlubmVyIHtcbiAgICB3aWR0aDogMjhweDtcbiAgICBoZWlnaHQ6IDI4cHg7XG4gICAgc3Ryb2tlOiAjNDQ0O1xuICAgIGZpbGw6ICMyMjI7XG4gfVxuXG4gLm92ZXJfbWFwX2JlbG93IHtcbiAgICBsZWZ0OiAxMCU7XG4gICAgcmlnaHQ6IDEwJTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgei1pbmRleDogOTk7XG4gICAgYm90dG9tOiAyNXB4O1xuICAgIG1heC1oZWlnaHQ6IDIyNXB4O1xuICAgIGhlaWdodDogMjI1cHg7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIG1hcmdpbjogMCAhaW1wb3J0YW50O1xuIH1cblxuIC50cmFja2VyLXNhbXBsZS12aWV3IHtcbiAgICBjb2xvcjogd2hpdGU7XG4gICAgZm9udC1zaXplOiA1MHB4O1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBib3JkZXItcmFkaXVzOiA2MHB4O1xuICAgIGJveC1zaGFkb3c6IDAgMCAycHggIzg4ODtcbiAgICBwYWRkaW5nOiAwLjVlbSAwLjZlbTsgXG59XG5cblxuLmN1c3RvbS1vd2wtaXRlbSB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyICFpbXBvcnRhbnQ7XG4gICAgYm94LXNoYWRvdzogMnB4IDAgNXB4IC0ycHggXG59XG4iXX0= */");

/***/ }),

/***/ 5701:
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/live-tracking/filter/filter.component.html ***!
  \********************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\n  <ion-toolbar >\n    <ion-buttons slot=\"start\">\n      <ion-back-button  defaultHref=\"live-tracking\" (click)=\"closeModal()\"></ion-back-button>\n    </ion-buttons>\n    <div style=\"text-align: center;\">      \n      <span  style=\"margin-right: 50px ;\" ><strong>Filter</strong></span>\n    </div>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding\">\n  <ion-grid>\n    <ion-row>\n      <ion-card id=\"tracker-history-card\">\n        <ion-card-header>\n          <ion-card-subtitle>Select Tracker</ion-card-subtitle>\n          <!-- <p style=\"font-size: 12px;\">Select a tracker to track</p> -->\n        </ion-card-header>\n        <ion-card-content *ngIf=\"hasTracker\">\n          <ion-list>\n              <ion-item *ngFor=\"let tracker of userTrackers; let i = index\" >\n                <ion-label *ngIf=\"tracker.tracker_icon\">\n                  <!-- <fa-icon \n                  class=\"tracker-sample-view\" \n                  [icon]=\"tracker.tracker_icon\" \n                  style=\"font-size: 15px; background-color: {{ tracker.tracker_icon_color }}\">\n                  </fa-icon> -->\n                  <img class=\"tracker-icon-view\" [src]=\"tracker.tracker_icon_url\" style=\"width: 29  %; margin: 0 auto;\"/> \n                     \n                  {{ tracker.tracker_name }}\n                </ion-label>\n                <ion-label *ngIf=\"!tracker.tracker_icon\">{{ tracker.tracker_name }}</ion-label>\n                <ion-checkbox slot=\"start\" [value]=\"i\" checked=\"{{ tracker.isChecked }}\" (ionChange)=\"selectTrackerEvent($event, tracker, i)\"></ion-checkbox>\n              </ion-item>\n          </ion-list>\n        </ion-card-content>\n        <ion-card-content *ngIf=\"!hasTracker\">\n          No activated tracker\n        </ion-card-content>\n      </ion-card>\n\n      <ion-card id=\"tracker-history-card\">\n        <ion-card-header>\n          <ion-card-subtitle>Tracker History</ion-card-subtitle>\n        </ion-card-header>\n        <ion-card-content>\n          <ion-list>\n            <ion-radio-group [(value)]=\"defaultTrackerHistory\" mode=\"md\">\n              <ion-item>\n                <ion-label text-wrap><strong style=\"font-size: 18px;\">No history, just trackers</strong><p style=\"font-size: 12px;\">Hide tracker lines and alerts.<br>\n                Just show tracker icons where they are right now</p></ion-label>\n                <ion-radio slot=\"start\" value=\"0\" (ionFocus)=\"selectedHistory('0')\"></ion-radio>\n              </ion-item>\n              <ion-item>\n                <ion-label text-wrap><strong style=\"font-size: 18px;\">Trackers and History</strong><p style=\"font-size: 12px;\">Show tracker icons as well as map lines and alerts for a <br>\n                  specific time window</p></ion-label>\n                  <ion-radio slot=\"start\" value=\"1\" (ionFocus)=\"selectedHistory('1')\"></ion-radio>\n              </ion-item>\n            </ion-radio-group>\n          </ion-list>\n          <ion-grid *ngIf=\"defaultTrackerHistory == '1'\">\n            <ion-row>\n              <p style=\"font-weight: bolder;\">Select a time window to view recent activity</p>\n            </ion-row>\n            <ion-row>\n              <ion-radio-group [(value)]=\"defaultTrackerActivityFilter\" style=\"width: 100%;\" mode=\"md\">\n                <ion-item>\n                  <ion-label><p style=\"font-size: 12px;\">Last Hour</p></ion-label>\n                  <ion-radio slot=\"start\" value=\"1\" (ionFocus)=\"selectedActivityFilter('1')\"></ion-radio>\n                </ion-item>\n                <ion-item>\n                  <ion-label><p style=\"font-size: 12px;\">Last 8 Hours</p></ion-label>\n                  <ion-radio slot=\"start\" value=\"8\" (ionFocus)=\"selectedActivityFilter('8')\"></ion-radio>\n                </ion-item>\n                <ion-item>\n                  <ion-label><p style=\"font-size: 12px;\">Last 12 Hours</p></ion-label>\n                  <ion-radio slot=\"start\" value=\"12\" (ionFocus)=\"selectedActivityFilter('12')\"></ion-radio>\n                </ion-item>\n                <ion-item>\n                  <ion-label><p style=\"font-size: 12px;\">Last 24 Hours</p></ion-label>\n                  <ion-radio slot=\"start\" value=\"24\" (ionFocus)=\"selectedActivityFilter('24')\"></ion-radio>\n                </ion-item>\n                <ion-item>\n                  <ion-label><p style=\"font-size: 12px;\">Today</p></ion-label>\n                  <ion-radio slot=\"start\" value=\"today\" (ionFocus)=\"selectedActivityFilter('today')\"></ion-radio>\n                </ion-item>\n                <ion-item>\n                  <ion-label><p style=\"font-size: 12px;\">All</p></ion-label>\n                  <ion-radio slot=\"start\" value=\"0\" (ionFocus)=\"selectedActivityFilter('0')\"></ion-radio>\n                </ion-item>\n              </ion-radio-group>\n            </ion-row>\n            <br>\n            <ion-row>\n              <p style=\"font-weight: bolder;\">Alerts</p>\n            </ion-row>\n            <ion-row>\n              <p>Select the alert types you want to visualize in the map</p>\n            </ion-row>\n            <ion-row>\n              <ion-list>\n                <ion-item>\n                  <ion-label class=\"alerts-label\">Boundary Entry</ion-label>\n                  <ion-checkbox slot=\"start\" value=\"0\"></ion-checkbox>\n                </ion-item>\n                <ion-item>\n                  <ion-label class=\"alerts-label\">Boundary Exit</ion-label>\n                    <ion-checkbox slot=\"start\" value=\"1\"></ion-checkbox>\n                </ion-item>\n                <ion-item>\n                  <ion-label class=\"alerts-label\">Ignition Off</ion-label>\n                    <ion-checkbox slot=\"start\" value=\"2\"></ion-checkbox>\n                </ion-item>\n                <ion-item>\n                  <ion-label class=\"alerts-label\">Ignition On</ion-label>\n                    <ion-checkbox slot=\"start\" value=\"3\"></ion-checkbox>\n                </ion-item>\n                <ion-item>\n                  <ion-label class=\"alerts-label\">Low Battery</ion-label>\n                    <ion-checkbox slot=\"start\" value=\"4\"></ion-checkbox>\n                </ion-item>\n                <ion-item>\n                  <ion-label class=\"alerts-label\">SOS</ion-label>\n                    <ion-checkbox slot=\"start\" value=\"5\"></ion-checkbox>\n                </ion-item>\n                <ion-item>\n                  <ion-label class=\"alerts-label\">Speeding</ion-label>\n                    <ion-checkbox slot=\"start\" value=\"6\"></ion-checkbox>\n                </ion-item>\n                <ion-item>\n                  <ion-label class=\"alerts-label\">Trip Finished</ion-label>\n                    <ion-checkbox slot=\"start\" value=\"7\"></ion-checkbox>\n                </ion-item>\n                <ion-item>\n                  <ion-label class=\"alerts-label\">Trip Exceeds Distance</ion-label>\n                    <ion-checkbox slot=\"start\" value=\"8\"></ion-checkbox>\n                </ion-item>\n                <ion-item>\n                  <ion-label class=\"alerts-label\">Trip Exceeds Duration</ion-label>\n                    <ion-checkbox slot=\"start\" value=\"9\"></ion-checkbox>\n                </ion-item>\n                <ion-item>\n                  <ion-label class=\"alerts-label\">Trip Started</ion-label>\n                    <ion-checkbox slot=\"start\" value=\"10\"></ion-checkbox>\n                </ion-item>\n            </ion-list>\n            </ion-row>\n          </ion-grid>\n        </ion-card-content>\n      </ion-card>\n      <ion-card id=\"tracker-history-card\">\n        <ion-card-header>\n          <ion-card-subtitle>Map Overlays</ion-card-subtitle>\n          <p style=\"font-size: 12px;\">Augment your experience with rich map overlays</p>\n        </ion-card-header>\n        <ion-card-content>\n          <ion-list>\n              <ion-item>\n                <ion-label><ion-icon name=\"remove-circle-outline\"></ion-icon>Boundaries</ion-label>\n                <ion-checkbox slot=\"start\" value=\"0\"></ion-checkbox>\n              </ion-item>\n              <ion-item>\n                <ion-label><ion-icon name=\"car-sport-outline\"></ion-icon>Traffic</ion-label>\n                  <ion-checkbox slot=\"start\" value=\"1\"></ion-checkbox>\n              </ion-item>\n              <ion-item>\n                <ion-label> <ion-icon name=\"cloud-outline\"></ion-icon> Weather</ion-label>\n                  <ion-checkbox slot=\"start\" value=\"2\"></ion-checkbox>\n              </ion-item>\n          </ion-list>\n        </ion-card-content>\n      </ion-card>\n      <ion-card id=\"tracker-history-card\">\n        <ion-card-header>\n          <ion-card-subtitle>Map Style</ion-card-subtitle>\n          <p style=\"font-size: 12px;\">Choose your map style</p>\n        </ion-card-header>\n        <ion-card-content>\n          <ion-list>\n            <ion-radio-group [(value)]=\"defaultMapStyle\" mode=\"md\">\n              <ion-item style=\"margin-bottom: 1em;\">\n                <img src=\"../../../../assets/maps/light.png\"><ion-label>Roadmap</ion-label>\n                <ion-radio slot=\"start\" value=\"roadmap\" (ionFocus)=\"selectedMapType('roadmap')\"></ion-radio>\n              </ion-item>\n              <ion-item style=\"margin-bottom: 1em;\">\n                <img src=\"../../../../assets/maps/satelite.png\"><ion-label>Satellite</ion-label>\n                  <ion-radio slot=\"start\" value=\"satellite\" (ionFocus)=\"selectedMapType('satellite')\"></ion-radio>\n              </ion-item>\n              <!-- <ion-item style=\"margin-bottom: 1em;\">\n                <img src=\"../../../../assets/maps/satelite.png\"><ion-label>Satelite</ion-label>\n                  <ion-radio slot=\"start\" value=\"2\"></ion-radio>\n              </ion-item> -->\n              <ion-item style=\"margin-bottom: 1em;\">\n                <img src=\"../../../../assets/maps/terrain.jpg\"><ion-label>Hybrid</ion-label>\n                  <ion-radio slot=\"start\" value=\"hybrid\" (ionFocus)=\"selectedMapType('hybrid')\"></ion-radio>\n              </ion-item>\n            </ion-radio-group>\n          </ion-list>\n        </ion-card-content>\n      </ion-card>\n    </ion-row>\n  </ion-grid>\n\n</ion-content>\n\n<ion-footer>\n  <ion-toolbar>\n    <ion-button id=\"pref\" slot=\"start\" color=\"dark\" (click)=\"resetFilter()\">\n      Reset\n    </ion-button>\n    <ion-button slot=\"end\" color=\"dark\" (click)=\"applyChanges()\">\n      Apply filters\n    </ion-button>\n  </ion-toolbar>\n</ion-footer>");

/***/ }),

/***/ 725:
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/live-tracking/live-tracking.page.html ***!
  \***************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\n  <ion-toolbar>\n    <ion-title></ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button menu=\"main-menu\"></ion-menu-button>\n    </ion-buttons>\n    <ion-buttons id=\"pref\" slot=\"end\" (click)=\"presentModal()\">\n      <p id=\"filterStatus\">{{ dataFilterHours }}</p>\n      <ion-icon name=\"settings\"></ion-icon>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content [fullscreen]=\"true\" id=\"wrapper\">\n  <div class=\"spin\" *ngIf=\"!dataLoad\">\n    <ion-spinner name=\"lines\"></ion-spinner>\n  </div>\n  <ion-card class=\"ion-text-center over_map\" *ngIf=\"!hasTracker && dataLoad && !hasTrackerActivity\">\n    <ion-card-header>\n      <ion-card-title>Activate a Tracker</ion-card-title>\n    </ion-card-header>\n  \n    <ion-card-content>\n      Add a tracker to enjoy the full Spytrack GPS<br> experience.\n      <br>\n      <br>\n      <ion-grid>\n        <ion-row>\n          <ion-col (click)=\"redirectActivate()\">\n            <p style=\"color: deepskyblue;\n            font-weight: 800;\">Purchase a tracker</p>\n          </ion-col>\n          <ion-col [routerLink]=\"['/activation']\">\n            <p style=\"color: darkgreen;\n            font-weight: 800;\">Activate a tracker</p>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </ion-card-content>\n  </ion-card>\n  <ion-card class=\"ion-text-center over_map\" *ngIf=\"!hasTrackerActivity && dataLoad && hasTracker\">\n    <ion-card-header>\n      <ion-card-title>No tracker activity found</ion-card-title>\n    </ion-card-header>\n  \n    <ion-card-content>\n      Drive around your tracker to get the best experience. <br>\n      <br>\n      <br>\n    </ion-card-content>\n  </ion-card>\n  <ion-card class=\"ion-text-center over_map_below\" *ngIf=\"dataLoad && hasTracker\">\n    <!-- <ion-segment [(ngModel)]=\"trackerImeiSelected\" mode=\"md\">\n      <ion-segment-button value=\"friends\" value=\"{{tracker.imei}}\" *ngFor=\"let tracker of selectedTrackers; let i = index\" (click)=\"showTrackerDetails(tracker); liveTrack(tracker.imei)\">\n        <ion-label>{{ tracker.tracker_name }}</ion-label>\n      </ion-segment-button>\n    </ion-segment> -->\n\n    <owl-carousel\n    #owlElement\n    [options]=\"slideOptions\"\n    style=\"box-shadow: 2px 0 5px -2px \"\n    [carouselClasses]=\"['owl-theme', 'sliding']\"\n    *ngIf=\"selectedTrackers?.length > 0\"\n  >\n    <div\n      class=\"item custom-owl-item\"\n      *ngFor=\"let activityTracker of selectedTrackers; let index = index\"\n      (click)=\"showTrackerDetails(activityTracker); liveTrack(activityTracker.imei)\"\n    >\n      <div style=\"align-content: center\">\n        <div>\n          <img\n            class=\"tracker-icon-view\"\n            [src]=\"activityTracker.tracker_icon_url\"\n            style=\"width: 20%; margin: 0 auto\"\n          />\n        </div>\n        {{ activityTracker.tracker_name }}\n      </div>\n    </div>\n  </owl-carousel>\n    <ion-card-header *ngIf=\"trackerDetails\" style=\"text-align: center; padding: 5px 5px\">\n      <!-- <fa-icon (click)=\"recenterToMarker()\" class=\"tracker-sample-view\" [icon]=\"trackerDetails.tracker_icon\" style=\"font-size: 20px; background-color: {{ trackerDetails.tracker_icon_color }}\"></fa-icon> -->\n      <img class=\"tracker-icon-view\"  (click)=\"recenterToMarker()\" [src]=\"trackerDetails.tracker_icon_url\" style=\"width: 20%; margin: 0 auto;\"/> \n    </ion-card-header>\n    <ion-card-content style=\"text-align: left; overflow-y: auto;\">\n      <h2>{{ trackerDetails.description }}</h2>\n      <h2>{{ trackerDetails.complete_address }}</h2>\n      <p *ngIf=\"trackerDetails.last_location.length != 0\">{{ trackerDetails.last_location[0].time_diff }}</p>\n      <p *ngIf=\"trackerDetails.last_location.length == 0\">No registered activity</p>\n      <p>Battery level: {{ trackerDetails.battery_level }}%</p>\n    </ion-card-content>\n\n  </ion-card>\n  <div #map id=\"map\" [style.display]=\"displayMap ? 'block' : 'none'\">\n  </div>\n  <!-- <ion-fab vertical=\"bottom\" horizontal=\"end\" slot=\"fixed\">\n    <ion-fab-button (click)=\"startTrack()\" *ngIf=\"!startTracking && hasTracker\" color=\"danger\">\n      <ion-icon name=\"play\"></ion-icon>\n    </ion-fab-button>\n    <ion-fab-button (click)=\"stopTrack()\" *ngIf=\"startTracking\" color=\"danger\">\n      <ion-icon name=\"stop\"></ion-icon>\n    </ion-fab-button>\n  </ion-fab> -->\n</ion-content>\n");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_live-tracking_live-tracking_module_ts.js.map