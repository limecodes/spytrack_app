(self["webpackChunkspytrack"] = self["webpackChunkspytrack"] || []).push([["src_app_pages_activities_activities_module_ts"],{

/***/ 179:
/*!*************************************************************************!*\
  !*** ./src/app/components/activitypopover/activitypopover.component.ts ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ActivitypopoverComponent": () => (/* binding */ ActivitypopoverComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_activitypopover_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./activitypopover.component.html */ 1649);
/* harmony import */ var _activitypopover_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./activitypopover.component.scss */ 3950);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);



/* eslint-disable max-len */

let ActivitypopoverComponent = class ActivitypopoverComponent {
    constructor() { }
    ngOnInit() {
        console.log(this.data);
    }
    redirectToFinish() {
        console.log(this.data);
        console.log('finish taped');
        this.getDirection(this.data.locations[this.data.locations.length - 1].latitude, this.data.locations[this.data.locations.length - 1].longitude);
    }
    redirectToStart() {
        console.log(this.data);
        console.log('start taped');
        this.getDirection(this.data.locations[0].latitude, this.data.locations[0].longitude);
    }
    getDirection(latitude, longitude) {
        const url = `https://www.google.com/maps/search/?api=1&query=${latitude},${longitude}`;
        window.open(url, '_system');
    }
};
ActivitypopoverComponent.ctorParameters = () => [];
ActivitypopoverComponent.propDecorators = {
    data: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input, args: ['data',] }]
};
ActivitypopoverComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Component)({
        selector: 'app-activitypopover',
        template: _raw_loader_activitypopover_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_activitypopover_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], ActivitypopoverComponent);



/***/ }),

/***/ 1858:
/*!***************************************************************!*\
  !*** ./src/app/pages/activities/activities-routing.module.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ActivitiesPageRoutingModule": () => (/* binding */ ActivitiesPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _activities_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./activities.page */ 8065);
/* harmony import */ var _details_activities_detail_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./details/activities-detail.page */ 9494);





const routes = [
    {
        path: '',
        component: _activities_page__WEBPACK_IMPORTED_MODULE_0__.ActivitiesPage
    },
    {
        path: 'detail',
        component: _details_activities_detail_page__WEBPACK_IMPORTED_MODULE_1__.ActivitiesDetailPage
    }
];
let ActivitiesPageRoutingModule = class ActivitiesPageRoutingModule {
};
ActivitiesPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__.RouterModule],
    })
], ActivitiesPageRoutingModule);



/***/ }),

/***/ 3299:
/*!*******************************************************!*\
  !*** ./src/app/pages/activities/activities.module.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ActivitiesPageModule": () => (/* binding */ ActivitiesPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _activities_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./activities-routing.module */ 1858);
/* harmony import */ var _activities_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./activities.page */ 8065);
/* harmony import */ var _filter_filter_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./filter/filter.component */ 9973);
/* harmony import */ var _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @fortawesome/angular-fontawesome */ 5002);
/* harmony import */ var _details_activities_detail_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./details/activities-detail.page */ 9494);
/* harmony import */ var ngx_owl_carousel__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-owl-carousel */ 3189);
/* harmony import */ var ngx_owl_carousel__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(ngx_owl_carousel__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var src_app_components_activitypopover_activitypopover_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/components/activitypopover/activitypopover.component */ 179);
/* harmony import */ var src_app_PipesModule_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/PipesModule.module */ 9024);













let ActivitiesPageModule = class ActivitiesPageModule {
};
ActivitiesPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_9__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_10__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_11__.IonicModule,
            _activities_routing_module__WEBPACK_IMPORTED_MODULE_0__.ActivitiesPageRoutingModule,
            _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_12__.FontAwesomeModule,
            ngx_owl_carousel__WEBPACK_IMPORTED_MODULE_4__.OwlModule,
            src_app_PipesModule_module__WEBPACK_IMPORTED_MODULE_6__.PipesModule,
        ],
        declarations: [_activities_page__WEBPACK_IMPORTED_MODULE_1__.ActivitiesPage, _filter_filter_component__WEBPACK_IMPORTED_MODULE_2__.FilterComponent, _details_activities_detail_page__WEBPACK_IMPORTED_MODULE_3__.ActivitiesDetailPage, src_app_components_activitypopover_activitypopover_component__WEBPACK_IMPORTED_MODULE_5__.ActivitypopoverComponent]
    })
], ActivitiesPageModule);



/***/ }),

/***/ 8065:
/*!*****************************************************!*\
  !*** ./src/app/pages/activities/activities.page.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ActivitiesPage": () => (/* binding */ ActivitiesPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_activities_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./activities.page.html */ 1433);
/* harmony import */ var _activities_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./activities.page.scss */ 5552);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ 5257);
/* harmony import */ var src_app_globals_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/globals.service */ 3221);
/* harmony import */ var src_app_rest_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/rest.service */ 1881);
/* harmony import */ var _filter_filter_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./filter/filter.component */ 9973);
/* harmony import */ var _components_activitypopover_activitypopover_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/activitypopover/activitypopover.component */ 179);



/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/prefer-for-of */
/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable quote-props */
/* eslint-disable no-trailing-spaces */
/* eslint-disable @typescript-eslint/quotes */








let ActivitiesPage = class ActivitiesPage {
    constructor(router, modalController, rest, loadingController, globalService, popoverCtrl) {
        this.router = router;
        this.modalController = modalController;
        this.rest = rest;
        this.loadingController = loadingController;
        this.globalService = globalService;
        this.popoverCtrl = popoverCtrl;
        this.selectedTrackerData = {};
        this.slideOptions = { items: 2, dots: false };
        this.dateF = "EEE, MMM dd";
        this.recordFetchInReq = 5;
        this.isAllRecordLoaded = false;
    }
    ngOnInit() {
    }
    ionViewWillEnter() {
        this.activityFilterSubscription = this.globalService.currectSelectedAvtivityFilter.subscribe((newFilter) => {
            this.activityFilter = newFilter;
            console.log("activity filter ===>", this.activityFilter);
            this.getActivityData();
        });
    }
    getActivityData() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            this.loadingGbl = yield this.loadingController.create({
                cssClass: 'my-custom-class',
                message: 'Please wait...'
            });
            yield this.loadingGbl.present();
            const sendData = Object.assign({}, this.activityFilter);
            delete sendData.seletedTracker;
            if (this.activityFilter.seletedTracker !== 'all') {
                sendData.imei = this.activityFilter.seletedTracker.imei;
            }
            else {
                delete sendData.imei;
            }
            const startTime = new Date(sendData.start_date);
            const endTime = new Date(sendData.end_date);
            const startDate = startTime.getFullYear() + '-' + (startTime.getMonth() + 1) + '-' + startTime.getDate();
            const endDate = endTime.getFullYear() + '-' + (endTime.getMonth() + 1) + '-' + endTime.getDate();
            sendData.start_date = startDate;
            sendData.end_date = endDate;
            this.rest.get('thorton/trackers/activities?' + new URLSearchParams(sendData).toString(), '', '').pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_7__.take)(1)).subscribe((resp) => (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
                console.log("resp ====>", resp);
                if (resp.length !== 0) {
                    for (let x = 0; x < resp.length; x++) {
                        resp[x].tracker_icon_url = this.globalService.getMarkerLocation(resp[x].tracker_icon, resp[x].tracker_icon_color);
                    }
                    this.activitiesData = resp;
                    this.selectedTrackerData = this.activitiesData[0];
                    this.renderData();
                }
                else {
                    yield this.loadingGbl.dismiss();
                }
            }));
        });
    }
    renderData() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            this.filteredData = this.selectedTrackerData.data.slice(0);
            this.totalRecord = this.filteredData.length;
            this.displayActivityData = this.filteredData.slice(0, this.recordFetchInReq);
            if (this.filteredData.length < this.recordFetchInReq) {
                this.isAllRecordLoaded = true;
            }
            else {
                this.isAllRecordLoaded = false;
            }
            console.log("selectedTrackerData ===>", this.filteredData);
            console.log("total rec ===>", this.totalRecord);
            if (this.owlElement) {
                this.owlElement.reInit();
            }
            yield this.loadingGbl.dismiss();
        });
    }
    loadMoreData() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            // this.loadingGbl = await this.loadingController.create({
            //   cssClass: 'my-custom-class',
            //   message: 'Please wait...'
            // });
            // await this.loadingGbl.present();
            if (this.displayActivityData.length < this.totalRecord) {
                const newRecords = this.filteredData.slice(0).splice(this.displayActivityData.length - 1, this.recordFetchInReq);
                this.displayActivityData.push(...newRecords);
                if (this.displayActivityData.length >= this.totalRecord) {
                    console.log("all record loaded!!");
                    this.isAllRecordLoaded = true;
                }
            }
            else {
                console.log("all record loaded!!");
                this.isAllRecordLoaded = true;
            }
            //setTimeout(async ()=>{
            //  await this.loadingGbl.dismiss();
            //},200);
        });
    }
    presentModal() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _filter_filter_component__WEBPACK_IMPORTED_MODULE_4__.FilterComponent,
                cssClass: 'filter-modal'
            });
            return yield modal.present();
        });
    }
    activityClicked(activityData) {
        console.log(activityData);
        const requiredData = Object.assign({ 'tracker_icon': this.selectedTrackerData.tracker_icon, 'tracker_icon_color': this.selectedTrackerData.tracker_icon_color, 'tracker_name': this.selectedTrackerData.tracker_name }, activityData);
        this.globalService.userSelectedNewActivity(requiredData);
        this.router.navigate(['/activities/detail']);
    }
    changeSelectedTracker(index) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            this.loadingGbl = yield this.loadingController.create({
                cssClass: 'my-custom-class',
                message: 'Please wait...'
            });
            yield this.loadingGbl.present();
            console.log("tracker changed index is ", index);
            this.selectedTrackerData = this.activitiesData[index];
            this.renderData();
        });
    }
    notifications(ev, activityData) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            const popover = yield this.popoverCtrl.create({
                component: _components_activitypopover_activitypopover_component__WEBPACK_IMPORTED_MODULE_5__.ActivitypopoverComponent,
                event: ev,
                animated: true,
                showBackdrop: true,
                cssClass: 'pop-over-style',
                componentProps: { data: activityData }
            });
            return yield popover.present();
        });
    }
    ionViewDidLeave() {
        var _a;
        (_a = this.activityFilterSubscription) === null || _a === void 0 ? void 0 : _a.unsubscribe();
    }
};
ActivitiesPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__.Router },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__.ModalController },
    { type: src_app_rest_service__WEBPACK_IMPORTED_MODULE_3__.RestService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__.LoadingController },
    { type: src_app_globals_service__WEBPACK_IMPORTED_MODULE_2__.GlobalService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__.PopoverController }
];
ActivitiesPage.propDecorators = {
    owlElement: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_10__.ViewChild, args: ['owlElement',] }]
};
ActivitiesPage = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_10__.Component)({
        selector: 'app-activities',
        template: _raw_loader_activities_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_activities_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], ActivitiesPage);



/***/ }),

/***/ 9494:
/*!********************************************************************!*\
  !*** ./src/app/pages/activities/details/activities-detail.page.ts ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ActivitiesDetailPage": () => (/* binding */ ActivitiesDetailPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_activities_detail_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./activities-detail.page.html */ 3377);
/* harmony import */ var _activities_detail_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./activities-detail.page.scss */ 8692);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ 5257);
/* harmony import */ var src_app_globals_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/globals.service */ 3221);
/* harmony import */ var _components_activitypopover_activitypopover_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../components/activitypopover/activitypopover.component */ 179);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ 9895);



/* eslint-disable max-len */
/* eslint-disable eqeqeq */
/* eslint-disable no-var */
/* eslint-disable prefer-const */
/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable quote-props */
/* eslint-disable no-trailing-spaces */
/* eslint-disable @typescript-eslint/quotes */







let ActivitiesDetailPage = class ActivitiesDetailPage {
    constructor(loadingController, globalService, popoverCtrl, platform, toastCtrl, route) {
        this.loadingController = loadingController;
        this.globalService = globalService;
        this.popoverCtrl = popoverCtrl;
        this.platform = platform;
        this.toastCtrl = toastCtrl;
        this.route = route;
        this.dateF = "EEE, MMM dd";
        this.mapHeight = "400px";
    }
    ngOnInit() {
    }
    notifications(ev) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            const popover = yield this.popoverCtrl.create({
                component: _components_activitypopover_activitypopover_component__WEBPACK_IMPORTED_MODULE_3__.ActivitypopoverComponent,
                event: ev,
                animated: true,
                showBackdrop: true,
                cssClass: 'pop-over-style',
                componentProps: { data: this.activityData }
            });
            return yield popover.present();
        });
    }
    ionViewWillEnter() {
        console.log("called");
        this.isDisplay = false;
        this.activitySubscription = this.globalService.selectedActivity.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.take)(1)).subscribe((activity) => {
            console.log("activity data", activity);
            this.activityData = activity;
            setTimeout(() => {
                this.drawMap();
            }, 0);
        });
    }
    drawMap(selectedLocation) {
        var _a, _b, _c, _d;
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            if (((_b = (_a = this.activityData) === null || _a === void 0 ? void 0 : _a.locations) === null || _b === void 0 ? void 0 : _b.length) > 0) {
                const totalHeight = this.platform.height();
                const headerHeight = ((_c = document === null || document === void 0 ? void 0 : document.getElementById('header-a-d')) === null || _c === void 0 ? void 0 : _c.offsetHeight) || 0;
                const detailsHeight = ((_d = document === null || document === void 0 ? void 0 : document.getElementById('detail-card-a-d')) === null || _d === void 0 ? void 0 : _d.offsetHeight) || 0;
                this.mapHeight = `${totalHeight - headerHeight - detailsHeight - 20}px`;
                console.log(totalHeight, headerHeight, detailsHeight, this.mapHeight);
                const currentTrackerLocation = this.activityData.locations;
                let latu = currentTrackerLocation[0].latitude;
                let long = currentTrackerLocation[0].longitude;
                this.activityData.tracker_icon_url = this.globalService.getMarkerLocation(this.activityData.tracker_icon, this.activityData.tracker_icon_color);
                this.activityDetailMapRef = null;
                //this.flightPath = [];
                console.log(this.activityData);
                this.activityDetailMapRef = new google.maps.Map(document.getElementById("activityDetailMapDiv"), {
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    mapTypeControl: false,
                    streetViewControl: false,
                    keyboardShortcuts: false,
                });
                let bounds = new google.maps.LatLngBounds();
                var poly = [];
                let index = 0;
                for (let location of currentTrackerLocation) {
                    const latLong = { lat: parseFloat(location.latitude), lng: parseFloat(location.longitude) };
                    if (index == 0) {
                        new google.maps.Marker({
                            position: latLong,
                            map: this.activityDetailMapRef,
                            title: location.formatted_address,
                            clickable: true,
                            animation: google.maps.Animation.DROP,
                            icon: {
                                // url: "../assets/markers/location-arrow/location-arrow-229954.png", //SVG path of awesomefont marker
                                // fillOpacity: 1,
                                // strokeWeight: 0,
                                //  size: new google.maps.Size(30, 30),
                                //  scaledSize: new google.maps.Size(30, 30),
                                //  anchor: new google.maps.Point(0, 32)
                                path: google.maps.SymbolPath.CIRCLE,
                                scale: 4,
                                strokeColor: "#04dd04",
                                strokeWeight: 3,
                                fillColor: "white",
                                fillOpacity: 1
                            }
                        });
                    }
                    else if (currentTrackerLocation.length - 1 == index) {
                        new google.maps.Marker({
                            position: latLong,
                            map: this.activityDetailMapRef,
                            title: location.formatted_address,
                            clickable: true,
                            animation: google.maps.Animation.DROP,
                            icon: {
                                // url: "../assets/markers/location-arrow/location-arrow-229954.png", //SVG path of awesomefont marker
                                // fillOpacity: 1,
                                // strokeWeight: 0,
                                //  size: new google.maps.Size(30, 30),
                                //  scaledSize: new google.maps.Size(30, 30),
                                //  anchor: new google.maps.Point(0, 32)
                                path: google.maps.SymbolPath.CIRCLE,
                                scale: 4,
                                strokeColor: "#b72a2a",
                                strokeWeight: 3,
                                fillColor: "white",
                                fillOpacity: 1
                            }
                        });
                    }
                    else {
                        new google.maps.Marker({
                            position: latLong,
                            map: this.activityDetailMapRef,
                            title: location.formatted_address,
                            clickable: true,
                            animation: google.maps.Animation.DROP,
                            icon: {
                                // url: "../assets/markers/location-arrow/location-arrow.png", //SVG path of awesomefont marker
                                // fillOpacity: 1,
                                // strokeWeight: 0,
                                //  size: new google.maps.Size(30, 30),
                                //  scaledSize: new google.maps.Size(30, 30),
                                //  anchor: new google.maps.Point(0, 32)
                                path: google.maps.SymbolPath.CIRCLE,
                                scale: 4,
                                strokeColor: "#454745",
                                strokeWeight: 3,
                                fillColor: "white",
                                fillOpacity: 0.7
                            }
                        });
                    }
                    index++;
                }
                let color = this.activityData.tracker_icon_color;
                let isboundExtended = false;
                for (var i = 0; i < currentTrackerLocation.length; i = i + 1) {
                    let j = i + 1;
                    if (currentTrackerLocation[i] && currentTrackerLocation[j]) {
                        var pos = new google.maps.LatLng(currentTrackerLocation[i].latitude, currentTrackerLocation[i].longitude);
                        var pos2 = new google.maps.LatLng(currentTrackerLocation[j].latitude, currentTrackerLocation[j].longitude);
                        //bounds.extend(pos);
                        bounds.extend(pos2);
                        isboundExtended = true;
                        poly.push(pos);
                        new google.maps.Polyline({
                            path: [pos, pos2],
                            geodesic: true,
                            strokeColor: color,
                            strokeOpacity: 1.0,
                            strokeWeight: 8,
                            map: this.activityDetailMapRef,
                        });
                        // new google.maps.Polyline({
                        //   path: [pos,pos2],
                        //   geodesic: true,
                        //   strokeColor: color,
                        //   strokeOpacity: 1.0,
                        //   strokeWeight: 5,
                        //   map: this.activityDetailMapRef,
                        // });
                    }
                }
                console.log("bounds => ", bounds);
                if (!isboundExtended && currentTrackerLocation.length > 0) {
                    bounds.extend(new google.maps.LatLng(currentTrackerLocation[0].latitude, currentTrackerLocation[0].longitude));
                }
                try {
                    setTimeout(() => (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
                        //this.displayMap = true;
                        this.activityDetailMapRef.fitBounds(bounds);
                        this.activityDetailMapRef.panToBounds(bounds);
                        this.activityDetailMapRef.panTo({
                            lat: parseFloat(latu),
                            lng: parseFloat(long)
                        });
                        //await this.loadingGbl.dismiss();
                        this.isDisplay = true;
                    }), 100);
                }
                catch (ex) {
                    this.isDisplay = true;
                }
            }
            else {
                this.toastCtrl.create({
                    message: 'Error occured. something went wrong!!',
                    position: 'bottom',
                    duration: 4000,
                    color: 'danger'
                }).then((toast) => {
                    toast.present();
                    this.route.navigate(['/activities']);
                });
                return '';
            }
        });
    }
    ionViewDidLeave() {
        if (this.activitySubscription) {
            this.activitySubscription.unsubscribe();
        }
    }
};
ActivitiesDetailPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.LoadingController },
    { type: src_app_globals_service__WEBPACK_IMPORTED_MODULE_2__.GlobalService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.PopoverController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.Platform },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.ToastController },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__.Router }
];
ActivitiesDetailPage.propDecorators = {
    mapRef: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.ViewChild, args: ['mapDiv', { read: _angular_core__WEBPACK_IMPORTED_MODULE_8__.ElementRef, static: false },] }]
};
ActivitiesDetailPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Component)({
        selector: 'app-activities-detail',
        template: _raw_loader_activities_detail_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_activities_detail_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], ActivitiesDetailPage);



/***/ }),

/***/ 9973:
/*!*************************************************************!*\
  !*** ./src/app/pages/activities/filter/filter.component.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FilterComponent": () => (/* binding */ FilterComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_filter_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./filter.component.html */ 4856);
/* harmony import */ var _filter_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./filter.component.scss */ 1889);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var src_app_globals_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/globals.service */ 3221);
/* harmony import */ var src_app_rest_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/rest.service */ 1881);







let FilterComponent = class FilterComponent {
    constructor(globalService, rest, modalController, toastCtrl, loadingController) {
        this.globalService = globalService;
        this.rest = rest;
        this.modalController = modalController;
        this.toastCtrl = toastCtrl;
        this.loadingController = loadingController;
        this.dataLoad = false;
        this.defaultFilterType = 'all';
        this.defaultFilterTracker = 'all';
        this.fromDate = new Date().toISOString();
        this.toDate = new Date().toISOString();
    }
    ngOnInit() { }
    ionViewDidEnter() {
        this.getUserTracker();
        this.presentLoading();
    }
    presentLoading() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.gblLoading = yield this.loadingController.create({
                cssClass: 'my-custom-class',
                message: 'Please wait...',
            });
            yield this.gblLoading.present();
            //  const { role, data } = await loading.onDidDismiss();
            console.log('Loading dismissed!');
        });
    }
    closeModal() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            const onClosedData = 'Wrapped Up!';
            yield this.modalController.dismiss(onClosedData);
        });
    }
    getUserTracker() {
        this.rest.get('thorton/trackers/user', '', '').subscribe((resp) => {
            console.log('all user tracker', resp);
            if (resp.length !== 0) {
                this.userTrackers = resp;
                console.log(this.userTrackers);
                this.dataLoad = true;
                this.gblLoading.dismiss();
            }
            else {
                this.dataLoad = true;
                this.gblLoading.dismiss();
            }
        });
    }
    resetFilter() {
        this.globalService.setActivityFilterDefault();
        this.closeModal();
    }
    applyActivityFilters() {
        this.globalService.userChangedActivityFilter({
            seletedTracker: this.defaultFilterTracker,
            activity: this.defaultFilterType,
            start_date: this.fromDate,
            end_date: this.toDate,
        });
        this.closeModal();
    }
};
FilterComponent.ctorParameters = () => [
    { type: src_app_globals_service__WEBPACK_IMPORTED_MODULE_2__.GlobalService },
    { type: src_app_rest_service__WEBPACK_IMPORTED_MODULE_3__.RestService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.ModalController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.ToastController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.LoadingController }
];
FilterComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-filter',
        template: _raw_loader_filter_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_filter_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], FilterComponent);



/***/ }),

/***/ 3950:
/*!***************************************************************************!*\
  !*** ./src/app/components/activitypopover/activitypopover.component.scss ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".activity-popover-grid {\n  text-align: center;\n  padding: 0;\n}\n\n.activity-popover-card {\n  background-color: #efefef;\n}\n\n.cell-class {\n  border-color: lightgrey;\n  border-width: 0.01em;\n  border-style: solid;\n  margin-bottom: -1px;\n  padding: 12px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFjdGl2aXR5cG9wb3Zlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFBO0VBQ0EsVUFBQTtBQUNKOztBQUVBO0VBQ0kseUJBQUE7QUFDSjs7QUFFQTtFQUNJLHVCQUFBO0VBQ0Esb0JBQUE7RUFDQSxtQkFBQTtFQUFvQixtQkFBQTtFQUNwQixhQUFBO0FBRUoiLCJmaWxlIjoiYWN0aXZpdHlwb3BvdmVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmFjdGl2aXR5LXBvcG92ZXItZ3JpZCB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHBhZGRpbmc6IDA7XG59XG5cbi5hY3Rpdml0eS1wb3BvdmVyLWNhcmQge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICNlZmVmZWY7XG59XG5cbi5jZWxsLWNsYXNzIHtcbiAgICBib3JkZXItY29sb3I6IGxpZ2h0Z3JleTsgXG4gICAgYm9yZGVyLXdpZHRoOiAuMDFlbTsgXG4gICAgYm9yZGVyLXN0eWxlOnNvbGlkOyBtYXJnaW4tYm90dG9tIDogLTFweDtcbiAgICBwYWRkaW5nOiAxMnB4O1xuICB9Il19 */");

/***/ }),

/***/ 5552:
/*!*******************************************************!*\
  !*** ./src/app/pages/activities/activities.page.scss ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("#filter {\n  font-size: 21px;\n  padding-right: 0.5em;\n}\n\nh4 {\n  text-align: center;\n  width: 100%;\n}\n\n.activity-header {\n  border-bottom: 1px solid lightgrey;\n  padding: 5px;\n  height: 60px;\n  margin: 2px 10px;\n}\n\n.activity-header-avatar {\n  float: left;\n}\n\n.activity-header-avatar ion-avatar {\n  width: 40px;\n}\n\n.activity-header-avatar img {\n  width: 30px;\n  height: 30px;\n}\n\n.activity-header-name-div {\n  float: left;\n}\n\n.activity-header-type {\n  font-weight: 850;\n  text-transform: capitalize;\n  color: #000;\n}\n\n.activity-header-time {\n  float: right;\n  color: #000;\n}\n\n.activity-content-avatar {\n  float: left;\n}\n\n.activity-location {\n  float: left;\n}\n\n.activity-content-time-div {\n  float: right;\n  text-align: right;\n}\n\n.activity-content-avatar ion-avatar {\n  width: 40px;\n  height: 50px;\n}\n\n.activity-content-avatar img {\n  width: 30px;\n  height: 30px;\n}\n\n.activity-card-content {\n  display: inline-block;\n  width: 100%;\n  padding-top: 2px;\n  padding-bottom: 2px;\n}\n\n.activity-header-distance {\n  margin-right: 10px;\n}\n\n.ellipsis-icon-menu {\n  margin-left: 8px;\n}\n\n.activity-location-1 {\n  color: #000;\n}\n\n.activity-content-time {\n  color: #000;\n}\n\n.btn-row {\n  width: 100%;\n  margin-bottom: 5px;\n}\n\n.load-more-btn {\n  margin: 0 auto 10px auto;\n  text-align: center;\n}\n\n.custom-owl-item {\n  text-align: center !important;\n  box-shadow: 2px 0 5px -2px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFjdGl2aXRpZXMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksZUFBQTtFQUNBLG9CQUFBO0FBQ0o7O0FBRUE7RUFDSSxrQkFBQTtFQUNBLFdBQUE7QUFDSjs7QUFJQTtFQUNJLGtDQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtBQURKOztBQUlBO0VBQ0ksV0FBQTtBQURKOztBQUlBO0VBQ0ksV0FBQTtBQURKOztBQUlBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUFESjs7QUFJQTtFQUNJLFdBQUE7QUFESjs7QUFJQTtFQUNJLGdCQUFBO0VBQ0EsMEJBQUE7RUFDQSxXQUFBO0FBREo7O0FBS0E7RUFDSSxZQUFBO0VBQ0EsV0FBQTtBQUZKOztBQU1BO0VBRUksV0FBQTtBQUpKOztBQU9BO0VBQ0ksV0FBQTtBQUpKOztBQU9BO0VBQ0ksWUFBQTtFQUNBLGlCQUFBO0FBSko7O0FBT0E7RUFDSSxXQUFBO0VBQ0EsWUFBQTtBQUpKOztBQU9BO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUFKSjs7QUFRQTtFQUNJLHFCQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7QUFMSjs7QUFRQTtFQUNJLGtCQUFBO0FBTEo7O0FBUUE7RUFDSSxnQkFBQTtBQUxKOztBQVFBO0VBQ0ksV0FBQTtBQUxKOztBQVFBO0VBQ0ksV0FBQTtBQUxKOztBQVFBO0VBQ0ksV0FBQTtFQUNBLGtCQUFBO0FBTEo7O0FBUUE7RUFDSSx3QkFBQTtFQUNBLGtCQUFBO0FBTEo7O0FBUUE7RUFDSSw2QkFBQTtFQUNBLDBCQUFBO0FBTEoiLCJmaWxlIjoiYWN0aXZpdGllcy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjZmlsdGVyIHtcbiAgICBmb250LXNpemU6IDIxcHg7XG4gICAgcGFkZGluZy1yaWdodDogMC41ZW07XG59XG5cbmg0IHtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgd2lkdGg6IDEwMCU7XG59XG5cblxuXG4uYWN0aXZpdHktaGVhZGVye1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyZXk7XG4gICAgcGFkZGluZzogNXB4O1xuICAgIGhlaWdodDogNjBweDtcbiAgICBtYXJnaW46IDJweCAxMHB4O1xufVxuXG4uYWN0aXZpdHktaGVhZGVyLWF2YXRhciB7XG4gICAgZmxvYXQ6IGxlZnQ7XG59XG5cbi5hY3Rpdml0eS1oZWFkZXItYXZhdGFyIGlvbi1hdmF0YXIge1xuICAgIHdpZHRoOiA0MHB4O1xufVxuXG4uYWN0aXZpdHktaGVhZGVyLWF2YXRhciBpbWcge1xuICAgIHdpZHRoOiAzMHB4O1xuICAgIGhlaWdodDogMzBweDtcbn1cblxuLmFjdGl2aXR5LWhlYWRlci1uYW1lLWRpdiB7XG4gICAgZmxvYXQ6IGxlZnQ7XG59XG5cbi5hY3Rpdml0eS1oZWFkZXItdHlwZSB7XG4gICAgZm9udC13ZWlnaHQ6IDg1MDtcbiAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgICBjb2xvcjogIzAwMDtcbn1cblxuXG4uYWN0aXZpdHktaGVhZGVyLXRpbWUge1xuICAgIGZsb2F0OiByaWdodDtcbiAgICBjb2xvcjogIzAwMDtcbn1cblxuXG4uYWN0aXZpdHktY29udGVudC1hdmF0YXIge1xuICAgIC8vZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIGZsb2F0OiBsZWZ0O1xufVxuXG4uYWN0aXZpdHktbG9jYXRpb24ge1xuICAgIGZsb2F0OiBsZWZ0O1xufVxuXG4uYWN0aXZpdHktY29udGVudC10aW1lLWRpdiB7XG4gICAgZmxvYXQ6IHJpZ2h0O1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xufVxuXG4uYWN0aXZpdHktY29udGVudC1hdmF0YXIgaW9uLWF2YXRhciB7XG4gICAgd2lkdGg6IDQwcHg7XG4gICAgaGVpZ2h0OiA1MHB4O1xufVxuXG4uYWN0aXZpdHktY29udGVudC1hdmF0YXIgaW1nIHtcbiAgICB3aWR0aDogMzBweDtcbiAgICBoZWlnaHQ6IDMwcHg7XG59XG5cblxuLmFjdGl2aXR5LWNhcmQtY29udGVudCB7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHBhZGRpbmctdG9wOiAycHg7XG4gICAgcGFkZGluZy1ib3R0b206IDJweDtcbn1cblxuLmFjdGl2aXR5LWhlYWRlci1kaXN0YW5jZSB7XG4gICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xufVxuXG4uZWxsaXBzaXMtaWNvbi1tZW51IHtcbiAgICBtYXJnaW4tbGVmdDogOHB4O1xufVxuXG4uYWN0aXZpdHktbG9jYXRpb24tMXtcbiAgICBjb2xvcjogIzAwMFxufVxuXG4uYWN0aXZpdHktY29udGVudC10aW1lIHtcbiAgICBjb2xvcjogIzAwMDtcbn1cblxuLmJ0bi1yb3d7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgbWFyZ2luLWJvdHRvbTogNXB4O1xufVxuXG4ubG9hZC1tb3JlLWJ0biB7XG4gICAgbWFyZ2luOiAwIGF1dG8gMTBweCBhdXRvO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmN1c3RvbS1vd2wtaXRlbSB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyICFpbXBvcnRhbnQ7XG4gICAgYm94LXNoYWRvdzogMnB4IDAgNXB4IC0ycHggXG59XG4iXX0= */");

/***/ }),

/***/ 8692:
/*!**********************************************************************!*\
  !*** ./src/app/pages/activities/details/activities-detail.page.scss ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".activity-header {\n  border-bottom: 1px solid lightgrey;\n  padding: 5px;\n  height: 60px;\n  margin: 2px 10px;\n}\n\n.activity-header-avatar {\n  float: left;\n}\n\n.activity-header-avatar ion-avatar {\n  width: 40px;\n}\n\n.activity-header-avatar img {\n  width: 30px;\n  height: 30px;\n}\n\n.activity-header-name-div {\n  float: left;\n}\n\n.activity-header-type {\n  font-weight: 850;\n  text-transform: capitalize;\n  color: #000;\n}\n\n.activity-header-time {\n  float: right;\n  color: #000;\n}\n\n.activity-content-avatar {\n  float: left;\n}\n\n.activity-location {\n  float: left;\n}\n\n.activity-content-time-div {\n  float: right;\n  text-align: right;\n}\n\n.activity-content-avatar ion-avatar {\n  width: 40px;\n  height: 50px;\n}\n\n.activity-content-avatar img {\n  width: 30px;\n  height: 30px;\n}\n\n.activity-card-content {\n  display: inline-block;\n  width: 100%;\n  padding-top: 2px;\n  padding-bottom: 2px;\n}\n\n.activity-header-distance {\n  margin-right: 10px;\n}\n\n.ellipsis-icon-menu {\n  margin-left: 8px;\n}\n\n.activity-location-1 {\n  color: #000;\n}\n\n.activity-content-time {\n  color: #000;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFjdGl2aXRpZXMtZGV0YWlsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtDQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtBQUNKOztBQUVBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUFDSjs7QUFFQTtFQUNJLFdBQUE7QUFDSjs7QUFFQTtFQUNJLGdCQUFBO0VBQ0EsMEJBQUE7RUFDQSxXQUFBO0FBQ0o7O0FBR0E7RUFDSSxZQUFBO0VBQ0EsV0FBQTtBQUFKOztBQUlBO0VBRUksV0FBQTtBQUZKOztBQUtBO0VBQ0ksV0FBQTtBQUZKOztBQUtBO0VBQ0ksWUFBQTtFQUNBLGlCQUFBO0FBRko7O0FBS0E7RUFDSSxXQUFBO0VBQ0EsWUFBQTtBQUZKOztBQUtBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUFGSjs7QUFNQTtFQUNJLHFCQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7QUFISjs7QUFNQTtFQUNJLGtCQUFBO0FBSEo7O0FBTUE7RUFDSSxnQkFBQTtBQUhKOztBQU1BO0VBQ0ksV0FBQTtBQUhKOztBQU1BO0VBQ0ksV0FBQTtBQUhKIiwiZmlsZSI6ImFjdGl2aXRpZXMtZGV0YWlsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5hY3Rpdml0eS1oZWFkZXJ7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGxpZ2h0Z3JleTtcbiAgICBwYWRkaW5nOiA1cHg7XG4gICAgaGVpZ2h0OiA2MHB4O1xuICAgIG1hcmdpbjogMnB4IDEwcHg7XG59XG5cbi5hY3Rpdml0eS1oZWFkZXItYXZhdGFyIHtcbiAgICBmbG9hdDogbGVmdDtcbn1cblxuLmFjdGl2aXR5LWhlYWRlci1hdmF0YXIgaW9uLWF2YXRhciB7XG4gICAgd2lkdGg6IDQwcHg7XG59XG5cbi5hY3Rpdml0eS1oZWFkZXItYXZhdGFyIGltZyB7XG4gICAgd2lkdGg6IDMwcHg7XG4gICAgaGVpZ2h0OiAzMHB4O1xufVxuXG4uYWN0aXZpdHktaGVhZGVyLW5hbWUtZGl2IHtcbiAgICBmbG9hdDogbGVmdDtcbn1cblxuLmFjdGl2aXR5LWhlYWRlci10eXBlIHtcbiAgICBmb250LXdlaWdodDogODUwO1xuICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICAgIGNvbG9yOiAjMDAwO1xufVxuXG5cbi5hY3Rpdml0eS1oZWFkZXItdGltZSB7XG4gICAgZmxvYXQ6IHJpZ2h0O1xuICAgIGNvbG9yOiAjMDAwO1xufVxuXG5cbi5hY3Rpdml0eS1jb250ZW50LWF2YXRhciB7XG4gICAgLy9kaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgZmxvYXQ6IGxlZnQ7XG59XG5cbi5hY3Rpdml0eS1sb2NhdGlvbiB7XG4gICAgZmxvYXQ6IGxlZnQ7XG59XG5cbi5hY3Rpdml0eS1jb250ZW50LXRpbWUtZGl2IHtcbiAgICBmbG9hdDogcmlnaHQ7XG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XG59XG5cbi5hY3Rpdml0eS1jb250ZW50LWF2YXRhciBpb24tYXZhdGFyIHtcbiAgICB3aWR0aDogNDBweDtcbiAgICBoZWlnaHQ6IDUwcHg7XG59XG5cbi5hY3Rpdml0eS1jb250ZW50LWF2YXRhciBpbWcge1xuICAgIHdpZHRoOiAzMHB4O1xuICAgIGhlaWdodDogMzBweDtcbn1cblxuXG4uYWN0aXZpdHktY2FyZC1jb250ZW50IHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgcGFkZGluZy10b3A6IDJweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMnB4O1xufVxuXG4uYWN0aXZpdHktaGVhZGVyLWRpc3RhbmNlIHtcbiAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XG59XG5cbi5lbGxpcHNpcy1pY29uLW1lbnUge1xuICAgIG1hcmdpbi1sZWZ0OiA4cHg7XG59XG5cbi5hY3Rpdml0eS1sb2NhdGlvbi0xe1xuICAgIGNvbG9yOiAjMDAwXG59XG5cbi5hY3Rpdml0eS1jb250ZW50LXRpbWUge1xuICAgIGNvbG9yOiAjMDAwO1xufVxuXG4jYWN0aXZpdHlEZXRhaWxNYXBEaXZ7XG4gICAgLy8gaGVpZ2h0OiAzNjBweDtcbn0iXX0= */");

/***/ }),

/***/ 1889:
/*!***************************************************************!*\
  !*** ./src/app/pages/activities/filter/filter.component.scss ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("ion-card {\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImZpbHRlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFdBQUE7QUFDSiIsImZpbGUiOiJmaWx0ZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY2FyZCB7XG4gICAgd2lkdGg6IDEwMCU7XG59Il19 */");

/***/ }),

/***/ 1649:
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/activitypopover/activitypopover.component.html ***!
  \*****************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!-- <ion-list class=\"ion-no-margin ion-no-padding\">  \n  <ion-list-header color=\"dark\" class=\"text-10\">  \n    <ion-label class=\"fw700\">Notifications</ion-label>  \n  </ion-list-header>  \n\n  <ion-item color=\"secondary\" class=\"text-10\">  \n    <ion-icon name=\"mail\" color=\"light\"></ion-icon>  \n    <ion-label>New Offer 30% OFF</ion-label>  \n  </ion-item>  \n\n  <ion-item color=\"light\" class=\"text-10\">  \n    <ion-icon name=\"mail-open\" color=\"primary\"></ion-icon>  \n    <ion-label>New Offer 15% OFF by month!</ion-label>  \n  </ion-item>  \n\n  <ion-item color=\"secondary\" class=\"text-10\">  \n    <ion-icon name=\"mail\" color=\"light\"></ion-icon>  \n    <ion-label>New Offer 45% OFF</ion-label>  \n  </ion-item>  \n\n  <ion-item color=\"light\" class=\"text-10\">  \n    <ion-icon name=\"mail-open\" color=\"primary\"></ion-icon>  \n    <ion-label>New Offer 25% OFF on Credit Card!</ion-label>  \n  </ion-item>  \n\n  <ion-item color=\"light\" class=\"text-10\">  \n    <ion-icon name=\"mail-open\" color=\"primary\"></ion-icon>  \n    <ion-label>New Offer 20% OFF by month!</ion-label>  \n  </ion-item>  \n</ion-list>   -->\n\n<ion-card class=\"activity-popover-card\">\n  <ion-grid class=\"activity-popover-grid\">\n    <ion-row>\n      <ion-col  class=\"cell-class\">\n        <div>\n        <fa-icon  class=\"tracker-sample-view\" icon=\"diamond-turn-right\" style=\"margin-right: 10px;\"></fa-icon>\n      </div>\n      <div>\n        Direction \n      </div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"cell-class\" (click)=\"redirectToStart()\" > To Start </ion-col>\n      <ion-col  class=\"cell-class\" (click)=\"redirectToFinish()\"> To Finish </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-card>");

/***/ }),

/***/ 1433:
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/activities/activities.page.html ***!
  \*********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>Activities </ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-menu-button menu=\"main-menu\"></ion-menu-button>\n    </ion-buttons>\n    <ion-buttons id=\"filter\" slot=\"end\" (click)=\"presentModal()\">\n      <ion-icon name=\"settings\"></ion-icon>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n<ion-content\n  *ngIf=\"selectedTrackerData && selectedTrackerData?.imei; else noTrackerData\"\n>\n  <ion-card style=\"margin-top: 5px\">\n    <owl-carousel\n      #owlElement\n      [options]=\"slideOptions\"\n      [carouselClasses]=\"['owl-theme', 'sliding']\"\n      *ngIf=\"activitiesData?.length > 1\"\n    >\n      <div\n        class=\"item custom-owl-item\"\n        *ngFor=\"let activityTracker of activitiesData; let index = index\"\n        (click)=\"changeSelectedTracker(index)\"\n      >\n        <div style=\"align-content: center\">\n          <div>\n            <img\n              class=\"tracker-icon-view\"\n              [src]=\"activityTracker.tracker_icon_url\"\n              style=\"width: 20%; margin: 0 auto\"\n            />\n          </div>\n          {{ activityTracker.tracker_name }}\n        </div>\n      </div>\n    </owl-carousel>\n  </ion-card>\n\n  <ion-card *ngFor=\"let activityData of displayActivityData\" class=\"activity\">\n    <ion-card-header class=\"activity-header\">\n      <div>\n        <div\n          class=\"activity-header-avatar\"\n          (click)=\"activityClicked(activityData)\"\n        >\n          <ion-avatar>\n            <img\n              *ngIf=\"activityData.location_type === 'stop'; else trimIcon\"\n              src=\"../assets/stop_trip.jpeg\"\n              width=\"30px\"\n              height=\"30px\"\n            />\n            <ng-template #trimIcon\n              ><img src=\"../assets/trip.jpeg\" width=\"30px\" height=\"30px\"\n            /></ng-template>\n          </ion-avatar>\n        </div>\n        <div\n          class=\"activity-header-name-div\"\n          (click)=\"activityClicked(activityData)\"\n        >\n          <div class=\"activity-header-type\">{{activityData.location_type}}</div>\n          <div class=\"activity-header-name\">{{activityData.tracker_name}}</div>\n        </div>\n        <div class=\"activity-header-time\">\n          <span (click)=\"activityClicked(activityData)\">\n            {{activityData.time}}</span\n          >\n          <span\n            class=\"ellipsis-icon-menu\"\n            (click)=\"notifications($event,activityData)\"\n          >\n            <fa-icon\n              class=\"tracker-sample-view\"\n              icon=\"ellipsis\"\n              style=\"margin-right: 10px\"\n            ></fa-icon>\n          </span>\n        </div>\n      </div>\n    </ion-card-header>\n    <div style=\"clr: both\"></div>\n    <ion-card-content\n      *ngIf=\"activityData?.locations.length > 0\"\n      class=\"activity-card-content\"\n      (click)=\"activityClicked(activityData)\"\n    >\n      <div class=\"activity-content-avatar\">\n        <ion-avatar>\n          <img\n            *ngIf=\"activityData.location_type === 'stop'; else locIcon\"\n            src=\"../assets/stop_point_map.jpeg\"\n            width=\"30px\"\n            height=\"30px\"\n            style=\"width: 22px\"\n          />\n          <ng-template #locIcon\n            ><img src=\"../assets/start_start.jpeg\" width=\"30px\" height=\"30px\"\n          /></ng-template>\n        </ion-avatar>\n      </div>\n      <div class=\"activity-location\">\n        <div class=\"activity-location-1\">\n          {{activityData.locations[0].formatted_address.split(\",\")[0]}}\n        </div>\n        <div class=\"activity-location-2\">\n          {{activityData.locations[0].formatted_address.split(\",\")[1]}},{{activityData.locations[0].formatted_address.split(\",\")[2]}}\n        </div>\n      </div>\n      <div class=\"activity-content-time-div\">\n        <div class=\"activity-content-time\">\n          {{activityData.locations[0].timestamp | dateTras | date :\n          'shortTime'}}\n        </div>\n        <div class=\"activity-content-date\">\n          {{activityData.locations[0].timestamp | dateTras | date: dateF}}\n        </div>\n      </div>\n    </ion-card-content>\n    <ion-card-content\n      *ngIf=\"activityData?.locations.length > 1\"\n      class=\"activity-card-content\"\n    >\n      <div class=\"activity-content-avatar\">\n        <ion-avatar>\n          <img src=\"../assets/stop_end.jpeg\" width=\"30px\" height=\"30px\" />\n        </ion-avatar>\n      </div>\n      <div class=\"activity-location\">\n        <div class=\"activity-location-1\">\n          {{activityData.locations[activityData.locations.length -\n          1].formatted_address.split(\",\")[0]}}\n        </div>\n        <div class=\"activity-location-2\">\n          {{activityData.locations[activityData.locations.length -\n          1].formatted_address.split(\",\")[1]}},{{activityData.locations[0].formatted_address.split(\",\")[2]}}\n        </div>\n      </div>\n      <div class=\"activity-content-time-div\">\n        <div class=\"activity-content-time\">\n          {{activityData.locations[activityData.locations.length - 1].timestamp\n          | dateTras | date : 'shortTime'}}\n        </div>\n        <div class=\"activity-content-date\">\n          {{activityData.locations[activityData.locations.length - 1].timestamp\n          | dateTras | date : dateF}}\n        </div>\n      </div>\n    </ion-card-content>\n  </ion-card>\n\n  <ion-card *ngIf=\"displayActivityData.length <= 0\" class=\"activity\">\n    <ion-card-content> No alerts found on this tracker for this filter.</ion-card-content>\n  </ion-card>\n\n  <ion-row>\n    <ion-button\n      class=\"load-more-btn\"\n      color=\"dark\"\n      *ngIf=\"!isAllRecordLoaded\"\n      (click)=\"loadMoreData()\"\n    >\n      Load more\n    </ion-button>\n  </ion-row>\n\n  <!-- <ion-grid>\n    <ion-row>\n      <h4>No activities found</h4>\n    </ion-row>\n    <ion-row>\n      <ion-button color=\"dark\" expand=\"block\" style=\"width: 100%;\" (click)=\"presentModal()\">Change filter</ion-button>\n    </ion-row>\n  </ion-grid> -->\n</ion-content>\n\n<ng-template #noTrackerData>\n  <ion-grid>\n    <ion-row>\n      <span>No Trackers found for this filter.</span>\n    </ion-row>\n  </ion-grid>\n</ng-template>\n");

/***/ }),

/***/ 3377:
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/activities/details/activities-detail.page.html ***!
  \************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header id=\"header-a-d\" >\n  <ion-toolbar>\n    <ion-title>Activities </ion-title>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"activities\"></ion-back-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content *ngIf=\"activityData && activityData?.id\" [style.opacity]=\"this.isDisplay ? 1 : 0\">\n\n  <div class=\"activityDetailMapDiv\" id=\"activityDetailMapDiv\" [style.height]=\"mapHeight\">\n \n  </div>\n  <ion-card  class=\"activity\" id=\"detail-card-a-d\">\n    <ion-card-header class=\"activity-header\">\n     <div>\n       <div class=\"activity-header-avatar\">\n         <ion-avatar>\n           <img *ngIf=\"activityData.location_type === 'stop'; else trimIcon\" src=\"../assets/stop_trip.jpeg\" width=\"30px\" height=\"30px\"/>\n           <ng-template #trimIcon><img src=\"../assets/trip.jpeg\" width=\"30px\" height=\"30px\"/></ng-template>\n         </ion-avatar>\n       </div>\n       <div class=\"activity-header-name-div\">\n         <div class=\"activity-header-type\"> {{activityData.location_type}}</div>\n         <div class=\"activity-header-name\"> {{activityData.tracker_name}} </div>\n       </div>\n       <div class=\"activity-header-time\">\n         <span> {{activityData.time}}</span>\n         <span class=\"ellipsis-icon-menu\" (click)=\"notifications($event)\"> <fa-icon  class=\"tracker-sample-view\" icon=\"ellipsis\" style=\"margin-right: 10px;\"></fa-icon> </span>\n       </div>\n     </div>\n   </ion-card-header> \n   <div style=\"clr: both\"></div>\n   <ion-card-content *ngIf=\"activityData?.locations.length > 0\" class=\"activity-card-content\">\n     <div class=\"activity-content-avatar\">\n       <ion-avatar>\n        <img *ngIf=\"activityData.location_type === 'stop'; else locIcon\" src=\"../assets/stop_point_map.jpeg\" width=\"30px\" height=\"30px\" style=\"width: 22px;\"/>\n        <ng-template #locIcon><img src=\"../assets/start_start.jpeg\" width=\"30px\" height=\"30px\"/></ng-template>\n       </ion-avatar>\n     </div>\n     <div class=\"activity-location\">\n       <div class=\"activity-location-1\">\n        {{activityData.locations[0].formatted_address.split(\",\")[0]}}\n       </div>\n       <div class=\"activity-location-2\">\n        {{activityData.locations[0].formatted_address.split(\",\")[1]}},{{activityData.locations[0].formatted_address.split(\",\")[2]}}\n       </div>\n     </div>\n     <div class=\"activity-content-time-div\">\n       <div class=\"activity-content-time\">\n        {{activityData.locations[0].timestamp | date : 'shortTime'}}\n       </div>\n       <div class=\"activity-content-date\">\n        {{activityData.locations[0].timestamp | date: dateF}}\n       </div>\n     </div>\n   </ion-card-content>\n   <ion-card-content *ngIf=\"activityData?.locations.length > 1\" class=\"activity-card-content\">\n    <div class=\"activity-content-avatar\">\n      <ion-avatar>\n        <img src=\"../assets/stop_end.jpeg\" width=\"30px\" height=\"30px\"/>\n      </ion-avatar>\n    </div>\n    <div class=\"activity-location\">\n      <div class=\"activity-location-1\">\n       {{activityData.locations[activityData.locations.length - 1].formatted_address.split(\",\")[0]}}\n      </div>\n      <div class=\"activity-location-2\">\n       {{activityData.locations[activityData.locations.length - 1].formatted_address.split(\",\")[1]}},{{activityData.locations[0].formatted_address.split(\",\")[2]}}\n      </div>\n    </div>\n    <div class=\"activity-content-time-div\">\n      <div class=\"activity-content-time\">\n       {{activityData.locations[activityData.locations.length - 1].timestamp| date : 'shortTime'}}\n      </div>\n      <div class=\"activity-content-date\">\n       {{activityData.locations[activityData.locations.length - 1].timestamp | date : dateF}}\n      </div>\n    </div>\n  </ion-card-content>\n </ion-card>\n</ion-content>");

/***/ }),

/***/ 4856:
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/activities/filter/filter.component.html ***!
  \*****************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\n  <ion-toolbar color=\"white\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"activities\" (click)=\"closeModal()\"></ion-back-button>\n    </ion-buttons>\n    <div style=\"text-align: center;\">      \n      <span id=\"title\" style=\"margin-right: 50px ;\" ><strong>Filters</strong></span>\n    </div>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding\" *ngIf=\"dataLoad\">\n  <ion-grid>\n    <ion-row>\n      <ion-card>\n        <ion-card-header style=\"padding: 5px 15px;\">\n          <ion-card-subtitle>Activity Type</ion-card-subtitle>\n        </ion-card-header>\n        <ion-card-content>\n          <ion-list>\n            <ion-radio-group [(ngModel)]=\"defaultFilterType\" mode=\"md\">\n              <ion-item>\n                <ion-label><p>All</p></ion-label>\n                <ion-radio slot=\"start\" value=\"all\"></ion-radio>\n              </ion-item>\n              <ion-item>\n                <ion-label><p>Stop</p></ion-label>\n                  <ion-radio slot=\"start\" value=\"stop\"></ion-radio>\n              </ion-item>\n              <ion-item>\n                <ion-label><p>Trip</p></ion-label>\n                  <ion-radio slot=\"start\" value=\"trip\"></ion-radio>\n              </ion-item>\n            </ion-radio-group>\n          </ion-list>\n        </ion-card-content>\n      </ion-card>\n      <ion-card>\n        <ion-card-header style=\"padding: 5px 15px;\">\n          <ion-card-subtitle>Select Tracker</ion-card-subtitle>\n        </ion-card-header>\n        <ion-card-content>\n          <ion-list>\n            <ion-radio-group [(ngModel)]=\"defaultFilterTracker\" mode=\"md\">\n              <ion-item>\n                <ion-label><p>All</p></ion-label>\n                <ion-radio slot=\"start\" value=\"all\"></ion-radio>\n              </ion-item>\n              <ion-item *ngFor=\"let tracker of userTrackers\">\n                <ion-label><p>{{ tracker.tracker_name }}</p></ion-label>\n                  <ion-radio slot=\"start\" [value]=\"tracker\"></ion-radio>\n              </ion-item>\n              <!-- <ion-item>\n                <ion-label><p>Tracker 2</p></ion-label>\n                  <ion-radio slot=\"start\" value=\"tracker2\"></ion-radio>\n              </ion-item> -->\n            </ion-radio-group>\n          </ion-list>\n          <ion-row>\n            <ion-col>\n                <ion-datetime presentation=\"date\" displayFormat=\"DD/MM/YYYY\" placeholder=\"From\" id=\"date-selection\" [(ngModel)]=\"fromDate\"></ion-datetime>\n            </ion-col>\n            <ion-col>\n              <ion-datetime presentation=\"date\" displayFormat=\"DD/MM/YYYY\" placeholder=\"To\" id=\"date-selection\" [(ngModel)]=\"toDate\"></ion-datetime>\n            </ion-col>\n          </ion-row>\n        </ion-card-content>\n      </ion-card>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n\n<ion-footer>\n  <ion-toolbar>\n    <ion-button id=\"pref\" slot=\"start\" color=\"dark\" (click)=\"resetFilter()\">\n      Reset\n    </ion-button>\n    <ion-button slot=\"end\" color=\"dark\" (click)=\"applyActivityFilters()\">\n      Apply filters\n    </ion-button>\n  </ion-toolbar>\n</ion-footer>");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_activities_activities_module_ts.js.map