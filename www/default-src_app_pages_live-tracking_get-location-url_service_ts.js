(self["webpackChunkspytrack"] = self["webpackChunkspytrack"] || []).push([["default-src_app_pages_live-tracking_get-location-url_service_ts"],{

/***/ 2311:
/*!*****************************************************************!*\
  !*** ./src/app/pages/live-tracking/get-location-url.service.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "GetLocationUrlService": () => (/* binding */ GetLocationUrlService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 7716);


let GetLocationUrlService = class GetLocationUrlService {
    getMarkerLocation(icon, color) {
        if (icon == 'fa-location-arrow') {
            if (color == '#229954') {
                return '../assets/markers/location-arrow/location-arrow-229954.png';
            }
            else if (color == '#CD6155') {
                return '../assets/markers/location-arrow/location-arrow-CD6155.png';
            }
            else if (color == '#A569BD') {
                return '../assets/markers/location-arrow/location-arrow-A569BD.png';
            }
            else if (color == '#85C1E9') {
                return '../assets/markers/location-arrow/location-arrow-85C1E9.png';
            }
            else if (color == '#45B39D') {
                return '../assets/markers/location-arrow/location-arrow-45B39D.png';
            }
            else if (color == '#F4D03F') {
                return '../assets/markers/location-arrow/location-arrow-F4D03F.png';
            }
            else if (color == '#DC7633') {
                return '../assets/markers/location-arrow/location-arrow-DC7633.png';
            }
            else if (color == '#CCCCFF') {
                return '../assets/markers/location-arrow/location-arrow-CCCCFF.png';
            }
            else if (color == '#6495ED') {
                return '../assets/markers/location-arrow/location-arrow-6495ED.png';
            }
            else if (color == '#40E0D0') {
                return '../assets/markers/location-arrow/location-arrow-40E0D0.png';
            }
            else if (color == '#9FE2BF') {
                return '../assets/markers/location-arrow/location-arrow-9FE2BF.png';
            }
            else if (color == '#DE3163') {
                return '../assets/markers/location-arrow/location-arrow-DE3163.png';
            }
            else if (color == '#DFFF00') {
                return '../assets/markers/location-arrow/location-arrow-DFFF00.png';
            }
            else if (color == '#FFBF00') {
                return '../assets/markers/location-arrow/location-arrow-FFBF00.png';
            }
            else if (color == '#FF7F50') {
                return '../assets/markers/location-arrow/location-arrow-FF7F50.png';
            }
        }
        else if (icon == 'fa-map-marker') {
            if (color == '#229954') {
                return '../assets/markers/map-marker/map-marker-229954.png';
            }
            else if (color == '#CD6155') {
                return '../assets/markers/map-marker/map-marker-CD6155.png';
            }
            else if (color == '#A569BD') {
                return '../assets/markers/map-marker/map-marker-A569BD.png';
            }
            else if (color == '#85C1E9') {
                return '../assets/markers/map-marker/map-marker-85C1E9.png';
            }
            else if (color == '#45B39D') {
                return '../assets/markers/map-marker/map-marker-45B39D.png';
            }
            else if (color == '#F4D03F') {
                return '../assets/markers/map-marker/map-marker-F4D03F.png';
            }
            else if (color == '#DC7633') {
                return '../assets/markers/map-marker/map-marker-DC7633.png';
            }
            else if (color == '#CCCCFF') {
                return '../assets/markers/map-marker/map-marker-CCCCFF.png';
            }
            else if (color == '#6495ED') {
                return '../assets/markers/map-marker/map-marker-6495ED.png';
            }
            else if (color == '#40E0D0') {
                return '../assets/markers/map-marker/map-marker-40E0D0.png';
            }
            else if (color == '#9FE2BF') {
                return '../assets/markers/map-marker/map-marker-9FE2BF.png';
            }
            else if (color == '#DE3163') {
                return '../assets/markers/map-marker/map-marker-DE3163.png';
            }
            else if (color == '#DFFF00') {
                return '../assets/markers/map-marker/map-marker-DFFF00.png';
            }
            else if (color == '#FFBF00') {
                return '../assets/markers/map-marker/map-marker-FFBF00.png';
            }
            else if (color == '#FF7F50') {
                return '../assets/markers/map-marker/map-marker-FF7F50.png';
            }
        }
        else if (icon == 'fa-map-marker-alt') {
            if (color == '#229954') {
                return '../assets/markers/map-marker-alt/map-marker-229954.png';
            }
            else if (color == '#CD6155') {
                return '../assets/markers/map-marker-alt/map-marker-CD6155.png';
            }
            else if (color == '#A569BD') {
                return '../assets/markers/map-marker-alt/map-marker-A569BD.png';
            }
            else if (color == '#85C1E9') {
                return '../assets/markers/map-marker-alt/map-marker-85C1E9.png';
            }
            else if (color == '#45B39D') {
                return '../assets/markers/map-marker-alt/map-marker-45B39D.png';
            }
            else if (color == '#F4D03F') {
                return '../assets/markers/map-marker-alt/map-marker-F4D03F.png';
            }
            else if (color == '#DC7633') {
                return '../assets/markers/map-marker-alt/map-marker-DC7633.png';
            }
            else if (color == '#CCCCFF') {
                return '../assets/markers/map-marker-alt/map-marker-CCCCFF.png';
            }
            else if (color == '#6495ED') {
                return '../assets/markers/map-marker-alt/map-marker-6495ED.png';
            }
            else if (color == '#40E0D0') {
                return '../assets/markers/map-marker-alt/map-marker-40E0D0.png';
            }
            else if (color == '#9FE2BF') {
                return '../assets/markers/map-marker-alt/map-marker-9FE2BF.png';
            }
            else if (color == '#DE3163') {
                return '../assets/markers/map-marker-alt/map-marker-DE3163.png';
            }
            else if (color == '#DFFF00') {
                return '../assets/markers/map-marker-alt/map-marker-DFFF00.png';
            }
            else if (color == '#FFBF00') {
                return '../assets/markers/map-marker-alt/map-marker-FFBF00.png';
            }
            else if (color == '#FF7F50') {
                return '../assets/markers/map-marker-alt/map-marker-FF7F50.png';
            }
        }
        else if (icon == 'fa-car') {
            if (color == '#229954') {
                return '../assets/markers/car/car-229954.png';
            }
            else if (color == '#CD6155') {
                return '../assets/markers/car/car-CD6155.png';
            }
            else if (color == '#A569BD') {
                return '../assets/markers/car/car-A569BD.png';
            }
            else if (color == '#85C1E9') {
                return '../assets/markers/car/car-85C1E9.png';
            }
            else if (color == '#45B39D') {
                return '../assets/markers/car/car-45B39D.png';
            }
            else if (color == '#F4D03F') {
                return '../assets/markers/car/car-F4D03F.png';
            }
            else if (color == '#DC7633') {
                return '../assets/markers/car/car-DC7633.png';
            }
            else if (color == '#CCCCFF') {
                return '../assets/markers/car/car-CCCCFF.png';
            }
            else if (color == '#6495ED') {
                return '../assets/markers/car/car-6495ED.png';
            }
            else if (color == '#40E0D0') {
                return '../assets/markers/car/car-40E0D0.png';
            }
            else if (color == '#9FE2BF') {
                return '../assets/markers/car/car-9FE2BF.png';
            }
            else if (color == '#DE3163') {
                return '../assets/markers/car/car-DE3163.png';
            }
            else if (color == '#DFFF00') {
                return '../assets/markers/car/car-DFFF00.png';
            }
            else if (color == '#FFBF00') {
                return '../assets/markers/car/car-FFBF00.png';
            }
            else if (color == '#FF7F50') {
                return '../assets/markers/car/car-FF7F50.png';
            }
        }
        else if (icon == 'fa-motorcycle') {
            if (color == '#229954') {
                return '../assets/markers/motorcycle/motorcycle-229954.png';
            }
            else if (color == '#CD6155') {
                return '../assets/markers/motorcycle/motorcycle-CD6155.png';
            }
            else if (color == '#A569BD') {
                return '../assets/markers/motorcycle/motorcycle-A569BD.png';
            }
            else if (color == '#85C1E9') {
                return '../assets/markers/motorcycle/motorcycle-85C1E9.png';
            }
            else if (color == '#45B39D') {
                return '../assets/markers/motorcycle/motorcycle-45B39D.png';
            }
            else if (color == '#F4D03F') {
                return '../assets/markers/motorcycle/motorcycle-F4D03F.png';
            }
            else if (color == '#DC7633') {
                return '../assets/markers/motorcycle/motorcycle-DC7633.png';
            }
            else if (color == '#CCCCFF') {
                return '../assets/markers/motorcycle/motorcycle-CCCCFF.png';
            }
            else if (color == '#6495ED') {
                return '../assets/markers/motorcycle/motorcycle-6495ED.png';
            }
            else if (color == '#40E0D0') {
                return '../assets/markers/motorcycle/motorcycle-40E0D0.png';
            }
            else if (color == '#9FE2BF') {
                return '../assets/markers/motorcycle/motorcycle-9FE2BF.png';
            }
            else if (color == '#DE3163') {
                return '../assets/markers/motorcycle/motorcycle-DE3163.png';
            }
            else if (color == '#DFFF00') {
                return '../assets/markers/motorcycle/motorcycle-DFFF00.png';
            }
            else if (color == '#FFBF00') {
                return '../assets/markers/motorcycle/motorcycle-FFBF00.png';
            }
            else if (color == '#FF7F50') {
                return '../assets/markers/motorcycle/motorcycle-FF7F50.png';
            }
        }
        else if (icon == 'fa-shuttle-van') {
            if (color == '#229954') {
                return '../assets/markers/shuttle-van/shuttle-van-229954.png';
            }
            else if (color == '#CD6155') {
                return '../assets/markers/shuttle-van/shuttle-van-CD6155.png';
            }
            else if (color == '#A569BD') {
                return '../assets/markers/shuttle-van/shuttle-van-A569BD.png';
            }
            else if (color == '#85C1E9') {
                return '../assets/markers/shuttle-van/shuttle-van-85C1E9.png';
            }
            else if (color == '#45B39D') {
                return '../assets/markers/shuttle-van/shuttle-van-45B39D.png';
            }
            else if (color == '#F4D03F') {
                return '../assets/markers/shuttle-van/shuttle-van-F4D03F.png';
            }
            else if (color == '#DC7633') {
                return '../assets/markers/shuttle-van/shuttle-van-DC7633.png';
            }
            else if (color == '#CCCCFF') {
                return '../assets/markers/shuttle-van/shuttle-van-CCCCFF.png';
            }
            else if (color == '#6495ED') {
                return '../assets/markers/shuttle-van/shuttle-van-6495ED.png';
            }
            else if (color == '#40E0D0') {
                return '../assets/markers/shuttle-van/shuttle-van-40E0D0.png';
            }
            else if (color == '#9FE2BF') {
                return '../assets/markers/shuttle-van/shuttle-van-9FE2BF.png';
            }
            else if (color == '#DE3163') {
                return '../assets/markers/shuttle-van/shuttle-van-DE3163.png';
            }
            else if (color == '#DFFF00') {
                return '../assets/markers/shuttle-van/shuttle-van-DFFF00.png';
            }
            else if (color == '#FFBF00') {
                return '../assets/markers/shuttle-van/shuttle-van-FFBF00.png';
            }
            else if (color == '#FF7F50') {
                return '../assets/markers/shuttle-van/shuttle-van-FF7F50.png';
            }
        }
        else if (icon == 'fa-truck') {
            if (color == '#229954') {
                return '../assets/markers/truck/truck-229954.png';
            }
            else if (color == '#CD6155') {
                return '../assets/markers/truck/truck-CD6155.png';
            }
            else if (color == '#A569BD') {
                return '../assets/markers/truck/truck-A569BD.png';
            }
            else if (color == '#85C1E9') {
                return '../assets/markers/truck/truck-85C1E9.png';
            }
            else if (color == '#45B39D') {
                return '../assets/markers/truck/truck-45B39D.png';
            }
            else if (color == '#F4D03F') {
                return '../assets/markers/truck/truck-F4D03F.png';
            }
            else if (color == '#DC7633') {
                return '../assets/markers/truck/truck-DC7633.png';
            }
            else if (color == '#CCCCFF') {
                return '../assets/markers/truck/truck-CCCCFF.png';
            }
            else if (color == '#6495ED') {
                return '../assets/markers/truck/truck-6495ED.png';
            }
            else if (color == '#40E0D0') {
                return '../assets/markers/truck/truck-40E0D0.png';
            }
            else if (color == '#9FE2BF') {
                return '../assets/markers/truck/truck-9FE2BF.png';
            }
            else if (color == '#DE3163') {
                return '../assets/markers/truck/truck-DE3163.png';
            }
            else if (color == '#DFFF00') {
                return '../assets/markers/truck/truck-DFFF00.png';
            }
            else if (color == '#FFBF00') {
                return '../assets/markers/truck/truck-FFBF00.png';
            }
            else if (color == '#FF7F50') {
                return '../assets/markers/truck/truck-FF7F50.png';
            }
        }
        else if (icon == 'fa-truck-pickup') {
            if (color == '#229954') {
                return '../assets/markers/truck-pickup/truck-pickup-229954.png';
            }
            else if (color == '#CD6155') {
                return '../assets/markers/truck-pickup/truck-pickup-CD6155.png';
            }
            else if (color == '#A569BD') {
                return '../assets/markers/truck-pickup/truck-pickup-A569BD.png';
            }
            else if (color == '#85C1E9') {
                return '../assets/markers/truck-pickup/truck-pickup-85C1E9.png';
            }
            else if (color == '#45B39D') {
                return '../assets/markers/truck-pickup/truck-pickup-45B39D.png';
            }
            else if (color == '#F4D03F') {
                return '../assets/markers/truck-pickup/truck-pickup-F4D03F.png';
            }
            else if (color == '#DC7633') {
                return '../assets/markers/truck-pickup/truck-pickup-DC7633.png';
            }
            else if (color == '#CCCCFF') {
                return '../assets/markers/truck-pickup/truck-pickup-CCCCFF.png';
            }
            else if (color == '#6495ED') {
                return '../assets/markers/truck-pickup/truck-pickup-6495ED.png';
            }
            else if (color == '#40E0D0') {
                return '../assets/markers/truck-pickup/truck-pickup-40E0D0.png';
            }
            else if (color == '#9FE2BF') {
                return '../assets/markers/truck-pickup/truck-pickup-9FE2BF.png';
            }
            else if (color == '#DE3163') {
                return '../assets/markers/truck-pickup/truck-pickup-DE3163.png';
            }
            else if (color == '#DFFF00') {
                return '../assets/markers/truck-pickup/truck-pickup-DFFF00.png';
            }
            else if (color == '#FFBF00') {
                return '../assets/markers/truck-pickup/truck-pickup-FFBF00.png';
            }
            else if (color == '#FF7F50') {
                return '../assets/markers/truck-pickup/truck-pickup-FF7F50.png';
            }
        }
        else if (icon == 'fa-caravan') {
            if (color == '#229954') {
                return '../assets/markers/caravan/caravan-229954.png';
            }
            else if (color == '#CD6155') {
                return '../assets/markers/caravan/caravan-CD6155.png';
            }
            else if (color == '#A569BD') {
                return '../assets/markers/caravan/caravan-A569BD.png';
            }
            else if (color == '#85C1E9') {
                return '../assets/markers/caravan/caravan-85C1E9.png';
            }
            else if (color == '#45B39D') {
                return '../assets/markers/caravan/caravan-45B39D.png';
            }
            else if (color == '#F4D03F') {
                return '../assets/markers/caravan/caravan-F4D03F.png';
            }
            else if (color == '#DC7633') {
                return '../assets/markers/caravan/caravan-DC7633.png';
            }
            else if (color == '#CCCCFF') {
                return '../assets/markers/caravan/caravan-CCCCFF.png';
            }
            else if (color == '#6495ED') {
                return '../assets/markers/caravan/caravan-6495ED.png';
            }
            else if (color == '#40E0D0') {
                return '../assets/markers/caravan/caravan-40E0D0.png';
            }
            else if (color == '#9FE2BF') {
                return '../assets/markers/caravan/caravan-9FE2BF.png';
            }
            else if (color == '#DE3163') {
                return '../assets/markers/caravan/caravan-DE3163.png';
            }
            else if (color == '#DFFF00') {
                return '../assets/markers/caravan/caravan-DFFF00.png';
            }
            else if (color == '#FFBF00') {
                return '../assets/markers/caravan/caravan-FFBF00.png';
            }
            else if (color == '#FF7F50') {
                return '../assets/markers/caravan/caravan-FF7F50.png';
            }
        }
        else if (icon == 'fa-user') {
            if (color == '#229954') {
                return '../assets/markers/shuttle-van/shuttle-van-229954.png';
            }
            else if (color == '#CD6155') {
                return '../assets/markers/shuttle-van/shuttle-van-CD6155.png';
            }
            else if (color == '#A569BD') {
                return '../assets/markers/shuttle-van/shuttle-van-A569BD.png';
            }
            else if (color == '#85C1E9') {
                return '../assets/markers/shuttle-van/shuttle-van-85C1E9.png';
            }
            else if (color == '#45B39D') {
                return '../assets/markers/shuttle-van/shuttle-van-45B39D.png';
            }
            else if (color == '#F4D03F') {
                return '../assets/markers/shuttle-van/shuttle-van-F4D03F.png';
            }
            else if (color == '#DC7633') {
                return '../assets/markers/shuttle-van/shuttle-van-DC7633.png';
            }
            else if (color == '#CCCCFF') {
                return '../assets/markers/shuttle-van/shuttle-van-CCCCFF.png';
            }
            else if (color == '#6495ED') {
                return '../assets/markers/shuttle-van/shuttle-van-6495ED.png';
            }
            else if (color == '#40E0D0') {
                return '../assets/markers/shuttle-van/shuttle-van-40E0D0.png';
            }
            else if (color == '#9FE2BF') {
                return '../assets/markers/shuttle-van/shuttle-van-9FE2BF.png';
            }
            else if (color == '#DE3163') {
                return '../assets/markers/shuttle-van/shuttle-van-DE3163.png';
            }
            else if (color == '#DFFF00') {
                return '../assets/markers/shuttle-van/shuttle-van-DFFF00.png';
            }
            else if (color == '#FFBF00') {
                return '../assets/markers/shuttle-van/shuttle-van-FFBF00.png';
            }
            else if (color == '#FF7F50') {
                return '../assets/markers/shuttle-van/shuttle-van-FF7F50.png';
            }
        }
        else if (icon == 'fa-male') {
            if (color == '#229954') {
                return '../assets/markers/male/male-229954.png';
            }
            else if (color == '#CD6155') {
                return '../assets/markers/male/male-CD6155.png';
            }
            else if (color == '#A569BD') {
                return '../assets/markers/male/male-A569BD.png';
            }
            else if (color == '#85C1E9') {
                return '../assets/markers/male/male-85C1E9.png';
            }
            else if (color == '#45B39D') {
                return '../assets/markers/male/male-45B39D.png';
            }
            else if (color == '#F4D03F') {
                return '../assets/markers/male/male-F4D03F.png';
            }
            else if (color == '#DC7633') {
                return '../assets/markers/male/male-DC7633.png';
            }
            else if (color == '#CCCCFF') {
                return '../assets/markers/male/male-CCCCFF.png';
            }
            else if (color == '#6495ED') {
                return '../assets/markers/male/male-6495ED.png';
            }
            else if (color == '#40E0D0') {
                return '../assets/markers/male/male-40E0D0.png';
            }
            else if (color == '#9FE2BF') {
                return '../assets/markers/male/male-9FE2BF.png';
            }
            else if (color == '#DE3163') {
                return '../assets/markers/male/male-DE3163.png';
            }
            else if (color == '#DFFF00') {
                return '../assets/markers/male/male-DFFF00.png';
            }
            else if (color == '#FFBF00') {
                return '../assets/markers/male/male-FFBF00.png';
            }
            else if (color == '#FF7F50') {
                return '../assets/markers/male/male-FF7F50.png';
            }
        }
        else if (icon == 'fa-female') {
            if (color == '#229954') {
                return '../assets/markers/female/female-229954.png';
            }
            else if (color == '#CD6155') {
                return '../assets/markers/female/female-CD6155.png';
            }
            else if (color == '#A569BD') {
                return '../assets/markers/female/female-A569BD.png';
            }
            else if (color == '#85C1E9') {
                return '../assets/markers/female/female-85C1E9.png';
            }
            else if (color == '#45B39D') {
                return '../assets/markers/female/female-45B39D.png';
            }
            else if (color == '#F4D03F') {
                return '../assets/markers/female/female-F4D03F.png';
            }
            else if (color == '#DC7633') {
                return '../assets/markers/female/female-DC7633.png';
            }
            else if (color == '#CCCCFF') {
                return '../assets/markers/female/female-CCCCFF.png';
            }
            else if (color == '#6495ED') {
                return '../assets/markers/female/female-6495ED.png';
            }
            else if (color == '#40E0D0') {
                return '../assets/markers/female/female-40E0D0.png';
            }
            else if (color == '#9FE2BF') {
                return '../assets/markers/female/female-9FE2BF.png';
            }
            else if (color == '#DE3163') {
                return '../assets/markers/female/female-DE3163.png';
            }
            else if (color == '#DFFF00') {
                return '../assets/markers/female/female-DFFF00.png';
            }
            else if (color == '#FFBF00') {
                return '../assets/markers/female/female-FFBF00.png';
            }
            else if (color == '#FF7F50') {
                return '../assets/markers/female/female-FF7F50.png';
            }
        }
        else if (icon == 'fa-child') {
            if (color == '#229954') {
                return '../assets/markers/child/child-229954.png';
            }
            else if (color == '#CD6155') {
                return '../assets/markers/child/child-CD6155.png';
            }
            else if (color == '#A569BD') {
                return '../assets/markers/child/child-A569BD.png';
            }
            else if (color == '#85C1E9') {
                return '../assets/markers/child/child-85C1E9.png';
            }
            else if (color == '#45B39D') {
                return '../assets/markers/child/child-45B39D.png';
            }
            else if (color == '#F4D03F') {
                return '../assets/markers/child/child-F4D03F.png';
            }
            else if (color == '#DC7633') {
                return '../assets/markers/child/child-DC7633.png';
            }
            else if (color == '#CCCCFF') {
                return '../assets/markers/child/child-CCCCFF.png';
            }
            else if (color == '#6495ED') {
                return '../assets/markers/child/child-6495ED.png';
            }
            else if (color == '#40E0D0') {
                return '../assets/markers/child/child-40E0D0.png';
            }
            else if (color == '#9FE2BF') {
                return '../assets/markers/child/child-9FE2BF.png';
            }
            else if (color == '#DE3163') {
                return '../assets/markers/child/child-DE3163.png';
            }
            else if (color == '#DFFF00') {
                return '../assets/markers/child/child-DFFF00.png';
            }
            else if (color == '#FFBF00') {
                return '../assets/markers/child/child-FFBF00.png';
            }
            else if (color == '#FF7F50') {
                return '../assets/markers/child/child-FF7F50.png';
            }
        }
        else if (icon == 'fa-baby') {
            if (color == '#229954') {
                return '../assets/markers/baby/baby-229954.png';
            }
            else if (color == '#CD6155') {
                return '../assets/markers/baby/baby-CD6155.png';
            }
            else if (color == '#A569BD') {
                return '../assets/markers/baby/baby-A569BD.png';
            }
            else if (color == '#85C1E9') {
                return '../assets/markers/baby/baby-85C1E9.png';
            }
            else if (color == '#45B39D') {
                return '../assets/markers/baby/baby-45B39D.png';
            }
            else if (color == '#F4D03F') {
                return '../assets/markers/baby/baby-F4D03F.png';
            }
            else if (color == '#DC7633') {
                return '../assets/markers/baby/baby-DC7633.png';
            }
            else if (color == '#CCCCFF') {
                return '../assets/markers/baby/baby-CCCCFF.png';
            }
            else if (color == '#6495ED') {
                return '../assets/markers/baby/baby-6495ED.png';
            }
            else if (color == '#40E0D0') {
                return '../assets/markers/baby/baby-40E0D0.png';
            }
            else if (color == '#9FE2BF') {
                return '../assets/markers/baby/baby-9FE2BF.png';
            }
            else if (color == '#DE3163') {
                return '../assets/markers/baby/baby-DE3163.png';
            }
            else if (color == '#DFFF00') {
                return '../assets/markers/baby/baby-DFFF00.png';
            }
            else if (color == '#FFBF00') {
                return '../assets/markers/baby/baby-FFBF00.png';
            }
            else if (color == '#FF7F50') {
                return '../assets/markers/baby/baby-FF7F50.png';
            }
        }
        else if (icon == 'fa-baby-carriage') {
            if (color == '#229954') {
                return '../assets/markers/baby-carriage/baby-carriage-229954.png';
            }
            else if (color == '#CD6155') {
                return '../assets/markers/baby-carriage/baby-carriage-CD6155.png';
            }
            else if (color == '#A569BD') {
                return '../assets/markers/baby-carriage/baby-carriage-A569BD.png';
            }
            else if (color == '#85C1E9') {
                return '../assets/markers/baby-carriage/baby-carriage-85C1E9.png';
            }
            else if (color == '#45B39D') {
                return '../assets/markers/baby-carriage/baby-carriage-45B39D.png';
            }
            else if (color == '#F4D03F') {
                return '../assets/markers/baby-carriage/baby-carriage-F4D03F.png';
            }
            else if (color == '#DC7633') {
                return '../assets/markers/baby-carriage/baby-carriage-DC7633.png';
            }
            else if (color == '#CCCCFF') {
                return '../assets/markers/baby-carriage/baby-carriage-CCCCFF.png';
            }
            else if (color == '#6495ED') {
                return '../assets/markers/baby-carriage/baby-carriage-6495ED.png';
            }
            else if (color == '#40E0D0') {
                return '../assets/markers/baby-carriage/baby-carriage-40E0D0.png';
            }
            else if (color == '#9FE2BF') {
                return '../assets/markers/baby-carriage/baby-carriage-9FE2BF.png';
            }
            else if (color == '#DE3163') {
                return '../assets/markers/baby-carriage/baby-carriage-DE3163.png';
            }
            else if (color == '#DFFF00') {
                return '../assets/markers/baby-carriage/baby-carriage-DFFF00.png';
            }
            else if (color == '#FFBF00') {
                return '../assets/markers/baby-carriage/baby-carriage-FFBF00.png';
            }
            else if (color == '#FF7F50') {
                return '../assets/markers/baby-carriage/baby-carriage-FF7F50.png';
            }
        }
    }
};
GetLocationUrlService = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_1__.Injectable)()
], GetLocationUrlService);



/***/ })

}]);
//# sourceMappingURL=default-src_app_pages_live-tracking_get-location-url_service_ts.js.map