(self["webpackChunkspytrack"] = self["webpackChunkspytrack"] || []).push([["src_app_registration_registration_module_ts"],{

/***/ 4261:
/*!*************************************************************!*\
  !*** ./src/app/registration/registration-routing.module.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RegistrationPageRoutingModule": () => (/* binding */ RegistrationPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _registration_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./registration.page */ 9454);




const routes = [
    {
        path: '',
        component: _registration_page__WEBPACK_IMPORTED_MODULE_0__.RegistrationPage
    }
];
let RegistrationPageRoutingModule = class RegistrationPageRoutingModule {
};
RegistrationPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], RegistrationPageRoutingModule);



/***/ }),

/***/ 5375:
/*!*****************************************************!*\
  !*** ./src/app/registration/registration.module.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RegistrationPageModule": () => (/* binding */ RegistrationPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 8583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _registration_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./registration-routing.module */ 4261);
/* harmony import */ var _registration_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./registration.page */ 9454);







let RegistrationPageModule = class RegistrationPageModule {
};
RegistrationPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.ReactiveFormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _registration_routing_module__WEBPACK_IMPORTED_MODULE_0__.RegistrationPageRoutingModule
        ],
        declarations: [_registration_page__WEBPACK_IMPORTED_MODULE_1__.RegistrationPage]
    })
], RegistrationPageModule);



/***/ }),

/***/ 9454:
/*!***************************************************!*\
  !*** ./src/app/registration/registration.page.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RegistrationPage": () => (/* binding */ RegistrationPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4762);
/* harmony import */ var _raw_loader_registration_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./registration.page.html */ 2657);
/* harmony import */ var _registration_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./registration.page.scss */ 5078);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 7716);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 9895);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 476);
/* harmony import */ var _rest_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../rest.service */ 1881);








let RegistrationPage = class RegistrationPage {
    constructor(_formBuilder, router, httpRequest, toastCtrl) {
        this._formBuilder = _formBuilder;
        this.router = router;
        this.httpRequest = httpRequest;
        this.toastCtrl = toastCtrl;
        this.emailValidation = true;
        this.passwordVal = {
            minLen: false,
            number: false,
            symbol: false,
            upperCase: false,
            lowerCase: false
        };
        this.cPasswordVal = false;
        this.slideOpts = {
            initialSlide: 0,
            speed: 400,
            allowTouchMove: false
        };
    }
    ngOnInit() {
        this.formInit();
    }
    ionViewWillEnter() {
        var _a;
        this.slideOpts.initialSlide = 0;
        (_a = this.regsitrationSlides) === null || _a === void 0 ? void 0 : _a.slideTo(0);
        this.regsitrationSlides.ionSlideTransitionStart.subscribe((end) => {
            this.content.scrollToTop(100);
        });
    }
    formInit() {
        this.regFormStepOne = this._formBuilder.group({
            firstname: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required),
            lastname: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required),
            phone: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__.FormControl('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.pattern('(([+][(]?[0-9]{1,3}[)]?)|([(]?[0-9]{4}[)]?))\s*[)]?[-\s\.]?[(]?[0-9]{1,3}[)]?([-\s\.]?[0-9]{3})([-\s\.]?[0-9]{3,4})')]),
        });
        this.regFormStepTwo = this._formBuilder.group({
            address: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required),
            city: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required),
            state: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required),
            zipcode: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required),
            country: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required),
        });
        this.regFormStepThree = this._formBuilder.group({
            username: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__.FormControl('', [_angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&]).{8,}')]),
            cpassword: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required),
            pinCode: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__.FormControl('', _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required),
        });
    }
    passwordInputValidation(event) {
        const e = event.target.value;
        const number = /\d/;
        const symbol = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
        const upperCase = /[A-Z]/;
        const lowerCase = /[a-z]/;
        this.regFormStepThree.controls.password.setValue(e);
        if (this.regFormStepThree.controls.password.value == '') {
            this.passwordVal = {
                minLen: false,
                number: false,
                symbol: false,
                upperCase: false,
                lowerCase: false
            };
        }
        else {
            if (this.regFormStepThree.controls.password.value.length >= 8) {
                this.passwordVal.minLen = true;
            }
            else {
                this.passwordVal.minLen = false;
            }
            if (number.test(this.regFormStepThree.controls.password.value)) {
                this.passwordVal.number = true;
            }
            else {
                this.passwordVal.number = false;
            }
            if (symbol.test(this.regFormStepThree.controls.password.value)) {
                this.passwordVal.symbol = true;
            }
            else {
                this.passwordVal.symbol = false;
            }
            if (upperCase.test(this.regFormStepThree.controls.password.value)) {
                this.passwordVal.upperCase = true;
            }
            else {
                this.passwordVal.upperCase = false;
            }
            if (lowerCase.test(this.regFormStepThree.controls.password.value)) {
                this.passwordVal.lowerCase = true;
            }
            else {
                this.passwordVal.lowerCase = false;
            }
        }
    }
    cpasswordInputValidation(event) {
        const e = event.target.value;
        this.regFormStepThree.controls.cpassword.setValue(e);
        if (this.regFormStepThree.controls.password.value == this.regFormStepThree.controls.cpassword.value) {
            this.cPasswordVal = true;
        }
        else {
            this.cPasswordVal = false;
        }
    }
    signUp() {
        let dataToSend = {
            firstname: this.regFormStepOne.controls.firstname.value,
            lastname: this.regFormStepOne.controls.lastname.value,
            email: this.regFormStepOne.controls.email.value,
            phone: this.regFormStepOne.controls.phone.value,
            address: this.regFormStepTwo.controls.address.value,
            city: this.regFormStepTwo.controls.city.value,
            state: this.regFormStepTwo.controls.state.value,
            zipcode: this.regFormStepTwo.controls.zipcode.value,
            country: this.regFormStepTwo.controls.country.value,
            username: this.regFormStepThree.controls.username.value,
            password: this.regFormStepThree.controls.password.value,
            pinCode: this.regFormStepThree.controls.pinCode.value,
        };
        this.httpRequest.post(JSON.stringify(dataToSend), 'register').subscribe((resp) => {
            this.token = resp.isSuccess.token;
            //localStorage.setItem('token', resp.isSuccess.token);
            this.swipeNext();
            // this.router.navigate(['/live-tracking']);
        }, error => {
            console.log(error);
            this.openToast();
        });
    }
    login() {
        // this.router.navigate(['/live-tracking']);
    }
    swipeNext() {
        this.regsitrationSlides.slideNext();
    }
    swipePrev() {
        this.regsitrationSlides.slidePrev();
    }
    openToast() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: 'Error occured. Please try again later.',
                position: 'bottom',
                duration: 4000,
                color: 'danger'
            });
            toast.present();
        });
    }
    resendVarificationEmail() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.httpRequest.newVerification('user/new_verification', this.regFormStepOne.controls.email.value).subscribe((resp) => (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
                if (resp.isSuccess) {
                    const toast = yield this.toastCtrl.create({
                        message: 'Email sent successfuly',
                        position: 'bottom',
                        duration: 1500,
                        color: 'success'
                    });
                    toast.present();
                }
                else {
                    const toast = yield this.toastCtrl.create({
                        message: resp.error.message || resp.message,
                        position: 'bottom',
                        duration: 1500,
                        color: 'danger'
                    });
                    toast.present();
                }
            }), error => {
                console.log(error);
                this.openToast();
            });
        });
    }
};
RegistrationPage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__.FormBuilder },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.Router },
    { type: _rest_service__WEBPACK_IMPORTED_MODULE_2__.RestService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.ToastController }
];
RegistrationPage.propDecorators = {
    regsitrationSlides: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_7__.ViewChild, args: ['regsitrationSlides',] }],
    content: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_7__.ViewChild, args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonContent,] }]
};
RegistrationPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-registration',
        template: _raw_loader_registration_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_registration_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], RegistrationPage);



/***/ }),

/***/ 5078:
/*!*****************************************************!*\
  !*** ./src/app/registration/registration.page.scss ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("ion-content.background {\n  --ion-background-color: #181C32;\n}\n\nion-item {\n  --ion-background-color: #fff;\n}\n\n.title {\n  color: #fff;\n  text-align: center;\n  margin-top: 20px;\n  margin-bottom: 30px;\n  font-weight: bolder;\n}\n\n.title-spy {\n  font-weight: bold;\n}\n\n.bg-form {\n  background-color: #ffffff;\n  padding: 25px;\n  color: #212529;\n  width: 350px;\n  margin-left: auto;\n  margin-right: auto;\n}\n\n.form-title {\n  font-size: 24px;\n  line-height: 1.5;\n  text-align: center;\n}\n\n.form-desc {\n  text-align: center;\n}\n\na {\n  color: #3699ff;\n  text-decoration: none;\n}\n\n.input {\n  border: 1px solid #bdbdbd;\n  margin-top: 15px;\n}\n\n.form {\n  margin-top: 20px;\n}\n\n.list {\n  margin-top: 20px;\n  text-align: left;\n}\n\n.icon {\n  color: #b5b5c3;\n  font-size: 18px;\n  margin-right: 15px;\n}\n\n.label {\n  color: #212529;\n}\n\n.button {\n  text-transform: capitalize;\n}\n\n.terms {\n  text-align: center;\n}\n\nion-list {\n  background: white;\n}\n\nion-item {\n  background: white;\n}\n\n.rounded {\n  margin-top: 1em;\n  border: 1px solid grey;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInJlZ2lzdHJhdGlvbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSwrQkFBQTtBQUNKOztBQUVBO0VBQ0ksNEJBQUE7QUFDSjs7QUFDQTtFQUNJLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQUVKOztBQUFBO0VBQ0ksaUJBQUE7QUFHSjs7QUFEQTtFQUNFLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7RUFDQSxZQUFBO0VBQ0UsaUJBQUE7RUFDQSxrQkFBQTtBQUlKOztBQUZBO0VBQ0MsZUFBQTtFQUNDLGdCQUFBO0VBQ0Esa0JBQUE7QUFLRjs7QUFIQTtFQUNDLGtCQUFBO0FBTUQ7O0FBSkE7RUFDQyxjQUFBO0VBQ0EscUJBQUE7QUFPRDs7QUFMQTtFQUNDLHlCQUFBO0VBQ0EsZ0JBQUE7QUFRRDs7QUFMQTtFQUNDLGdCQUFBO0FBUUQ7O0FBTkE7RUFDQyxnQkFBQTtFQUNHLGdCQUFBO0FBU0o7O0FBUEE7RUFDQyxjQUFBO0VBQ0EsZUFBQTtFQUNDLGtCQUFBO0FBVUY7O0FBUkE7RUFDQyxjQUFBO0FBV0Q7O0FBVEE7RUFDQywwQkFBQTtBQVlEOztBQVZBO0VBQ0Msa0JBQUE7QUFhRDs7QUFWQTtFQUNJLGlCQUFBO0FBYUo7O0FBVkE7RUFDSSxpQkFBQTtBQWFKOztBQVZBO0VBQ0ksZUFBQTtFQUNBLHNCQUFBO0FBYUoiLCJmaWxlIjoicmVnaXN0cmF0aW9uLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50LmJhY2tncm91bmQge1xuICAgIC0taW9uLWJhY2tncm91bmQtY29sb3I6ICMxODFDMzI7XG59XG5cbmlvbi1pdGVtIHtcbiAgICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xufVxuLnRpdGxlIHtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICBtYXJnaW4tYm90dG9tOiAzMHB4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG59XG4udGl0bGUtc3B5IHtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5iZy1mb3Jte1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmOyBcbiAgcGFkZGluZzogMjVweDtcbiAgY29sb3I6ICMyMTI1Mjk7XG4gIHdpZHRoOiAzNTBweDtcbiAgICBtYXJnaW4tbGVmdDogYXV0bztcbiAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XG59XG4uZm9ybS10aXRsZXtcblx0Zm9udC1zaXplOiAyNHB4OyBcbiAgbGluZS1oZWlnaHQ6IDEuNTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmZvcm0tZGVzY3tcblx0dGV4dC1hbGlnbjogY2VudGVyO1xufVxuYXtcblx0Y29sb3I6ICMzNjk5ZmY7XG5cdHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbn1cbi5pbnB1dHtcblx0Ym9yZGVyOiAxcHggc29saWQgI2JkYmRiZDtcblx0bWFyZ2luLXRvcDogMTVweDtcblx0XG59XG4uZm9ybXtcblx0bWFyZ2luLXRvcDogMjBweDtcbn1cbi5saXN0e1xuXHRtYXJnaW4tdG9wOiAyMHB4O1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG59XG4uaWNvbntcblx0Y29sb3I6ICNiNWI1YzM7IFxuXHRmb250LXNpemU6IDE4cHg7XG5cdCBtYXJnaW4tcmlnaHQ6IDE1cHg7XG59XG4ubGFiZWx7XG5cdGNvbG9yOiAjMjEyNTI5O1xufVxuLmJ1dHRvbntcblx0dGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG59XG4udGVybXN7XG5cdHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuaW9uLWxpc3R7XG4gICAgYmFja2dyb3VuZDogd2hpdGU7XG59XG5cbmlvbi1pdGVtIHtcbiAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbn1cblxuLnJvdW5kZWQge1xuICAgIG1hcmdpbi10b3A6IDFlbTtcbiAgICBib3JkZXI6IDFweCBzb2xpZCBncmV5O1xufSJdfQ== */");

/***/ }),

/***/ 2657:
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/registration/registration.page.html ***!
  \*******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-content class=\"background\"> \n\t<div class=\"title\">\n        <h2 class=\"title-spy\">\n            Spytrack\n        </h2>\n    </div>\n\t<ion-slides pager=\"true\" [options]=\"slideOpts\" #regsitrationSlides>\n        <ion-slide >\n\t\t\t<div class=\"bg-form\">\n\t\t\t\t<p>Step 1 of 4 - Contact Info</p>\n\t\t\t\t<div>\n\t\t\t\t\t<p class=\"form-title\">Create an account to activate your device and start tracking</p>\n\t\t\t\t\t<p class=\"form-desc\">Already have an account? <a [routerLink]=\"['/login']\">Log in here.</a></p>\n\t\t\t\t</div>\n\t\t\t\t<form class=\"form\" [formGroup]=\"regFormStepOne\">\n\t\t\t\t\t<ion-item class=\"rounded\">\n\t\t\t\t\t\t<ion-label position=\"floating\">First Name</ion-label>\n\t\t\t\t\t\t<ion-input formControlName=\"firstname\"></ion-input>\n\t\t\t\t\t</ion-item>\n\t\t\t\t\t<ion-item class=\"rounded\">\n\t\t\t\t\t\t<ion-label position=\"floating\">Last Name</ion-label>\n\t\t\t\t\t\t<ion-input formControlName=\"lastname\"></ion-input>\n\t\t\t\t\t</ion-item>\n\t\t\t\t\t<ion-item class=\"rounded\">\n\t\t\t\t\t\t<ion-label position=\"floating\">Email</ion-label>\n\t\t\t\t\t\t<ion-input type=\"email\"\n\t\t\t\t\t\tformControlName=\"email\"\n\t\t\t\t\t\t[email]=\"emailValidation\">\n\t\t\t\t\t\t</ion-input>\n\t\t\t\t\t</ion-item>\n\t\t\t\t\t<ion-item class=\"rounded\">\n\t\t\t\t\t\t<ion-label position=\"floating\">Phone Number</ion-label>\n\t\t\t\t\t\t<ion-input formControlName=\"phone\"></ion-input>\n\t\t\t\t\t</ion-item>\n\t\t\t\t\t<!-- [disabled]=\"!regFormStepOne.valid\"  -->\n\t\t\t\t\t<div style=\"margin-top: 15px;\">\n\t\t\t\t\t\t<ion-button class=\"button\" expand=\"block\" color=\"secondary\" \n\t\t\t\t\t\t[disabled]=\"!regFormStepOne.valid\"\n\t\t\t\t\t\t(click)=\"swipeNext()\">Next</ion-button>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div style=\"margin-top: 15px;\">\n\t\t\t\t\t\t<ion-button class=\"button\" expand=\"block\" color=\"dark\" [routerLink]=\"['/login']\" >Cancel</ion-button>\n\t\t\t\t\t</div>\n\t\t\t\t</form>\n\t\t\t</div>\n        </ion-slide>\n        <ion-slide>\n\t\t\t<div class=\"bg-form\">\n\t\t\t\t<p>Step 2 of 4 - Address</p>\n\t\t\t\t<div>\n\t\t\t\t\t<p class=\"form-title\">Create an account to activate your device and start tracking</p>\n\t\t\t\t\t<p class=\"form-desc\">Already have an account? <a [routerLink]=\"['/login']\">Log in here.</a></p>\n\t\t\t\t</div>\n\t\t\t\t<form class=\"form\" [formGroup]=\"regFormStepTwo\">\n\t\t\t\t\t<ion-item class=\"rounded\">\n\t\t\t\t\t\t<ion-label position=\"floating\">Address</ion-label>\n\t\t\t\t\t\t<ion-input formControlName=\"address\"></ion-input>\n\t\t\t\t\t</ion-item>\n\t\t\t\t\t<ion-item class=\"rounded\">\n\t\t\t\t\t\t<ion-label position=\"floating\">City</ion-label>\n\t\t\t\t\t\t<ion-input formControlName=\"city\"></ion-input>\n\t\t\t\t\t</ion-item>\n\t\t\t\t\t<ion-item class=\"rounded\">\n\t\t\t\t\t\t<ion-label position=\"floating\">State</ion-label>\n\t\t\t\t\t\t<ion-input formControlName=\"state\"></ion-input>\n\t\t\t\t\t</ion-item>\n\t\t\t\t\t<ion-item class=\"rounded\">\n\t\t\t\t\t\t<ion-label position=\"floating\">Zip Code</ion-label>\n\t\t\t\t\t\t<ion-input formControlName=\"zipcode\"></ion-input>\n\t\t\t\t\t</ion-item>\n\t\t\t\t\t<ion-item class=\"rounded\">\n\t\t\t\t\t\t<ion-label position=\"floating\">Country</ion-label>\n\t\t\t\t\t\t<ion-input formControlName=\"country\"></ion-input>\n\t\t\t\t\t</ion-item>\n\t\t\t\t\t<!-- [disabled]=\"!regFormStepTwo.valid\"  -->\n\t\t\t\t\t<div style=\"margin-top: 15px;\">\n\t\t\t\t\t\t<ion-button class=\"button\" expand=\"block\" color=\"secondary\" \n\t\t\t\t\t\t[disabled]=\"!regFormStepTwo.valid\"\n\t\t\t\t\t\t(click)=\"swipeNext()\" >Next</ion-button>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div style=\"margin-top: 15px;\">\n\t\t\t\t\t\t<ion-button class=\"button\" expand=\"block\" color=\"dark\" (click)=\"swipePrev()\" >Back</ion-button>\n\t\t\t\t\t</div>\n\t\t\t\t</form>\n\t\t\t</div>\n        </ion-slide>\n        <ion-slide>\n\t\t\t<div class=\"bg-form\">\n\t\t\t\t<p>Step 3 of 4 - Account Info</p>\n\t\t\t\t<div>\n\t\t\t\t\t<p class=\"form-title\">Create an account to activate your device and start tracking</p>\n\t\t\t\t\t<p class=\"form-desc\">Already have an account? <a [routerLink]=\"['/login']\">Log in here.</a></p>\n\t\t\t\t</div>\n\t\t\t\t<form class=\"form\" [formGroup]=\"regFormStepThree\" autocomplete=\"off\">\n\t\t\t\t\t<ion-item class=\"rounded\">\n\t\t\t\t\t\t<ion-label position=\"floating\">Username</ion-label>\n\t\t\t\t\t\t<ion-input formControlName=\"username\"></ion-input>\n\t\t\t\t\t</ion-item>\n\t\t\t\t\t<div *ngIf=\"regFormStepThree.get('username').invalid && \n\t\t\t\t\t\t\tregFormStepThree.get('username').errors && \n\t\t\t\t\t\t\t(regFormStepThree.get('username').dirty || regFormStepThree.get('username').touched)\">\n\t\t\t\t\t\t<small class=\"text-danger\"\n\t\t\t\t\t\t\t*ngIf=\"regFormStepThree.get('username').hasError('required')\">\n\t\t\t\t\t\t\tUsername is required.\n\t\t\t\t\t\t</small>\n\t\t\t\t\t</div>\n\t\t\t\t\t<ion-item class=\"rounded\">\n\t\t\t\t\t\t<ion-label position=\"floating\">Pin Code</ion-label>\n\t\t\t\t\t\t<ion-input formControlName=\"pinCode\" name=\"pinC\" autocomplete=\"off\" type=\"text\"></ion-input>\n\t\t\t\t\t</ion-item>\n\t\t\t\t\t<ion-item class=\"rounded\">\n\t\t\t\t\t\t<ion-label position=\"floating\">Password</ion-label>\n\t\t\t\t\t\t<ion-input type=\"password\" (input)='passwordInputValidation($event)' formControlName=\"password\" name=\"pd\" autocomplete=\"new-password\"></ion-input>\n\t\t\t\t\t</ion-item>\n\t\t\t\t\t<div *ngIf=\"(regFormStepThree.get('password').invalid && regFormStepThree.get('password').touched) || regFormStepThree.get('password').dirty\">\n\t\t\t\t\t\t<small class=\"text-danger\" *ngIf=\"regFormStepThree.get('password').errors?.required\">Password is required</small><br>\n\t\t\t\t\t</div>\n\t\t\t\t\t<ion-item class=\"rounded\">\n\t\t\t\t\t\t<ion-label position=\"floating\">Confirm Password</ion-label>\n\t\t\t\t\t\t<ion-input type=\"password\" name=\"cpd\" (input)='cpasswordInputValidation($event)' formControlName=\"cpassword\" autocomplete=\"off\"></ion-input>\n\t\t\t\t\t</ion-item>\n\t\t\t\t\t<ion-list class=\"list\">\n\t\t\t\t\t\t<ion-icon class=\"icon\" name=\"checkmark-circle\" style=\"color: green;\" *ngIf=\"passwordVal.minLen\"></ion-icon>\n\t\t\t\t\t\t<ion-icon class=\"icon\" name=\"checkmark-circle\" *ngIf=\"!passwordVal.minLen\"></ion-icon>\n\t\t\t\t\t\t<ion-label class=\"label\">Be at least 8 characters long</ion-label><br>\n\n\t\t\t\t\t\t<ion-icon class=\"icon\" name=\"checkmark-circle\" style=\"color: green;\" *ngIf=\"passwordVal.number\"></ion-icon>\n\t\t\t\t\t\t<ion-icon class=\"icon\" name=\"checkmark-circle\" *ngIf=\"!passwordVal.number\"></ion-icon>\n\t\t\t\t\t\t<ion-label class=\"label\">Have at least one number</ion-label><br>\n\n\t\t\t\t\t\t<ion-icon class=\"icon\" name=\"checkmark-circle\" style=\"color: green;\" *ngIf=\"passwordVal.symbol\"></ion-icon>\n\t\t\t\t\t\t<ion-icon class=\"icon\" name=\"checkmark-circle\" *ngIf=\"!passwordVal.symbol\"></ion-icon>\n\t\t\t\t\t\t<ion-label class=\"label\">Have at least one symbol</ion-label><br>\n\n\t\t\t\t\t\t<ion-icon class=\"icon\" name=\"checkmark-circle\" style=\"color: green;\" *ngIf=\"passwordVal.upperCase\"></ion-icon>\n\t\t\t\t\t\t<ion-icon class=\"icon\" name=\"checkmark-circle\" *ngIf=\"!passwordVal.upperCase\"></ion-icon>\n\t\t\t\t\t\t<ion-label class=\"label\">Have at least one capital letter</ion-label><br>\n\t\t\t\t\t\t\n\t\t\t\t\t\t<ion-icon class=\"icon\" name=\"checkmark-circle\" style=\"color: green;\" *ngIf=\"passwordVal.lowerCase\"></ion-icon>\n\t\t\t\t\t\t<ion-icon class=\"icon\" name=\"checkmark-circle\" *ngIf=\"!passwordVal.lowerCase\"></ion-icon>\n\t\t\t\t\t\t<ion-label class=\"label\">Have at least one small letter</ion-label><br>\n\t\t\t\t\t</ion-list>\n\t\t\t\t\t<!-- [disabled]=\"!regFormStepThree.valid || !cPasswordVal\"  -->\n\t\t\t\t\t<div style=\"margin-top: 15px;\">\n\t\t\t\t\t\t<ion-button class=\"button\" expand=\"block\" color=\"secondary\" \n\t\t\t\t\t\t[disabled]=\"!regFormStepThree.valid || !cPasswordVal\"\n\t\t\t\t\t\t(click)=\"signUp()\" >Sign up for Spytrack</ion-button>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div style=\"margin-top: 15px;\">\n\t\t\t\t\t\t<ion-button class=\"button\" expand=\"block\" color=\"dark\" (click)=\"swipePrev()\" >Back</ion-button>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div>\n\t\t\t\t\t\t<p class=\"terms\">By creating an account you agree to the <a href=\"\">Terms</a> and <a href=\"\"> conditions Policy</a></p>\n\t\t\t\t\t</div>\n\t\t\t\t\t\n\t\t\t\t</form>\n\t\t\t</div>\n        </ion-slide>\n\t\t<ion-slide>\n\t\t\t<div class=\"bg-form\">\n\t\t\t\t<p>Step 4 of 4 - Account Varification</p>\n\t\t\t\t<div>\n\t\t\t\t\t<p class=\"form-title\">Please check you inbox for verify your email id.</p>\n\t\t\t\t\t<p class=\"form-desc\">Didn't get an email please click <a href=\"javascript:;\" (click)=\"resendVarificationEmail()\">here</a> to resend the email.</p>\n\t\t\t\t\t<p class=\"form-desc\">Already have an account? or verification done ? <a [routerLink]=\"['/login']\">Log in here.</a></p>\n\t\t\t\t</div>\n\t\t\t</div>\n        </ion-slide>\n      </ion-slides>\n   \n\n</ion-content>");

/***/ })

}]);
//# sourceMappingURL=src_app_registration_registration_module_ts.js.map